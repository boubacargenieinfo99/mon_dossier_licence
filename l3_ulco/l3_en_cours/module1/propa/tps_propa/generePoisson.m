function poissonVariable = generePoisson(a, n)
    % a : paramètre de la loi de Poisson
    % n : nombre de réalisations

    % Générer n réalisations de la variable aléatoire uniforme U
    u = rand(1, n);

    % Initialiser le résultat
    poissonVariable = zeros(1, n);

    % Générer la variable aléatoire de Poisson à partir de U
    for i = 1:n
        k = 0;
        p = exp(-a);
        F = p;

        while u(i) >= F
            p = (a * p) / (k + 1);
            F = F + p;
            k = k + 1;
        end

        poissonVariable(i) = k;
    end
end