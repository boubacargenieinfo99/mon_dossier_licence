
#ifndef TRAINING_GEOMETRY_HPP
#define TRAINING_GEOMETRY_HPP
#pragma once

class Geometry {
public:
    virtual ~Geometry() = default;
    virtual void displayInfo() const = 0;
};

#endif //TRAINING_GEOMETRY_HPP
