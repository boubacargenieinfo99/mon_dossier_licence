var Express = require('express');
var router = Express.Router();

const userControllers = require('../controllers/userControllers');

//router.get('/:id',userControllers.home);

router.get('/',userControllers.getDate);

router.get('/form',userControllers.getPostPage);

router.post('/form',userControllers.postPage);

module.exports = router;