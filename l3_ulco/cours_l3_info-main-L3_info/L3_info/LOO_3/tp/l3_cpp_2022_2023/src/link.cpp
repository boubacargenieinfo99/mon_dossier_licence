#include "link.hpp"

#include <cmath>
#include <cstddef>
#include <cstdio>
#include <cstdlib>
#include <cstring>
//#include <iostream>

Link::Link(char *id, unsigned int length){

    this->id = new char[strlen(id+1)];
    this->length = length;
    this->cell_number = floor((double) length / CELL_LENGTH);
    this->cells = new Cell[this->cell_number];
    for (unsigned int i = 0; i < this->cell_number; ++i) {
        this->cells[i].vehicle = nullptr;
    }
    this->out_node = nullptr;
}

Link::~Link() {
    delete[] id;
    delete[] cells;
}

void Link::add_vehicle_to_link(Vehicle *vehicle, unsigned int time) {
    printf("%d: %s enters %s\n", time, vehicle->id,id);
    //std::cout << time << ": " << vehicle->id << " enters " << link->id << std::endl;
    this->cells[0].vehicle = vehicle;
    vehicle->link = this;
    vehicle->last_time = time;
}

void Link::attach_in_node_to_link(Node *node) {
    in_node = node;
}

void Link::attach_out_node_to_link(Node *node) {
    out_node = node;
}

struct Vehicle *Link::go_out_link(unsigned int time) {
    Vehicle *last_vehicle = cells[cell_number - 1].vehicle;
    Vehicle *out_vehicle =
            last_vehicle != nullptr and last_vehicle->last_time < time ? last_vehicle : nullptr;

    if (out_vehicle != nullptr) {
        printf("%d: %s exits %s\n", time, out_vehicle->id, id);
        cells[cell_number - 1].vehicle = nullptr;
        out_vehicle->link = nullptr;
    }
    return out_vehicle;
}

struct Vehicle *Link::run_link(unsigned int time) {
    Vehicle *out_vehicle = go_out_link(time);

    for (unsigned int i = cell_number - 1; i > 0; --i) {
        Vehicle *vehicle = cells[i - 1].vehicle;

        if (vehicle != nullptr and vehicle->last_time < time and cells[i].vehicle == nullptr) {
            cells[i].vehicle = cells[i - 1].vehicle;
            cells[i].vehicle->last_time = time;
            cells[i - 1].vehicle = nullptr;
        }
    }
    return out_vehicle;
}

