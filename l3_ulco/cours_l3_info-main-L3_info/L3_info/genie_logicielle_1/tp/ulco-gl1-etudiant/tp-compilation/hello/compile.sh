#/bin/sh

g++ -O2 -w -Wall -Wextra main.cpp hello.cpp -o say-hello
chmod +x say-hello

'
    g++ -O2 -Wall -Xextra -c hello.cpp
    g++ -O2 -Wall -Xextra -c main.cpp
    g++ -O2 -Wall -Xextra -o say-hello main.o hello.o
'