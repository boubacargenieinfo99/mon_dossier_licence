import System.Environment

countLines :: String -> Int
countLines str = length (lines str)

countWords :: String -> Int
countWords str = length (words str)

countChars :: String -> Int
countChars str = length (str)

countNoWhitespaceChars :: String -> Int
countNoWhitespaceChars str = length (concat(words str))

main ::IO ()
main = do
    args <- getArgs
    case args of
        [filename] -> do
            content <- readFile filename
            print (filename)
            print ("lines: " ++ show (countLines content))
            print ("words: " ++ show (countWords content))
            print ("chars: " ++ show (countChars content))
            print ("non-whitespace chars: " ++ show (countNoWhitespaceChars content))
        _ -> putStrLn "usage <file>"