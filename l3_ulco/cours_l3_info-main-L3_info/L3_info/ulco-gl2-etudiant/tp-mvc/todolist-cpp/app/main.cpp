#include <todolist-cpp/todolist-cpp.hpp>
#include <todolist-cpp/views.hpp>

#include <iostream>

int main() {
    std::cout << "this is todolist-cpp" << std::endl << std::endl;
    
    struct Task tache{1,"premiere tache"};
    View v;
    std::cout << v.fmtTask(tache) << std::endl << std::endl; 

    Board maListe;
    maListe.addTask("ma premiere tache");
    maListe.addTask("ma deuxieme tache");
    std::cout << v.fmtBoard(maListe) << std::endl << std::endl;





    return 0;
}

