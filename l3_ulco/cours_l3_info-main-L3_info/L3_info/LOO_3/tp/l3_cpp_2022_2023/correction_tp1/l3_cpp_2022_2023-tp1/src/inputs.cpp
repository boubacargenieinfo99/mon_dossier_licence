#include "inputs.hpp"

#include <cstring>

typedef char *char_ptr;

Inputs::Inputs(unsigned int vehicle_number) : vehicle_number(vehicle_number),
                                              vehicles(vehicle_number == 0 ? nullptr : new VehiclePtr[vehicle_number]),
                                              times(vehicle_number == 0 ? nullptr : new unsigned int[vehicle_number]),
                                              node_ids(vehicle_number == 0 ? nullptr : new char_ptr[vehicle_number]) {
  for (unsigned int i = 0; i < vehicle_number; ++i) {
    vehicles[i] = nullptr;
    times[i] = 0;
    node_ids[i] = nullptr;
  }
}

Inputs::~Inputs() {
  for (unsigned int i = 0; i < vehicle_number; ++i) {
    if (vehicles[i] != nullptr) {
      delete vehicles[i];
      delete node_ids[i];
    }
  }
  delete[] vehicles;
  delete[] times;
  delete[] node_ids;
}

void Inputs::add_vehicle(Vehicle *vehicle, unsigned int time, const char *node_id) {
  unsigned int index = 0;

  while (index < vehicle_number and vehicles[index] != nullptr) {
    ++index;
  }
  if (index < vehicle_number) {
    vehicles[index] = vehicle;
    times[index] = time;
    node_ids[index] = new char[strlen(node_id) + 1];
    strcpy(node_ids[index], node_id);
  }
}