#ifndef TRAFFIC_PATH_HPP
#define TRAFFIC_PATH_HPP

#include <cstdint>
#include <valarray>

class Path {
public:
  Path(const std::initializer_list<uint8_t> &indexes) : _indexes(indexes), _index(0) {}

  uint8_t current() const { return _indexes[_index]; }

  void next() { ++_index; }

private:
  std::valarray<uint8_t> _indexes;
  unsigned int _index;
};

#endif //TRAFFIC_PATH_HPP
