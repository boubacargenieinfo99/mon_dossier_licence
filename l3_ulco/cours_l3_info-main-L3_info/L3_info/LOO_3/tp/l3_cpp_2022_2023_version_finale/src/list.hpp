#ifndef TRAFFIC_LIST_HPP
#define TRAFFIC_LIST_HPP

#include "cell.hpp"

#include <iostream>

template < typename Data>
class List {
public:
  List() : _first(nullptr), _last(nullptr) {

//    std::cout << "[List] constructor" << std::endl;

  }

  List(const List<Data> &other) : _first(nullptr), _last(nullptr) {
    Cell<Data> *current = other._first;

    while (current != nullptr) {
      add_last(current->get_link());
      current = current->get_next();
    }

//    std::cout << "[List] copy constructor" << std::endl;

  }

  void add_first(const std::shared_ptr<Data> &data) {
    Cell<Data> *new_cell = new Cell<Data>(data, _first);

    _first = new_cell;
    _last = _last == nullptr ? _first : _last;
  }

  void add_last(const std::shared_ptr<Data> &data) {
    Cell<Data> *new_cell = new Cell<Data>(data, nullptr);

    if (_last != nullptr) {
      _last->set_next(new_cell);
    }
    _last = new_cell;
    _first = _first == nullptr ? _last : _first;
  }

  void append(const List &list) {
    Cell<Data> *current = list._first;

    while (current != nullptr) {
      add_last(current->get_link());
      current = current->get_next();
    }
  }

  Cell<Data> *get_first() const { return _first; }

  unsigned int size() {
    unsigned int size = 0;
    Cell<Data> *current = _first;

    while (current != _last) {
      ++size;
      current = current->get_next();
    }
    return size;
  }

  void remove_first() {
    if (_first != nullptr) {
      Cell<Data> *removed_cell = _first;

      _first = _first->get_next();
      if (_first == nullptr) {
        _last = nullptr;
      }
      delete removed_cell;
    }
  }

  void remove_last() {
    if (_last != nullptr) {
      Cell<Data> *removed_cell = _last;

      if (_last == nullptr) {
        _first = nullptr;
      }
      delete removed_cell;
    }
  }

  ~List() {

//    std::cout << "[List] destructor" << std::endl;

    while (_first != nullptr) {
      Cell<Data> *next = _first->get_next();

      delete _first;
      _first = next;
    }
  }

private:
  Cell<Data> *_first;
  Cell<Data> *_last;
};

#endif //TRAFFIC_LIST_HPP