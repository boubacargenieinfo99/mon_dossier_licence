#include "spatialElement.hpp"
#include <memory>

class Buffer : public SpatialElement {
public:
    Buffer(std::string nom,int capacity,std::unique_ptr<Geometry> geometry):
    SpatialElement(nom,std::move(geometry)),nom(nom),capacity(capacity){}

private:
    int capacity;
    std::string nom;
};
