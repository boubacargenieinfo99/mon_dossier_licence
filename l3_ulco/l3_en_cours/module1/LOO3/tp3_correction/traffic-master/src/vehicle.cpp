#include "vehicle.hpp"

namespace traffic {

Vehicle::Vehicle(const std::string &id, unsigned int speed, const std::vector<std::string> &path) : _id(id),
                                                                                                    _speed(speed),
                                                                                                    _path(path),
                                                                                                    _path_index(0) {}

}