#include "map.h"

struct Map *create_new_map() {
  struct Map *map = create_map(3, 2);
  struct Link *link_1 = create_link("L1", 500);
  struct Link *link_2 = create_link("L2", 500);
  struct Link *link_3 = create_link("L3", 500);

  struct Node *node_1 = create_node("N1", 2, 1, 1);
  struct Node *node_2 = create_node("N2", 2, 1, 1);

  add_link_to_map(link_1, map);
  add_link_to_map(link_2, map);
  add_link_to_map(link_3, map);

  add_node_to_map(node_1, map);
  add_node_to_map(node_2, map);

  attach_input_link_to_node(link_1, node_1);
  attach_output_link_to_node(link_2, node_1);
  attach_input_link_to_node(link_2, node_2);
  attach_output_link_to_node(link_3, node_2);
  return map;
}

void run(struct Map *map) {
  unsigned int time = 0;

  while (time < 10000) {
    run_map(map, time);
    ++time;
  }
}

int main() {
  struct Map *map = create_new_map();
  struct Vehicle *vehicle = create_vehicle("V1");

  add_vehicle_to_link(vehicle, map->links[0], 0);

  run(map);

  destroy_vehicle(vehicle);
  destroy_map(map);
  return 0;
}