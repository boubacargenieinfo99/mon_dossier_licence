#ifndef TRAFFIC_LINK_HPP
#define TRAFFIC_LINK_HPP

#include "vehicle.hpp"

struct Node;

struct Cell {
  Vehicle *vehicle;
};

struct Link {
  Link(const char *id, unsigned int length);

  ~Link();

  void add_vehicle(Vehicle *vehicle,unsigned int time);

  void attach_in_node(Node *node);

  void attach_out_node(Node *node);

  Vehicle *go_out(unsigned int time);

  Vehicle *run(unsigned int time);

  static const unsigned int cell_length;

  char *id;
  unsigned int length;
  unsigned int cell_number;
  Cell *cells;
  Node *in_node;
  Node *out_node;
};

typedef Link* LinkPtr;

#endif //TRAFFIC_LINK_HPP
