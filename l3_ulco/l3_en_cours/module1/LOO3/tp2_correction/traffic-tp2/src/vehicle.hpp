#ifndef TRAFFIC_VEHICLE_HPP
#define TRAFFIC_VEHICLE_HPP

#include <string>

namespace traffic {

class Vehicle {
public:
  Vehicle(const std::string& id, unsigned int speed);

  const std::string& id() const { return _id; }

  unsigned int speed() const { return _speed; }

private:
  std::string _id;
  unsigned int _speed;
};

}

#endif //TRAFFIC_VEHICLE_HPP
