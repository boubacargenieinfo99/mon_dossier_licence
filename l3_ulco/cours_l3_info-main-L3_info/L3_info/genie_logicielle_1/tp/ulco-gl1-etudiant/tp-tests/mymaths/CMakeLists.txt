cmake_minimum_required( VERSION 3.0 )
project( mymaths )

# library
add_library( mymaths SHARED
    src/mymaths/add.cpp
    src/mymaths/mul.cpp )
target_include_directories( mymaths PUBLIC "include" )
INSTALL( TARGETS mymaths 
    LIBRARY DESTINATION lib
    INCLUDES DESTINATION include )

# executable
add_executable( mymaths-app src/app/main.cpp )
target_link_libraries( mymaths-app mymaths )
install( TARGETS mymaths-app DESTINATION bin )

add_executable( test-mymaths
    test/test_negatif.cpp
    test/test_null.cpp
    test/test_positif.cpp
    test/main_test.cpp)
target_link_libraries( test-mymaths mymaths )

enable_testing()
add_test( NAME test-mymaths COMMAND test-mymaths )


