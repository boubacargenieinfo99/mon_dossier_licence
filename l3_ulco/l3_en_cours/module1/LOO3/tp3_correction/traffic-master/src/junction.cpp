#include "link.hpp"
#include "junction.hpp"

#include <iostream>

namespace traffic {

Junction::Junction(const std::string &id, unsigned int duration, size_t in_number, size_t out_number) :
  Node(id, duration, in_number, out_number) {}

void Junction::push_vehicle(unsigned int t, std::unique_ptr<Vehicle> &vehicle) {

  std::cout << t << ": " << vehicle->id() << " enters " << id() << std::endl;

  _vehicle.reset(vehicle.release());
  _out_time = t + duration();
};

void Junction::run(unsigned int t) {
  if (_vehicle and t >= _out_time) {
    unsigned int link_index = 0;
    bool found = false;

    while (link_index < out_links().size() and not found) {
      if (out_links()[link_index]->id() == _vehicle->next_path_id()) {
        found = true;
      } else {
        ++link_index;
      }
    }
    if (not out_links().empty() and found and out_links()[link_index]->is_free()) {
      _vehicle->advance();
      out_links()[link_index]->push_vehicle(t, _vehicle);
    }
  }
}

}