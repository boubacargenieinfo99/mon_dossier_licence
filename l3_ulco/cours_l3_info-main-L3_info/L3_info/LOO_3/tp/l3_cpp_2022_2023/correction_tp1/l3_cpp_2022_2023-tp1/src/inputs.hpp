#ifndef TRAFFIC_INPUTS_HPP
#define TRAFFIC_INPUTS_HPP

#include "vehicle.hpp"

struct Inputs {
  Inputs(unsigned int vehicle_number);
  ~Inputs();

  void add_vehicle(Vehicle* vehicle, unsigned int time, const char* node_id);

  unsigned int vehicle_number;
  Vehicle** vehicles;
  unsigned int* times;
  char** node_ids;
};

#endif //TRAFFIC_INPUTS_HPP
