doubler :: [Int] -> [Int]
doubler xs = [x*2 | x<-xs]

pairs :: [Int] -> [Int]
pairs xs = [x | x<-xs,even x]

mymap :: (a -> b) -> [a] -> [b]
mymap f xs = [f x | x<-xs]

myfilter :: (a -> Bool) -> [a] -> [a]
myfilter f xs = [x | x<-xs , f x]

multiples :: Int -> [Int]
multiples p = [x |x<-[1..p],(p `mod` x) == 0]

combinaisons :: [a] -> [b] -> [(a,b)]
combinaisons l1 l2 = [(x,y) |x<-l1 ,y<-l2]

tripletsPyth :: Int -> [(Int,Int,Int)]
tripletsPyth p = [(x,y,z) |x<-[1..p] , y<-[x..p] , z<-[1..p] , x*x+y*y==z*z]

myConcat1 :: [[a]] -> [a]
myConcat1 p = foldl (++) [] p

myConcat2 :: [[a]] -> [a]
myConcat2 p = [x |xs<-p,x<-xs]

getHeads1 :: [[a]] -> [a]
getHeads1 xss = map head xss

getHeads2 :: [[a]] -> [a]
getHeads2 xss = [x | (x:_)<-xss]

isNotNull :: [a] -> Bool
isNotNull b = do
    if null b 
        then True 
        else False

--getSafeHeads1 :: [[a]] -> [a]
--getSafeHeads1 xss = map head (filter isNotNull xss)

getSafeHeads2 :: [[a]] -> [a]
getSafeHeads2 xss = [x | xs<-xss, (x:_)<-xss , isNotNull xs]
--getSafeHeads2 :: [String] -> String
--getSafeHeads2 xss = [x | (x:_)<-xss , xss/=[]]

main :: IO()
main = do
    print (doubler [1,2,3])
    print (pairs [1,2,3,4])
    print (mymap (*2) [4,5,6,7,8])
    print (myfilter even [10,11,12,13,14])
    print (multiples 35)
    print (combinaisons ["pomme","poire"] [13,37,42])
    print (tripletsPyth 13)
    print(myConcat1 ["foo","bar"])
    print(myConcat2 ["foo","bar"])
    print (getHeads1 ["foo","bar"])
    print (getHeads2 ["foo","bar"])
    --print (getSafeHeads1 ["foo","","bar"])
    print (getSafeHeads2 ["foo","","bar"])

