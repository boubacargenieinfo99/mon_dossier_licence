alphabet = ['a'..'z']


main::IO()
main = do
    print(map (\x->x*2) [1..4])
    print(map (\x->x/2) [1..4])
    --print(zip ['a'..'z'] [1..26])
    print(map (\(x,y)->[x] ++ "-" ++ show y) (zip ['a'..'z'] [1..26] ))
    print(zipWith (\ x y ->[x] ++ "-" ++ show y) ['a'..'z'] [1..26] )