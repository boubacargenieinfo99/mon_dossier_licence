#include <iostream>
#include <memory>
#include <string>
#include <vector>

class Machine;
class Factory;

class Conveyor {
public:
    Conveyor(std::string id, std::shared_ptr<Machine> source, std::shared_ptr<Machine> destination, int length, int speed, int capacity)
            : id(id), source(source), destination(destination), length(length), speed(speed), capacity(capacity) {}

private:
    std::string id;
    std::shared_ptr<Machine> source;
    std::shared_ptr<Machine> destination;
    int length;
    int speed;
    int capacity;
};