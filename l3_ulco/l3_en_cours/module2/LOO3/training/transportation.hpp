

#ifndef TRAINING_TRANSPORTATION_HPP
#define TRAINING_TRANSPORTATION_HPP
#pragma once

#include "unavailableElement.hpp"
#include "coordinates.hpp"
#include "path.hpp"
#include <vector>
#include <memory>

class Transportation : public UnavailableElement {
public:
    Transportation(const std::string& id, double speed, const Coordinates& initialLocation,
                   const std::initializer_list<Path>& paths);

    void displayInfo() const;

    const Coordinates& getInitialLocation() const;
    const std::vector<Path>& getPaths() const;

private:
    Coordinates initialLocation;
    std::vector<Path> paths;
};

#endif //TRAINING_TRANSPORTATION_HPP
