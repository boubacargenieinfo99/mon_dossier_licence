import OpenXum from '../../../lib/openxum-core/index.mjs';
import AI from '../../../lib/openxum-ai/index.mjs';

let e = new OpenXum.komivoki.Engine(OpenXum.komivoki.GameType.YYY, OpenXum.komivoki.Color.AAA);
let p1 = new AI.Generic.RandomPlayer(OpenXum.komivoki.Color.AAA, OpenXum.komivoki.Color.BBB, e);
let p2 = new AI.Generic.RandomPlayer(OpenXum.komivoki.Color.BBB, OpenXum.komivoki.Color.AAA, e);
let p = p1;
let moves = [];

while (!e.is_finished()) {
  const move = p.move();

  moves.push(move);
  e.move(move);
  p = p === p1 ? p2 : p1;
}

console.log("Winner is " + (e.winner_is() === OpenXum.komivoki.Color.AAA ? "aaa" : "bbb"));
for (let index = 0; index < moves.length; ++index) {
  console.log(moves[index].to_string());
}