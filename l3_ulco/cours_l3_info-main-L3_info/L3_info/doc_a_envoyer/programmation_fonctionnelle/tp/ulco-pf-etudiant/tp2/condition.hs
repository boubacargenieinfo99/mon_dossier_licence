formaterParite::Int -> String
formaterParite numero=do
    if even numero -- ou x `mod` 2 == 0
        then "pair"
        else "impair"  

formaterPariteGarde::Int -> String
formaterPariteGarde numero
    | even numero = "pair"
    | otherwise = "impair"
      

formaterSigne::Int -> String
formaterSigne numero=do
    if numero < 0
        then "négatif"
        else do
            if numero == 0
                then "nul"
                else "positif"  

formaterSigneGarde::Int -> String
formaterSigneGarde numero
    | numero < 0 = "negatif"
    | numero == 0 = "nul"
    | otherwise = "positif"

main::IO()
main = do
    putStrLn (formaterParite 21)
    putStrLn (formaterParite 42)

    putStrLn (formaterSigne (-5))
    putStrLn (formaterSigne 0)
    putStrLn (formaterSigne 5)

    putStrLn (formaterPariteGarde 21)
    putStrLn (formaterPariteGarde 42)

    putStrLn (formaterSigneGarde (-5))
    putStrLn (formaterSigneGarde 0)
    putStrLn (formaterSigneGarde 5)
