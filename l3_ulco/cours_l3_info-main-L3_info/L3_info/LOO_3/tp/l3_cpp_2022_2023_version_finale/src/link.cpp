#include "link.hpp"
#include "vehicle.hpp"

void Link::Cell::enter(Vehicle *vehicle, unsigned int time) {
  vehicle->last_time(time);
  _vehicle.reset(vehicle);
}

Vehicle *Link::Cell::exit() {
  Vehicle *vehicle = _vehicle.release();

  _vehicle = nullptr;
  vehicle->remove_link();
  return vehicle;
}

unsigned int Link::Cell::vehicle_last_time() const {
  return _vehicle->last_time();
}