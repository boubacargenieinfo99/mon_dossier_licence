/**
 * @file openxum/core/common/one_player_engine.hpp
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2011-2024 Openxum Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPENXUM_CORE_COMMON_ONE_PLAYER_ENGINE_HPP
#define OPENXUM_CORE_COMMON_ONE_PLAYER_ENGINE_HPP

#include <openxum/core/common/abstract_engine.hpp>
#include <openxum/core/common/move.hpp>

namespace openxum::core::common {

template<class Decision>
class OnePlayerEngine : public AbstractEngine {
public:
  OnePlayerEngine() = default;

  virtual ~OnePlayerEngine() = default;

  virtual OnePlayerEngine *clone() const = 0;

  virtual Moves<Decision> get_possible_move_list() const = 0;

  void move(const nlohmann::json &move) override {
    Move<Decision> m{};

    m.from_object(move);
    this->move(m);
  }

  virtual void move(const Move<Decision> &move) = 0;
};

}

#endif // OPENXUM_CORE_COMMON_ONE_PLAYER_ENGINE_HPP