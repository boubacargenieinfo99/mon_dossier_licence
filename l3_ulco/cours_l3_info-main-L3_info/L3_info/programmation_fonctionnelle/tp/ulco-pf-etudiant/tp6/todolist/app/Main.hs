{-# LANGUAGE OverloadedStrings #-}

import qualified GI.Gtk as Gtk
import Data.Text

main :: IO ()
main = do
    _ <- Gtk.init Nothing
    window <- Gtk.windowNew Gtk.WindowTypeToplevel 
    Gtk.windowSetDefaultSize window 450 500
    Gtk.windowSetTitle window "TODO List"
    _ <- Gtk.onWidgetDestroy window Gtk.mainQuit


    box <- Gtk.vBoxNew True 10
    Gtk.containerAdd window box

    entree_TODO <- Gtk.entryNew
    Gtk.containerAdd box entree_TODO

    buttonAdd <- Gtk.buttonNewWithLabel "add"
    Gtk.containerAdd box buttonAdd
    _ <- Gtk.onButtonClicked buttonAdd Gtk.mainQuit

    buttonAdd <- Gtk.buttonNewWithLabel "click"
    Gtk.containerAdd box buttonAdd
    _ <- Gtk.onButtonClicked buttonAdd Gtk.mainQuit

    texte <- Gtk.textViewNew
    Gtk.containerAdd box texte

    
    
    --label <- Gtk.labelNew (Just "Hello World!")
    --Gtk.containerAdd window label

    Gtk.widgetShowAll window
    Gtk.main

