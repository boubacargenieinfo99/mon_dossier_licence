//charge les variables d'environnement définies dans .env
require('dotenv').config({ path: './.env'} ) ;


const port = process.env.PORT || 3000;

// Inclusion du module prédéfini de Node.js permettant d'exécuter un serveur http
const http = require('http');
const app_ex4 = require('./app_ex04');


const server = http.createServer(app_ex4);

//Démarrage de l'écoute des requêtes sur le port indiqué
server.listen(port,()=>{
    console.log(`Le server écoute sur http://127.0.0.1:${port}`);
});