#include "n_ways_link.hpp"
#include "junction.hpp"
#include "vehicle.hpp"

#include <cassert>
#include <iostream>

NWaysLink::NWaysLink(const std::string &id, unsigned int length, unsigned int way_number) :
  Link(id, length), _ways(Way(cell_number()), way_number) {
}

NWaysLink::~NWaysLink() {}

void NWaysLink::enter_vehicle(Vehicle *vehicle, unsigned int time) {
  unsigned int index = 0;

  while (index < _ways.size() and not _ways[index][0].is_free()) { ++index; }
  if (index < _ways.size()) {
    std::cout << time << ": " << vehicle->id() << " enters " << id() << " on way " << index << std::endl;
    _ways[index][0].enter(vehicle, time);
    vehicle->add_link(this);
  } else {
    assert(false);
  }
}

void NWaysLink::exit_vehicle(Vehicle *vehicle, unsigned int time) {
  if (out_node().use_count() != 0) {
    std::cout << time << ": " << vehicle->id() << " exits " << id() << std::endl;
    out_node().lock()->enter_vehicle(vehicle, time);
  } else {
    delete vehicle;
  }
}

Vehicle *NWaysLink::go_out(unsigned int time) {
  if (out_node().lock()->is_full()) { return nullptr; }
  else {
    Vehicle *out_vehicle = nullptr;
    bool found = false;
    unsigned int index = 0;

    while (index < _ways.size() and not found) {
      const auto &last_vehicle = _ways[index][cell_number() - 1].vehicle();

      if (last_vehicle != nullptr and last_vehicle->last_time() < time) {
        out_vehicle = _ways[index][cell_number() - 1].exit();
        found = true;
      } else {
        ++index;
      }
    }
    return out_vehicle;
  }
}

Vehicle *NWaysLink::run(unsigned int time) {
  Vehicle *out_vehicle = go_out(time);

  for (unsigned int k = 0; k < _ways.size(); ++k) {
    for (unsigned int i = cell_number() - 1; i > 0; --i) {
      if (not _ways[k][i - 1].is_free() and _ways[k][i - 1].vehicle_last_time() + _duration == time and
          _ways[k][i].is_free()) {
        _ways[k][i].enter(_ways[k][i - 1].exit(), time);
      }
    }
  }
  return out_vehicle;
}