#include <chrono>
#include <iostream>
#include <random>
#include <thread>

#include "argparse/argparse.hpp"

#include "sockpp/tcp_acceptor.h"
#include "sockpp/tcp_connector.h"
#include "sockpp/tcp_socket.h"

using namespace std::chrono_literals;

std::string node_identifier;
in_port_t server_port;
sockpp::tcp_connector ping_conn;
sockpp::tcp_connector message_conn;
std::random_device rd;
std::mt19937 rng(rd());

std::map<std::string, std::chrono::time_point<std::chrono::system_clock>> ping_times;

int ping_client(std::string neighbour_host, in_port_t neighbour_port) {
  std::clog << "[node - " << node_identifier << "] - Ping client starting..." << std::endl;

  std::this_thread::sleep_for(2s);

  std::clog << "[node - " << node_identifier << "] - Ping client started..." << std::endl;

  if (auto res = ping_conn.connect(neighbour_host, neighbour_port, 10s); !res) {
    std::cerr << "[node - " << node_identifier << "] - Error connecting to server at: '" << neighbour_host
              << "' on port "
              << neighbour_port << ":\n\t" << res.error_message() << std::endl;
    return 1;
  }

  std::clog << "[node - " << node_identifier << "] - Created a connection from " << ping_conn.address() << std::endl;

  std::string ping_message = "ping " + node_identifier;

  while (true) {
    if (auto res = ping_conn.write(ping_message); res != ping_message.size()) {
      std::cerr << "[node - " << node_identifier << "] - Error writing to the TCP stream: " << res.error_message()
                << std::endl;
      break;
    }
    std::clog << "[node - " << node_identifier << "] - Ping send from " << ping_conn.address() << std::endl;
    std::this_thread::sleep_for(5s);
  }
  return 0;
}

void process_ping(sockpp::tcp_socket sock) {
  while (true) {
    char buf[512];

    while (sock.read(buf, sizeof(buf))) {
      std::string msg(buf);

      if (msg.substr(0, 4) == "ping") {
        std::string identifier = msg.substr(5);

        if (identifier != node_identifier) {
          ping_times[identifier] = std::chrono::system_clock::now();

          std::clog << "[node - " << node_identifier << "] - Server receive ping" << std::endl;
          for (const auto &n: ping_times) {
            auto t = std::chrono::system_clock::to_time_t(n.second);

            std::clog << "[node - " << node_identifier << "] - Node[" << n.first << "] -> "
                      << std::ctime(&t) << std::endl;
          }

          if (auto res = ping_conn.write(msg); res != msg.size()) {
            std::cerr << "[node - " << node_identifier << "] - Error writing to the TCP stream: " << res.error_message()
                      << std::endl;
          }
        }
      }
      memset(buf, 0, 512);
    }
  }
}

int ping_server(in_port_t port) {
  std::clog << "[node - " << node_identifier << "] - Ping server started on " << port << std::endl;

  sockpp::error_code ec;
  sockpp::tcp_acceptor acc{port, 4, ec};

  if (ec) {
    std::cerr << "[node - " << node_identifier << "] - Ping server - Error creating the acceptor: " << ec.message()
              << std::endl;
    return 1;
  }
  std::clog << "[node - " << node_identifier << "] - Ping server - Awaiting connections on port " << port << "..."
            << std::endl;

  while (true) {
    sockpp::inet_address peer;

    if (auto res = acc.accept(&peer); !res) {
      std::cerr << "[node - " << node_identifier << "] - Ping server - Error accepting incoming connection: "
                << res.error_message()
                << std::endl;
      return 1;
    } else {
      std::clog << "[node - " << node_identifier << "] - Ping server - Received a connection request from " << peer
                << std::endl;
      sockpp::tcp_socket sock = res.release();

      std::thread thr(process_ping, std::move(sock));
      thr.detach();
    }
  }
}

int message_client(std::string neighbour_host, in_port_t neighbour_port) {
  std::uniform_int_distribution<int> duration_distrib(10, 30);

  std::clog << "[node - " << node_identifier << "] - Message client starting..." << std::endl;

  std::this_thread::sleep_for(std::chrono::seconds(duration_distrib(rng)));

  std::clog << "[node - " << node_identifier << "] - Message client started..." << std::endl;

  if (auto res = message_conn.connect(neighbour_host, neighbour_port, 10s); !res) {
    std::cerr << "[node - " << node_identifier << "] - Error connecting to server at: '" << neighbour_host
              << "' on port "
              << neighbour_port << ":\n\t" << res.error_message() << std::endl;
    return 1;
  }

  std::clog << "[node - " << node_identifier << "] - Created a connection from " << message_conn.address() << std::endl;

  while (true) {
    std::uniform_int_distribution<int> index_distrib(0, (int) ping_times.size());

    auto index = index_distrib(rng);
    auto now = std::chrono::system_clock::now();
    auto ping_it = ping_times.cbegin();

    while (ping_it != ping_times.cend()) {
      std::chrono::duration<double> elapsed_seconds = now - ping_it->second;

      if (index == 0 and elapsed_seconds.count() < 30) { break; }
      ++ping_it;
      --index;
    }
    if (ping_it != ping_times.cend()) {
      std::string message = "hello " + ping_it->first;

      if (auto res = message_conn.write(message); res != message.size()) {
        std::cerr << "[node - " << node_identifier << "] - Error writing to the TCP stream: " << res.error_message()
                  << std::endl;
        break;
      }
      std::clog << "[node - " << node_identifier << "] - Message send from " << message_conn.address() << " to "
                << ping_it->first << std::endl;
    }
    std::this_thread::sleep_for(std::chrono::seconds(duration_distrib(rng)));
  }
  return 0;
}

void process_message(sockpp::tcp_socket sock) {

  std::clog << "[node - " << node_identifier << "] - Server receive message - process" << std::endl;

  while (true) {
    char buf[512];

    while (sock.read(buf, sizeof(buf))) {
      std::string msg(buf);

      if (msg.substr(0, 4) == "hello") {
        std::string identifier = msg.substr(5);

        std::clog << "=====> " << identifier << std::endl;

        if (identifier != node_identifier) {
          if (auto res = message_conn.write(msg); res != msg.size()) {
            std::cerr << "[node - " << node_identifier << "] - Error writing to the TCP stream: " << res.error_message()
                      << std::endl;
          }
        } else {
          std::clog << "[node - " << node_identifier << "] - Server receive message" << std::endl;
        }
      }
      memset(buf, 0, 512);
    }
  }
}

int message_server(in_port_t port) {
  std::clog << "[node - " << node_identifier << "] - Message server started on " << port << std::endl;

  sockpp::error_code ec;
  sockpp::tcp_acceptor acc{port, 4, ec};

  if (ec) {
    std::cerr << "[node - " << node_identifier << "] - Message server - Error creating the acceptor: " << ec.message()
              << std::endl;
    return 1;
  }
  std::clog << "[node - " << node_identifier << "] - Message server - Awaiting connections on port " << port << "..."
            << std::endl;

  while (true) {
    sockpp::inet_address peer;

    if (auto res = acc.accept(&peer); !res) {
      std::cerr << "[node - " << node_identifier << "] - Message server - Error accepting incoming connection: "
                << res.error_message()
                << std::endl;
      return 1;
    } else {
      std::clog << "[node - " << node_identifier << "] - Message server - Received a connection request from " << peer
                << std::endl;
      sockpp::tcp_socket sock = res.release();

      std::thread thr(process_message, std::move(sock));
      thr.detach();
    }
  }
}

int main(int argc, char *argv[]) {
  argparse::ArgumentParser program("node");

  program.add_argument("-i", "--identifier")
    .required()
    .help("node identifier");

  program.add_argument("-p", "--port")
    .required()
    .help("server port");

  program.add_argument("-h", "--neighbour-host")
    .required()
    .help("neighbour host");

  program.add_argument("-n", "--neighbour-port")
    .required()
    .help("neighbour port");

  try {
    program.parse_args(argc, argv);
  }
  catch (const std::exception &err) {
    std::cerr << err.what() << std::endl;
    std::cerr << program;
    std::exit(1);
  }

  node_identifier = program.get<std::string>("--identifier");
  server_port = (in_port_t) std::stoi(program.get<std::string>("--port"));

  sockpp::initialize();

  std::thread ping_client_thread(ping_client, program.get<std::string>("--neighbour-host"),
                                 (in_port_t) std::stoi(program.get<std::string>("--neighbour-port")));
  std::thread ping_server_thread(ping_server, server_port);
  std::thread message_client_thread(message_client, program.get<std::string>("--neighbour-host"),
                                    (in_port_t) (std::stoi(program.get<std::string>("--neighbour-port")) + 1000));
  std::thread message_server_thread(message_server, server_port + 1000);

  ping_client_thread.join();
  ping_server_thread.join();
  message_client_thread.join();
  message_server_thread.join();

  return 0;
}
