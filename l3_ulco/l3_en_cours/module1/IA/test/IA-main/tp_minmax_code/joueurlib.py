import interfacelib
import numpy as np

class Joueur:
	def __init__(self, partie, couleur, opts={}):
		self.couleur = couleur
		self.couleurval = interfacelib.couleur_to_couleurval(couleur)
		self.jeu = partie
		self.opts = opts
		self.nb_appels_jouer=0

	def demande_coup(self):
		pass


class Humain(Joueur):

	def demande_coup(self):
		pass



class IA(Joueur):

	def __init__(self, partie, couleur, opts={}):
		super().__init__(partie,couleur,opts)
		self.temps_exe = 0
		self.nb_appels_jouer = 0
		


class Random(IA):

	def demande_coup(self):
		liste_coups = self.jeu.plateau.liste_coups_valides(self.couleurval)
		r = np.random.randint(0, len(liste_coups))
		return liste_coups[r]



class Minmax(IA):

	def eval(self, plateau):
		liste_coups = plateau.liste_coups_valides(self.couleurval)
		if not liste_coups:
			return 0
		ev=0
		for coup in liste_coups:
			plateau_copie=plateau.copie()
			plateau_copie.jouer(coup,self.couleurval)
			ev+=plateau_copie.score()[1]-plateau_copie.score([2])
		return ev

	def minimax(self,plateau,profondeur,alpha,beta,maximisateur=True):
		#le cas de base
		if profondeur==0 or plateau.jeu.partie_finie:
			#retournons l'etat du plateau actuelle
			return self.eval(plateau)
		#recuperons la listes des coups valides pour le joueur en cours
		liste_coups =plateau.liste_coups_valides(self.couleurval if maximisateur else -self.couleurval)

		#le maximisateur
		if maximisateur:
			max_val=float('-inf')
			for coup in liste_coups:
				#simulons le coup
				plateau_copie=plateau.copie()
				plateau_copie.jouer(coup,self.couleurval)

				#parcours à un niveau profond
				eval_coup=self.minimax(plateau_copie,profondeur-1,alpha,beta,False)
				#prend la valeur maximale
				max_val=max(max_val, eval_coup)

				#mise de jour de alpha
				alpha=max(alpha,eval_coup)

				#comparons alpha et beta
				if beta <= alpha:
					break

			return max_val
		else:
			min_val=float('inf')
			for coup in liste_coups:
				plateau_copie=plateau.copie()
				plateau_copie.jouer(coup, -self.couleurval)
				eval_coup=self.minimax(plateau_copie,profondeur-1,alpha,beta,True)
				min_val=min(min_val,eval_coup)
				beta=min(beta,eval_coup)
				if beta <= alpha:
					break
			return min_val








	def demande_coup(self):
		self.nb_appels_jouer+=1
		pass #de même


class AlphaBeta(IA):

	def demande_coup(self):
		pass #you know the drill
