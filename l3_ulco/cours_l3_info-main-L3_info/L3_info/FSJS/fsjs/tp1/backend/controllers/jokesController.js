var fs = require('fs');

fs.readFile('data/jokes.json', 'utf8', (err, data) => {
  if (err) {
    console.error(err);
    return;
  }
  //console.log(data);
});

module.exports.list = (req, res)=>{
    fs.readFile('data/jokes.json', 'utf8', (err, dataEntier) => {
        if (err) {
          console.error(err);
          return;
        }
        //console.log(data);
        let data = JSON.parse(dataEntier);
        res.render("pages/jokesList",{data});
    });
}

module.exports.random = (req, res)=>{
    fs.readFile('data/jokes.json', 'utf8', (err, dataEntier) => {
        if (err) {
          console.error(err);
          return;
        }
        //console.log(data);
        let data = JSON.parse(dataEntier);
        res.render("pages/jokesRandom",{data});
    });
}