#ifndef TRAFFIC_VEHICLE_HPP
#define TRAFFIC_VEHICLE_HPP

struct Link;
struct Node;

struct Vehicle {
  Vehicle(const char *id);

  ~Vehicle();

  char *id{};
  struct Link *link;
  struct Node *node;
  unsigned int last_time;
};

typedef Vehicle *VehiclePtr;

#endif //TRAFFIC_VEHICLE_HPP
