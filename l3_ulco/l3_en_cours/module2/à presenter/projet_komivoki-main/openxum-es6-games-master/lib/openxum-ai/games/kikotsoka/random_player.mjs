"use strict";

import OpenXum from '../../../openxum-core/openxum/index.mjs';
import Color from "../../../openxum-core/games/kikotsoka/color.mjs";
import Coordinates from "../../../openxum-core/games/kikotsoka/coordinates.mjs";
import Move from "../../../openxum-core/games/kikotsoka/move.mjs";
import Phase from "../../../openxum-core/games/kikotsoka/phase.mjs";

class RandomPlayer extends OpenXum.Player {
    constructor(c, o, e) {
        super(c, o, e);
    }

// public methods
    confirm() {
        return true;
    }

    is_ready() {
        return true;
    }

    is_remote() {
        return false;
    }

    move() {
        if (this._engine.phase() === Phase.PUT_SHIDO) {
            let list = this._engine.get_possible_move_list();

            return list[Math.floor(Math.random() * list.length)];
        } else {
            const possible_patterns = this._engine._is_possible_patterns();
            const current_level = this._engine._color === Color.BLACK ? this._engine._black_level : this._engine._white_level;
            const moves = this._search_min(possible_patterns[current_level]);

            if (moves.length > 0) {
                return moves[Math.floor(Math.random() * moves.length)];
            } else {
                return new Move(Phase.PASS, this._color, null, -1);
            }
        }
    }

    reinit(e) {
        this._engine = e;
    }

    // private methods
    _search_min(possible_patterns) {
        let pattern_index = 0;
        let min = 9;
        let list = [];

        while (pattern_index < possible_patterns.length) {
            let moves_index = 0;

            while (moves_index < possible_patterns[pattern_index].length) {
                const size = possible_patterns[pattern_index][moves_index].list.length;

                if (min > size) {
                    min = size;
                }
                ++moves_index;
            }
            ++pattern_index;
        }
        for (pattern_index = 0; pattern_index < possible_patterns.length; ++pattern_index) {
            for (let moves_index = 0; moves_index < possible_patterns[pattern_index].length; ++moves_index) {
                const size = possible_patterns[pattern_index][moves_index].list.length;

                if (size === min) {
                    for (let index = 0; index < size; ++index) {
                        if (this._engine._is_connect(possible_patterns[pattern_index][moves_index].list[index].line, possible_patterns[pattern_index][moves_index].list[index].column)) {
                            list.push(new Move(Phase.PUT_PIECE, this._color,
                                new Coordinates(possible_patterns[pattern_index][moves_index].list[index].column,
                                    possible_patterns[pattern_index][moves_index].list[index].line), -1));
                        }
                    }
                }
            }
        }
        return list;
    }
}

export default RandomPlayer;
