// ------------------------
// Question 1 
// ------------------------
function croix01(cote){
	// Création d'une géométrie pour le polygone
	const geometry = new THREE.BufferGeometry();


	// Définition des coordonnées des sommets pour les triangles
	const vertices = new Float32Array([

		 cote/ 6, cote / 2, 0,
		-cote / 6, cote / 2, 0,
		-cote / 6, -cote / 2, 0,


		 cote/ 6, cote / 2, 0,
		 cote/6,-cote/ 2,   0,
		-cote / 6, -cote / 2, 0,

		//deuxieme carre

		-cote/6, cote/6,0,
		-cote/2, -cote/6,0,
		-cote/2, cote/6,0,

		-cote/6, cote/6,0,
		-cote/2, -cote/6,0,
		-cote/6, -cote/6,0,

		cote/6, -cote/6,0,
		cote/2, cote/6,0,
		cote/6, cote/6,0,


		cote/6, -cote/6,0,
		cote/2, cote/6,0,
		cote/2, -cote/6,0

	]);


	// Ajout des coordonnées des sommets à la géométrie
	geometry.setAttribute('position', new THREE.BufferAttribute(vertices, 3));

	// Utilisation d'un matériau basique avec une couleur uniforme
	const material = new THREE.MeshBasicMaterial({ color: 0x0000ff,side: THREE.DoubleSide  });

	// Création de l'objet Mesh avec la géométrie et le matériau
	const mesh = new THREE.Mesh(geometry, material);

	return mesh;
}

// ------------------------
// Question 2
// ------------------------
function croix02(cote){
	// Création d'une géométrie pour le polygone
	const geometry = new THREE.BufferGeometry();


	// Définition des coordonnées des sommets pour les triangles
	const vertices = new Float32Array([

		 cote/ 6, cote / 2, 0,
		-cote / 6, cote / 2, 0,
		-cote / 6, -cote / 2, 0,


		cote/ 6, cote / 2, 0,
		cote/6,-cote/ 2,   0,
		-cote / 6, -cote / 2, 0,

		//deuxieme carre

		-cote/6, cote/6,0,
		-cote/2, -cote/6,0,
		-cote/2, cote/6,0,

		-cote/6, cote/6,0,
		-cote/2, -cote/6,0,
		-cote/6, -cote/6,0,

		cote/6, -cote/6,0,
		cote/2, cote/6,0,
		cote/6, cote/6,0,


		cote/6, -cote/6,0,
		cote/2, cote/6,0,
		cote/2, -cote/6,0
	]);
	const colors = new Float32Array([
		// Couleurs pour le triangle 1
		1.0, 0.0, 0.0, // Rouge (Sommet 1)
		1.0, 0.0, 0.0, // Vert (Sommet 2)
		1.0, 0.0, .0, // Bleu (Sommet 3)

		// Couleurs pour le triangle 2
		0.0, 1.0, 0.0, // Vert (Sommet 1)
		0.0, 1.0, 0.0, // Bleu (Sommet 2)
		0.0, 1.0, 0.0, // Rouge (Sommet 3)

		// Couleurs pour le triangle 3
		0.0, 1.0, 0.0, // Vert (Sommet 1)
		0.0, 0.0, 1.0, // Bleu (Sommet 2)
		1.0, 1.0, 0.0, // Rouge (Sommet 3)

		// Couleurs pour le triangle 4
		0.0, 1.0, 1.0, // Vert (Sommet 1)
		0.0, 0.0, 1.0, // Bleu (Sommet 2)
		1.0, 0.0, 0.0, // Rouge (Sommet 3)

		// Couleurs pour le triangle 5
		0.0, 1.0, 0.0, // Vert (Sommet 1)
		0.0, 0.0, 1.0, // Bleu (Sommet 2)
		1.0, 0.0, 1.0, // Rouge (Sommet 3)

		// Couleurs pour le triangle 2
		1.0, 1.0, 0.0, // Vert (Sommet 1)
		0.0, 0.0, 1.0, // Bleu (Sommet 2)
		1.0, 0.0, 0.0, // Rouge (Sommet 3)

	]);


	// Ajout des coordonnées des sommets à la géométrie
	geometry.setAttribute('position', new THREE.BufferAttribute(vertices, 3));
	geometry.setAttribute('color', new THREE.BufferAttribute(colors, 3));

	// Utilisation d'un matériau avec vertex colors pour utiliser les couleurs des sommets
	const material = new THREE.MeshBasicMaterial({ vertexColors: true, side: THREE.DoubleSide });

	// Utilisation d'un matériau basique avec une couleur uniforme
	//const material = new THREE.MeshBasicMaterial({ color: 0x0000ff,side: THREE.DoubleSide  });

	// Création de l'objet Mesh avec la géométrie et le matériau
	const mesh = new THREE.Mesh(geometry, material);

	return mesh;

}

// ------------------------
// Question 3
// ------------------------
function croix03(cote){
	// Création d'une géométrie pour le polygone
	const geometry = new THREE.BufferGeometry();


	// Définition des coordonnées des sommets pour les triangles
	const vertices = new Float32Array([

		cote/ 6, cote / 2, 0,
		-cote / 6, cote / 2, 0,
		-cote / 6, -cote / 2, 0,


		cote/ 6, cote / 2, 0,
		cote/6,-cote/ 2,   0,
		-cote / 6, -cote / 2, 0,

		//deuxieme carre

		-cote/6, cote/6,0,
		-cote/2, -cote/6,0,
		-cote/2, cote/6,0,

		-cote/6, cote/6,0,
		-cote/2, -cote/6,0,
		-cote/6, -cote/6,0,

		cote/6, -cote/6,0,
		cote/2, cote/6,0,
		cote/6, cote/6,0,


		cote/6, -cote/6,0,
		cote/2, cote/6,0,
		cote/2, -cote/6,0

	]);

	const uvs = new Float32Array([
		// Triangle 1
		2/3, 1.0,
		1/3, 1.0,
		0.0, 0.0,

		// Triangle 2
		0.0, 0.0,
		1/3, 0.0,
		2/3, 0.0,

		// Deuxième carré
		0.0, 0.0,
		0.0, 1/3,
		0.0, 2/3,

		0.0, 1/3,
		0.0, 0.0,
		0.0, 0.0,

		1/3, 0.0,
		1.0, 0.0,
		0.5, 0.0,

		0.0, 0.0,
		1.0, 2/3,
		1.0, 1/3
	]);



	// Ajout des coordonnées des sommets à la géométrie
	geometry.setAttribute('position', new THREE.BufferAttribute(vertices, 3));
	geometry.setAttribute('uv', new THREE.BufferAttribute(uvs, 2));

	const textureLoader = new THREE.TextureLoader();
	const texture = textureLoader.load('../Images/swjoke.jpg');

	// Utilisation d'un matériau basique avec une couleur uniforme
	const material = new THREE.MeshBasicMaterial({ map: texture, side: THREE.DoubleSide   });

	// Création de l'objet Mesh avec la géométrie et le matériau
	const mesh = new THREE.Mesh(geometry, material);

	return mesh;
}

// NE RIEN MODIFIER CI-APRES

// construction d'un cube de côté c centrée à l'origine
// Chaque face du cube est d'une couleur différente
// prise parmi les 3 primaires et leur complémentaire
function cubeColore(c){
    const geometry = new THREE.BufferGeometry();
    const vertices = new Float32Array( [
	// face supérieure
	-c/2.0, c/2.0,- c/2.0,  -c/2.0, c/2.0, c/2.0,  c/2.0, c/2.0, c/2.0,  -c/2.0, c/2.0, -c/2.0,  c/2.0, c/2.0, c/2.0,  c/2.0, c/2.0, -c/2.0,
	// face inférieure
	-c/2.0, -c/2.0, -c/2.0,  c/2.0, -c/2.0, -c/2.0,  c/2.0, -c/2.0, c/2.0,  -c/2.0, -c/2.0, -c/2.0,  c/2.0, -c/2.0, c/2.0,  -c/2.0, -c/2.0, c/2.0,
	// face gauche
	-c/2.0, c/2.0, c/2.0,  -c/2.0, c/2.0, -c/2.0,  -c/2.0, -c/2.0, c/2.0,  -c/2.0, -c/2.0, c/2.0,  -c/2.0, c/2.0, -c/2.0,  -c/2.0, -c/2.0, -c/2.0,
	// face droite
	c/2.0, c/2.0, c/2.0,  c/2.0, -c/2.0, c/2.0,  c/2.0, c/2.0, -c/2.0,  c/2.0, c/2.0, -c/2.0,  c/2.0, -c/2.0, c/2.0, c/2.0, -c/2.0, -c/2.0,
	// face avant
	-c/2.0, c/2.0, c/2.0,   -c/2.0, -c/2.0, c/2.0,  c/2.0, c/2.0, c/2.0,  c/2.0, c/2.0, c/2.0,  -c/2.0, -c/2.0, c/2.0, c/2.0, -c/2.0, c/2.0,
	// face arrière
	-c/2.0, c/2.0, -c/2.0,  c/2.0, c/2.0, -c/2.0,  -c/2.0, -c/2.0, -c/2.0,  -c/2.0, -c/2.0, -c/2.0,  c/2.0, c/2.0, -c/2.0,  c/2.0, -c/2.0, -c/2.0
    ] );
    
    const normals = new Float32Array( [
	// face supérieure - normale orientée selon Oy
	0, 1, 0,  0, 1, 0,  0, 1, 0,  0, 1, 0,  0, 1, 0,  0, 1, 0,
	// face inférieure - normale orientée selon -Oy
	0, -1, 0,  0, -1, 0,  0, -1, 0,  0, -1, 0,  0, -1, 0,  0, -1, 0,
	// face gauche - normale orientée selon -Ox
	-1, 0, 0,  -1, 0, 0,  -1, 0, 0,  -1, 0, 0,  -1, 0, 0,  -1, 0, 0,
	// face droite - normale orientée selon Ox
	1, 0, 0,  1, 0, 0,  1, 0, 0,  1, 0, 0,  1, 0, 0,  1, 0, 0,
	// face avant - normale orientée selon Oz
	0, 0, 1,  0, 0, 1,  0, 0, 1,  0, 0, 1,  0, 0, 1,  0, 0, 1,
	// face arrière - normale orientée selon -Oz
	0, 0, -1,  0, 0, -1,  0, 0, -1,  0, 0, -1,  0, 0, -1,  0, 0, -1
    ]);
    
    const colors = new Float32Array( [
	// face supérieure rouge
	1, 0, 0,  1, 0, 0,  1, 0, 0, 1, 0, 0,  1, 0, 0,  1, 0, 0,
	// face inférieure cyan
	0, 1, 1,  0, 1, 1,  0, 1, 1, 0, 1, 1,  0, 1, 1,  0, 1, 1,
	// face gauche vert
	0, 1, 0,  0, 1, 0,  0, 1, 0,  0, 1, 0,  0, 1, 0,  0, 1, 0,
	// face droite magenta
	1, 0, 1,  1, 0, 1,  1, 0, 1,  1, 0, 1,  1, 0, 1,  1, 0, 1,
	// face avant bleu
	0, 0, 1,  0, 0, 1,  0, 0, 1,  0, 0, 1,  0, 0, 1,  0, 0, 1,
	// face arrière jaune
	1, 1, 0,  1, 1, 0,  1, 1, 0,  1, 1, 0,  1, 1, 0,  1, 1, 0
    ]);
    
    geometry.setAttribute( 'position', new THREE.Float32BufferAttribute( vertices, 3 ) );
    geometry.setAttribute( 'normal', new THREE.Float32BufferAttribute( normals, 3 ) );
    geometry.setAttribute( 'color', new THREE.Float32BufferAttribute( colors, 3 ) );

    const lambert = new THREE.MeshLambertMaterial( {
	color: 0xffffff, // reflectance diffuse du materiau
	side: THREE.DoubleSide,// tenir compte des deux faces
	vertexColors: true // il y a des couleurs en chaque sommet
    });
    
    const mesh = new THREE.Mesh( geometry, lambert );
    return mesh;
}

