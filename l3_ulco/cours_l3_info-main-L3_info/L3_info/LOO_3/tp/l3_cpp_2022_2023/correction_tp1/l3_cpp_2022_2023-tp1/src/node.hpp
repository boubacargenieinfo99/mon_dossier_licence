#ifndef TRAFFIC_NODE_HPP
#define TRAFFIC_NODE_HPP

struct Link;
struct Vehicle;

struct Node {
  Node(const char *id, unsigned int duration, unsigned int in_number, unsigned int out_number);

  ~Node();

  void add_vehicle(Vehicle *vehicle, unsigned int time);

  void attach_input_link(Link *link);

  void attach_output_link(Link *link);

  char *id;
  unsigned int duration;
  unsigned int in_number;
  unsigned int out_number;
  Link **in_links;
  Link **out_links;
  Vehicle *vehicle;
};

typedef Node *NodePtr;

#endif //TRAFFIC_NODE_HPP
