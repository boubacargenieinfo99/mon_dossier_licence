#include "MyClass.hpp"

#include <catch2/catch.hpp>
#include <sstream>

TEST_CASE( "init & getter", "[MyClass]" ) {
    // TODO créer un objet MyClass et testez le getter mydata
    MyClass x;
    auto result1 = x.mydata();
    REQUIRE( result1 == "" );
}

TEST_CASE( "setter", "[MyClass]" ) {
    // TODO tester le setter mydata
    MyClass x;
    x.mydata() = "foobar";
    auto result1 = x.mydata();
    REQUIRE( result1 == "foobar");
}

TEST_CASE( "reset", "[MyClass]" ) {
    // TODO set puis reset puis tester
    MyClass x;
    x.mydata() = "foo";
    x.reset();
    auto result1 = x.mydata();
    REQUIRE( result1 == "");
}

TEST_CASE( "fail", "[MyClass]" ) {
    // TODO tester que fail lance une exception (et tester le contenu de cette exception)
    MyClass x;
    //REQUIRE_THROWS_WITH(x.fail(),"this is MyClass::fail");
    try{
        x.fail();
        REQUIRE(false);
    }
    catch(const std::string & e){
        REQUIRE(e == "this is MyClass::fail");
    }
    catch(...){
        REQUIRE(false);
    }
}

TEST_CASE( "sqrt2", "[MyClass]" ) {
    // TODO tester le résultat de sqrt2 à 0.001 près
    MyClass x;
    auto result1 = x.sqrt2();
    REQUIRE(result1 == Approx(1.414).epsilon(0.001));
}

TEST_CASE( "operator<<", "[MyClass]" ) {
    // TODO tester l'operateur << (après un set).
    // Indication : utiliser std::ostringstream.
    MyClass x;
    x.mydata() = "foobar";
    std::ostringstream oxx;
    oxx << x;
    auto result1 = oxx.str();
    REQUIRE( result1 == "foobar");
}

