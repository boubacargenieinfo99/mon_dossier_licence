
#ifndef TRAINING_AGV_HPP
#define TRAINING_AGV_HPP
#pragma once

#include "transportation.hpp"

class AGV : public Transportation {
public:
    AGV(const std::string& id, double speed,
        const Coordinates& initialLocation,
        std::initializer_list<Path> paths);
};

#endif //TRAINING_AGV_HPP
