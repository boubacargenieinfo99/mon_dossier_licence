#include <iostream>
#include <string>
#include "sockpp/tcp_connector.h"
#include "sockpp/tcp_acceptor.h"
#include <thread>

using namespace std;

void run_echo(sockpp::tcp_socket  sock){
    ssize_t n;
    char buff[512];

    while ((n=sock.read(buff, sizeof(buff)))>0)
        sock.write_n(buff, n);

    cout << "Connection closed from " << sock.peer_address() << endl;
}
int main(int argc, char* argv[]) {
    std::cout << "Exemple TCP" << std::endl;
    in_port_t port = (argc > 1) ? atoi(argv[1]) : 14445;

    // Initialisation via initialize
    sockpp::initialize();

    // Instanciation tcp_acceptor
    sockpp::tcp_acceptor acceptor(port);

    if (!acceptor) {
        std::cerr << "Erreur de création de l'accepteur" << std::endl;
        return 1;
    }
    std::cout << "En attente du port " << port << "..." << std::endl;

    while (true) {
        // Accepter une connexion entrante
        sockpp::inet_address client_address;
        auto result_socket = acceptor.accept(&client_address);

        auto sock=result_socket.release();

        if (!result_socket) {
            std::cerr << "Erreur lors de l'acceptation du client : " << std::endl;
        } else {
            thread thr(run_echo, std::move(sock));
            thr.detach();
        }
    }


    return 0;
}
