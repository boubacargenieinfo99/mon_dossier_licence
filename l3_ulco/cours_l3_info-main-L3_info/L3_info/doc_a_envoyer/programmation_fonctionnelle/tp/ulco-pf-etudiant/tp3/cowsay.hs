
cow :: String
cow = 
    "    \\   ^__^ \n\
    \     \\  (oo)\\_______ \n\
    \        (__)\\       )\\/\\ \n\
    \            ||----w | \n\
    \            ||     || "

cowSimplifie :: String
cowSimplifie = "    \\   ^__^ \n     \\  (oo)\\_______ \n        (__)\\       )\\/\\ \n            ||----w | \n            ||     || "

main :: IO ()
main = do
    putStrLn cow
    putStrLn ""
    putStrLn cowSimplifie


