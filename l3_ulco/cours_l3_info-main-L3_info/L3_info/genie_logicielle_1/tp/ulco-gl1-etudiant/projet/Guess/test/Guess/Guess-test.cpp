#include <Guess/Guess.hpp>

#include <catch2/catch.hpp>

TEST_CASE( "newgame(42) = target()" ) {
    Game g;
    g.newGame(42);
    int result = g.target();
    REQUIRE(result == 42);
}

TEST_CASE( "newgame(50) = target()" ) {
    Game g;
    g.newGame(50);
    int result = g.target();
    REQUIRE(result == 50);
}

TEST_CASE( "play(42,false) && target = 42" ) {
    Game g;
    g.newGame(42);
    g.play(42,false);
    Result result = g.get_resultat();
    REQUIRE(result == Win);
}


TEST_CASE( "play(42,false) && target = 50" ) {
    Game g;
    g.newGame(50);
    g.play(42,false);
    Result result = g.get_resultat();
    REQUIRE(result == Too_low);
}

TEST_CASE( "play(42,false) && target = 30" ) {
    Game g;
    g.newGame(30);
    g.play(42,false);
    Result result = g.get_resultat();
    REQUIRE(result == Too_high);
}

TEST_CASE( "play(42,true) && target = 42" ) {
    Game g;
    g.newGame(42);
    g.play(42,true);
    Result result = g.get_resultat();
    REQUIRE(result == Win);
}


TEST_CASE( "play(42,true) && target = 50" ) {
    Game g;
    g.newGame(50);
    g.play(42,true);
    Result result = g.get_resultat();
    REQUIRE(result == Lose);
}

TEST_CASE( "play(42,true) && target = 30" ) {
    Game g;
    g.newGame(30);
    g.play(42,true);
    Result result = g.get_resultat();
    REQUIRE(result == Lose);
}

TEST_CASE( "remainingTries()" ) {
    Game g;
    g.newGame(30);
    int nbFoisExecute = 0;
    while(g.remainingTries()){
        nbFoisExecute ++;
    }
    REQUIRE(nbFoisExecute == 5);
}



