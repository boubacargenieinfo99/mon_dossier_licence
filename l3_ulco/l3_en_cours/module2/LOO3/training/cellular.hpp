
#ifndef TRAINING_CELLULAR_HPP
#define TRAINING_CELLULAR_HPP
#pragma once

#include "geometry.hpp"
#include <vector>
#include <memory>

class Cell;

class Cellular : public Geometry {
public:
    Cellular(int width, int height);
    void addCell(std::unique_ptr<Cell> cell);
    void displayInfo() const override;

private:
    int width;
    int height;
    std::vector<std::unique_ptr<Cell>> cells;
};
#endif //TRAINING_CELLULAR_HPP
