#include "link.hpp"
#include "node.hpp"

namespace traffic {

Link::Link(const std::string &id, unsigned int length, unsigned int way_number) :
  _id(id), _length(length), _ways(way_number, Way(length)) {}

void Link::attach_in_node(const std::shared_ptr<Node> &node) {
  _in_node = node;
}

void Link::attach_out_node(const std::shared_ptr<Node> &node) {
  _out_node = node;
}

bool Link::is_free() const {
  unsigned int way_index = 0;
  bool found = false;

  while (way_index < _ways.size() and not found) {
    if (_ways[way_index].is_free()) { found = true; } else { ++way_index; }
  }
  return found;
}

void Link::push_vehicle(unsigned int t, std::unique_ptr<Vehicle> &vehicle) {
  unsigned int way_index = 0;
  bool found = false;

  while (way_index < _ways.size() and not found) {
    if (_ways[way_index].is_free()) {
      _ways[way_index].push_vehicle(t, vehicle);
      found = true;
    } else { ++way_index; }
  }
}

}