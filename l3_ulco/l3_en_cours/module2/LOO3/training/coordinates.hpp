

#ifndef TRAINING_COORDINATES_HPP
#define TRAINING_COORDINATES_HPP
#pragma once

#include <initializer_list>

class Coordinates {
public:
    Coordinates();
    Coordinates(std::initializer_list<double> values);

   virtual void displayInfo() const;
    double getX() const;
    double getY() const;

private:
    double x;
    double y;
};



#endif //TRAINING_COORDINATES_HPP
