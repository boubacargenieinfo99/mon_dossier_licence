pgcd :: Int -> Int -> Int
pgcd a 0 = a
pgcd x n = pgcd n (x `mod` n)

main :: IO()
main = do
    print (pgcd 49 35)