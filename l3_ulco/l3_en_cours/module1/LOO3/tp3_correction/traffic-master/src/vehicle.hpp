#ifndef TRAFFIC_VEHICLE_HPP
#define TRAFFIC_VEHICLE_HPP

#include <string>
#include <vector>

namespace traffic {

class Vehicle {
public:
  Vehicle(const std::string &id, unsigned int speed, const std::vector<std::string> &path);

  void advance() { ++_path_index; }

  const std::string &first_path_id() const { return _path.front(); }

  const std::string &id() const { return _id; }

  const std::string &next_path_id() const { return _path[_path_index + 1]; }

  unsigned int speed() const { return _speed; }

private:
  std::string _id;
  unsigned int _speed;
  std::vector<std::string> _path;
  unsigned int _path_index;
};

}

#endif //TRAFFIC_VEHICLE_HPP
