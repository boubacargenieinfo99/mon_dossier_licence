import interface
import numpy as np
import random
import math
import time

class Joueur:
	def __init__(self, partie, couleur, opts={}):
		self.couleur = couleur
		self.couleurval = interface.couleur_to_couleurval(couleur)
		self.jeu = partie
		self.opts = opts
		self.compteur = 0
		self.compteur_temps = 0

	def demande_coup(self):
		pass


class Humain(Joueur):

	def demande_coup(self):
		pass



class IA(Joueur):

	def __init__(self, partie, couleur, opts={}):
		super().__init__(partie,couleur,opts)
		self.temps_exe = 0
		self.nb_appels_jouer = 0



class Random(IA):

	def demande_coup(self): # plat: plateau de jeu , couleurVal: -1 -> blanc et 1 -> noir
		if self.couleurval == 1:
			self.jeu.compteur[1] += 1
		if self.couleurval == -1:
			self.jeu.compteur[0] += 1
		listeCoupPossible = self.jeu.plateau.liste_coups_valides(self.couleurval)
		nbAlea = np.random.randint(0, len(listeCoupPossible))
		return listeCoupPossible[nbAlea]


class Minmax(IA):

	def demande_coup(self):
		date_debut = time.time()
		profondeur = 3
		plat = self.jeu.plateau.copie()
		coup = (self.minmax(plat,1,profondeur))
		if self.couleurval == 1: # noir
			self.jeu.compteur_temps[0] += time.time() - date_debut
			self.jeu.score[0] += coup[0]
			self.jeu.nb_demande[0] += 1
		if self.couleurval == -1: # blanc
			self.jeu.compteur_temps[1] += time.time() - date_debut
			self.jeu.score[1] += coup[0]
			self.jeu.nb_demande[1] += 1
		return coup[1]

	def minmax(self,plat,J,profondeur): #N : noeud , J : joueur
		if self.couleurval == 1:
			self.jeu.compteur[0] += 1
		if self.couleurval == -1:
			self.jeu.compteur[1] += 1
		if profondeur == 0:
			return (plat.evaluation_moyen(self.couleurval),None)  #score(),None)
		else:
			coupPossible = plat.liste_coups_valides(self.couleurval)
			if coupPossible == None:
				return (plat.score(),None)
			if J == 1:	#recherche du meilleur coup
				M = -float('inf') #-math.inf
				for coup in coupPossible:
					copie_plateau = plat.copie()
					copie_plateau.jouer(coup,self.couleurval)
					val = self.minmax(copie_plateau,2,profondeur-1)[0]
					if M < val:
						M = val
						coupAFaire = coup
				return (M,coupAFaire)
			elif J == 2:	#recherche du pire coup
				m = float('inf') #math.infini
				for coup in coupPossible:
					copie_plateau = plat.copie()
					copie_plateau.jouer(coup,self.couleurval)
					val = self.minmax(copie_plateau,1,profondeur-1)[0]
					if m > val:
						m = val
						coupAFaire = coup
				return (m,coupAFaire)



	def evaluation_position(self):	# on regarde la position des pion le plus pres de la victoire et on l avance si possible sur un pion
		pass

class AlphaBeta(IA):

	def demande_coup(self):
		date_debut = time.time()
		profondeur = 1
		plat = self.jeu.plateau.copie()
		coup = (self.alphabeta(plat, 1, profondeur))
		if self.couleurval == 1:  # noir
			self.jeu.compteur_temps[0] += time.time() - date_debut
			self.jeu.score[0] += coup[0]
			self.jeu.nb_demande[0] += 1
		if self.couleurval == -1:  # blanc
			self.jeu.compteur_temps[1] += time.time() - date_debut
			self.jeu.score[1] += coup[0]
			self.jeu.nb_demande[1] += 1
		return coup[1]
	def alphabeta(self,plat,J,profondeur,A=-float('inf'),B=float('inf')):
		if self.couleurval == 1:
			self.jeu.compteur[0] += 1
		if self.couleurval == -1:
			self.jeu.compteur[1] += 1
		if profondeur == 0:
			return (plat.evaluation_moyen(self.couleurval),None)  #score(),None)
		else:
			coupPossible = plat.liste_coups_valides(self.couleurval)
			if coupPossible == None:
				return (plat.score(), None)
			if J == 1:	#recherche du meilleur coup
				M = -float('inf') #-math.inf
				for coup in coupPossible:
					copie_plateau = plat.copie()
					copie_plateau.jouer(coup,self.couleurval)
					val = self.alphabeta(copie_plateau,2,profondeur-1,A,B)[0]
					if M < val:
						M = val
						coupAFaire = coup
						A = max(A,M)
					if A >= B :
						break
				return (M,coupAFaire)
			elif J == 2:	#recherche du pire coup
				m = float('inf') #math.infini
				for coup in coupPossible:
					copie_plateau = plat.copie()
					copie_plateau.jouer(coup,self.couleurval)
					val = self.alphabeta(copie_plateau,1,profondeur-1,A,B)[0]
					if m > val:
						m = val
						coupAFaire = coup
						B = min(B,m)
					if A >= B:
						break
				return (m,coupAFaire)

class Noeud:
	def __init__(self, score, g = None, d = None):
		self.sousNoeud = g # type Noeud []
		self.score = score

class Arbre:
	def __init__(self, racine = None):
		self.racine = racine # type : Noeud

