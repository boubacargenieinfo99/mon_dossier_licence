#ifndef TICTACTOE_HPP
#define TICTACTOE_HPP

#include <array>
#include <iostream>

enum class Status { Egalite, RougeGagne, VertGagne, RougeJoue, VertJoue };

enum class Cell { Vide, Rouge, Vert };

// Moteur de jeu de tictactoe.
/** class Jeu **/
class Jeu {
    private:
        std::array<std::array<Cell, 3>, 3> _plateau;
        
        int nbLAndC =3;
        

        ///etat du jeu (rouge commence)
        Status etat;

    public:
        // Constructeur à utiliser.
        ///constructeur du jeu
        Jeu();

        // Retourne le status du jeu courant (Egalite, RougeGagne, VertGagne, RougeJoue, VertJoue).
        /** retourne le status du jeu (Egalite, RougeGagne, VertGagne, RougeJoue, VertJoue) **/
        Status getStatus() const; 

        // Retourne l'état d'une case du plateau
        /** retourne la cellule de la ligne i et de colonne j **/ 
        Cell getCell(int i, int j) const;

        // Joue un coup pour le joueur courant.
        // 
        // i ligne choisie (0, 1 ou 2)
        // j colonne choisie (0, 1 ou 2)
        // 
        // Si le coup est invalide, retourne false. Si le coup est valide,
        // joue le coup et retourne true.
        /** joue sur la case de ligne i et colonne j 
         * (retourne true si la cellule est vide ou false si la case n est pas vide ) **/
        bool jouer(int i, int j);

        // Réinitialise le jeu.
        /** remise a zéro du jeu **/
        void raz();

        /** retourne le nombre de ligne et de colonne (nombre de ligne = nombre de colonne) **/
        int getNbLAndC() const{return nbLAndC;}

        /// verifie si le jeu est terminée et change le status en fonction du vainqueur
        void checkIfEnd();
};

/** pour inserer dans le ostream le tableau **/
std::ostream & operator<<(std::ostream & os, const Jeu & jeu);

#endif

