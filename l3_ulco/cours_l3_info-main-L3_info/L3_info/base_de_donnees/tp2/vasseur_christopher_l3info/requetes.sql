--4.a

--SELECT * FROM Etudiant				/*  selectionne les etudiants 	*/
--	NATURAL JOIN Formation;			/*	  qui ont une formation		*/
	--WHERE Libelle = 'L3 Info';

SELECT NomE,Sexe FROM Etudiant
	NATURAL JOIN Formation
	WHERE Libelle = 'L3 Info'
	ORDER BY NomE DESC;
	
	/* resultat:
	
		NOME			 		 S
		------------------------ -
		Yang			 		 M
		Martin			 		 F
		Gros			 		 F
		Dupond			 		 M
		Dubois			 		 M
	
	*/


--4.b

SELECT NomE,Sexe FROM Etudiant
	NATURAL JOIN Formation
	WHERE Libelle = 'L3 Info'
	ORDER BY Sexe ASC;
	
	/* resultat:
	
		NOME			 		 S
		------------------------ -
		Gros			 		 F
		Martin			 		 F
		Yang			 		 M
		Dubois			 		 M
		Dupond			 		 M
	
	*/
	
--4.c

SELECT NomE,NomM FROM Etudiant
	NATURAL JOIN Matiere
	NATURAL JOIN Noter
	WHERE Note IS NULL;
	
	/*	resultat:
		
		NOME			 		 NOMM
		------------------------ --------------------
		Bouziane		 		 Statistiques
		Yang			 		 Programmation

	*/
	
--4.d

SELECT NomE FROM Etudiant
	NATURAL JOIN Matiere
	NATURAL JOIN Noter
	WHERE NomE NOT IN (SELECT NomE FROM Etudiant
						NATURAL JOIN Matiere
						NATURAL JOIN Noter
						WHERE Note IS NULL)
	GROUP BY NomE;

	/* resultat:
		
		NOME
		------------------------
		Dubois
		Paris
		Humbert
		Favier
		Dupond
		Henri
		Martin
		Gros
		
	*/
	
--4.e

SELECT NomE FROM Etudiant
	WHERE NomE NOT IN (SELECT NomE FROM Etudiant
							NATURAL JOIN Matiere
							NATURAL JOIN Noter
							GROUP BY NomE);
							
	/* resultat:
		
		NOME
		------------------------
		Romain

	
	*/

--4.f

SELECT * FROM Etudiant
	NATURAL JOIN Noter
	NATURAL JOIN Matiere
	WHERE nomM = 'Programmation' AND Note = (SELECT MIN(Note) FROM Noter
												NATURAL JOIN Etudiant
												NATURAL JOIN Matiere
												where nomM = 'Programmation');

	/*resultat:

		    NUMMAT     NUMETU NOME                     		DATENAIS   	S      CODEF       NOTE NOMM                 COEFFICIENT
			---------- ---------- ------------------------ ---------- 	- ---------- ---------- -------------------- -----------
         	1          1 Dupond                   			18-03-1994 	M          3          7 Programmation                  5
         	1          5 Henri                    			12-10-1996 	M          2          7 Programmation                  5


	*/

--4.g

SELECT NomE,ROUND(AVG(Note),2) AS Moyenne_generale FROM Etudiant
	NATURAL JOIN Noter
	GROUP BY NomE;

	/*resultat:

		NOME                     MOYENNE_GENERALE
		------------------------ ----------------
		Dupond                                9.7
		Bouziane                            10.25
		Dubois                               11.4
		Paris                                9.33
		Gros                                11.13
		Yang                                11.38
		Favier                                9.5
		Humbert                             11.33
		Henri                                10.4
		Martin                               13.8

	*/

--4.h

SELECT NomE,Note FROM Etudiant
	NATURAL JOIN Noter
	NATURAL JOIN Matiere
	WHERE nomM = 'Programmation' and note > (SELECT MAX(Note) FROM Noter
												NATURAL JOIN Etudiant
												WHERE NomE = 'Dubois');

	/*resultat:

		NOME                           NOTE
		------------------------ ----------
		Martin                           18

	*/

--4.i

UPDATE (SELECT * FROM Noter NATURAL JOIN Matiere)
	SET Note = Note + 2
	WHERE nomM = 'Programmation' and Note IS NOT NULL;

	/*resultat: SELECT nomE,Note FROM Etudiant
    			NATURAL JOIN Noter
  				Natural join Matiere
  				WHERE nomM = 'Programmation';



		NOME                           NOTE
		------------------------ ----------
		Dupond                            9
		Dubois                           13
		Favier                           16
		Gros                           13.5
		Henri                             9
		Bouziane                       14.5
		Martin                           20
		Paris                            10
		Yang

	*/

--4.j

CREATE VIEW VueF AS SELECT e.nomE,e.Sexe,f.Libelle
 FROM Etudiant e,Formation f
 WHERE e.CodeF = f.CodeF;

 SELECT nomE FROM VueF
	WHERE Libelle = 'L3 Info' and Sexe = 'M';


	/*resultat:

		NOME
		------------------------
		Dupond
		Dubois
		Yang

	*/