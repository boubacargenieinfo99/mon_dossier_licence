#include "conveyor.hpp"
#include <iostream>
#include "machine.hpp"

Conveyor::Conveyor(const std::string& id, int capacity, int length, double speed,
                   const Machine& source, const Machine& destination)
        : UnavailableElement(id), capacity(capacity), length(length), speed(speed),
          sourceMachine(source), destinationMachine(destination) {}
const Machine& Conveyor::getSourceMachine() const {
    return sourceMachine;
}

const Machine& Conveyor::getDestinationMachine() const {
    return destinationMachine;
}

