
#ifndef TRAINING_CELL_HPP
#define TRAINING_CELL_HPP
#pragma once

#include "coordinates.hpp"

class Cell : public Coordinates {
public:
    Cell(int x, int y);
    void displayInfo() const override;

private:
    int x;
    int y;
};


#endif //TRAINING_CELL_HPP
