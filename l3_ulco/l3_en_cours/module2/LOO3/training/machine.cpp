#include "machine.hpp"

Machine::Machine(std::string id, int startupTime, int operationTime) : SpatialElement(), UnavailableElement(id) {}

void Machine::addLabor(std::shared_ptr<Labor> labor) {
    labors.push_back(labor);
}
