"use strict";

import Coordinates from './coordinates.mjs';
import OpenXum from '../../openxum/index.mjs';

class Move extends OpenXum.Move {
  constructor(t, c/* et le reste*/) {
    super();
    this._type = t;
    this._color = c;
    // + les attributs spécifiques au jeu
  }

// public methods
    decode(str) {
    }

    encode() {
    }

    from_object(data) {
    }

    to_object() {
    }

    to_string() {
    }
}

export default Move;
