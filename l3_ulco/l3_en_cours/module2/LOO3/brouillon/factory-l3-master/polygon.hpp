#include <vector>
#include <initializer_list>
#include "point.hpp"

class Polygon:public Geometry{
public:
    Polygon(std::initializer_list<Point> points){
        if(points.size()>=3) points=points;
    }
    std::unique_ptr<Geometry> clone() const override{
        return std::make_unique<Polygon>(*this);
    }
private:
    std::vector<Point> points;
};