/**
 * @file openxum/app/main.cpp
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2011-2024 Openxum Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <memory>
#include <system_error>
#include <thread>

#include <nlohmann/json.hpp>

#include <corvusoft/restbed/request.hpp>
#include <corvusoft/restbed/resource.hpp>
#include <corvusoft/restbed/service.hpp>
#include <corvusoft/restbed/session.hpp>
#include <corvusoft/restbed/settings.hpp>
#include <corvusoft/restbed/status_code.hpp>
#include <corvusoft/restbed/web_socket.hpp>
#include <corvusoft/restbed/web_socket_message.hpp>

#include <openssl/sha.h>

#include "games.hpp"

using json = nlohmann::json;
using namespace restbed;

struct Game {
  std::shared_ptr<WebSocket> socket;
  std::unique_ptr<std::thread> thread;
  std::unique_ptr<openxum::core::common::AbstractPlayer> player;
};

std::map<std::string, Game> active_games{};

typedef unsigned char uchar;

static const std::string b = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

static std::string base64_encode(const u_int8_t *in, size_t size) {
  std::string out;

  int val = 0, valb = -6;
  for (size_t i = 0; i < size; ++i) {
    uchar c = in[i];

    val = (val << 8) + c;
    valb += 8;
    while (valb >= 0) {
      out.push_back(b[(val >> valb) & 0x3F]);
      valb -= 6;
    }
  }
  if (valb > -6) out.push_back(b[((val << 8) >> (valb + 8)) & 0x3F]);
  while (out.size() % 4) out.push_back('=');
  return out;
}

void run_game(std::string key) {
  auto it = active_games.find(key);
  json ret = {{"action", "next_move"}};
  json json_move = it->second.player->move();

  ret["move"] = json_move;
  it->second.player->move(json_move);
  it->second.socket->send(to_string(ret));
  it->second.thread = nullptr;
}

std::multimap<std::string, std::string>
build_websocket_handshake_response_headers(const std::shared_ptr<const Request> &request) {
  auto key = request->get_header("Sec-WebSocket-Key");
  Byte hash[SHA_DIGEST_LENGTH];
  std::multimap<std::string, std::string> headers;

  key.append("258EAFA5-E914-47DA-95CA-C5AB0DC85B11");
  SHA1(reinterpret_cast< const unsigned char * >(key.data()), key.length(), hash);
  headers.insert(std::make_pair("Upgrade", "websocket"));
  headers.insert(std::make_pair("Connection", "Upgrade"));
  headers.insert(make_pair("Sec-WebSocket-Accept", base64_encode(hash, SHA_DIGEST_LENGTH)));
  return headers;
}

void close_handler(const std::shared_ptr<WebSocket> &socket) {
  if (socket->is_open()) {
    socket->send(std::make_shared<WebSocketMessage>(WebSocketMessage::CONNECTION_CLOSE_FRAME, Bytes({10, 00})));
  }

  const auto key = socket->get_key();

  active_games.erase(key);

  std::clog << "Closed connection to " << key.data() << std::endl;
}

void error_handler(const std::shared_ptr<WebSocket> &socket, const std::error_code error) {
  std::cerr << "WebSocket Errored '" << error.message().data() << "' for " << socket->get_key().data() << std::endl;
}

void message_handler(const std::shared_ptr<WebSocket> &source, const std::shared_ptr<WebSocketMessage> &message) {
  const auto opcode = message->get_opcode();

  if (opcode == WebSocketMessage::CONNECTION_CLOSE_FRAME) {
    source->close();
  } else if (opcode == WebSocketMessage::TEXT_FRAME) {
    std::string action(reinterpret_cast< const char * >(message->get_data().data()), message->get_data().size());
    const auto &key = source->get_key();
    json m = json::parse(action);
    auto it = active_games.find(key);

    if (it != active_games.end()) {
      if (m["action"] == "start") {
        json ret = {{"action", "started"}};

        it->second.player = std::unique_ptr<openxum::core::common::AbstractPlayer>(
          openxum::core::common::Builder::instance().build(m["game"], m["player_type"], m["player_color"],
                                                           m["opponent_color"], m["type"], m["color"]));
        it->second.socket->send(to_string(ret));
      } else if (m["action"] == "apply_move") {
        json ret = {{"action", "apply_move"}};
        json json_move = json::parse(m["move"].get<std::string>());

        it->second.player->move(json_move);
        it->second.socket->send(to_string(ret));
      } else if (m["action"] == "next_move") {
        it->second.thread = std::make_unique<std::thread>(run_game, key);
        it->second.thread->detach();
      } else {
        json ret = {{"message", "unknown action"}};

        it->second.socket->send(to_string(ret));
      }
    }
  }
}

void get_method_handler(const std::shared_ptr<Session> &session) {
  const auto request = session->get_request();
  const auto connection_header = request->get_header("connection", String::lowercase);

  if (connection_header.find("upgrade") != std::string::npos) {
    if (request->get_header("upgrade", String::lowercase) == "websocket") {
      const auto headers = build_websocket_handshake_response_headers(request);

      session->upgrade(SWITCHING_PROTOCOLS, headers, [](const std::shared_ptr<WebSocket> &socket) {
        if (socket->is_open()) {
          json ret = {{"action", "connected"}};

          socket->set_close_handler(close_handler);
          socket->set_error_handler(error_handler);
          socket->set_message_handler(message_handler);
          socket->send(to_string(ret), [](const std::shared_ptr<WebSocket> &socket) {
            auto it = active_games.insert(make_pair(socket->get_key(), Game()));
            it.first->second.socket = socket;
          });
          std::clog << "Connection to " << socket->get_key() << std::endl;
        } else {
          std::cerr << "WebSocket Negotiation Failed: Client closed connection" << std::endl;
        }
      });
      return;
    }
  }
  session->close(BAD_REQUEST);
}

void service_ready_handler(Service &) {
  std::clog << "The service is up and running." << std::endl;
}

int main(int, const char **) {
  auto resource = std::make_shared<Resource>();

  resource->set_path("/openxum");
  resource->set_method_handler("GET", get_method_handler);

  auto settings = std::make_shared<Settings>();

  settings->set_port(3000);
  settings->set_default_header("Connection", "close");

  auto service = std::make_shared<Service>();

  service->publish(resource);
  service->set_ready_handler(service_ready_handler);
  service->start(settings);

  return EXIT_SUCCESS;
}