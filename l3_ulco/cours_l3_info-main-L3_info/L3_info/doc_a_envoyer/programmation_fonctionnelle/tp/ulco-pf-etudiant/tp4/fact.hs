fact :: Int -> Int
fact 0 = 0
fact 1 = 1
fact a = fact (a-1) * a 

factTco :: Int -> Int
factTco x = go 1 x
    where
        go _ 0 = 0
        go acc 1 = acc
        go acc d = go (acc * d) (d-1)


main :: IO()
main = do
    print(fact 2)
    print(fact 5)

    print(factTco 2)
    print(factTco 5)