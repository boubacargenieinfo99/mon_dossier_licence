#include "path.hpp"
#include <iostream>

Path::Path(const std::initializer_list<Coordinates>& coordinates) : coordinates(coordinates) {}

void Path::displayInfo() const {
    for (const auto& coord : coordinates) {
        coord.displayInfo();
        std::cout << " ";
    }
}

const std::vector<Coordinates>& Path::getCoordinates() const {
    return coordinates;
}
