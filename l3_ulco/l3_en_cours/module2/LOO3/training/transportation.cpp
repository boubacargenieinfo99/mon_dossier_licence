#include "transportation.hpp"
#include <iostream>

Transportation::Transportation(const std::string& id, double speed, const Coordinates& initialLocation,
                               const std::initializer_list<Path>& paths)
        : UnavailableElement(id), initialLocation(initialLocation), paths(paths) {}

void Transportation::displayInfo() const {
    std::cout << "Transportation - ID: " << getId() << " - Speed: " << getSpeed() << std::endl;
    std::cout << "Initial Location: ";
    initialLocation.displayInfo();
    std::cout << "Paths: ";
    for (const auto& path : paths) {
        path.displayInfo();
    }
    std::cout << std::endl;
}

const Coordinates& Transportation::getInitialLocation() const {
    return initialLocation;
}

const std::vector<Path>& Transportation::getPaths() const {
    return paths;
}
