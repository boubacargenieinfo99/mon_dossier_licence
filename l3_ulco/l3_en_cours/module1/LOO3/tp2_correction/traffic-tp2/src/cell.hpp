#ifndef TRAFFIC_CELL_HPP
#define TRAFFIC_CELL_HPP

#include <memory>

#include "vehicle.hpp"

namespace traffic {

class Cell {
public:
  Cell();

  Cell(const Cell& other);

  bool is_free() const;

  void push_vehicle(unsigned int t, std::unique_ptr<Vehicle> &vehicle);

  unsigned int out_time() const { return _out_time; }

  std::unique_ptr<Vehicle>& vehicle() { return _vehicle; }

private:
  std::unique_ptr<Vehicle> _vehicle;
  unsigned int _out_time;
};

}

#endif //TRAFFIC_CELL_HPP
