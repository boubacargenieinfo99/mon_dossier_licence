#include "vehicle.hpp"

#include <cstring>
#include <iostream>

Vehicle::Vehicle(const std::string &id, const std::initializer_list<uint8_t> &indexes) : _id(id), _path(indexes) {
  _link = nullptr;
  _node = nullptr;
  _last_time = 0;

//  std::cout << "[Vehicle] constructor - " << _id << std::endl;

}

Vehicle::Vehicle(const Vehicle &v) : _id(v._id), _path(v._path) {
  _link = v._link;
  _node = v._node;
  _last_time = v._last_time;

//  std::cout << "[Vehicle] copy constructor - " << _id << std::endl;

}

Vehicle::~Vehicle() {

//  std::cout << "[Vehicle] destructor - " << _id << std::endl;

}