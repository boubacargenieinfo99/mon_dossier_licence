#include "link.hpp"
#include "round_about.hpp"

#include <iostream>

namespace traffic {

RoundAbout::RoundAbout(const std::string &id, unsigned int duration, size_t in_number, size_t out_number,
                       unsigned int capacity) :
  Node(id, duration, in_number, out_number), _capacity(capacity) {}

void RoundAbout::push_vehicle(unsigned int t, std::unique_ptr<Vehicle> &vehicle) {

  std::cout << t << ": " << vehicle->id() << " enters " << id() << " [" << (_vehicles.size() + 1) << "/" << _capacity
            << "]" << std::endl;

  _vehicles.emplace_back(vehicle.release());
  _out_times.push_back(t + duration());
};

void RoundAbout::run(unsigned int t) {
  while (not _vehicles.empty() and t >= _out_times.front()) {
    unsigned int link_index = 0;
    bool found = false;

    while (link_index < out_links().size() and not found) {
      if (out_links()[link_index]->id() == _vehicles.front()->next_path_id()) {
        found = true;
      } else {
        ++link_index;
      }
    }
    if (not out_links().empty() and found and out_links()[link_index]->is_free()) {
      _vehicles.front()->advance();
      out_links()[link_index]->push_vehicle(t, _vehicles.front());
      _vehicles.pop_front();
      _out_times.pop_front();
    } else {
      break;
    }
  }
}

}