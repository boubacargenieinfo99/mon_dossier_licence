palindrome :: String -> Bool
palindrome l = l == reverse l

main::IO ()
main = do 
    print (palindrome "radar")
    print (palindrome "foobar")