
#ifndef TP4_CELLULAR_HPP
#define TP4_CELLULAR_HPP
#include <iostream>
#include <vector>
using namespace std;

class Cellular:public Geometry{
public:
    Cellular(float width,float height,Cell& cell):width(width),height(height){}

private:
    float width;
    float height;
    vector<Cell>celles;
};

#endif //TP4_CELLULAR_HPP
