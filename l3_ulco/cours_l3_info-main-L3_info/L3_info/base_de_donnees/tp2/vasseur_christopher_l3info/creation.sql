--VASSEUR CHRISTOPHER
--tp2
--1

--if EXISTS TABLE Noter
	DROP TABLE Noter;

--if EXISTS TABLE Etudiant
	DROP TABLE Etudiant;

--if EXISTS TABLE Matiere
	DROP TABLE Matiere;
	
--if EXISTS TABLE Formation
	DROP TABLE Formation;
	
CREATE TABLE Formation(
	CodeF Number(2),
		PRIMARY KEY (CodeF),
	Libelle Varchar2(10) NOT NULL
);

CREATE TABLE Matiere(
	NumMat Number(2),
		PRIMARY KEY (NumMat),
	NomM Varchar2(20) NOT NULL,
	Coefficient Number(2) NOT NULL,
		CONSTRAINT ck_Coefficient CHECK (Coefficient > 0)
);
	
CREATE TABLE  Etudiant(
	NumEtu Number(2),
		PRIMARY KEY (NumEtu),
	NomE Varchar2(24) NOT NULL UNIQUE,
	DateNais date NOT NULL,
	Sexe char(1) DEFAULT ON NULL 'M',
		CONSTRAINT ck_Sexe CHECK ((Sexe = 'M') or (Sexe = 'F')),
	CodeF Number(2),
		FOREIGN KEY (CodeF) REFERENCES Formation(CodeF)
);


CREATE TABLE Noter(
	NumEtu Number(2),
		FOREIGN KEY (NumEtu) REFERENCES Etudiant(NumEtu),
	NumMat Number(2),
		FOREIGN KEY (NumMat) REFERENCES Matiere(NumMat),
	Note Number(4,2),
		CONSTRAINT ck_Note CHECK (Note >= 0 and Note <=20),
	PRIMARY KEY (NumEtu,NumMat)
);





