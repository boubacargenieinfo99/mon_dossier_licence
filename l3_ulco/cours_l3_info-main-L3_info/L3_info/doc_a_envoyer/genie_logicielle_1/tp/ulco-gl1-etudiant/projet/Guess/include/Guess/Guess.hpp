#pragma once

enum Result {Win,Lose,Too_low,Too_high};

/// \brief classe de jeu
///
class Game{
    public:
        void newGame();
        ///launch a new game with the target select , initialise _tries to 5
        void newGame(int target);
        ///return target of the class Game
        int target() const{return _target;}
        ///check if the number is egal at _target , or too_low , too_high , if the last chance and is not egal . \n modified the resultat , 4 value possible (Win,Lose,Too_low,Too_high)
        void play(int number,bool lastChance);
        ///return the value of resultat
        Result get_resultat(){return resultat;}
        ///return true if _tries is not null and dicremented _tries , else return false
        bool remainingTries();
    private:
        ///target to find
        int _target;
        ///result of an test with your value
        Result resultat;
        /// remaining tries
        int _tries;
};



//int saisieJoueur();



