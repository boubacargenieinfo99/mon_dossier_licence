#include <iostream>
#include "vector"
#include "machine.hpp"


class Zone{
public:
    std::vector<Machine>& addMachine(const Machine& machine)const;
private:
    std::vector<Machine> machines;
};