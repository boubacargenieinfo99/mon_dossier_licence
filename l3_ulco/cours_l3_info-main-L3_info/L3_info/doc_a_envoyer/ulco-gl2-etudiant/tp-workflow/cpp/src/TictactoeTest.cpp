#include "Tictactoe.hpp"
#include <sstream>

#include <catch2/catch.hpp>

TEST_CASE("test bidon") { 
    REQUIRE( true );
}

TEST_CASE("test affichage vide"){
    Jeu j;
    std::ostringstream mes;
    mes<<(j);
    std::string message;
    message = mes.str();
    REQUIRE("...\n...\n...\nrouge joue\n" == message);
}

TEST_CASE("test jouer sans victoire ou égalité"){
    Jeu j;
    j.jouer(0,0);
    std::ostringstream mes;
    mes<<(j);
    std::string message;
    message = mes.str();
    REQUIRE("R..\n...\n...\nvert joue\n" == message);
    j.jouer(0,0);
    mes.str("");
    mes<<(j);
    message = mes.str();
    REQUIRE("R..\n...\n...\nvert joue\n" == message);
    j.jouer(1,1);
    mes.str("");
    mes<<(j);
    message = mes.str();
    REQUIRE("R..\n.V.\n...\nrouge joue\n" == message);
    j.jouer(0,1);
    j.jouer(0,2);
    j.jouer(1,0);
    j.jouer(1,2);
    mes.str("");
    mes<<(j);
    message = mes.str();
    REQUIRE("RRV\nRVV\n...\nrouge joue\n" == message);
}

TEST_CASE("test rouge gagne"){
    Jeu j;
    j.jouer(0,0);
    j.jouer(0,1);
    j.jouer(1,0);
    j.jouer(0,2);
    j.jouer(2,0);
    std::ostringstream mes;
    mes<<(j);
    std::string message;
    message = mes.str();
    REQUIRE("RVV\nR..\nR..\nrouge a gagné\n" == message);

    mes.str("");
    j.raz();
    j.jouer(0,1);
    j.jouer(0,0);
    j.jouer(1,1);
    j.jouer(0,2);
    j.jouer(2,1);
    mes<<(j);
    message = mes.str();
    REQUIRE("VRV\n.R.\n.R.\nrouge a gagné\n" == message);

    mes.str("");
    j.raz();
    j.jouer(0,2);
    j.jouer(0,0);
    j.jouer(1,2);
    j.jouer(0,1);
    j.jouer(2,2);
    mes<<(j);
    message = mes.str();
    REQUIRE("VVR\n..R\n..R\nrouge a gagné\n" == message);

    mes.str("");
    j.raz();
    j.jouer(0,0);
    j.jouer(1,0);
    j.jouer(0,1);
    j.jouer(1,1);
    j.jouer(0,2);
    mes<<(j);
    message = mes.str();
    REQUIRE("RRR\nVV.\n...\nrouge a gagné\n" == message);

    mes.str("");
    j.raz();
    j.jouer(1,0);
    j.jouer(0,0);
    j.jouer(1,1);
    j.jouer(0,1);
    j.jouer(1,2);
    mes<<(j);
    message = mes.str();
    REQUIRE("VV.\nRRR\n...\nrouge a gagné\n" == message);

    mes.str("");
    j.raz();
    j.jouer(2,0);
    j.jouer(0,0);
    j.jouer(2,1);
    j.jouer(0,1);
    j.jouer(2,2);
    mes<<(j);
    message = mes.str();
    REQUIRE("VV.\n...\nRRR\nrouge a gagné\n" == message);

    mes.str("");
    j.raz();
    j.jouer(0,0);
    j.jouer(0,1);
    j.jouer(1,1);
    j.jouer(0,2);
    j.jouer(2,2);
    mes<<(j);
    message = mes.str();
    REQUIRE("RVV\n.R.\n..R\nrouge a gagné\n" == message);

    mes.str("");
    j.raz();
    j.jouer(2,0);
    j.jouer(0,1);
    j.jouer(1,1);
    j.jouer(2,1);
    j.jouer(0,2);
    mes<<(j);
    message = mes.str();
    REQUIRE(".VR\n.R.\nRV.\nrouge a gagné\n" == message);
}

TEST_CASE("test vert gagne"){
    Jeu j;
    j.jouer(0,1);
    j.jouer(0,0);
    j.jouer(0,2);
    j.jouer(1,0);
    j.jouer(1,1);
    j.jouer(2,0);
    std::ostringstream mes;
    mes<<(j);
    std::string message;
    message = mes.str();
    REQUIRE("VRR\nVR.\nV..\nvert a gagné\n" == message);
}

