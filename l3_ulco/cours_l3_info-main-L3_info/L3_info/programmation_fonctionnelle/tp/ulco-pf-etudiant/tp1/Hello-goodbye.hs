import System.IO

main :: IO()
main = do
    
    putStr "What's your name: "
    hFlush stdout
    nom <- getLine
    if nom == ""
        then  putStrLn "Goodbye!"
        else do
            putStrLn ("Hello " ++ nom ++ "!")
            main