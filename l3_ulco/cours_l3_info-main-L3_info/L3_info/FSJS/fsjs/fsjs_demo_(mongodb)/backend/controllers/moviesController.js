const Movie = require("../models/Movie");

//definitiondu filtre pour trouver le film a supprimer
const filter = {
    title:'House'
}
    
//sauvegarde dans sa collection
/*moviee.save()
    .then((moviee)=>{
        console.log(moviee) // affiche le movie cree dans la console
    })
    .catch(error=>res.status(900).send(error));*/

module.exports.list = (req,res)=>{
    Movie.find() //recupere tous les Movie
    .sort({title:"asc"}) //trie par 'title' ascendant
    .then(movies => {
        res.render('pages/moviesList',{movies});
    })
    .catch(error => res.status(400).send(error))
}

module.exports.create = (req,res)=>{
    const movieData = {
        //NB: le champ _id est automatiquement cree
        title:'House',
        //synopsis : non requis, il prendra la valeur par defaut
    }

    Movie.create(movieData)
    .then((movie)=>{
        console.log(movie); //affiche le movie cree dans la console
        res.redirect('/movies');
    })
    .catch(error => res.status(500).send(error));
}



module.exports.read = (req,res)=>{
    Movie.findOne(filter)
    .then((movie)=>{
        console.log('test',movie);
        res.render('pages/presentMovie',{movie});
    })
    .catch(error=>res.status(600).send(error));
}

module.exports.update = (req,res)=>{
    const movieDat = {
        synopsis:'Une super histoire qui fait peur ... et rire !'
    };

    //selection du document et mise a jour avec les nouvelles donnee
    const updateMovies = Movie.findOneAndUpdate(
        filter,
        movieDat,
        {new:true}// pour que .then recupere le movie modifier
    ).then((movie)=>{
        console.log(movie);
        res.redirect('/movies');
    })
    .catch(error=>res.status(700).send(error));
}

module.exports.delete = (req,res)=>{
    const deleteMovies = Movie.findOneAndDelete(filter)
    .then((filter)=>{
        console.log('movies delete');
        res.redirect('/movies');
    })
    .catch(error=>res.status(800).send(error));
}

