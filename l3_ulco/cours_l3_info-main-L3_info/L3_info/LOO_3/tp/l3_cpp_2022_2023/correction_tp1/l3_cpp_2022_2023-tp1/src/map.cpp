#include "map.hpp"

#include <iostream>
#include <cstring>

Map::Map(unsigned int link_number, unsigned int node_number) :
  link_number(link_number), node_number(node_number),
  links(link_number == 0 ? nullptr : new LinkPtr[link_number]),
  nodes(node_number == 0 ? nullptr : new NodePtr[node_number]),
  input_index(0) {
  for (unsigned int i = 0; i < link_number; ++i) {
    links[i] = nullptr;
  }
  for (unsigned int i = 0; i < node_number; ++i) {
    nodes[i] = nullptr;
  }
}

Map::~Map() {
  for (unsigned int i = 0; i < link_number; ++i) {
    if (links[i] != nullptr) {
      delete links[i];
    }
  }
  for (unsigned int i = 0; i < node_number; ++i) {
    if (nodes[i] != nullptr) {
      delete nodes[i];
    }
  }
  delete[] links;
  delete[] nodes;
}

void Map::add_link(Link *link) {
  unsigned int index = 0;

  while (links[index] != nullptr && index < link_number) { ++index; }
  if (index < link_number) {
    links[index] = link;
  }
}

void Map::add_node(Node *node) {
  unsigned int index = 0;

  while (nodes[index] != nullptr && index < node_number) { ++index; }
  if (index < node_number) {
    nodes[index] = node;
  }
}

int Map::find_node_index(const char *node_id) const {
  unsigned int index = 0;

  while (index < node_number and strcmp(nodes[index]->id, node_id) != 0) { ++index; }
  return index < node_number ? (int) index : -1;
}

void Map::run(unsigned int time) {
  run_link(time);
  run_node(time);
}

void Map::run(unsigned int time, const Inputs &inputs) {
  while (input_index < inputs.vehicle_number and inputs.times[input_index] == time) {
    int node_index = find_node_index(inputs.node_ids[input_index]);

    if (node_index != -1) {
      nodes[node_index]->add_vehicle(inputs.vehicles[input_index], time);
    }
    ++input_index;
  }
  run_link(time);
  run_node(time);
}

void Map::run_link(unsigned int time) {
  for (unsigned int i = 0; i < link_number; ++i) {
    Link *link = links[i];

    if (link != nullptr) {
      Vehicle *out_vehicle = link->run(time);

      // has a vehicle left the link?
      if (out_vehicle != nullptr) {
        // is there a next node?
        if (link->out_node != nullptr) {
          link->out_node->add_vehicle(out_vehicle, time);
        }
      }
    }
  }
}

void Map::run_node(unsigned int time) {
  for (unsigned int i = 0; i < node_number; ++i) {
    Node *node = nodes[i];

    if (node != nullptr) {
      // if a vehicle is present and this vehicle is present until the transit duration
      if (node->vehicle != nullptr && node->vehicle->last_time + node->duration <= time) {
        std::cout << time << ": " << node->vehicle->id << " exits " << node->id << std::endl;
        // the vehicle goes to the first out link (feature: use path)
        if (node->out_number != 0) {
          node->out_links[0]->add_vehicle(node->vehicle, time);
        }
        // now, no vehicle is present in node
        node->vehicle = nullptr;
      }
    }
  }
}