"use strict";

const GameType = {GUI: 0, ONLINE: 1, OFFLINE: 2, LOCAL_AI: 3, REMOTE_AI: 4};

export default {
    GameType: GameType
};