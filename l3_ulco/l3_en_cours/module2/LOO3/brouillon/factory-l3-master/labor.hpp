#include <string>
#include <vector>
#include <memory>

class Factory;
class Zone;
class Machine;
class Transporter;

class Labor {
public:
    Labor(std::string id, std::shared_ptr<Factory> factory) : id(id), factory(factory) {}


    void assignToZone(std::shared_ptr<Zone> zone) {
        zones.push_back(zone);
    }

    void assignToMachine(std::shared_ptr<Machine> machine) {
        machines.push_back(machine);
    }

    void assignToTransporter(std::shared_ptr<Transporter> transporter) {
        transporters.push_back(transporter);
    }

    std::string getId() const {
        return id;
    }

private:
    std::string id;
    std::shared_ptr<Factory> factory;
    std::vector<std::shared_ptr<Zone>> zones;
    std::vector<std::shared_ptr<Machine>> machines;
    std::vector<std::shared_ptr<Transporter>> transporters;
};
