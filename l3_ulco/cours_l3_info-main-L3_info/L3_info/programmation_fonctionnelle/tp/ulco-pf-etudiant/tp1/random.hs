import System.Random


main :: IO()
main = do
    n <- randomRIO (0, 100::Int)
    print (n)