#include <todolist-cpp/views.hpp>
#include <todolist-cpp/todolist-cpp.hpp>

std::string View::fmtTask(Task tache){
    std::string msg = tache.desc;
    return msg;
}

std::string View::fmtBoard(Board liste){
    std::string message = "Todo:\n";
    std::vector<Task> vec;
    std::vector<Task>::iterator it;
    vec = liste.getTodo();
    for(it = vec.begin();it != vec.end();it++){
        message += (*it).desc;
        message += "\n";
    }
    message += "\nDone:\n";
    vec = liste.getDone();
    for(it = vec.begin();it != vec.end();it++){
        message += (*it).desc;
        message += "\n";
    }
    return message;
}