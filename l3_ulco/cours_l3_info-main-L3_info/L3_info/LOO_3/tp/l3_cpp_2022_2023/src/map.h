#ifndef TRAFFIC_MAP_H
#define TRAFFIC_MAP_H

#include "link.h"
#include "node.h"

struct Map {
  unsigned int link_number;
  unsigned int node_number;
  struct Link **links;
  struct Node **nodes;
};

struct Map *create_map(unsigned int link_number, unsigned int node_number);

void destroy_map(struct Map *map);

void add_link_to_map(struct Link *link, struct Map *map);

void add_node_to_map(struct Node *node, struct Map *map);

void run_map(struct Map* map, unsigned int time);

#endif //TRAFFIC_MAP_H
