/**
 * @file openxum/core/common/abstract_player.hpp
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2011-2024 Openxum Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPENXUM_CORE_COMMON_ABSTRACT_PLAYER_HPP
#define OPENXUM_CORE_COMMON_ABSTRACT_PLAYER_HPP

#include <nlohmann/json.hpp>

#include <openxum/core/common/abstract_engine.hpp>

namespace openxum::core::common {

class AbstractPlayer {
public:
  AbstractPlayer(int color, int opponent_color, AbstractEngine *e)
    : _engine(e), _color(color), _opponent_color(opponent_color), _level(0) {}

  virtual ~AbstractPlayer() = default;

  void move(const nlohmann::json &move) {
    _engine->move(move);
  }

  int color() const { return _color; }

  int opponent_color() const { return _opponent_color; }

  int level() const { return _level; }

  void level(int level) { _level = level; }

  virtual nlohmann::json move() = 0;

protected:
  std::unique_ptr<AbstractEngine> _engine;

private:
  int _color;
  int _opponent_color;
  int _level;
};

}

#endif