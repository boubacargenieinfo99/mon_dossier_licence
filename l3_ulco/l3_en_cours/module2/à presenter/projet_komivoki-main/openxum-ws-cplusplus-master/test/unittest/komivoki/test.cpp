#define CATCH_CONFIG_MAIN

#include "../catch.hpp"

#include <openxum/core/games/komivoki/engine.hpp>
#include <openxum/core/games/komivoki/game_type.hpp>
#include <openxum/core/games/komivoki/decision.hpp>

using namespace openxum::core::games::komivoki;

TEST_CASE("default coordinates", "komivoki")
{
  openxum::core::games::komivoki::Coordinates coordinates;

  REQUIRE(coordinates.x() == -1);
  REQUIRE(coordinates.y() == -1);
}

TEST_CASE("invalid coordinates", "komivoki")
{
  openxum::core::games::komivoki::Coordinates coordinates;

  REQUIRE(not coordinates.is_valid());
}

TEST_CASE("valid coordinates", "komivoki")
{
  openxum::core::games::komivoki::Coordinates coordinates(3, 6);

  REQUIRE(coordinates.is_valid());
}

TEST_CASE("coordinates to string", "komivoki")
{
  openxum::core::games::komivoki::Coordinates coordinates(3, 6);

  REQUIRE(coordinates.to_string() == "d7");
}

TEST_CASE("color BLACK", "komivoki")
{
  openxum::core::games::komivoki::Color color = openxum::core::games::komivoki::BLACK;

  REQUIRE(color == openxum::core::games::komivoki::BLACK);
}

TEST_CASE("color WHITE", "komivoki")
{
  openxum::core::games::komivoki::Color color = openxum::core::games::komivoki::WHITE;

  REQUIRE(color == openxum::core::games::komivoki::WHITE);
}

TEST_CASE("default color", "komivoki")
{
  openxum::core::games::komivoki::Color color = openxum::core::games::komivoki::NONE;

  REQUIRE(color == openxum::core::games::komivoki::NONE);
}

TEST_CASE("Decision encoding and decoding", "[komivoki][decision]")
{
  Decision decision(DecisionType::MOVE, Coordinates(1, 2), Coordinates(3, 4));
  std::string encoded = decision.encode();

  SECTION("Encoding is correct")
  {
    REQUIRE(encoded == "Mb3d5");
  }

  SECTION("Decoding is correct")
  {
    Decision decodedDecision;
    decodedDecision.decode(encoded);
    REQUIRE(decodedDecision.type() == DecisionType::MOVE);
    REQUIRE(decodedDecision.from().x() == 1);
    REQUIRE(decodedDecision.from().y() == 2);
    REQUIRE(decodedDecision.to().x() == 3);
    REQUIRE(decodedDecision.to().x() == 3);
  }
}

TEST_CASE("Decision to string", "[komivoki][decision]")
{
  Decision moveDecision(DecisionType::MOVE, Coordinates(1, 2), Coordinates(3, 4));
  Decision passDecision(DecisionType::PASS, Coordinates(), Coordinates());

  SECTION("Move decision to string")
  {
    REQUIRE(moveDecision.to_string() == "move entity from b3 to d5");
  }

  SECTION("Pass decision to string")
  {
    REQUIRE(passDecision.to_string() == "pass");
  }
}

TEST_CASE("Engine constructor", "[komivoki][engine]")
{
  SECTION("Constructor initializes engine with correct color and type")
  {
    Engine engine(0, Color::BLACK);

    REQUIRE(engine.current_color() == Color::BLACK);

    REQUIRE(engine.type() == 0);
  }
}

TEST_CASE("Engine Initialization", "[Engine]")
{
  SECTION("8x8 Board Initialization")
  {
    FiveFiveBoardInitializer initializer;
    Engine engine(0, Color::BLACK, {8, 8}, 2, initializer);

    REQUIRE(engine.current_color() == Color::BLACK);
    REQUIRE(engine.is_finished() == false);
    REQUIRE(engine.get_possible_move_list().empty() == false);
  }

  SECTION("8x8 Board Initialization")
  {
    EightEightBoardInitializer initializer;
    Engine engine(0, Color::BLACK, {8, 8}, 2, initializer);

    REQUIRE(engine.current_color() == Color::BLACK);
    REQUIRE(engine.is_finished() == false);
    REQUIRE(engine.get_possible_move_list().empty() == false);
  }
}

TEST_CASE("Engine Game Status", "[Engine]")
{
  FourFourBoardInitializer initializer;
  Engine engine(0, Color::BLACK, {8, 8}, 2, initializer);
  Decision passDecision(DecisionType::PASS, Coordinates(), Coordinates());

  REQUIRE_FALSE(engine.is_finished());
  if (passDecision.to_string() == "pass")
  {
    engine.move(passDecision);
    if (engine.is_finished() && engine.winner_is())
    {
      auto possibleMoves = engine.get_possible_move_list();
      REQUIRE(possibleMoves.empty());
    }
  }
}

TEST_CASE("Entity Capturing", "[Engine]")
{
  FourFourBoardInitializer initializer;
  Engine engine(1, 0, {8, 8}, 2, initializer);

  Decision moveDecision(DecisionType::MOVE, Coordinates(1, 0), Coordinates(1, 1));
  engine.move(moveDecision);
  auto possibleMoves = engine.get_possible_move_list();
  REQUIRE(possibleMoves.empty() == false);
}

TEST_CASE("Largest stack captures weakest entity, only one capture per round", "[Engine]")
{
  FiveFiveBoardInitializer initializer;
  Engine engine(1, 0, {8, 8}, 2, initializer);

  Decision moveDecision1(DecisionType::MOVE, Coordinates(2, 0), Coordinates(2, 1));
  engine.move(moveDecision1);
  Decision moveDecision2(DecisionType::MOVE, Coordinates(1, 0), Coordinates(2, 1));
  engine.move(moveDecision2);

  Decision moveDecision3(DecisionType::MOVE, Coordinates(1, 4), Coordinates(2, 3));
  engine.move(moveDecision3);

  Decision moveDecision4(DecisionType::MOVE, Coordinates(2, 1), Coordinates(2, 3));
  engine.move(moveDecision4);

  openxum::core::games::komivoki::Entity *blackEntity = new openxum::core::games::komivoki::Entity(Color::BLACK, 1);

  engine.put_entity(Coordinates(2, 3), blackEntity);
  REQUIRE(engine.get_capture_count() == 0);
}
