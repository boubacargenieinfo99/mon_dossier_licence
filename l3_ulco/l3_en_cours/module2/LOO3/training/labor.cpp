#include "labor.hpp"
#include "factory.hpp"
#include <iostream>

Labor::Labor(const std::string& id, const std::shared_ptr<Factory>& factory)
        : UnavailableElement(id), factory(factory) {}

void Labor::attachToZone(const std::string& zoneId) {
    attachedZoneId = zoneId;
}

void Labor::attachToMachine(const std::string& machineId) {
    attachedMachineId = machineId;
}

void Labor::attachToTransportation(const std::string& transportationId) {
    attachedTransportationId = transportationId;
}

void Labor::displayInfo() const {
    std::cout << "Labor - ID: " << getId() << std::endl;
    std::cout << "Attached Zone ID: " << attachedZoneId << std::endl;
    std::cout << "Attached Machine ID: " << attachedMachineId << std::endl;
    std::cout << "Attached Transportation ID: " << attachedTransportationId << std::endl;
}