#pragma once
#include <iostream>
#include <vector>

struct Task{
    int id;
    std::string desc;
};

class Board{
    public:
        Board():nextId(1){}

        ~Board(){}

        const std::vector<Task>& getTodo() const{return Todo;}

        const std::vector<Task>& getDone() const{return Done;}

        void addTask(std::string description);



    private:
        std::vector<Task> Todo;
        std::vector<Task> Done;
        int nextId;
};

