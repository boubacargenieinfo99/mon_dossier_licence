from ply.lex import lex
from ply.yacc import yacc
import sys

global lineNumber

if __name__ == "__main__":
    lineNumber = 0


    def t_ignore_NEWLINE(t):
        r"""\n"""
        t.lexer.lineno += 1


    def t_error(t):
        print(f'Illegal character {t.value[0]!r}')
        t.lexer.skip(1)


    ### TOKENS ###
    tokens = ("NON_TERMINAUX","TOKENS","TERMINAUX")

    #t_NON_TERMINAUX = r"E" | r"E'" | r"F" | r"F'" | r"G"
    t_TERMINAUX = r'$'

    def t_NON_TERMINAUX(t):
        r"""[A-Z]'?"""
        # instruction pour traitement
        t.value = t.value + '$'
        return t

    #def t_TERMINAUX(t):
    #    r"""[a-z]+"""
    #    # instruction pour traitement
    #    t.value = float(t.value)
    #    return t

    def t_TOKENS(t):
        r"""[a-z]+""" | r"\+" | r"\*"    #r"id" | r"\+" | r"\*"
        # instruction pour traitement
        t.value = t.value + '$'
        return t



    ######################

    # decommenter la ligne du dessous une fois les tokens créés
    lexer = lex()


    ### GRAMMAR ###

    def p_regle_assignation_variable(p):
        """
        regle_assignation_variable : VARIABLE EGAL regle_binop
                                     | regle_binop
        """
        if len(p) == 2:
            p[0] = p[1]
        if len(p) == 4:
            # p[1][1] = p[3]
            p[0] = ('variable', (p[1])[0], p[3])  # (p[1])[1])


    def p_regle_plus_ou_moins(p):
        """
        regle_binop : regle_mul_div PLUS regle_binop
                       | regle_mul_div MOINS regle_binop
                       | regle_mul_div
        """
        if len(p) == 2:
            p[0] = p[1]
        else:
            p[0] = ('binop', p[2], p[1], p[3])
        print(p[0])


    def p_regle_operation_prioritaire(p):
        """
        regle_mul_div : regle_nb MUL regle_mul_div
                        | regle_nb DIV regle_mul_div
                        | regle_nb
                        | regle_neg MUL regle_mul_div
                        | regle_neg DIV regle_mul_div
                        | regle_neg
                        | regle_parenthese MUL regle_mul_div
                        | regle_parenthese DIV regle_mul_div
                        | regle_parenthese
        """
        if len(p) == 2:
            p[0] = p[1]
        else:
            p[0] = ("mul_div", p[2], p[1], p[3])
        print(p[0])


    def p_regle_parenthese(p):
        """
        regle_parenthese : PARENTHESE_GAUCHE regle_binop PARENTHESE_DROITE
        """
        p[0] = ("entre_parenthese", p[2])


    def p_regle_nb(p):
        """
        regle_nb : NB
                    | MOINS MOINS NB
        """
        if len(p) == 2:
            p[0] = ('number', p[1])
        else:
            if p[1] == '-':
                p[0] = ('number', p[3])
            else:
                p[0] = ('number', p[1] + 0.1 * p[3])
        print("1", p[0])


    def p_regle_nb_neg(p):
        """
        regle_neg : MOINS NB
        """
        if len(p) == 3:
            p[2] = p[2] * (-1)
            p[0] = ("number", p[2])
        else:
            print(p[2], p[4])
            p[0] = ('number', (-1) * (p[2] + 0.1 * p[4]))
        print(p[0])


    ########################

    def p_error(p):
        if hasattr(p, "value"):
            print(f'Syntax error at {p.value!r}')
        else:
            print("Error", p)


    ########################

    def partitionnement(texte):
        ligne = texte.split("\n")
        dicos = {}

        compteur = 0
        for x in ligne:
            part_un = (x.split(":"))[0]
            part_deux = (x.split(":"))[1]
            p_un = part_un.split()
            p_deux = part_deux.split()
            dicos[compteur] = (p_un, p_deux)
            compteur = compteur + 1
        return dicos

    # decommenter les lignes parser, ast et print pour les grammaires
    parser = yacc()

    file = open(sys.argv[1], "r")
    program = file.read()
    print(program)

    dico = partitionnement(program)

    for x in range(len(dico)):
        print(dico[x])

    ast = parser.parse(program)
    print(ast)
