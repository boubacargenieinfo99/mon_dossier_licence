
#ifndef TRAINING_MACHINE_HPP
#define TRAINING_MACHINE_HPP
#include <memory>
#include <vector>
#include "spatialElement.hpp"
#include "labor.hpp"
#include "buffer.hpp"
#include "unavailableElement.hpp"
#include "conveyor.hpp"

class Machine : public SpatialElement, public UnavailableElement {
public:
    Machine(std::string id, int startupTime, int operationTime);
    void addLabor(std::shared_ptr<Labor> labor);

private:
    std::vector<std::shared_ptr<Labor>> labors;
    std::shared_ptr<Buffer> downstreamBuffer;
    std::shared_ptr<Buffer> upstreamBuffer;
    std::shared_ptr<Conveyor> downstreamConveyor;
    std::shared_ptr<Conveyor> upstreamConveyor;

};

#endif //TRAINING_MACHINE_HPP
