

// fonction permettant la création d'un carré
// c : côté du carré

function carre(c){

    const geometry = new THREE.BufferGeometry();

    // sommets des deux triangles du carré dans le plan 0xy, centré à l'origine
    const vertices = new Float32Array( [
        -c/2, -c/2, 0.0,
        c/2, -c/2, 0.0,
        c/2, c/2, 0.0,

        -c/2, -c/2, 0.0,
        c/2, c/2, 0.0,
        -c/2, c/2, 0.0
    ] );


    // assignation des sommets à la géométrie
    geometry.setAttribute( 'position', new THREE.Float32BufferAttribute( vertices, 3 ) );

    // définition du matériel
    const diffus = new THREE.MeshBasicMaterial( {
        color: 0xffffff, side: THREE.DoubleSide });

    // construction du mesh
    const mesh = new THREE.Mesh( geometry, diffus );
    return mesh;
}

function carreTexture(c,texture){

    const geometry = new THREE.BufferGeometry();

    // sommets des deux triangles du carré dans le plan 0xy, centré à l'origine
    const vertices = new Float32Array( [
        -c/2, -c/2, 0.0,
        c/2, -c/2, 0.0,
        c/2, c/2, 0.0,

        -c/2, -c/2, 0.0,
        c/2, c/2, 0.0,
        -c/2, c/2, 0.0
    ] );

    //textures
    const uvs = new Float32Array([
        0, 0,
        1, 0,
        1, 1,

        0, 0,
        1, 1,
        0, 1
    ]);

    // assignation des sommets à la géométrie
    geometry.setAttribute( 'position', new THREE.Float32BufferAttribute( vertices, 3 ) );

    geometry.setAttribute('uv',new THREE.Float32BufferAttribute(uvs,2));

    // définition du matériel
    const diffus = new THREE.MeshBasicMaterial( {
        map:texture, side: THREE.DoubleSide });

    // construction du mesh
    const mesh = new THREE.Mesh( geometry, diffus );
    return mesh;
}

function carreTextureZeroCinq(c,texture){

    const geometry = new THREE.BufferGeometry();

    // sommets des deux triangles du carré dans le plan 0xy, centré à l'origine
    const vertices = new Float32Array( [
        -c/2, -c/2, 0.0,
        c/2, -c/2, 0.0,
        c/2, c/2, 0.0,

        -c/2, -c/2, 0.0,
        c/2, c/2, 0.0,
        -c/2, c/2, 0.0
    ] );

    //textures
    const uvs = new Float32Array([
        0, 0,
        0.5, 0,
        0.5, 1,

        0, 0,
        0.5, 1,
        0, 1
    ]);

    // assignation des sommets à la géométrie
    geometry.setAttribute( 'position', new THREE.Float32BufferAttribute( vertices, 3 ) );

    geometry.setAttribute('uv',new THREE.Float32BufferAttribute(uvs,2));

    // définition du matériel
    const diffus = new THREE.MeshBasicMaterial( {
        map:texture, side: THREE.DoubleSide });

    // construction du mesh
    const mesh = new THREE.Mesh( geometry, diffus );
    return mesh;
}

function carreTextureDeux(c,texture){

    const geometry = new THREE.BufferGeometry();

    // sommets des deux triangles du carré dans le plan 0xy, centré à l'origine
    const vertices = new Float32Array( [
        -c/2, -c/2, 0.0,
        c/2, -c/2, 0.0,
        c/2, c/2, 0.0,

        -c/2, -c/2, 0.0,
        c/2, c/2, 0.0,
        -c/2, c/2, 0.0
    ] );

    //textures
    const uvs = new Float32Array([
        0, 0,
        2, 0,
        2, 1,

        0, 0,
        2, 1,
        0, 1
    ]);

    // assignation des sommets à la géométrie
    geometry.setAttribute( 'position', new THREE.Float32BufferAttribute( vertices, 3 ) );

    geometry.setAttribute('uv',new THREE.Float32BufferAttribute(uvs,2));

    // définition du matériel
    const diffus = new THREE.MeshBasicMaterial( {
        map:texture, side: THREE.DoubleSide });

    // construction du mesh
    const mesh = new THREE.Mesh( geometry, diffus );
    return mesh;
}

function carreTextureQuad(c,texture){

    const geometry = new THREE.BufferGeometry();

    // sommets des deux triangles du carré dans le plan 0xy, centré à l'origine
    const vertices = new Float32Array( [
        -c/2, -c/2, 0.0,
        c/2, -c/2, 0.0,
        c/2, c/2, 0.0,

        -c/2, -c/2, 0.0,
        c/2, c/2, 0.0,
        -c/2, c/2, 0.0
    ] );

    //textures
    const uvs = new Float32Array([
        0, 0,
        2, 0,
        2, 2,

        0, 0,
        2, 2,
        0, 2
    ]);

    // assignation des sommets à la géométrie
    geometry.setAttribute( 'position', new THREE.Float32BufferAttribute( vertices, 3 ) );

    geometry.setAttribute('uv',new THREE.Float32BufferAttribute(uvs,2));

    // définition du matériel
    const diffus = new THREE.MeshBasicMaterial( {
        map:texture, side: THREE.DoubleSide });

    // construction du mesh
    const mesh = new THREE.Mesh( geometry, diffus );
    return mesh;
}

function carreTexture2(c,texture,nbRepeat){

    const geometry = new THREE.BufferGeometry();

    // sommets des deux triangles du carré dans le plan 0xy, centré à l'origine
    const vertices = new Float32Array( [
        -c/2, -c/2, 0.0,
        c/2, -c/2, 0.0,
        c/2, c/2, 0.0,

        -c/2, -c/2, 0.0,
        c/2, c/2, 0.0,
        -c/2, c/2, 0.0
    ] );

    //textures
    const uvs = new Float32Array([
        0, 0,
        nbRepeat, 0,
        nbRepeat, nbRepeat,

        0, 0,
        nbRepeat, nbRepeat,
        0, nbRepeat
    ]);

    // assignation des sommets à la géométrie
    geometry.setAttribute( 'position', new THREE.Float32BufferAttribute( vertices, 3 ) );

    geometry.setAttribute('uv',new THREE.Float32BufferAttribute(uvs,2));

    // définition du matériel
    const diffus = new THREE.MeshBasicMaterial( {
        map:texture, side: THREE.DoubleSide });

    // construction du mesh
    const mesh = new THREE.Mesh( geometry, diffus );
    return mesh;
}

function cubeTexture(c,texture1,texture2,texture3){
    // part 1

    const geometry = new THREE.BufferGeometry();

    // sommets des deux triangles du carré dans le plan 0xy, centré à l'origine
    const vertices = new Float32Array( [
        -c/2, -c/2, -c/2,
        c/2, -c/2, -c/2,
        c/2, c/2, -c/2,

        -c/2, -c/2, -c/2,
        c/2, c/2, -c/2,
        -c/2, c/2, -c/2,

        //deuxieme carre droite


        c/2, -c/2, -c/2,
        c/2, -c/2, c/2,
        c/2, c/2, c/2,

        //deuxieme triangle
        c/2, -c/2, -c/2,
        c/2, c/2, c/2,
        c/2, c/2, -c/2,

        //troisieme triangle

        c/2, -c/2, c/2,
        -c/2, -c/2, c/2,
        -c/2, c/2, c/2,

        //

        c/2, -c/2, c/2,
        -c/2, c/2, c/2,
        c/2, c/2, c/2,

        //


        -c/2, -c/2, c/2,
        -c/2, -c/2, -c/2,
        -c/2, c/2, -c/2,

        //

        -c/2, -c/2, c/2,
        -c/2, c/2, -c/2,
        -c/2, c/2, c/2
    ] );

    //textures
    const uvs = new Float32Array([
        0, 0,
        0.25, 0,
        0.25, 1,

        0, 0,
        0.25, 1,
        0, 1,

        //troisieme

        0.25, 0,
        0.5, 0,
        0.5, 1,

        0.25, 0,
        0.5, 1,
        0.25, 1,

        //quatrieme
        0.5, 0,
        0.75, 0,
        0.75, 1,

        0.5, 0,
        0.75, 1,
        0.5, 1,

        //

        0.75, 0,
        1, 0,
        1, 1,

        0.75, 0,
        1, 1,
        0.75, 1
    ]);

    // assignation des sommets à la géométrie
    geometry.setAttribute( 'position', new THREE.Float32BufferAttribute( vertices, 3 ) );

    geometry.setAttribute('uv',new THREE.Float32BufferAttribute(uvs,2));

    // définition du matériel
    const diffus = new THREE.MeshBasicMaterial( {
        map:texture1, side: THREE.DoubleSide });

    // construction du mesh
    const mesh = new THREE.Mesh( geometry, diffus );


    //part 2

    const geometryPart2 = new THREE.BufferGeometry();

    // sommets des deux triangles du carré dans le plan 0xy, centré à l'origine
    const verticesPart2 = new Float32Array( [
        -c/2, c/2, -c/2,
        c/2, c/2, -c/2,
        c/2, c/2, c/2,

        -c/2, c/2, -c/2,
        c/2, c/2, c/2,
        -c/2, c/2, c/2
    ] );

    //textures
    const uvsPart2 = new Float32Array([
        0, 0,
        1, 0,
        1, 1,

        0, 0,
        1, 1,
        0, 1
    ]);

    // assignation des sommets à la géométrie
    geometryPart2.setAttribute( 'position', new THREE.Float32BufferAttribute( verticesPart2, 3 ) );

    geometryPart2.setAttribute('uv',new THREE.Float32BufferAttribute(uvsPart2,2));

    // définition du matériel
    const diffusPart2 = new THREE.MeshBasicMaterial( {
        map:texture2, side: THREE.DoubleSide });

    // construction du mesh
    const meshPart2 = new THREE.Mesh( geometryPart2, diffusPart2 );


    // part 3

    const geometryPart3 = new THREE.BufferGeometry();

    // sommets des deux triangles du carré dans le plan 0xy, centré à l'origine
    const verticesPart3 = new Float32Array( [
        -c/2, -c/2, -c/2,
        c/2, -c/2, -c/2,
        c/2, -c/2, c/2,

        -c/2, -c/2, -c/2,
        c/2, -c/2, c/2,
        -c/2, -c/2, c/2
    ] );

    //textures
    const uvsPart3 = new Float32Array([
        0, 0,
        1, 0,
        1, 1,

        0, 0,
        1, 1,
        0, 1
    ]);

    // assignation des sommets à la géométrie
    geometryPart3.setAttribute( 'position', new THREE.Float32BufferAttribute( verticesPart3, 3 ) );

    geometryPart3.setAttribute('uv',new THREE.Float32BufferAttribute(uvsPart3,2));

    // définition du matériel
    const diffusPart3 = new THREE.MeshBasicMaterial( {
        map:texture3, side: THREE.DoubleSide });

    // construction du mesh
    const meshPart3 = new THREE.Mesh( geometryPart3, diffusPart3 );


    const groupCube = new THREE.Group();
    groupCube.add(mesh);
    groupCube.add(meshPart2);
    groupCube.add(meshPart3);

    return groupCube;
}