n = 1000;
m = 5;
a = 2;

resultats = simulationLoiNormale(n, m, a);

% Afficher un histogramme des réalisations
figure;
hist(resultats, 50);
title('Histogramme de variables aléatoires normales N(5, 2)');
xlabel('Valeurs');
ylabel('Fréquence');