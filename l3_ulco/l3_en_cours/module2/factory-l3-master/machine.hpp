
#ifndef TP4_MACHINE_HPP
#define TP4_MACHINE_HPP
#include <iostream>
#include <vector>
using namespace std;
class Machine: public SpatialElement, public UnavailableElement{
private:
    int setup_time;
    int process_time;
    vector<Labor>labors;
    Buffer& downstream_buffer;
    Buffer& upstream_buffer;
    Convoyeur& downstream_convoyeur;
    Convoyeur& upstream_convoyeur;
public:
Machine(int setup_time, int process_time);
};
#endif //TP4_MACHINE_HPP
