#ifndef TRAFFIC_NODE_HPP
#define TRAFFIC_NODE_HPP

#include <memory>
#include <string>
#include <valarray>

class Link;

class Vehicle;

class Node {
public:
  Node(const std::string &id, unsigned int duration, unsigned int in_number, unsigned int out_number);

  virtual ~Node();

  void attach_input_link(const std::shared_ptr<Link> &link);

  void attach_output_link(const std::shared_ptr<Link> &link);

  virtual void enter_vehicle(Vehicle *vehicle, unsigned int time) = 0;

  virtual void exit_vehicle(unsigned int time) = 0;

  const std::string &id() const { return _id; }

  unsigned int duration() const { return _duration; }

  unsigned int out_number() const { return _out_links.size(); }

  virtual bool is_full() const = 0;

  void put_vehicle_into_out_link(Vehicle *vehicle, unsigned int time);

private:
  std::string _id;
  unsigned int _duration;
  std::valarray<std::weak_ptr<Link>> _out_links;
  std::valarray<std::weak_ptr<Link>> _in_links;
};

#endif //TRAFFIC_NODE_HPP
