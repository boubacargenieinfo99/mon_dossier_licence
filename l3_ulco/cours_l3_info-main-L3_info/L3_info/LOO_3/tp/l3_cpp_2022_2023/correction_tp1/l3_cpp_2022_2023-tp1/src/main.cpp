#include "map.hpp"

Map *create_new_map() {
  Map *map = new Map(3, 4);
  Link *link_1 = new Link("L1", 500);
  Link *link_2 = new Link("L2", 500);
  Link *link_3 = new Link("L3", 500);

  Node *node_1 = new Node("N1", 2, 0, 1);
  Node *node_2 = new Node("N2", 2, 1, 1);
  Node *node_3 = new Node("N3", 2, 1, 1);
  Node *node_4 = new Node("N4", 2, 1, 0);

  map->add_link(link_1);
  map->add_link(link_2);
  map->add_link(link_3);

  map->add_node(node_1);
  map->add_node(node_2);
  map->add_node(node_3);
  map->add_node(node_4);

  node_1->attach_output_link(link_1);
  node_2->attach_input_link(link_1);
  node_2->attach_output_link(link_2);
  node_3->attach_input_link(link_2);
  node_3->attach_output_link(link_3);
  node_4->attach_input_link(link_3);
  return map;
}

void run(Map *map, const Inputs& inputs) {
  unsigned int time = 0;

  while (time < 10000) {
    map->run(time, inputs);
    ++time;
  }
}

int main() {
  Map *map = create_new_map();
  Inputs inputs(1);

  inputs.add_vehicle(new Vehicle("V1"), 0, "N1");

  run(map, inputs);

  delete map;
  return 0;
}