a = 5;  % paramètre de la loi de Poisson
n = 1000;  % nombre de réalisations

poissonVariable = generePoisson(a, n);

% Afficher un histogramme des réalisations
figure;
hist(poissonVariable, max(poissonVariable) - min(poissonVariable) + 1);
title('Histogramme de variables aléatoires de Poisson');
xlabel('Valeurs');
ylabel('Fréquence');