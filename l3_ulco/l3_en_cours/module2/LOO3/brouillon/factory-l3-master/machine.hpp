#ifndef MACHINE_HPP
#define MACHINE_HPP

#include <string>
#include <memory>
#include "spatialElement.hpp"
#include "buffer.hpp"

class Conveyor;

class Machine : public SpatialElement {
public:
    Machine(std::string nom, std::unique_ptr<Geometry> geometry, int setup_time, int process_time, std::unique_ptr<Buffer> downstreamBuff, std::unique_ptr<Buffer> upstreamBuff)
            : SpatialElement(nom, std::move(geometry)),
            nom(nom), setup_time(setup_time), process_time(process_time),
            downstreamBuffer(std::move(downstreamBuff)),
            upstreamBuffer(std::move(upstreamBuff)),
            downstreamConveyor(nullptr),
            upstreamConveyor(nullptr){}

    void attachDownstreamConveyor(std::shared_ptr<Conveyor>conveyor){
        downstreamConveyor=conveyor;
    }
    void attachUpstreamConveyor(std::shared_ptr<Conveyor>conveyor){
        upstreamConveyor=conveyor;
    }

    int getSetupTime() const {
        return setup_time;
    }
    int getProcessTime() const {
        return process_time;
    }

    std::shared_ptr<Conveyor> getDownstreamConveyor() const {
        return downstreamConveyor;
    }

    std::shared_ptr<Conveyor> getUpstreamConveyor() const {
        return upstreamConveyor;
    }

    std::string getId()const  {return  nom;}

private:
    int setup_time;
    int process_time;
    std::string nom;
    std::unique_ptr<Buffer>   downstreamBuffer;
    std::unique_ptr<Buffer>   upstreamBuffer;
    std::shared_ptr<Conveyor> downstreamConveyor;
    std::shared_ptr<Conveyor> upstreamConveyor;

};
#endif
