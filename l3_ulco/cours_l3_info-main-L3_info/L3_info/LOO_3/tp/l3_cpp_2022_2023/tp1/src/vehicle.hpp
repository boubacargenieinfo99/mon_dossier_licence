#ifndef TRAFFIC_VEHICLE_H
#define TRAFFIC_VEHICLE_H

struct Link;
struct Node;

struct Vehicle{
    //constructeur

    Vehicle(const char *id);

    //destructeur

    ~Vehicle();

    //attributs

    char *id;
    Link *link;
    Node *node;
    unsigned int last_time;

    //fonctions



};

#endif //TRAFFIC_VEHICLE_H