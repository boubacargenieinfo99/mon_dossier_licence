const express = require('express'); // inclusion d express

// instanciation d une application express
const app = express();

const path = require('path');

// Ajoute un middleware qui retourne les documents statiques situés sous le dossier /public.
// NB : il faut le mettre avant tout autres use qui modifie res 
// pour que le cas des fichiers static soit bien traité en 1er dans la chaine des middlewares.
app.use(express.static(path.join(__dirname,'public')));

app.set('view engine','ejs');// Definition du moteur de rendu
app.set('views',path.join(__dirname,'views'));// Declaration du dossier contenant les vues

// Importation de 'express_ejs_layouts' :
const expressLayouts = require('express-ejs-layouts');
// ajout du middleware :
app.use(expressLayouts);
//Definition du layout par default :
app.set('layout','../views/layouts/layout');

//middleware : ex. d affichage d informations dans la console
app.use((req,res,next) =>{
    const now = new Date().toDateString();
    console.log(`${now} : une requête ${req.method} est arrivée !`);
    next(); // l'appel a next() transmet les informations pour traitement dans le middleware suivant
});

// ROUTAGE
const homeRouter = require('./routes/homeRouter');
app.use('/',homeRouter);

const presentation = require('./routes/presentation');
app.use('/presentation',presentation);



const researchRouter = require('./routes/researchRouter');
app.use('/research',researchRouter);

const teachingRouter = require('./routes/teachingRouter');
app.use('/teaching',teachingRouter);


//route declenche pout toute requetes get non matchée précédement
// ATTENTION : l ordre des requetes est important
app.get("*",(req,res) =>{
    res.redirect('/');
})


//exportation de notre application express
module.exports = app;