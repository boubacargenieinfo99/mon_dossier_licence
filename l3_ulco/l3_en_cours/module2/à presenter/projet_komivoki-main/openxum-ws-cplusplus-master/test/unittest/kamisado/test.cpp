#define CATCH_CONFIG_MAIN

#include "../catch.hpp"

#include <openxum/core/games/kamisado/engine.hpp>
#include <openxum/core/games/kamisado/game_type.hpp>
#include <openxum/core/games/kamisado/decision.hpp>

using namespace openxum::core::games::kamisado;

static TowerColor::Values initial_colors[8][8] = {
    {TowerColor::ORANGE, TowerColor::BLUE, TowerColor::PURPLE, TowerColor::PINK, TowerColor::YELLOW,
     TowerColor::RED, TowerColor::GREEN, TowerColor::BROWN},
    {TowerColor::RED, TowerColor::ORANGE, TowerColor::PINK, TowerColor::GREEN, TowerColor::BLUE,
     TowerColor::YELLOW, TowerColor::BROWN, TowerColor::PURPLE},
    {TowerColor::GREEN, TowerColor::PINK, TowerColor::ORANGE, TowerColor::RED, TowerColor::PURPLE,
     TowerColor::BROWN, TowerColor::YELLOW, TowerColor::BLUE},
    {TowerColor::PINK, TowerColor::PURPLE, TowerColor::BLUE, TowerColor::ORANGE, TowerColor::BROWN,
     TowerColor::GREEN, TowerColor::RED, TowerColor::YELLOW},
    {TowerColor::YELLOW, TowerColor::RED, TowerColor::GREEN, TowerColor::BROWN, TowerColor::ORANGE,
     TowerColor::BLUE, TowerColor::PURPLE, TowerColor::PINK},
    {TowerColor::BLUE, TowerColor::YELLOW, TowerColor::BROWN, TowerColor::PURPLE, TowerColor::RED,
     TowerColor::ORANGE, TowerColor::PINK, TowerColor::GREEN},
    {TowerColor::PURPLE, TowerColor::BROWN, TowerColor::YELLOW, TowerColor::BLUE, TowerColor::GREEN,
     TowerColor::PINK, TowerColor::ORANGE, TowerColor::RED},
    {TowerColor::BROWN, TowerColor::GREEN, TowerColor::RED, TowerColor::YELLOW, TowerColor::PINK,
     TowerColor::PURPLE, TowerColor::BLUE, TowerColor::ORANGE}};

TEST_CASE("default coordinates", "kamisado")
{
  openxum::core::games::kamisado::Coordinates coordinates;

  REQUIRE(coordinates.x() == -1);
  REQUIRE(coordinates.y() == -1);
}

TEST_CASE("invalid coordinates", "kamisado")
{
  openxum::core::games::kamisado::Coordinates coordinates;

  REQUIRE(not coordinates.is_valid());
}

TEST_CASE("valid coordinates", "kamisado")
{
  openxum::core::games::kamisado::Coordinates coordinates(3, 6);

  REQUIRE(coordinates.is_valid());
}

TEST_CASE("coordinates to string", "kamisado")
{
  openxum::core::games::kamisado::Coordinates coordinates(3, 6);

  REQUIRE(coordinates.to_string() == "d7");
}
