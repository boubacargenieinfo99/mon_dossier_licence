#include <iostream>
#include "factory.hpp"
#include "polygon.hpp"
#include "cellular.hpp"
#include "conveyor.hpp"

int main() {
    // Création des polygones pour les zones
    auto polygonA = std::make_unique<Polygon>(std::initializer_list<Point>{Point(0, 0), Point(100, 0), Point(100, 200)});
    auto polygonB1 = std::make_unique<Polygon>(std::initializer_list<Point>{Point(100, 200), Point(400, 200), Point(400, 600)});
    auto polygonB2 = std::make_unique<Polygon>(std::initializer_list<Point>{Point(100, 600), Point(400, 600), Point(400, 1000)});
    auto polygonC = std::make_unique<Polygon>(std::initializer_list<Point>{Point(400, 200), Point(1000, 200), Point(1000, 600)});

    // Création des zones avec les polygones correspondants
    Zone zoneA("A", std::move(polygonA), {});
    Zone zoneB1("B1", std::move(polygonB1), {});
    Zone zoneB2("B2", std::move(polygonB2), {});
    Zone zoneC("C", std::move(polygonC), {});

    // Création de la Factory avec les zones initialisées
    Factory factory("Inoxetum", {zoneA, zoneB1, zoneB2, zoneC});

    // Création des machines
    auto machine1 = std::make_shared<Machine>("M1", nullptr, 30, 90, nullptr, nullptr);
    auto machine2 = std::make_shared<Machine>("M2", nullptr, 10, 120, nullptr, nullptr);
    auto machine3 = std::make_shared<Machine>("M3", nullptr, 0, 45, nullptr, nullptr);
    auto machine4 = std::make_shared<Machine>("M4", nullptr, 15, 30, nullptr, nullptr);
    auto machine5 = std::make_shared<Machine>("M5", nullptr, 20, 20, nullptr, nullptr);
    auto machine6 = std::make_shared<Machine>("M6", nullptr, 12, 150, nullptr, nullptr);
    auto machine7 = std::make_shared<Machine>("M7", nullptr, 5, 100, nullptr, nullptr);
    auto machine8 = std::make_shared<Machine>("M8", nullptr, 8, 75, nullptr, nullptr);

    // Création du convoyeur
    auto C1 = std::make_shared<Conveyor>("C1", machine3, machine4, 10, 100, 1);
    auto C2 = std::make_shared<Conveyor>("C2", machine5, machine6, 20, 50, 0.5);

    // Attacher le convoyeur aux machines en utilisant la Factory
    factory.attach_downstream_conveyor_to_machine("M3", C1);
    factory.attach_upstream_conveyor_to_machine("M4", C1);
    factory.attach_downstream_conveyor_to_machine("M5", C2);
    factory.attach_upstream_conveyor_to_machine("M6", C2);

    return 0;
}
