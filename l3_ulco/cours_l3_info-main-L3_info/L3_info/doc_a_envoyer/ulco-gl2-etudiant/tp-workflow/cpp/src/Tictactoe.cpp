#include "Tictactoe.hpp"

Jeu::Jeu(){
    raz();
}


Status Jeu::getStatus() const {
    return etat;
}

Cell Jeu::getCell(int i, int j) const {
    return _plateau[i][j];
}

std::ostream & operator<<(std::ostream & os, const Jeu & jeu) {
    // TODO
    for(int i=0;i<jeu.getNbLAndC();i++){
        for(int k=0;k<jeu.getNbLAndC();k++){
            if(jeu.getCell(i,k) == Cell::Vide){
                os << '.';
            }
            else if (jeu.getCell(i,k) == Cell::Rouge){
                os << 'R';
            }
            else if (jeu.getCell(i,k) == Cell::Vert){
                os << 'V';
            }
        }
        os << std::endl;
    }
    if(jeu.getStatus() == Status::RougeJoue){
        os << "rouge joue" << std::endl;
    }
    else if (jeu.getStatus() == Status::VertJoue){
        os << "vert joue" << std::endl;
    }
    else if (jeu.getStatus() == Status::RougeGagne){
        os << "rouge a gagné" << std::endl;
    }
    else if (jeu.getStatus() == Status::VertGagne){
        os << "vert a gagné" << std::endl;
    }
    else if (jeu.getStatus() == Status::Egalite){
        os << "égalité" << std::endl;
    }

    return os;
}

bool Jeu::jouer(int i, int j) {
    if(etat == Status::RougeGagne or etat == Status::VertGagne or etat == Status::Egalite){
        
    }
    else{
        if(_plateau[i][j] == Cell::Vide){
            if(etat == Status::RougeJoue){
                _plateau[i][j] = Cell::Rouge;
                etat = Status::VertJoue;
                checkIfEnd();
                return true;
            }
            else if (etat == Status::VertJoue){
                _plateau[i][j] = Cell::Vert;
                etat = Status::RougeJoue;
                checkIfEnd();
                return true;
            }
        }
    }
    return false;
}

void Jeu::raz() {
    etat = Status::RougeJoue;
    for(int i=0;i<nbLAndC;i++){
        for(int j=0;j<nbLAndC;j++){
            _plateau[i][j] = Cell::Vide;
        }
    }
}

void Jeu::checkIfEnd(){
    Cell result[(nbLAndC*2+2)];
    //initialise result
    for(int u=0;u<nbLAndC;u++){
        result[u] = _plateau[u][0]; //on prend la cellule du mur gauche
        result[nbLAndC+u] = _plateau[0][u]; //on prend la cellule du mur haut
    }
    result[nbLAndC*2] = _plateau[0][0];//on prend la cellule en haut a gauche (pour les diagonales)
    result[nbLAndC*2+1] = _plateau[nbLAndC-1][0];//on prend la cellule en bas a gauche (pour les diagonales)
    
    bool win[(nbLAndC*2+2)];
    for(int l=0;l<sizeof(win);l++){
        win[l] = true;
    }
    for(int i=0;i<nbLAndC*2;i++){ //on verifie les lignes et colonnes (pas les diagonales)
        if(i < nbLAndC){ // on verifie les lignes
            for(int j=0;j<nbLAndC;j++){
                win[i] = win[i] and (result[i] == _plateau[i][j]) and (result[i] != Cell::Vide); 
            }
        }
        if(i >= nbLAndC and i < nbLAndC*2){//verifie les colonne 
            for(int k=0;k<nbLAndC;k++){
                win[i] = win[i] and (result[i] == _plateau[k][i-nbLAndC]) and (result[i] != Cell::Vide);
            }
        }
    }
    for(int i=0;i<nbLAndC;i++){//verifie les diagonales
        win[nbLAndC*2] = win[nbLAndC*2] and (result[nbLAndC*2] == _plateau[i][i]) and (result[nbLAndC*2] != Cell::Vide);
        win[nbLAndC*2+1] = win[nbLAndC*2+1] and (result[nbLAndC*2+1] == _plateau[nbLAndC-1-i][i]) and (result[nbLAndC*2+1] != Cell::Vide);
    }

    for(int i=0;i<sizeof(win);i++){
        if(win[i]){
            if(result[i] == Cell::Rouge){
                etat = Status::RougeGagne;
                break;
            }
            if(result[i] == Cell::Vert){
                etat = Status::VertGagne;
                break;
            }
        }
    }



    
}
