
#define GEOMETRY_HPP

#include <memory>

class Geometry {

public:
    virtual std::unique_ptr<Geometry> clone() const = 0;
};

