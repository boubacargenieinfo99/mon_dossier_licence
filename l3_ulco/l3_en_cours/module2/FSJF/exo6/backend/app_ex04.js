const express=require('express');//inclure express

const app_ex4=express();

const  path =require('path');

//middleware qui retourne les documents statique 1er
app_ex4.use(express.static(path.join(__dirname,'public')));

//definition moteur de rendu
app_ex4.set('view engine', 'ejs');
//declaration du dossier contenant les vues
app_ex4.set('views',path.join(__dirname,'views'));

//importation de l'express_ejs_layouts
const expressLayouts=require('express-ejs-layouts');
app_ex4.use(expressLayouts);
app_ex4.set('layout','../views/layouts/layout_ex4');

//les Routages
const homeRouter = require('./routes/homeRouter');
app_ex4.use('/',homeRouter);

const about = require('./routes/about');
app_ex4.use('/about',about);


app_ex4.get('/*',(req,res)=>{
    res.redirect('/');
});



module.exports=app_ex4;