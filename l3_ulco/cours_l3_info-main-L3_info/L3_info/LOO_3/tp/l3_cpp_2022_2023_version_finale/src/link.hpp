#ifndef TRAFFIC_LINK_HPP
#define TRAFFIC_LINK_HPP

#include <cmath>
#include <memory>
#include <string>

class Node;

class Vehicle;

class Link {
public:
  Link(const std::string &id, unsigned int length) : _id(id), _length(length),
                                                     _cell_number(std::floor((double) length / _cell_length)) {}

  virtual ~Link() = default;

  void attach_in_node(const std::shared_ptr<Node> &node) { _in_node = node; }

  void attach_out_node(const std::shared_ptr<Node> &node) { _out_node = node; }

  unsigned int cell_number() const { return _cell_number; }

  virtual void enter_vehicle(Vehicle *vehicle, unsigned int time) = 0;

  virtual void exit_vehicle(Vehicle *vehicle, unsigned int time) = 0;

  const std::string &id() const { return _id; }

  const std::weak_ptr<Node> &out_node() const { return _out_node; }

  virtual Vehicle *run(unsigned int time) = 0;

protected:
  class Cell {
  public:
    Cell() : _vehicle(nullptr) {}

    Cell(const Cell& cell) : _vehicle(nullptr) {}

    void enter(Vehicle* vehicle, unsigned int time);

    Vehicle* exit();

    bool is_free() const { return _vehicle == nullptr; }

    unsigned int vehicle_last_time() const;

    const std::unique_ptr<Vehicle> &vehicle() const { return _vehicle; }

  private:
    std::unique_ptr<Vehicle> _vehicle;
  };

protected:
  static const unsigned int _duration{2};

private:
  static const unsigned int _cell_length{5};

  std::string _id;
  unsigned int _length;
  unsigned int _cell_number;
  std::weak_ptr<Node> _in_node;
  std::weak_ptr<Node> _out_node;
};

#endif //TRAFFIC_LINK_HPP
