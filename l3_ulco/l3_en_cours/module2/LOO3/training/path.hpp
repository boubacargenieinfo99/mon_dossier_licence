

#ifndef TRAINING_PATH_HPP
#define TRAINING_PATH_HPP
#pragma once

#include "coordinates.hpp"
#include <vector>

class Path {
public:
    Path(const std::initializer_list<Coordinates>& coordinates);

    void displayInfo() const;

    const std::vector<Coordinates>& getCoordinates() const;

private:
    std::vector<Coordinates> coordinates;
};


#endif //TRAINING_PATH_HPP
