module.exports.home = (req, res)=>{
    res.render('pages/show_route_params',{id_value : req.params.id});
}

module.exports.getDate = (req, res)=>{
    let query = req.query; // recuperation des paramètres GET
    res.render('pages/show_get_params',{query});
}

module.exports.getPostPage = (req, res)=>{
    res.render('pages/smpl_form');
}

module.exports.postPage = (req, res)=>{
    let data = req.body; // ne pas envoyer au render une option nommée 'body' car cela interférerait avec express-ejs-layout !
    res.render('pages/show_post_data',{data});
}