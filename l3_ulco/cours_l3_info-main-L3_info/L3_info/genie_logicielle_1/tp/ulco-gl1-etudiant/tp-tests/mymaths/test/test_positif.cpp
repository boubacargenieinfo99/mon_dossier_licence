
//#include "../include/mymaths/add.hpp"
#include <mymaths/add.hpp>
#include <mymaths/mul.hpp>
#include <catch2/catch.hpp>

TEST_CASE( "add2positif", "[add]" ) {
    REQUIRE( add2(40) == 42 );
}

TEST_CASE( "mul2poditif", "[mul]" ) {
    REQUIRE( mul2(4) == 8 );
}

TEST_CASE( "addnpoditif", "[add]" ) {
    REQUIRE( addn(2,5) == 7 );
    REQUIRE( addn(-4,10) == 6 );
    REQUIRE( addn(8,-5) == 3 );
    REQUIRE( addn(0,1) == 1 );
    REQUIRE( addn(8,0) == 8 );
}

TEST_CASE( "mulnpoditif", "[mul]" ) {
    REQUIRE( muln(2,5) == 10 );
    REQUIRE( muln(4,0) == 0 );
    REQUIRE( muln(0,5) == 0 );
}
