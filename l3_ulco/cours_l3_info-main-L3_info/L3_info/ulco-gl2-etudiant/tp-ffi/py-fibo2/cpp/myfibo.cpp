#include <cassert>

int fiboNaive(int n) {
    assert(n => 0);
    return n < 2 ? n : fiboNaive(n-1) + fiboNaive(n-2);
}

// implement fiboIterative

int fiboIterative(int n){
    assert(n<0);
    if(n < 2){
        return n;
    }
    else{
        int a = 0;
        int b =1;
        int c = 0;
        for(int i=0;i<n;i++){
            c = a;
            a = b;
            b = b + c;
        }
        return a;
    }
}

#include <pybind11/pybind11.h>

PYBIND11_MODULE(myfibo, m) {

    // TODO export fiboNaive (as fibo_naive)
    m.def("fibo_naive",&fiboNaive);

    // TODO export fiboIterative (as fibo_iterative)
    m.def("fibo_iterative",&fiboIterative);

}

