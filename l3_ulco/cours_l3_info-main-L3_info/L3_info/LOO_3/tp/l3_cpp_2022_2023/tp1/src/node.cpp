#include "node.hpp"
#include "link.hpp"


#include <cstddef>
#include <cstdio>
#include <cstdlib>
#include <cstring>

Node::Node(char *id, unsigned int duration, unsigned int in_number, unsigned int out_number){
    this->id = new char[strlen(id) + 1];
    this->duration = duration;
    this->in_number = in_number;
    this->out_number = out_number;
    in_links = new LinkPtr[in_number];
    for (unsigned int i = 0; i < in_number; ++i) {
        in_links[i] = nullptr;
    }
    out_links = new LinkPtr[out_number];
    for (unsigned int i = 0; i < out_number; ++i) {
        out_links[i] = nullptr;
    }
    vehicle = nullptr;
}

Node::~Node() {
    delete id;
    delete in_links;
    delete out_links;
}

void Node::add_vehicle_to_node(Vehicle *vehicle, unsigned int time) {
    printf("%d: %s enters %s\n", time, vehicle->id, id);
    this->vehicle = vehicle;
    vehicle->node = this;
    vehicle->last_time = time;
}

void Node::attach_input_link_to_node(Link *link) {
    unsigned int index = 0;

    while (in_links[index] != nullptr && index < in_number) {
        ++index;
    }
    if (index < in_number) {
        in_links[index] = link;
        link->attach_out_node_to_link(this);
    }
}

void Node::attach_output_link_to_node(Link *link) {
    unsigned int index = 0;

    while (out_links[index] != nullptr && index < out_number) {
        ++index;
    }
    if (index < out_number) {
        out_links[index] = link;
        link->attach_in_node_to_link(this);
    }
}

