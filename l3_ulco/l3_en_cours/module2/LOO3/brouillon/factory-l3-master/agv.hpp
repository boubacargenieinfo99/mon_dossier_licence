
#include <string>
#include "transportation.hpp"

class AGV : public Transportation {
public:
    AGV(std::string id, double speed, const Path& initialPath)
            : Transportation(id, speed, initialPath) {}
};