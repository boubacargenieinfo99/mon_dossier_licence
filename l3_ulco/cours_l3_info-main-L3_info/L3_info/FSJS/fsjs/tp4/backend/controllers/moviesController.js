const Movie = require("../models/Movie");

const PUBLIC_DIR = "./public" ;
const UPLOAD_DIR = '/data/uploads/' ;
const multer  = require('multer') ;
const fs = require('fs') ;




// OPTIONNEL : on peut définir une technique de storage
const storage = multer.diskStorage({
    destination: (req, file, cb) =>{
        const dest = PUBLIC_DIR + UPLOAD_DIR ;
        if (!fs.existsSync(dest)){
            fs.mkdirSync(dest, { recursive: true });
        }
        cb(null, dest) ;
    },
    filename: (req, file, cb)=>{
        const fileName = file.originalname.toLowerCase().split(' ').join('_');
        cb(null, fileName)
    }
})

// OPTIONNEL : on peut définir un filtre de vérification des fichiers
const filter = (req, file, cb)=>{
    if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg") {
        cb(null, true);
    } else {
        cb(new Error('Only .png, .jpg and .jpeg format allowed!'), false);
    }
}

// Configuration de l'uploader
const uploader = multer({
    // dest: UPLOAD_DIR, // <- à mettre si on ne définit pas de storage
    storage: storage,
    fileFilter: filter,
    limits:{
        fileSize: 700_000
    }
}) ;
    
//sauvegarde dans sa collection
/*moviee.save()
    .then((moviee)=>{
        console.log(moviee) // affiche le movie cree dans la console
    })
    .catch(error=>res.status(900).send(error));*/

module.exports.list = (req,res)=>{
    Movie.find() //recupere tous les Movie
    .sort({title:"asc"}) //trie par 'title' ascendant
    .then(movies => {
        res.render('pages/moviesList',{movies});
    })
    .catch(error => res.status(400).send(error))
}

module.exports.create = (req,res)=>{
    res.render('pages/create');
}

/*module.exports.createPost = (req,res)=>{

    uploader.single('image')(req, res, (err)=>{

        if(err instanceof multer.MulterError){
            // une erreur de multer
            console.log('MulterError');
            res.send(err);
            return;
        }else if(err){
            // une erreur inconnue
            res.send(err.message);
            return;
        }

        // Pas d'erreur..
        // les infos du fichier uploadés sont dans req.file
        // console.log(req.file)
        let data = req.body;
        const moviec = new Movie({
            title:data.title,
            synopsis:data.synopsis,
            image:data.image
        });
        if (req.file){
            moviec.image =  UPLOAD_DIR + req.file.filename ;
        };


       

        // ATTENTION : il faudrait vérifier que le formulaire a bien été rempli !
        moviec.save() // sauvegarde dans la BD
            .then((m)=>{
                console.log(m); //affiche le movie cree dans la console
                // si tout s'est bien passé, on réaffiche la liste
                res.redirect('/movies/');
            })
            .catch(error => res.status(400).send(error));

    });
}*/

module.exports.createPost = (req, res, next)=>{

    // Utilisation de multer : ici on précise qu'il n'y a qu'un fichier (single)
    // -> on n'oublie pas de donner la propriété name de l'input (ici : name="fileInput")
    uploader.single('image')(req, res, (err)=>{

        if(err instanceof multer.MulterError){
            // une erreur de multer
            console.log('MulterError')
            res.send(err)
            return;
        }else if(err){
            // une erreur inconnue
            res.send(err.message);
            return;
        }

        // Pas d'erreur..
        // les infos du fichier uploadés sont dans req.file
        // console.log(req.file)
        const movie = new Movie({
            ...req.body,
        })
        console.log(req.file);
        if (req.file){
            movie.image =  UPLOAD_DIR + req.file.filename ;
        }

        // ATTENTION : il faudrait vérifier que le formulaire a bien été rempli !
        movie.save() // sauvegarde dans la BD
            .then((m)=>{
                // si tout s'est bien passé, on réaffiche la liste
                res.redirect('/movies/')
            })
            .catch(error => res.status(400).send(error))

    }) ;

}


module.exports.read = (req,res)=>{
    //Movie.findOne(filter)
    Movie.findById(more)
    .then((movie)=>{
        res.render('pages/presentMovie',{movie});
    })
    .catch(error=>res.status(600).send(error));
}

module.exports.readPost = (req,res)=>{
    let data = req.body;
    //Movie.findOne(filter)
    Movie.findById(data.more)
    .then((movie)=>{
        res.render('pages/presentMovie',{movie});
    })
    .catch(error=>res.status(600).send(error));
}

module.exports.update = (req,res)=>{
    let data = req.body;

    if(data.id != undefined){
        const movieDat = {
            title:data.title,
            synopsis:data.synopsis,
            image:data.image
        };

        const updateMovies = Movie.findByIdAndUpdate(
            data.id,
            movieDat,
            {new:true} // pour que .then recupere le movie modifier
            ).then((movie)=>{
                console.log(movie);
                res.redirect('/movies');
            })
            .catch(error=>res.status(700).send(error));

        //selection du document et mise a jour avec les nouvelles donnee
        
    }
    else{
        Movie.findById(data.updId)
        .then((movie)=>{
            res.render('pages/edit',{movie});
        })
        .catch(error=>res.status(600).send(error));
    }
}

module.exports.delete = (req,res)=>{
    let data = req.body;

    const deleteMovies = Movie.findByIdAndDelete(data.delId)
    .then((filter)=>{
        console.log('movies delete');
        res.redirect('/movies');
    })
    .catch(error=>res.status(800).send(error));
}

