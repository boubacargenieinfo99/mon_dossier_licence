const express = require('express');
const app = express();

// Dossier public (pour le css, les images, etc.)
const path = require('path');
app.use(express.static(path.join(__dirname, 'public')));

// View engine
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

// Layout
const expressLayouts = require('express-ejs-layouts');
app.use(expressLayouts);
app.set('layout', '../views/layouts/layout') ;


// --- ROUTES ---
app.get("/", (req, res) => {
    res.render('pages/home', { location: 'Accueil'})
});

app.get('/about', (req, res) => {
    res.render('pages/about', { location: 'À propos'})
});

app.get('*', (req, res) => {
    res.redirect('/')
});


module.exports = app