#ifndef TRAFFIC_ITERATOR_HPP
#define TRAFFIC_ITERATOR_HPP

#include "list.hpp"

template<typename Data>
class Iterator {
public:
  Iterator(const List<Data> &list)
    : _list(list), _current_cell(list.get_first()) {}

  const std::shared_ptr<Data> &current() { return _current_cell->get_data(); }

  bool has_more() { return _current_cell != nullptr; }

  void next() { _current_cell = _current_cell->get_next(); }

private:
  const List<Data> &_list;
  Cell<Data> *_current_cell;
};

#endif //TRAFFIC_ITERATOR_HPP
