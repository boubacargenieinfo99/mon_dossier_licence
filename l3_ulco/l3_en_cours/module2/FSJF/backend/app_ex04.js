const express=require('express');//inclure express

const app_ex4=express();

const  path =require('path');

//middleware qui retourne les documents statique 1er
app_ex4.use(express.static(path.join(__dirname,'public')));

//definition moteur de rendu
app_ex4.set('view engine', 'ejs');
//declaration du dossier contenant les vues
app_ex4.set('views',path.join(__dirname,'views'));

//importation de l'express_ejs_layouts
const expressLayouts=require('express-ejs-layouts');
app_ex4.use(expressLayouts);

//definition de layout par defaut
app_ex4.set('layout','../views/layouts/layout_ex4');

//middleware d'affichage des info dans la console
app_ex4.use((req,res,next)=>{
    const now=new Date().toDateString();
    console.log(`${now}:une requete ${req.method} est arrivée !`);
   next();
});

//declenche uniqument pour get sur lurl "/"
app_ex4.get("/",(req,res)=>{
    res.render('pages/home_ex4');
});
app_ex4.get("/about",(req,res)=>{
   res.render('pages/about_ex4');
});



//route pour toutes requetes different de ceux d'en haut
app_ex4.get("*",(req,res)=>{
    res.redirect('/');
});

//exportation de notre application express
module.exports = app_ex4;