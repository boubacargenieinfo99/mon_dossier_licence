cmake_minimum_required(VERSION 3.26)
project(tp4)

set(CMAKE_CXX_STANDARD 17)

add_executable(tp4 main.cpp
        factory.cpp
        zone.hpp
        zone.cpp
        machine.hpp
        machine.cpp
        spatialElement.cpp
        spatialElement.hpp
        geometry.hpp
        geometry.cpp
        cellular.hpp
        cellular.cpp)
