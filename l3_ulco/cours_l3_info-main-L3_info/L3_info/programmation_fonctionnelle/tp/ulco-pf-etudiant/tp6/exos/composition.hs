{-estPositif :: Int -> Bool
estPositif x = (x > 0)

add42 :: Int -> Int
add42 = (+42)

-- >>>add42 5
-- 47
--

plus42Positif :: Int -> Bool
plus42Positif l = estPositif (add42 l)
-}

plus42Positif :: Int -> Bool
plus42Positif = (>0).(+42)

mul2PlusUn ::Num a => a -> a
mul2PlusUn = (+1).(*2)

mul2MoinsUn :: Num a => a -> a
mul2MoinsUn = (+(-1)).(*2)

applyTwice ::Num a =>(a -> a) -> a -> a
--applyTwice f x = f(f(x))
applyTwice f = f . f

main::IO()
main = do
    print(plus42Positif 2)
    print(plus42Positif (-84))
    print(mul2PlusUn 10)
    print(mul2MoinsUn 10)
    print(applyTwice mul2PlusUn 10)