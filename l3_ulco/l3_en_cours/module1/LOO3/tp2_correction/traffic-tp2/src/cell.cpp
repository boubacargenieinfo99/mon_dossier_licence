#include "cell.hpp"

namespace traffic {

Cell::Cell() {}

Cell::Cell(const Cell & /* other */) {}

bool Cell::is_free() const { return not _vehicle; }

void Cell::push_vehicle(unsigned int t, std::unique_ptr<Vehicle> &vehicle) {
  _vehicle.reset(vehicle.release());
  _out_time = t + _vehicle->speed() / 10;
}

}