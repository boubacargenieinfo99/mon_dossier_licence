"use strict";

const StatusValues = {CONNECTING: 0, WAITING: 1, READY: 2 };

export default {
    StatusValues: StatusValues
};