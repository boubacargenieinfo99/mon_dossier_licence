#ifndef TRAFFIC_ROUND_ABOUT_HPP
#define TRAFFIC_ROUND_ABOUT_HPP

#include "node.hpp"

#include <deque>

namespace traffic {

class Link;

class RoundAbout : public Node {
public:
  RoundAbout(const std::string &id, unsigned int duration, size_t in_number, size_t out_number, unsigned int capacity);

  bool is_free() const override { return _vehicles.size() < _capacity; }

  void push_vehicle(unsigned int t, std::unique_ptr<Vehicle> &vehicle) override;

  void run(unsigned int t) override;

private:
  const unsigned int _capacity;
  std::deque<std::unique_ptr<Vehicle>> _vehicles;
  std::deque<unsigned int> _out_times;
};

}

#endif //TRAFFIC_ROUND_ABOUT_HPP
