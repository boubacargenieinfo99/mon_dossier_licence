#ifndef TRAFFIC_VEHICLE_H
#define TRAFFIC_VEHICLE_H

struct Link;
struct Node;

struct Vehicle {
  char *id;
  struct Link *link;
  struct Node *node;
  unsigned int last_time;
};

struct Vehicle *create_vehicle(char *id);

void destroy_vehicle(struct Vehicle *vehicle);

#endif //TRAFFIC_VEHICLE_H
