
#ifndef TRAINING_BUFFER_HPP
#define TRAINING_BUFFER_HPP
#pragma once

#include "spatialElement.hpp"
#include <memory>

class Buffer : public SpatialElement {
public:
    Buffer(int capacity);
    int getCapacity() const;
    void displayInfo() const override;

private:
    int capacity;
};


#endif //TRAINING_BUFFER_HPP
