var Express = require('express');
var router = Express.Router();

const researchControllers = require('../controllers/researchControllers');

router.get('/',researchControllers.home);

module.exports = router;