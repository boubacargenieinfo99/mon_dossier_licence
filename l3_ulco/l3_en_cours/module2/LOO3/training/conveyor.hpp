
#ifndef TRAINING_CONVEYOR_HPP
#define TRAINING_CONVEYOR_HPP

#pragma once

#include "unavailableElement.hpp"
#include "machine.hpp"
#include <memory>

class Machine;

class Conveyor : public UnavailableElement {
public:
    Conveyor(const std::string& id, int capacity, int length, double speed,
             const Machine& source, const Machine& destination);


    const Machine& getSourceMachine() const;
    const Machine& getDestinationMachine() const;
    void displayInfo() const;

private:
    int capacity;
    int length;
    int speed;
    const Machine& sourceMachine;
    const Machine& destinationMachine;
};


#endif //TRAINING_CONVEYOR_HPP
