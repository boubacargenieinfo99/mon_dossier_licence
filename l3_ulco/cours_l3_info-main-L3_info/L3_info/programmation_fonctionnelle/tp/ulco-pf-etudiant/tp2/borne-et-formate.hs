borneEtFormate1 :: Double -> String
borneEtFormate1 numero = show numero ++ " -> " ++ show resultat
    where 
        resultat = if numero < 0
                        then 0
                        else if numero > 1
                            then 1
                            else numero

borneEtFormate2 :: Double -> String
borneEtFormate2 numero = show numero ++ " -> " ++ show resultat
    where resultat    
            | numero < 0 = 0
            | numero > 1 = 1
            | otherwise = numero

main ::IO()
main = do
    print (borneEtFormate1 (-1.2))
    print (borneEtFormate1 0.2)
    print (borneEtFormate1 0.9)
    print (borneEtFormate1 1.1)

    print (borneEtFormate2 (-1.2))
    print (borneEtFormate2 0.2)
    print (borneEtFormate2 0.9)
    print (borneEtFormate2 1.1)