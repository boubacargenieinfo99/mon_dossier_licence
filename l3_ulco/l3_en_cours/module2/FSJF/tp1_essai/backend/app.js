const port =process.env.PORT || 7000;

const express = require('express');
const app = express();

const path = require('path');
app.use(express.static(path.join(__dirname,'public')));

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
const expressLayouts=require('express-ejs-layouts');
app.use(expressLayouts);
app.set('layout', '../views/layouts/layout');


//routage
const homeRouter=require('./routes/homeRouter')
app.use('/', homeRouter)

const jokesRouter=require('./routes/jokesRouter')
app.use('/jokes',jokesRouter)

//lancement du serveur
app.listen(port, ()=>{
    console.log(`Server started on port http://127.0.0.1:${port}`);
})