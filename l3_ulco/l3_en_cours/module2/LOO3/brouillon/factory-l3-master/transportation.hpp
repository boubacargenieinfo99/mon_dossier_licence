#include <iostream>
#include <vector>
#include "path.hpp"


using namespace std;

class Transportation{
public:
    Transportation(string id, double speed, const Path& initialPath):
    id(id), speed(speed), paths({initialPath}){}

    std::string getId() const {
        return id;
    }
private:
    string id;
    double speed;
    vector<Path> paths;
};