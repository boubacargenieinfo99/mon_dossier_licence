function ball(){
    const sphereGeom=new THREE.SphereGeometry(0.1,32,16);
    const sphereMat=new THREE.MeshLambertMaterial({color:0xffff00});
    const sphere =new  THREE.Mesh(sphereGeom,sphereMat);


    sphere.position.x=(Math.random()*(4-0)+0-2).toFixed(1);
    sphere.position.y=(Math.random()*(4-0)+0-2).toFixed(1);
    sphere.position.z=(Math.random()*(4-0)+0-2).toFixed(1);
    sphere.castShadow=true;

    return sphere;
}

function wall(){
    const geometry =new THREE.PlaneGeometry(4.2,4.2);
    const material=new THREE.MeshLambertMaterial({color:0x00ff00});
    const mesh=new THREE.Mesh(geometry,material);
    return mesh;
}