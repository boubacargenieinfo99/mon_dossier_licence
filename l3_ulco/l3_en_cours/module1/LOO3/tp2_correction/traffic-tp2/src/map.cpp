#include "map.hpp"

namespace traffic {

Map::Map() {}

void Map::add_link(const std::string &id, unsigned int length, unsigned int way_number) {
  _links.emplace_back(std::make_shared<Link>(id, length, way_number));
}

void Map::add_node(const std::string &id, unsigned int duration, size_t in_number, size_t out_number) {
  _nodes.emplace_back(std::make_shared<Node>(id, duration, in_number, out_number));
}

void Map::attach_in_link(size_t index, const std::string &link_id, const std::string &node_id) {
  bool found = false;
  size_t link_index = 0;

  while (link_index < _links.size() and not found) {
    if (_links[link_index]->id() == link_id) { found = true; } else { ++link_index; }
  }
  if (found) {
    size_t node_index = 0;

    found = false;
    while (node_index < _nodes.size() and not found) {
      if (_nodes[node_index]->id() == node_id) { found = true; } else { ++node_index; }
    }
    if (found) {
      _nodes[node_index]->attach_in_link(index, _links[link_index]);
      _links[link_index]->attach_out_node(_nodes[node_index]);
    }
  }
}

void Map::attach_out_link(size_t index, const std::string &node_id, const std::string &link_id) {
  bool found = false;
  size_t link_index = 0;

  while (link_index < _links.size() and not found) {
    if (_links[link_index]->id() == link_id) { found = true; } else { ++link_index; }
  }
  if (found) {
    size_t node_index = 0;

    found = false;
    while (node_index < _nodes.size() and not found) {
      if (_nodes[node_index]->id() == node_id) { found = true; } else { ++node_index; }
    }
    if (found) {
      _nodes[node_index]->attach_out_link(index, _links[link_index]);
      _links[link_index]->attach_in_node(_nodes[node_index]);
    }
  }
}

void Map::push_vehicle(unsigned int t, std::unique_ptr<Vehicle>& vehicle, const std::string& node_id) {
  size_t node_index = 0;
  bool found = false;

  while (node_index < _nodes.size() and not found) {
    if (_nodes[node_index]->id() == node_id) { found = true; } else { ++node_index; }
  }
  if (found) {
    _nodes[node_index]->push_vehicle(t, vehicle);
  }
}

}