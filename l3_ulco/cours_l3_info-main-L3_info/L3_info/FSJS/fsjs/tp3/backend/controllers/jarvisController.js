module.exports.home = (req, res)=>{
    let logged = false
    res.render("pages/jarvis_interface/home",{logged});
}

module.exports.login = (req, res)=>{
    let logged = false
    res.render("pages/jarvis_interface/login",{logged});
}

module.exports.connected = (req, res)=>{
    let data = req.body;
    if(data.username === "stark" && data.password === "warmachinerox"){
        let logged = true;
        res.render("pages/jarvis_interface/connected",{logged});
    }
    else{
        let errorConnection; // 1 si username et password manquant
                             // 2 si username manquant
                             // 3 si password manquant
                             // 4 si erreur d username et/ou de password
        if(typeof data.username === 'undefined' && data.password === 'undefined'){
            errorConnection = 1
        }
        else {
            if (typeof data.username === 'undefined') {
                errorConnection = 2;
            } else {
                if (typeof data.password === 'undefined') {
                    errorConnection = 3;
                }
                else{
                    errorConnection = 4;
                }
            }
        }
        res.render('pages/jarvis_interface/login',{errorConnection});
    }
}

module.exports.logout = (req, res)=>{
    res.redirect('/jarvis_interface')
}