/**
 * @file test/main_komivoki_minimal.cpp
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2011-2024 Openxum Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>

#include <openxum/core/games/komivoki/engine.hpp>
#include <openxum/core/games/komivoki/game_type.hpp>
#include <openxum/ai/specific/komivoki/mcts_player.hpp>
#include <openxum/ai/specific/komivoki/random_player.hpp>
#include <openxum/ai/common/random_player.hpp>

using namespace openxum::core::games;

void play() {
//  auto engine = new komivoki::Engine(komivoki::SIMPLE, komivoki::Color::BLACK, {4, 4}, 2,
//                                     komivoki::FourFourBoardInitializer());
//  auto engine = new komivoki::Engine(komivoki::SIMPLE, komivoki::Color::BLACK, {5, 5}, 2,
//                                     komivoki::FiveFiveBoardInitializer());
  auto engine = new komivoki::Engine(komivoki::SIMPLE, komivoki::Color::BLACK, {8, 8}, 3,
                                     komivoki::EightEightBoardInitializer());
  std::shared_ptr<openxum::core::common::Player<komivoki::Decision>> player_one = std::make_shared<openxum::ai::specific::komivoki::RandomPlayer>(
    komivoki::Color::BLACK,
    komivoki::Color::WHITE,
    engine->clone());
  std::shared_ptr<openxum::core::common::Player<komivoki::Decision>> player_two = std::make_shared<openxum::ai::specific::komivoki::RandomPlayer>(
    komivoki::Color::WHITE,
    komivoki::Color::BLACK,
    engine->clone());
//  std::shared_ptr<openxum::core::common::Player<komivoki::Decision>> player_two = std::make_shared<openxum::ai::specific::komivoki::MCTSPlayer>(
//    komivoki::Color::WHITE,
//    komivoki::Color::BLACK,
//    engine->clone());
  auto current_player = player_one;
  auto other_player = player_two;
  unsigned int turn_number = 0;

  std::cout << "init:" << std::endl;
  std::cout << engine->to_string() << std::endl;

  while (not engine->is_finished()) {
    const openxum::core::common::Move<komivoki::Decision> move = current_player->get_move();
    const int color = engine->current_color();

    std::cout << turn_number << ": ["
              << (engine->current_color() == komivoki::Color::BLACK ? "black" : "white")
              << "] => " << move.to_string() << std::endl;

    engine->move(move);
    player_one->move(move);
    player_two->move(move);

    std::cout << engine->to_string() << std::endl;

    if (engine->current_color() == player_one->color()) {
      current_player = player_one;
      other_player = player_two;
    } else {
      current_player = player_two;
      other_player = player_one;
    }
    if (color != engine->current_color()) { ++turn_number; }
  }
  std::cout << "Winner is: " << (engine->winner_is() == komivoki::Color::BLACK ? "black" : "white")
            << std::endl;
  delete engine;
}

int main(int, const char **) {
  play();
  return EXIT_SUCCESS;
}