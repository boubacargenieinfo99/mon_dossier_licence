#ifndef TRAFFIC_LINK_H
#define TRAFFIC_LINK_H

#include "vehicle.hpp"

#define CELL_LENGTH 5 // meters

struct Node;

struct Cell {
    struct Vehicle *vehicle;
};

struct Link {

    //constructeur

    Link(char *id, unsigned int length);

    //destructeur

    ~Link();

    //attributes

    char *id;
    unsigned int length;
    Cell *cells;
    unsigned int cell_number;
    Node* in_node;
    Node* out_node;

    //fonctions

    void add_vehicle_to_link(Vehicle *vehicle, unsigned int time);
    void attach_in_node_to_link(Node *node);
    void attach_out_node_to_link(Node *node);
    struct Vehicle *go_out_link(unsigned int time);
    struct Vehicle *run_link(unsigned int time);

};

typedef Link* LinkPtr;

#endif //TRAFFIC_LINK_H