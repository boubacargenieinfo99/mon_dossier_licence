from __future__ import division

import numpy as np

import joueur
import interface
import time


class Jeu:
	
	def __init__(self, opts={}):
		self.winner = None
		self.noir = None
		self.blanc = None
		self.compteur = [0,0]
		self.compteur_temps = [0,0]
		self.score = [0,0]
		self.nb_demande = [0,0]
		if "choix_joueurs" not in opts or opts["choix_joueurs"]:
			possibilites = [joueur.Humain] + joueur.IA.__subclasses__()
			if len(possibilites) == 1:
				print("Un seul type de joueur est actuellement implémenté : "+
					  str(possibilites[0])+".\nCe type sera choisi pour noir et blanc.")
				self.noir = possibilites[0](self, "noir", opts)
				self.blanc = possibilites[0](self, "blanc", opts)
			else:
				print("Quel type de joueur souhaitez vous sélectionner pour noir ?",
					  "Les possibilités sont :")
				for i in range(len(possibilites)):
					print(i+1," : "+str(possibilites[i]))
				choix = input("Tapez le nombre correspondant.\n")
				self.noir = possibilites[int(choix)-1](self, "noir", opts)
				print("Quel type de joueur souhaitez vous sélectionner pour blanc ?\nLes possibilités sont :")
				for i in range(len(possibilites)):
					print(i+1," : "+str(possibilites[i]))
				choix = input("Tapez le nombre correspondant.\n")
				self.blanc = possibilites[int(choix)-1](self, "blanc", opts)
		
		if "taille" in opts:
			taille = opts["taille"]
		else:
			taille = 8
		self.plateau = Plateau(taille)
		self.tour = 1
		self.partie_finie = False
		self.joueur_courant = self.noir
		
		if "interface" not in opts or opts["interface"]:
			self.interface = True
			self.gui = interface.Interface(self)
		else:
			self.interface = False

	def demarrer(self):
		if isinstance(self.joueur_courant, joueur.IA):
			self.gui.active_ia()
		else:
			self.gui.active_humain()
		self.gui.fenetre.mainloop()
		
	def jouer(self, coup, verbose=1):
		if self.tour == 1 and self.joueur_courant == None:
			if self.noir == None:
				raise ValueError("Vous devez initialiser le joueur noir.")
			self.joueur_courant = self.noir
		self.plateau.jouer(coup, self.joueur_courant.couleurval)
		if self.interface:
			self.gui.actualise_plateau()
		
		self.tour += 1
		self.partie_finie, v = self.plateau.check_partie_finie()

		if self.partie_finie:
			if v == 1:
				sv = "noir"
			else:
				sv = "blanc"
			m = "Victoire de "+sv+"."
			print(m)
			self.winner = sv
			print('nombre de coups du joueur 1 (noir) ', self.compteur[0] , ' ,temps : ' , self.compteur_temps[0])
			print('nombre de coups du joueur 2 (blanc) ', self.compteur[1] , ' ,temps : ' , self.compteur_temps[1])
			if self.interface:
				self.gui.desactive_humain()
				self.gui.desactive_ia()
				self.gui.message_tour.set("Partie finie.\n"+m)
			self.joueur_courant = None
			
		else:
			if self.tour%2 == 1:
				self.joueur_courant = self.noir
				if self.interface:
					self.gui.message_tour.set("A noir de jouer")
			else:
				self.joueur_courant = self.blanc
				if self.interface:
					self.gui.message_tour.set("A blanc de jouer")


class Plateau:
	
	def __init__(self, taille=8):
		self.tableau_cases = [[0 for j in range(taille)] for i in range(taille)]
		for i in range(taille):
			self.tableau_cases[i][0] = self.tableau_cases[i][1] = 1
			self.tableau_cases[i][-1] = self.tableau_cases[i][-2] = -1
		self.taille = taille
		self.nb_noir = 2*taille #nombre de pions noirs
		self.nb_blanc = 2*taille
		
	def jouer(self, coup, couleurval):
		try:
			case_depart, case_arrivee = coup
		except:
			print(coup)
			raise Exception("")
		if self.est_coup_valide(coup, couleurval):
			if self.tableau_cases[case_arrivee[0]][case_arrivee[1]] == 1:
				self.nb_noir -= 1
			elif self.tableau_cases[case_arrivee[0]][case_arrivee[1]] == -1:
				self.nb_blanc -= 1
			self.tableau_cases[case_arrivee[0]][case_arrivee[1]] = self.tableau_cases[case_depart[0]][case_depart[1]]
			self.tableau_cases[case_depart[0]][case_depart[1]] = 0
		else:
			raise Exception("Le coup donné n'est pas valide")
			
	def check_case_in_bounds(self, case):
		if case[0] < 0 or case[0] >= self.taille or case[1] < 0 or case[1] >= self.taille:
			return False
		return True

	def est_coup_valide(self, coup, couleurval=None):
		depart, arrivee = coup
		if not self.check_case_in_bounds(depart) or not self.check_case_in_bounds(arrivee):
			return False
		c_depart = self.tableau_cases[depart[0]][depart[1]]
		if c_depart == 0:
			return False
		if couleurval is not None and c_depart != couleurval:
			return False #pas la bonne couleur
		c_arrivee = self.tableau_cases[arrivee[0]][arrivee[1]]
		if c_arrivee != 0:
			if c_depart == c_arrivee or depart[0] == arrivee[0]:
				return False
		if c_depart == 1:
			sens = 1
		else:
			sens = -1
		if arrivee[1] != depart[1]+sens:
			return False
		if arrivee[0] < depart[0] - 1 or arrivee[0] > depart[0] + 1:
			return False
		return True

	def liste_coups_valides(self, couleurval):
		'''Rend la liste des coups valides pour un joueur de couleur couleurval.'''
		coups_valides = []
		for i in range(self.taille):
			for j in range(self.taille):
				if self.tableau_cases[i][j] == couleurval:
					for k in (-1,0,1):
						if self.est_coup_valide(((i,j), (i+k,j+couleurval)), couleurval):
							coups_valides.append(((i,j),(i+k,j+couleurval)))
		return coups_valides
	
	def copie(self):
		copie = Plateau(self.taille)
		copie.tableau_cases = [[self.tableau_cases[i][j] for j in range(self.taille)] for i in range(self.taille)]
		return copie
	
	def __getitem__(self, coords):
		assert abs(coords[0]) < self.taille and abs(coords[1]) < self.taille
		if coords[1] < 0:
			return self.tableau_cases[(coords[0]+1)*self.taille+coords[1]]
		else:
			return self.tableau_cases[coords[0]*self.taille+coords[1]]
	
	def check_partie_finie(self):
		if self.nb_noir == 0:
			return True, -1
		elif self.nb_blanc == 0:
			return True, 1
		for i in range(self.taille):
			if self.tableau_cases[i][-1] == 1:
				return True, 1
			if self.tableau_cases[i][0] == -1:
				return True, -1
		return False, None

	def evaluation_simple(self,couleurVal):
		nb_ligne_plateau = self.taille
		nb_colonne_plateau = self.taille #tableau_cases.shape  # on suppose (0,0) -> haut gauche
		dicti = {}
		compteur = 0
		score = 0
		if couleurVal == -1:
			for l in range(nb_ligne_plateau):
				for c in range(nb_colonne_plateau):
					if self.tableau_cases[l][c] == -1:
						dicti[compteur] = (l,c)
						compteur += 1
			for n in range(len(dicti)):
				if dicti[n][1] == 0 or dicti[n][1] == 7:
					score += 1
				else:
					if dicti[n][1] == 1 or dicti[n][1] == 6:
						score += 3
					else:
						score += 4
				score += (7-dicti[n][0]) * 10
		if couleurVal == 1:
			for l in range(nb_ligne_plateau):
				for c in range(nb_colonne_plateau):
					if self.tableau_cases[l][c] == 1:
						dicti[compteur] = (l,c)
						compteur += 1
			for n in range(len(dicti)):
				if dicti[n][1] == 0 or dicti[n][1] == 7:
					score += 1
				else:
					if dicti[n][1] == 1 or dicti[n][1] == 6:
						score += 3
					else:
						score += 4
				score += dicti[n][0] * 10
		return score


	def evaluation_moyen(self, couleurVal):
		nb_ligne_plateau = self.taille
		nb_colonne_plateau = self.taille # tableau_cases.shape  # on suppose (0,0) -> haut gauche
		dictBlanc = {}
		compteurBlanc = 0
		dictNoir = {}
		compteurNoir = 0
		score = 0
		for l in range(nb_ligne_plateau):
			for c in range(nb_colonne_plateau):
				if self.tableau_cases[l][c] == -1:
					dictBlanc[compteurBlanc] = (l, c)
					compteurBlanc += 1
				else:
					if self.tableau_cases[l][c] == 1:
						dictNoir[compteurNoir] = (l, c)
						compteurNoir += 1

		if couleurVal == -1:
			for n in range(len(dictBlanc)):  # score que du blanc
				if dictBlanc[n][1] == 0 or dictBlanc[n][1] == 7:
					score += 1
				else:
					if dictBlanc[n][1] == 1 or dictBlanc[n][1] == 6:
						score += 3
					else:
						score += 4
				score += (-1 * (7 - dictBlanc[n][0])) * 10
			for n in range(len(dictNoir)):  # score blanc moins noir
				if dictNoir[n][1] == 0 or dictNoir[n][1] == 7:
					score = score - 1
				else:
					if dictNoir[n][1] == 1 or dictNoir[n][1] == 6:
						score = score - 3
					else:
						score = score - 4
				if dictNoir[n][0] < 3:
					score = score - (dictNoir[n][0] * 5)
				else:
					score = score - (dictNoir[n][0] * 10)

		if couleurVal == 1:
			for n in range(len(dictNoir)):
				if dictNoir[n][1] == 0 or dictNoir[n][1] == 7:
					score += 1
				else:
					if dictNoir[n][1] == 1 or dictNoir[n][1] == 6:
						score += 3
					else:
						score += 4
				score += dictNoir[n][0] * 10
			for n in range(len(dictBlanc)):  # score blanc moins noir
				if dictBlanc[n][1] == 0 or dictBlanc[n][1] == 7:
					score = score - 1
				else:
					if dictBlanc[n][1] == 1 or dictBlanc[n][1] == 6:
						score = score - 3
					else:
						score = score - 4
				if dictBlanc[n][0] > 3:
					score = score - (7 - dictBlanc[n][0] * 5)
				else:
					score = score - (7 - dictNoir[n][0] * 10)
		return score

	# a voir plus tard ( reduire ou augmenter le score selon les pions pouvant etre mangé)
	"""def evaluation_complexe(self): 
		nb_ligne_plateau, nb_colonne_plateau = self.jeu.plateau.tableau_cases.shape  # on suppose (0,0) -> haut gauche
		dictBlanc = {}
		compteurBlanc = 0
		dictNoir = {}
		compteurNoir = 0
		score = 0
		for l in range(nb_ligne_plateau):
			for c in range(nb_colonne_plateau):
				if self.jeu.plateau.tableau_cases[l][c] == -1:
					dictBlanc[compteurBlanc] = (l, c)
					compteurBlanc += 1
				else:
					if self.jeu.plateau.tableau_cases[l][c] == 1:
						dictNoir[compteurNoir] = (l, c)
						compteurNoir += 1

		if self.couleurval == -1:
			for n in range(len(dictBlanc)):  # score que du blanc
				if dictBlanc[n][1] == 0 or dictBlanc[n][1] == 7:
					score += 1
				else:
					if dictBlanc[n][1] == 1 or dictBlanc[n][1] == 6:
						score += 3
					else:
						score += 4
				score += (-1 * (7 - dictBlanc[n][0])) * 10
			for n in range(len(dictNoir)):  # score blanc moins noir
				if dictNoir[n][1] == 0 or dictNoir[n][1] == 7:
					score = score - 1
				else:
					if dictNoir[n][1] == 1 or dictNoir[n][1] == 6:
						score = score - 3
					else:
						score = score - 4
				if dictNoir[n][0] < 3:
					score = score - (dictNoir[n][0] * 5)
				else:
					score = score - (dictNoir[n][0] * 10)
		if self.couleurval == 1:
			for n in range(len(dictNoir)):
				if dictNoir[n][1] == 0 or dictNoir[n][1] == 7:
					score += 1
				else:
					if dictNoir[n][1] == 1 or dictNoir[n][1] == 6:
						score += 3
					else:
						score += 4
				score += dictNoir[n][0] * 10
			for n in range(len(dictBlanc)):  # score blanc moins noir
				if dictBlanc[n][1] == 0 or dictBlanc[n][1] == 7:
					score = score - 1
				else:
					if dictBlanc[n][1] == 1 or dictBlanc[n][1] == 6:
						score = score - 3
					else:
						score = score - 4
				if dictBlanc[n][0] > 3:
					score = score - (7 - dictBlanc[n][0] * 5)
				else:
					score = score - (7 - dictNoir[n][0] * 10)
		return score"""


"""def pion_selectionne(self):
	nb_ligne_plateau,nb_colonne_plateau = self.jeu.plateau.tableau_cases.shape	#on suppose (0,0) -> haut gauche
	if self.couleurval == -1 :	#si l ia a la couleur blanc
		for l in range(nb_ligne_plateau):
			dicti = {}
			nb_pion_sur_la_ligne = 0
			for c in range(nb_colonne_plateau):
				if self.jeu.plateau.tableau_cases[l][c] == -1:
					dicti[nb_pion_sur_la_ligne] = (l,c)
					nb_pion_sur_la_ligne += 1
			if len(dicti) != 0:	#si il y a un element dans la ligne le plus haut
				if l == nb_ligne_plateau - 2:
					return dicti[0]
				else:
					nb_pion_a_ecrasé = np.zeros(len(dicti))
					for c in range(len(dicti)):
						if dicti[c][0] > 0:	# pas censé arrivé
							if dicti[c][1] > 0:
								if self.jeu.plateau.tableau_cases[dicti[c][0]-1][dicti[c][1]-1] == 1: # si la case haut gauche du pion blanc est noir
									if dicti[c][0]-2 < 0: #test si le deuxieme diagonale gauche existe
										if dicti[c][]
										if self.jeu.plateau.tableau_cases[dicti[c][0]-2][dicti[c]] == 0:
											return dicti[c]	# si on peut ecraser un pion et si il n y a personne derriere
									else:
							if dicti[c][1] < 7:
								if self.jeu.plateau.tableau_cases[dicti[c][0]-1][dicti[c][1]+1] == 1:
									nb_pion_a_ecrasé[c] += 1

		#code a finir pour avoir -> on regarde les pions sur la lignes la plus haute et on regarde si on peut ecraser un pion -si oui on regarde derriere si il n y a pas de pion -si oui on return ce pion
																																													# -si non on abandone ce pion
																																# -si non on regarde derriere les positions possible si il n y a pas de pions - si oui on abandone
																																																			# - si non on return ce pion
																										# si non on avance un pion avec pas de pion derriere
"""