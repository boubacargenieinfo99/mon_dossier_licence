#pragma once
#include <iostream>

struct Task;
class Board;

class View{
    public:
        virtual std::string fmtTask(Task tache);

        virtual std::string fmtBoard(Board liste);
};