"use strict";

import OpenXum from '../../openxum/manager.mjs';
import KikotsokaPolyomino from '../../../openxum-core/games/kikotsoka-polyomino/index.mjs';

class Manager extends OpenXum.Manager {
    constructor(t, e, g, o, s, w, f) {
        super(t, e, g, o, s, w, f);
        this.that(this);
    }

    build_move() {
        return new KikotsokaPolyomino.Move();
    }

    get_current_color() {
        return this.engine().current_color() === KikotsokaPolyomino.Color.BLACK ? 'Black' : 'White';
    }

    static get_name() {
        return 'kikotsokaPolyomino';
    }

    get_winner_color() {
        return this.engine().winner_is() === KikotsokaPolyomino.Color.BLACK ? 'black' : 'white';
    }

    process_move() {
    }
}

export default Manager;
