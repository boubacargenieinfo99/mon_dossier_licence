import System.IO (hFlush,stdout)

loopEcho :: Int -> IO String
loopEcho 0 = return "loop terminated"
loopEcho n = do
    putStr ("> ")
    hFlush stdout
    nom <- getLine
    if (nom =="")
        then return "empty line"
    else do
        putStrLn nom
        loopEcho (n-1)
        


main :: IO()
main = do
    msg <- loopEcho 3
    print msg