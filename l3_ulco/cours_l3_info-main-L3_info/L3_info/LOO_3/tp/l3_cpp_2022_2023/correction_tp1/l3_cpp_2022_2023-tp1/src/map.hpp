#ifndef TRAFFIC_MAP_HPP
#define TRAFFIC_MAP_HPP

#include "inputs.hpp"
#include "link.hpp"
#include "node.hpp"

struct Map {
  Map(unsigned int link_number, unsigned int node_number);

  ~Map();

  void add_link(Link *link);

  void add_node(Node *node);

  int find_node_index(const char *node_id) const;

  void run(unsigned int time);

  void run(unsigned int time, const Inputs &inputs);

  void run_link(unsigned int time);

  void run_node(unsigned int time);

  unsigned int link_number;
  unsigned int node_number;
  Link **links;
  Node **nodes;
  unsigned int input_index;
};

#endif //TRAFFIC_MAP_HPP
