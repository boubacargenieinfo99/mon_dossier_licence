--USER ADMIN AORISE TOUS LES DROIT (SI PROBLEME)

CREATE TABLE promotions(
	numero VARCHAR2(15),
	numproduit	VARCHAR2(15),
	pourcentage_remise	NUMBER(4,2),
	date_debut	DATE,
	date_fin 	DATE,
	encours		NUMBER(1),
	qtevendue	NUMBER(8)
);

/*
CREATE OR REPLACE PROCEDURE new_promo
AS
	num_promo 	promotions.numero%TYPE;
	num_produit 	promotions.numproduit%TYPE;
	remise 		promotions.pourcentage_remise%TYPE;
	dd		promotions.date_debut%type;
	df		promotions.date_fin%TYPE;
BEGIN
	DBMS_OUTPUT.PUT_LINE('pas de produit avec ce numero');
	num_promo := TO_CHAR(((TO_NUMBER(TO_CHAR(SYSDATE,'DD'))) * 60 * 60 * 24) + (60 * 60 * TO_NUMBER(TO_CHAR(SYSDATE,'HH24'))) + (60 * TO_NUMBER(TO_CHAR(SYSDATE,'MI'))) + (TO_NUMBER(TO_CHAR(SYSDATE,'SS'))));
	num_produit := &saisir_num_produit;
	remise := &saisir_remise;
	dd := SYSDATE();
	df := ADD_MONTHS(dd,1);
	INSERT INTO promotions(numero,numproduit,pourcentage_remise,date_debut,date_fin,encours,qtevendue)
		VALUES(num_promo,num_produit,remise,dd,df,1,0);
	DBMS_OUTPUT.PUT_LINE('promotions inseree');
END;
/
*/

CREATE OR REPLACE FUNCTION date_depasee (dat DATE) RETURN BOOLEAN
AS
BEGIN
	IF (EXTRACT(YEAR FROM SYSDATE) > EXTRACT(YEAR FROM dat)) OR (EXTRACT(YEAR FROM SYSDATE) = EXTRACT(YEAR FROM dat) AND EXTRACT(MONTH FROM SYSDATE) > EXTRACT(MONTH FROM dat)) OR (EXTRACT(YEAR FROM SYSDATE) = EXTRACT(YEAR FROM dat) AND EXTRACT(MONTH FROM SYSDATE) = EXTRACT(MONTH FROM dat) AND EXTRACT(DAY FROM SYSDATE) > EXTRACT(DAY FROM dat)) THEN
		RETURN true;
	ELSE
		RETURN false;
	END IF;
	EXCEPTION
		WHEN OTHERS THEN
			NULL;
END;
/

CREATE OR REPLACE FUNCTION date_avant_debut (dat DATE) RETURN BOOLEAN
AS
BEGIN
	IF 
	(EXTRACT(YEAR FROM SYSDATE) < EXTRACT(YEAR FROM dat)) OR (EXTRACT(YEAR FROM SYSDATE) = EXTRACT(YEAR FROM dat) AND EXTRACT(MONTH FROM SYSDATE) < EXTRACT(MONTH FROM dat)) OR (EXTRACT(YEAR FROM SYSDATE) = EXTRACT(YEAR FROM dat) AND EXTRACT(MONTH FROM SYSDATE) = EXTRACT(MONTH FROM dat) AND EXTRACT(DAY FROM SYSDATE) < EXTRACT(DAY FROM dat)) THEN
		RETURN true;
	ELSE
		RETURN false;
	END IF;
	EXCEPTION
		WHEN OTHERS THEN
			NULL;
END;
/



CREATE OR REPLACE PROCEDURE promo_fin
AS
	CURSOR pro IS
		SELECT * FROM promotions;
BEGIN
	FOR iter IN pro LOOP
		IF date_depasee(iter.date_fin) THEN
			UPDATE promotions
			SET encours = 0
			WHERE promotions.numero = iter.numero;
		END IF;
	END LOOP;
END;
/


--test q1
/*
INSERT INTO promotions(numero,numproduit,pourcentage_remise,date_debut,date_fin,encours,qtevendue)
	VALUES('123456','PR7227','10',ADD_MONTHS(SYSDATE,-2),ADD_MONTHS(SYSDATE,-1),'1','550');

SELECT * FROM promotions;

EXECUTE promo_fin

SELECT * FROM promotions;
*/



--q2

CREATE OR REPLACE TRIGGER upper_name_fname
	BEFORE INSERT
	ON EMPLOYE
	FOR EACH ROW
DECLARE
	nom_up EMPLOYE.NOM%TYPE;
BEGIN
	:NEW.NOM := UPPER(:NEW.NOM);
	:NEW.PRENOM := UPPER(:NEW.PRENOM);
	EXCEPTION
		WHEN OTHERS THEN
			NULL;
END;
/

--test q2
/*
INSERT INTO EMPLOYE(NUMERO,NOM,PRENOM,ADRESSE)
	VALUES('EMP9173','val','jean','75 rue casse briques, paris');

DECLARE
	nome varchar2(30);
	prenome varchar2(30);
BEGIN
	select NOM,PRENOM INTO nome,prenome
	from EMPLOYE
	where numero = 'EMP9173';
	dbms_output.put_line(nome || ' ' || prenome);
END;
/

DELETE FROM EMPLOYE
	WHERE NUMERO = 'EMP9173';
*/




--q3

CREATE OR REPLACE TRIGGER don_encours
	BEFORE INSERT ON promotions
	FOR EACH ROW
DECLARE
BEGIN
	IF NOT (date_avant_debut(:NEW.date_debut)) AND NOT (date_depasee(:NEW.date_fin)) THEN
		:NEW.encours := 1;
	ELSE
		:NEW.encours := 0;
	END IF;
	EXCEPTION
		WHEN OTHERS THEN
			NULL;
END;
/

--test
/*
INSERT INTO promotions(numero,numproduit,pourcentage_remise,date_debut,date_fin,encours,qtevendue)
	VALUES('123457','PR7228','10',ADD_MONTHS(SYSDATE,-2),ADD_MONTHS(SYSDATE,-1),'1','550');
INSERT INTO promotions(numero,numproduit,pourcentage_remise,date_debut,date_fin,encours,qtevendue)
	VALUES('123458','PR7229','10',ADD_MONTHS(SYSDATE,2),ADD_MONTHS(SYSDATE,3),'1','550');
INSERT INTO promotions(numero,numproduit,pourcentage_remise,date_debut,date_fin,encours,qtevendue)
	VALUES('123459','PR7230','10',SYSDATE,ADD_MONTHS(SYSDATE,1),'0','550');

SELECT * FROM promotions;

DELETE FROM promotions
	WHERE numero = '123457';
DELETE FROM promotions
	WHERE numero = '123458';
DELETE FROM promotions
	WHERE numero = '123459';

*/




--q4

CREATE OR REPLACE TRIGGER verif_encours
	before INSERT
	ON promotions
	FOR EACH ROW
DECLARE
	out_date EXCEPTION;
	PRAGMA exception_init(out_date,-20060);
BEGIN
	IF date_depasee(:NEW.date_fin) THEN
		RAISE out_date;
	END IF;
	EXCEPTION
		WHEN out_date THEN
			DBMS_OUTPUT.PUT_LINE('erreur,date depassee');
			NULL;
		WHEN OTHERS THEN
			NULL;
END;
/

--test
/*
INSERT INTO promotions(numero,numproduit,pourcentage_remise,date_debut,date_fin,encours,qtevendue)
	VALUES('123457','PR7228','10',ADD_MONTHS(SYSDATE,-2),ADD_MONTHS(SYSDATE,-1),'1','550');
*/

--q5

CREATE OR REPLACE TRIGGER modif_quantite
	BEFORE UPDATE
	ON PRODUITS
	FOR EACH ROW
DECLARE
	nouveau_chiffre_affaire	RAYON.CHIFFREAFFAIRE%TYPE;
BEGIN
	IF :NEW.QUANTITESTOCKHYPER < :OLD.QUANTITESTOCKHYPER THEN
		SELECT CHIFFREAFFAIRE INTO nouveau_chiffre_affaire
			FROM RAYON 
			WHERE RAYON.NUMERO = :OLD.NUMERORAYON; 
		nouveau_chiffre_affaire := nouveau_chiffre_affaire + ((:OLD.QUANTITESTOCKHYPER - :NEW.QUANTITESTOCKHYPER) * :OLD.PRIXUNITAIRE);
		UPDATE RAYON
		SET CHIFFREAFFAIRE = nouveau_chiffre_affaire
		WHERE :OLD.NUMERORAYON = RAYON.NUMERO;
	END IF; 
	EXCEPTION
		WHEN NO_DATA_FOUND THEN 
			DBMS_OUTPUT.PUT_LINE('le produits n existe pas');
			NULL;
		WHEN OTHERS THEN
			NULL;
END;
/

--test
/*
SELECT * FROM RAYON 
WHERE NUMERO = 'RAY67';

SELECT * FROM PRODUITS 
WHERE NUMERO = 'PR5783';

UPDATE PRODUITS 
SET QUANTITESTOCKHYPER = '50000'
WHERE NUMERO = 'PR5783';

SELECT * FROM RAYON 
WHERE NUMERO = 'RAY67';

SELECT * FROM PRODUITS 
WHERE NUMERO = 'PR5783';

UPDATE PRODUITS 
SET QUANTITESTOCKHYPER = '50000'
WHERE NUMERO = 'PR5783';
*/


--q6

CREATE OR REPLACE TRIGGER maj_qtevendue_promo
	BEFORE UPDATE 
	ON PRODUITS 
	FOR EACH ROW 
DECLARE
	dd promotions.date_debut%TYPE;
	df promotions.date_fin%TYPE;
	quantite_vendue promotions.qtevendue%TYPE;
BEGIN
	select date_debut,date_fin INTO dd,df
		FROM promotions
		WHERE promotions.numero = :OLD.NUMERO;
	IF NOT (date_avant_debut(dd)) AND NOT (date_depasee(df)) THEN
		IF :OLD.QUANTITESTOCKHYPER > :NEW.QUANTITESTOCKHYPER THEN
			quantite_vendue := :OLD.QUANTITESTOCKHYPER - :NEW.QUANTITESTOCKHYPER;
			UPDATE promotions 
			SET qtevendue = qtevendue + quantite_vendue
			WHERE promotions.numproduit = :OLD.NUMERO;
		END IF;
	END IF;
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			DBMS_OUTPUT.PUT_LINE('la promotions n existe pas');
			NULL;
		WHEN OTHERS THEN
			NULL;
END;
/

--test 
/*
INSERT INTO PRODUITS(NUMERO,LIBELLE,NUMERORAYON,PRIXUNITAIRE,UNITE,QUANTITESTOCKHYPER)
	VALUES('PR1973','fraise','RAY45',50.00,'unite',10000);

INSERT INTO promotions(numero,numproduit,pourcentage_remise,date_debut,date_fin,encours,qtevendue)
	VALUES('134679','PR1973',10,SYSDATE,ADD_MONTHS(SYSDATE,1),'1','2');

SELECT * FROM promotions
WHERE numero = '1234679';

UPDATE PRODUITS 
SET QUANTITESTOCKHYPER = 9000
WHERE NUMERO = 'PR1973';

SELECT * FROM promotions
WHERE numero = '1234679';

DELETE FROM promotions
WHERE numero = '1234679';

DELETE FROM PRODUITS 
WHERE NUMERO = 'PR1973';
*/


--q7

CREATE OR REPLACE TRIGGER insert_produit
	BEFORE INSERT
	ON PRODUITS
	FOR EACH ROW
DECLARE
	deja_present EXCEPTION;
	PRAGMA exception_init(deja_present,-20020);
	num_produits_existant PRODUITS.NUMERO%TYPE;
BEGIN
	SELECT NUMERO INTO num_produits_existant
		FROM PRODUITS
		WHERE NUMERO = :NEW.NUMERO;
	IF num_produits_existant = :NEW.NUMERO THEN 
		RAISE deja_present;
	END IF;
	EXCEPTION
		WHEN deja_present THEN
			DBMS_OUTPUT.PUT_LINE('produits deja existant (meme numero)');
			UPDATE PRODUITS
			SET QUANTITESTOCKHYPER = :NEW.QUANTITESTOCKHYPER
			WHERE PRODUITS.NUMERO = :NEW.NUMERO;
		--WHEN NO_DATA_FOUND THEN
		WHEN OTHERS THEN
			NULL;
END;
/

--test
/*
SELECT * FROM PRODUITS
WHERE NUMERO = 'PR6765';

INSERT INTO PRODUITS(NUMERO,LIBELLE,NUMERORAYON,PRIXUNITAIRE,UNITE,QUANTITESTOCKHYPER)
	VALUES('pro7458','monopoly','RAY98','25.00','unite','5000');

INSERT INTO PRODUITS(NUMERO,LIBELLE,NUMERORAYON,PRIXUNITAIRE,UNITE,QUANTITESTOCKHYPER)
	VALUES('PR6765','SWEAT CORERUNNER','RAY45',59.00,'unité',1000);

SELECT * FROM PRODUITS
WHERE NUMERO = 'PR6765' OR NUMERO = 'pro7458';

DELETE FROM PRODUITS
WHERE NUMERO ='pro7458';

INSERT INTO PRODUITS(NUMERO,LIBELLE,NUMERORAYON,PRIXUNITAIRE,UNITE,QUANTITESTOCKHYPER)
	VALUES('PR6765','SWEAT CORERUNNER','RAY45',59.00,'unité',87778);
*/

--q8

CREATE TABLE logs(
	numero VARCHAR2(15),
	date_logs DATE,
	username VARCHAR2(30),
	message VARCHAR2(60)
);

CREATE OR REPLACE TRIGGER upd_logs
	BEFORE INSERT OR UPDATE
	ON promotions
	FOR EACH ROW
DECLARE
	num_logs logs.numero%TYPE;
	user_logs logs.username%TYPE;
	mes_logs logs.message%TYPE;
	CURSOR liste_produits IS
		SELECT NUMERO 
		FROM PRODUITS; 
BEGIN
	SELECT USER INTO user_logs
		FROM DUAL;
	num_logs := substr(user_logs,1,2) + TO_CHAR( ((TO_NUMBER(TO_CHAR(SYSDATE,'DD'))) * 60 * 60 * 24) + (60 * 60 * TO_NUMBER(TO_CHAR(SYSDATE,'HH24'))) + (60 * TO_NUMBER(TO_CHAR(SYSDATE,'MI'))) + (TO_NUMBER(TO_CHAR(SYSDATE,'SS'))) );
	FOR pl IN liste_produits LOOP
		IF :NEW.numero = pl.NUMERO THEN
			mes_logs := 'modification d une promotions';
		END IF;
	END LOOP;
	IF mes_logs != 'modification d une promotions' THEN
		mes_logs := 'ajout dans la table promotions';
	END IF;
	INSERT INTO logs(numero,date_logs,username,message)
		VALUES(num_logs,SYSDATE,user_logs,mes_logs);
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			DBMS_OUTPUT.PUT_LINE('erreur , pas d user');
			NULL;
		WHEN OTHERS THEN
			NULL;
END;
/

--test
/*
SELECT * FROM logs;

INSERT INTO promotions(numero,numproduit,pourcentage_remise,date_debut,date_fin,encours,qtevendue)
	VALUES('134679','PR1973',10,SYSDATE,ADD_MONTHS(SYSDATE,1),'1','2');

UPDATE promotions
SET pourcentage_remise = 20
WHERE numero = '1234679';

SELECT * FROM logs;

DELETE FROM promotions
WHERE numero = '1234679';

DELETE FROM logs;
*/

--q8.2

CREATE OR REPLACE TRIGGER upd_logs
	BEFORE INSERT OR UPDATE
	ON promotions
	FOR EACH ROW
DECLARE
	num_logs logs.numero%TYPE;
	user_logs logs.username%TYPE;
	mes_logs logs.message%TYPE;
	magasin_ferme EXCEPTION;
	PRAGMA exception_init(magasin_ferme,-20050);
	CURSOR liste_produits IS
		SELECT NUMERO 
		FROM PRODUITS; 
BEGIN
	SELECT USER INTO user_logs
		FROM DUAL;
	num_logs := substr(user_logs,1,2) + TO_CHAR( ((TO_NUMBER(TO_CHAR(SYSDATE,'DD'))) * 60 * 60 * 24) + (60 * 60 * TO_NUMBER(TO_CHAR(SYSDATE,'HH24'))) + (60 * TO_NUMBER(TO_CHAR(SYSDATE,'MI'))) + (TO_NUMBER(TO_CHAR(SYSDATE,'SS'))) );
	IF (to_number(to_char(SYSDATE,'HH24')) > 8) AND (to_number(to_char(SYSDATE,'HH24')) < 20) THEN 
		FOR pl IN liste_produits LOOP
			IF :NEW.numero = pl.NUMERO THEN
				mes_logs := 'modification d une promotions';
			END IF;
		END LOOP;
		IF mes_logs != 'modification d une promotions' THEN
			mes_logs := 'ajout dans la table promotions';
		END IF;
		INSERT INTO logs(numero,date_logs,username,message)
			VALUES(num_logs,SYSDATE,user_logs,mes_logs);
	ELSE
		RAISE magasin_ferme;
	END IF;
	EXCEPTION
		WHEN magasin_ferme THEN
			DBMS_OUTPUT.PUT_LINE('erreur , heure non conforme');
			INSERT INTO logs(numero,date_logs,username,message)
				VALUES(num_logs,SYSDATE,user_logs,'erreur, heure non conforme');
			NULL;
		WHEN NO_DATA_FOUND THEN
			DBMS_OUTPUT.PUT_LINE('erreur, pas d user');
			NULL;
		WHEN OTHERs THEN
			NULL;
END;
/

--test 
/*
SELECT * FROM logs;

INSERT INTO promotions(numero,numproduit,pourcentage_remise,date_debut,date_fin,encours,qtevendue)
	VALUES('134679','PR1973',10,SYSDATE,ADD_MONTHS(SYSDATE,1),'1','2');

UPDATE promotions
SET pourcentage_remise = 20
WHERE numero = '1234679';

SELECT * FROM logs;

DELETE FROM promotions
WHERE numero = '1234679';

DELETE FROM logs;
*/
