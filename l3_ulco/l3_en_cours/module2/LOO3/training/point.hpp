
#ifndef TRAINING_POINT_HPP
#define TRAINING_POINT_HPP
#pragma once

#include "coordinates.hpp"

class Point : public Coordinates {
public:
    Point(double x, double y);
    void displayInfo() const override;

private:
    double x;
    double y;
};

#endif //TRAINING_POINT_HPP
