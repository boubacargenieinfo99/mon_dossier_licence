#ifndef TRAFFIC_ONE_WAY_LINK_HPP
#define TRAFFIC_ONE_WAY_LINK_HPP

#include "link.hpp"

#include <memory>
#include <string>
#include <valarray>

class Junction;
class Vehicle;

class OneWayLink : public Link {


public:
  OneWayLink(const std::string &id, unsigned int length);

  OneWayLink(const OneWayLink &link);

  ~OneWayLink() override;

  void enter_vehicle(Vehicle *vehicle, unsigned int time) override;

  void exit_vehicle(Vehicle *vehicle, unsigned int time) override;

  Vehicle *run(unsigned int time) override;

private:
  Vehicle *go_out(unsigned int time);

  std::valarray<Cell> _cells;
};

typedef OneWayLink *LinkPtr;

#endif //TRAFFIC_ONE_WAY_LINK_HPP
