import numpy as np
from math import log

class DataPoint:
    def __init__(self, x, y, cles):
        self.x = {}
        for i in range(len(cles)):
            self.x[cles[i]] = float(x[i])
        self.y = int(y)
        self.dim = len(self.x)
        
    def __repr__(self):
        return 'x: '+str(self.x)+', y: '+str(self.y)


def load_data(filelocation):
    with open(filelocation,'r') as f:
        data = []
        attributs = f.readline()[:-1].split(',')[:-1]
        for line in f:
            z = line.split(',')
            if z[-1] == '\n':
                z = z[:-1]
            x = z[:-1]
            y = int(z[-1])
            data.append(DataPoint(x,y,attributs))
    return data


class Noeud:
    def __init__(self, profondeur_max=np.infty):
        self.question = None
        self.enfants = {}
        self.profondeur_max = profondeur_max
        self.proba = None
        self.hauteur = None

    def prediction(self, x):
        pass
        
    def grow(self, data,hauteur=0):
        self.hauteur = hauteur
        d_entropie = entropie(data)
        question = best_split(data)
        if d_entropie >= 0:
            if self.hauteur < self.profondeur_max:
                if len(d_entropie[0]) > 0 and len(d_entropie[1]) > 0:
                    self.question = question
                    self.enfants[0] = d_entropie[0]
                    self.enfants[1] = d_entropie[1]
                    self.enfants[0].grow(data,hauteur+1)
                    self.enfants[1].grow(data,hauteur+1)
            else:
                self.proba = proba_empirique(data)



def proba_empirique(d): # d : liste de datapoint

    ObjectClass = {}
    for j in range(len(d)):
        if ObjectClass.get(d[j].y,"null") == "null":
            ObjectClass[d[j].y] = 1/len(d) # cree la clee si elle n existe pas et lui donne la valeur 1
        else:
            ObjectClass[d[j].y] += 1/len(d)
    return ObjectClass

def printDictionnaire(dict):
    for i in dict.items():
        print(i)



def question_inf(x,a,s):    # x:1 datapoint ,a:nom d 1 attribut et s:valeur
    if x.x[a] < s: # x[a] < s:
        return True
    else:
        return False

def split(d,a,s):   #d: liste de datapoint , a:nom d 1 attribur et s:valeur
    d1 = []
    d2 = []
    for datapoint in d:
        if question_inf(datapoint,a,s):
            d1.append(datapoint)
        else:
            d2.append(datapoint)
    return d1, d2

def list_separ_attributs(d,a):    #d: datapoint et a:nom d attribut (length a > 1)
    malist = []
    for datas in d:
        malist.append(datas.x[a])
    malist = list(set(malist))    # supprime les doublons (mais devient un ensemble) et retransforme l ensemble en liste
    malist.sort()                  # trie dans l ordre la liste
    liste_separ_attributs = []
    for i in range(len(malist) - 1):
        liste_separ_attributs.append((a,((malist[i] + malist[i + 1]) / 2)))  # ajoute le millieu entre l element i et le suivant
    return liste_separ_attributs

def liste_questions(d):     # d:datapoint
    tabNomAttribut = []     #va contenir les noms des attributs d un datapoint
    for atr in (d[0].x).keys():     #recupere les noms des attributs du premier element (on suppose que tous les elements ont le meme dictionnaire)
        tabNomAttribut.append(atr)      # et on les ajoute
    liste_q = []
    for j in range (len(tabNomAttribut)):
        liste_q = liste_q + (list_separ_attributs(d,tabNomAttribut[j]))
    return liste_q

def printDataAge(d):  # exemple
    for datas in d:
        print(datas.x["Age"])
def entropie(d):        # d:datapoint
    dicti = proba_empirique(d)
    somme = 0

    for k in dicti:
        dicti[k]
    for val in dicti.values():
        if len(dicti) <= 1:
            return 0
        if val != 0:
            somme += val * log(val, len(dicti))
    H = - somme
    return H

def gain_entropie(d,q):
    a, s = q
    d1, d2 = split(d, a, s)
    r1 = len(d1)/len(d)
    r2 = len(d2)/len(d)
    GainE = entropie(d) - r1 * entropie(d1) - r2 * entropie(d2)
    return GainE

def best_split(d):
    listeQuestion = liste_questions(d)
    meilleurGainE = 0
    for question in listeQuestion:
        if(gain_entropie(d,question) > meilleurGainE):
            meilleurGainE = gain_entropie(d,question)
            meilleurQuestion = question
    return meilleurQuestion