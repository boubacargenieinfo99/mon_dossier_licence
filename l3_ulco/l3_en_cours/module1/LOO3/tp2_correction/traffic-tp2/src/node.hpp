#ifndef TRAFFIC_NODE_HPP
#define TRAFFIC_NODE_HPP

#include <memory>
#include <string>
#include <vector>

namespace traffic {

class Link;

class Node {
public:
  Node(const std::string &id, unsigned int duration, size_t in_number, size_t out_number);

  void attach_in_link(size_t index, const std::shared_ptr<Link> &link);

  void attach_out_link(size_t index, const std::shared_ptr<Link> &link);

  const std::string &id() const { return _id; }

  bool is_free() const { return not _vehicle;}

  void push_vehicle(unsigned int t, std::unique_ptr<Vehicle> &vehicle);

private:
  std::string _id;
  unsigned int _duration;
  std::vector<std::shared_ptr<Link>> _in_links;
  std::vector<std::shared_ptr<Link>> _out_links;
  std::unique_ptr<Vehicle> _vehicle;
};

}

#endif //TRAFFIC_NODE_HPP
