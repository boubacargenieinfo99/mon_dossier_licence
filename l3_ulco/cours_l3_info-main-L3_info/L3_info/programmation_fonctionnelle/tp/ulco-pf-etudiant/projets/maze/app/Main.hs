import qualified Data.Vector as V

import Control.Monad (forM_)
import System.Environment
import System.IO
import Data.Text --(splitOn)

type Maze = V.Vector Char

-- runMaze :: Int -> Int -> Int -> Int -> Maze -> IO ()
-- runMaze ni nj i0 j0 maze = ...

mymap :: Text -> String
mymap mot = if (Data.Text.length mot)/=1
    then (Data.Text.head mot) : mymap (Data.Text.tail mot)
    else [(Data.Text.head mot)]

{-affichageLab :: String -> String -> IO()
affichageLab info maze = do
    let chaine = Data.Text.split (==' ') (pack info)
        nbligne = read (mymap (chaine !! 0)) :: Int
        nbcolonne = read (mymap (chaine !! 1)) :: Int
    forM_ [0 .. (nbcolonne*nbligne)] $ \i -> do
        if (i `mod` nbcolonne) == 0 --and i/=0
            then putStr [(maze !! i)]
            else putStrLn [(maze !! i)]
-}

{-affichageLab :: Maze -> Int -> IO()
affichageLab maze nbColonne = affichageLab maze nbColonne 0
    where affichageLab maze nbColonne compteur = do
                                                if (compteur == ((V.length maze)-1)) 
                                                    then do 
                                                        if (compteur+1) `mod` nbColonne == 0 && compteur /= 0 
                                                            then putStrLn [(maze V.! compteur)]
                                                            else do
                                                                putStr [(maze V.! compteur)]
                                                                affichageLab maze nbColonne (compteur+1)
                                                            else putStrLn [(maze V.! compteur)]
-}

affichageLab :: Maze -> Int -> Int -> IO()
affichageLab maze nbColonne compteur = do
                                        if (compteur == ((V.length maze)-1)) 
                                            then do 
                                                if (compteur+1) `mod` nbColonne == 0 && compteur /= 0 
                                                    then do
                                                        putStrLn [(maze V.! compteur)]
                                                        affichageLab maze nbColonne (compteur +1)
                                                    else do
                                                        putStr [(maze V.! compteur)]
                                                        affichageLab maze nbColonne (compteur+1)
                                            else putStrLn [(maze V.! compteur)]                                                            
    

main :: IO ()
main = do
    hSetBuffering stdin NoBuffering
    file <- readFile "data/maze1.txt"
    let (infoLine:mazeLines) = Prelude.lines file
        maze = V.fromList (Prelude.concat mazeLines)
    print infoLine

    let chaine = Data.Text.split (==' ') (pack infoLine)
        nbligne = read (mymap (chaine !! 0)) :: Int
        nbcolonne = read (mymap (chaine !! 1)) :: Int

    print (V.length maze)
    affichageLab maze nbcolonne 0
    forM_ [0 .. 9 ] $ \i -> do
        let c = maze V.! i
        putStr [c]
    putStrLn "\npress a key..."
    x <- getChar
    putStrLn ("\nyou pressed: " ++ [x])

