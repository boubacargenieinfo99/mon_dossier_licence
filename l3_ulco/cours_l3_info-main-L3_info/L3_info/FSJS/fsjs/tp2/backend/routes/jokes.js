var Express = require('express');
var router = Express.Router();

const jokesController = require('../controllers/jokesController');

router.get('/list',jokesController.list);

router.get('/random',jokesController.random);

router.get('/joke/:id',jokesController.present);

module.exports = router;