const mongoose = require('mongoose');

const movieSchema = mongoose.Schema({
    //NB: le champ _id est automatiquement cree
    title: { type: String,required:true},
    synopsis: {type:String,default:"No synopsis available yet !"},
    image: {type:String,default:'/'}
});


module.exports = mongoose.model('movies',movieSchema);