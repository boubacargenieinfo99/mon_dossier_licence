#ifndef TRAFFIC_N_WAYS_LINK_HPP
#define TRAFFIC_N_WAYS_LINK_HPP

#include "link.hpp"

#include <valarray>

class NWaysLink : public Link {
public:
  NWaysLink(const std::string &id, unsigned int length, unsigned int way_number);

  ~NWaysLink() override;

  void enter_vehicle(Vehicle *vehicle, unsigned int time) override;

  void exit_vehicle(Vehicle *vehicle, unsigned int time) override;

  Vehicle *run(unsigned int time) override;

private:
  Vehicle *go_out(unsigned int time);

  typedef std::valarray<Cell> Way;

  std::valarray<Way> _ways;
};

#endif //TRAFFIC_N_WAYS_LINK_HPP
