#include "node.hpp"
#include "link.hpp"

#include <cstring>
#include <iostream>

Node::Node(const char *id, unsigned int duration, unsigned int in_number, unsigned int out_number) :
  id(new char[strlen(id) + 1]), duration(duration), in_number(in_number), out_number(out_number),
  in_links(in_number == 0 ? nullptr : new LinkPtr[in_number]),
  out_links(out_number == 0 ? nullptr : new LinkPtr[out_number]), vehicle(nullptr) {
  strcpy(this->id, id);
  for (unsigned int i = 0; i < in_number; ++i) {
    in_links[i] = nullptr;
  }
  for (unsigned int i = 0; i < out_number; ++i) {
    out_links[i] = nullptr;
  }
}

Node::~Node() {
  delete[] id;
  delete[] in_links;
  delete[] out_links;
}

void Node::add_vehicle(Vehicle *vehicle, unsigned int time) {
  std::cout << time << ": " << vehicle->id << " enters " << id << std::endl;
  this->vehicle = vehicle;
  vehicle->node = this;
  vehicle->last_time = time;
}

void Node::attach_input_link(Link *link) {
  unsigned int index = 0;

  while (in_links[index] != nullptr && index < in_number) { ++index; }
  if (index < in_number) {
    in_links[index] = link;
    link->attach_out_node(this);
  }
}

void Node::attach_output_link(Link *link) {
  unsigned int index = 0;

  while (out_links[index] != nullptr && index < out_number) { ++index; }
  if (index < out_number) {
    out_links[index] = link;
    link->attach_in_node(this);
  }
}
