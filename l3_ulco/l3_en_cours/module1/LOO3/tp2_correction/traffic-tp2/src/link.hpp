#ifndef TRAFFIC_LINK_HPP
#define TRAFFIC_LINK_HPP

#include <memory>
#include <string>
#include <vector>

#include "way.hpp"

namespace traffic {

class Node;

class Link {
public:
  Link(const std::string &id, unsigned int length, unsigned int way_number);

  void attach_in_node(const std::shared_ptr<Node> &node);

  void attach_out_node(const std::shared_ptr<Node> &node);

  const std::string &id() const { return _id; }

  bool is_free() const;

  void push_vehicle(unsigned int t, std::unique_ptr<Vehicle> &vehicle);

private:
  std::string _id;
  unsigned int _length;
  std::shared_ptr<Node> _in_node;
  std::shared_ptr<Node> _out_node;
  std::vector<Way> _ways;
};

}

#endif //TRAFFIC_LINK_HPP
