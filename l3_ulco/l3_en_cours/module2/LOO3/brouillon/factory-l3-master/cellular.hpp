#include "cell.hpp"
class Cellular : public Geometry {
public:
    Cellular(std::initializer_list<Cell>cell){
        if(cell.size()>=1) cells=cell;
    }
    Cellular(double width, double height): Geometry(),width(width),height(height){}

private:
    double width;
    double height;
    std::vector<Cell> cells;
};