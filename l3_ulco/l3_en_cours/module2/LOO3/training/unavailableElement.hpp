
#ifndef TRAINING_UNAVAILABLEELEMENT_HPP
#define TRAINING_UNAVAILABLEELEMENT_HPP
#pragma once

#include <iostream>

class UnavailableElement {
public:
    UnavailableElement(const std::string& id) : id(id) {}

    virtual ~UnavailableElement() = default;

    // Méthode commune à toutes les classes dérivées
    virtual void displayInfo() const {
        std::cout << "ID: " << id << "\n";
    }

private:
    std::string id;
};

#endif //TRAINING_UNAVAILABLEELEMENT_HPP
