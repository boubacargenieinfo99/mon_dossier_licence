#include <csvstats/csvstats.hpp>
#include <cmath>
#include <sstream>
#include <iostream>

int mul2(int n) {
    return n*2;
}



Csv::Csv(){ 
    std::vector<std::vector<double>> vecData;
    data = vecData;
    //vecData.push_back()
}

void Csv::loadData(std::ifstream& file){
    int compteurDePoint;
    double numero;
    int positionApresPoint=1;
    std::string ligne;
    std::string nb;
    int erreur =0;
    std::vector<std::vector<double>> vec;
    std::vector<double> vecLigne;

    while(getline(file,ligne)){
        std::stringstream ss(ligne);
        vecLigne.clear();
        while(getline(ss,nb,',')){
            compteurDePoint = 0;
            numero =0;
            positionApresPoint = 1;
            for(int i=0;i<nb.length();i++){
                if(nb[i] == '.'){
                    if(compteurDePoint > 0){
                        std::cout << "erreur de point dans le fichier csv" << std::endl;
                        erreur=1;
                        break;//return 0;
                    }
                    else{
                        compteurDePoint ++;
                    }
                }
                else{
                    if(int(nb[i]) > 47 and int(nb[i]) < 58 ){ // si nb[i] est un chiffre entre 0 et 9
                        if(compteurDePoint == 0){
                            numero = numero * 10 + (nb[i]-48);
                        }
                        else{
                            numero = numero + ((nb[i]-48) * pow(10.0,(-positionApresPoint)));
                            positionApresPoint ++;
                        }
                    }
                    else{
                        if(int(nb[i]) != 32){ //si ce n est pas un espace
                            std::cout << "erreur caractere non autorisé" << std::endl;
                            //return 0;
                            erreur=1;
                            break;
                        }
                    }
                }
            }
            if(erreur==1){
                break;
            }
            //std::cout << numero << std::endl;
            vecLigne.push_back(numero);
        }
        if(erreur==1){
            vec.clear();
            data = vec;
            break;
        }
        vec.push_back(vecLigne);
    }
    data = vec;
}

void Csv::afficheData(){
    for(int i=0;i<data.size();i++){
        for (int j = 0; j < (data[i]).size(); j++){
            std::cout << data[i][j] << " ";
        }
        std::cout << std::endl;
    }
}

std::vector<std::vector<double>> Csv::getData(){
    return data;
}

// loaddata (std::istream d)

