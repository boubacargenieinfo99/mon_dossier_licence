formateNul1 :: Int -> String
formateNul1 numeroBis = show numeroBis ++ " est " ++ fmt numeroBis
    where
        fmt numero = case numero of
                    0 -> "nul"
                    _ -> "non nul"


formateNul2 :: Int -> String
formateNul2 numero = show numero ++ " est " ++ fmt numero
    where
        fmt 0 = "nul"
        fmt _ = "non nul" 

main ::IO()
main = do
    print (formateNul1 0)
    print (formateNul1 42)

    print (formateNul2 0)
    print (formateNul2 41)