fuzzyLength :: [a] -> String
fuzzyLength [] = "empty"
fuzzyLength [_] = "one"
fuzzyLength [_,_] = "two"
fuzzyLength _ = "many"

main :: IO ()
main = do
    print (fuzzyLength [])
    print (fuzzyLength [1])
    print (fuzzyLength [1,2])
    print (fuzzyLength [1..4])