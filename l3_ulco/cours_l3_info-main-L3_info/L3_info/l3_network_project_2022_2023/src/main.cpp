#include <iostream>
#include <vector>
#include <thread>
#include <fstream>
#include <string>
#include "sockpp/tcp_acceptor.h"
#include "sockpp/tcp_connector.h"

using namespace std;
using namespace std::chrono;

//cree variable globale
vector<pair<string,int>> adresses;
string ip = "127.0.0.1";
int compteur = 0;
int my_port = -1;

void client(){
    in_port_t port = adresses[compteur].second;
    string host = ip;
    sleep(10);
    //sockpp::tcp_connector conn({host, port}, seconds{5});
    sockpp::tcp_connector* c = new sockpp::tcp_connector({host, port}, seconds{5});
    while (!*c) {
        cerr << "Error connecting to server at "
             << sockpp::inet_address(host, port)
             << "\n\t" << c->last_error_str() << endl;
        delete c;
        sleep(2);
        c = new sockpp::tcp_connector({host, port}, seconds{5});
        //return ;
    }

    cout << "Created a connection from " << c->address() << endl;

    // Set a timeout for the responses
    if (!c->read_timeout(seconds(5))) {
        cerr << "Error setting timeout on TCP stream: "
             << c->last_error_str() << endl;
    }

    string s, sret;
    cout << "client " << my_port << " demande la table a " << port << endl;
    s = "Table";
    sret = "";
    char buf[512];
    while(sret == ""){ // tant qu on a pas recue la table
        if (c->write(s) != ssize_t(s.length())) {
            cerr << "Error writing to the TCP stream: "
                 << c->last_error_str() << endl;
            break;
        }
        /*c->read(buf,sizeof(512));
        for(int i=0;i<sizeof(buf);i++){
            sret += buf[i];
        }*/
        sret.resize(512);
        ssize_t n = c->read_n(&sret[0], 512);
        for(int i=0;i<n;i++){
            cout << sret[i];
        }
        //cout << sret;
        cout << endl;
    }



    compteur++;
    client();
}

void run_echo(sockpp::tcp_socket sock)
{
    ssize_t n;
    char buf[512];

    while ((n = sock.read(buf, sizeof(buf))) > 0) {
        if(strcmp(buf,"Table") == 0){
            char* chaine ;
            char * chaineBis;
            size_t fullSize = 0;
            int compteur=0;
            for_each(adresses.begin(),adresses.end(),[&chaine,&fullSize,&chaineBis,&compteur](pair<string,int> pa){
                char* adfirst = strcpy(new char[pa.first.length()+1],pa.first.c_str());
                char* adsecond = strcpy(new char[to_string(pa.second).length()+1],to_string(pa.second).c_str());
                if (compteur != 0){
                    chaineBis = (char *) malloc(fullSize);
                    chaineBis = chaine;
                }
                fullSize += strlen( adfirst ) + 1 + strlen(":") + 1 + strlen(adsecond) + 1 + strlen(";") +1;

                if(compteur != 0) {
                    chaine = (char *) malloc(fullSize + strlen(chaineBis));
                    chaine = chaineBis;
                }
                else{
                    chaine = (char *) malloc(fullSize);
                    compteur=1;
                }
                strcat(chaine,adfirst);
                strcat(chaine,":");
                strcat(chaine,adsecond);
                strcat(chaine,";");

                delete[] adfirst;
                delete[] adsecond;
            });

            for(int i=0;i<strlen(chaine);i++){
                buf[i] = chaine[i];
            }
            sock.write_n(buf,strlen(buf));
        }
    }

    cout << "Connection closed from " << sock.peer_address() << endl;
}

void server(in_port_t port){    //return true si tout c est bien passée
    sockpp::tcp_acceptor acc(port);

    if (!acc) {
        cerr << "Error creating the acceptor: " << acc.last_error_str() << endl;
        return ;
    }
    cout << "Awaiting connections on port " << port << "..." << endl;

    while (true) {
        sockpp::inet_address peer;

        // Accept a new client connection
        sockpp::tcp_socket sock = acc.accept(&peer);
        cout << "Received a connection request from " << peer << endl;

        if (!sock) {
            cerr << "Error accepting incoming connection: "
                 << acc.last_error_str() << endl;
        }
        else {
            // Create a thread and transfer the new stream to it.
            thread thr(run_echo, std::move(sock));
            thr.detach();
        }
    }
}


int main(int argc, char* argv[]) {
    // lire argv pour le port et le numero peer
    if(argc != 3){
        cout << "argument manquant" << endl;
        return 1;
    }

    in_port_t port = atoi(argv[1]);
    my_port = port;
    int num_peer = atoi(argv[2]);

    sockpp::initialize();

    // lire fichier data correspondant et remplir addresses;
    string name_repository = "../../data/peer_" + to_string(num_peer) + "/nodes.txt" ;
    ifstream file(name_repository);

    // 1 seul pour l instant
    string num_node;
    if (!file.is_open()) {
        cout << "erreur d ouverture du fichier " << name_repository << endl;
        return 1;
    }

    file >> num_node;
    while(file.good()) {
        adresses.push_back({num_node.substr(0, 9), stoi(num_node.substr(10, 13))});
        file >> num_node;
    }

    // cree des threads avec des mutex qui vont modifier adresses en fonctions des connaissances du noeud avec qui ont communique
    thread ser(server,port+num_peer);
    ser.detach();

    //vector<pair<string,int>>::iterator it;
    //it = adresses.begin();
    /*while(it !=adresses.end()){
        thread cli(client,it.second);
        ++it;
        if(it == adresses.end()){
            cli.join();
        }
    }*/
    thread cli(client);
    cli.join();

  return 0;
}
