#include "transporter.hpp"

Transporter::Transporter(const std::string& id, double speed,
                         const Coordinates& initialLocation,
                         std::initializer_list<Path> paths,
                         std::shared_ptr<Labor> labor)
        : Transportation(id, speed, initialLocation, paths), labor(labor) {}

const std::shared_ptr<Labor>& Transporter::getLabor() const {
    return labor;
}