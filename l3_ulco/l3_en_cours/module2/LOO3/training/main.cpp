#include "factory.hpp"
#include "zone.hpp"
#include "machine.hpp"
#include "transporter.hpp"
#include "labor.hpp"
#include "conveyor.hpp"

int main() {
    // Instanciation de la factory
    Factory inoxetum("Inoxetum");

    // Instanciation des zones
    auto zoneA = std::make_shared<Zone>("A");
    auto zoneB1 = std::make_shared<Zone>("B1");
    auto zoneB2 = std::make_shared<Zone>("B2");
    auto zoneC = std::make_shared<Zone>("C");

    // Ajout des sous-zones à la zone B
    zoneB1->addSubZone(zoneC);
    zoneB2->addSubZone(zoneC);

    // Ajout des zones à la factory
    inoxetum.addZone(zoneA);
    inoxetum.addZone(zoneB1);
    inoxetum.addZone(zoneB2);

    // Instanciation des machines
    auto machineM1 = std::make_shared<Machine>("M1", 30, 90);
    auto machineM2 = std::make_shared<Machine>("M2", 10, 120);
    auto machineM3 = std::make_shared<Machine>("M3", 0, 45);
    auto machineM4 = std::make_shared<Machine>("M4", 15, 30);
    auto machineM5 = std::make_shared<Machine>("M5", 20, 20);
    auto machineM6 = std::make_shared<Machine>("M6", 12, 150);
    auto machineM7 = std::make_shared<Machine>("M7", 5, 100);
    auto machineM8 = std::make_shared<Machine>("M8", 8, 75);

    // Ajout des machines aux zones correspondantes
    zoneA->addMachine(machineM1);
    zoneA->addMachine(machineM2);
    zoneB1->addMachine(machineM3);
    zoneB1->addMachine(machineM4);
    zoneB2->addMachine(machineM5);
    zoneB2->addMachine(machineM6);
    zoneB2->addMachine(machineM7);
    zoneC->addMachine(machineM8);

    // Instanciation des convoyeurs
    auto C1 = std::make_shared<Conveyor>("C1", 10, 100, 1.0, machineM5, machineM4);
    auto conveyorC2 = std::make_shared<Conveyor>("C2", 20, 50, 2, machineM5, machineM6);

    // Connexion des convoyeurs aux machines
    inoxetum.connectMachines(conveyorC1);
    inoxetum.connectMachines(conveyorC2);


    // Affichage des informations
    inoxetum.displayInformation();


    return 0;
}
