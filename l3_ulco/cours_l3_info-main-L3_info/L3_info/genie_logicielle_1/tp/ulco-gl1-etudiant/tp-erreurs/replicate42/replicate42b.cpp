#include <iostream>
#include <exception>
#include <vector>

enum class NumError { TooLow, TooHigh, Is37, IsNegative };

// Reads a number in stdin. Throws a NumError if 37 or negative.
int readPositiveButNot37() {
    std::string line;
    std::cout << "enter num: ";
    std::flush(std::cout);
    std::getline(std::cin, line);
    int num = stod(line);
    // TODO handle errors
    if(num == 37){
        throw NumError::Is37;
    }
    if(num < 0){
        throw NumError::IsNegative;
    }
    return num;
}

// Reads a number in stdin. Throws a NumError if too low or too high.
std::vector<int> replicate42(int n) {
    // TODO handle errors
    if(n <1 ){
        throw NumError::TooLow;
    }
    if(n > 42){
        throw NumError::TooHigh;
    }
    return std::vector<int>(n, 42);
}

int main() {

    // TODO handle errors
    
    try{
    // read number
    const int num = readPositiveButNot37();
    std::cout << "num = " << num << std::endl;
    // build vector
    const auto v = replicate42(num);

    // print vector
    for (const int x : v)
        std::cout << x << " ";
    std::cout << std::endl;

    }
    catch(NumError e){
        std::string msgError = "error : ";
        if(e == NumError::Is37){
            std::cout << msgError << "Is37";
        }
        if(e == NumError::IsNegative){
            std::cout << msgError << "negatif";
        }
        if(e == NumError::TooLow){
            std::cout << msgError << "too low";
        }
        if(e == NumError::TooHigh){
            std::cout << msgError << "too high";
        }
        return -1;
    }
    catch(std::exception er){
        std::cout << "error exception " << er.what() << std::endl;
    }
   catch(...){
    std::cout << "unknow error" << std::endl;
   }
    
    
    

    return 0;
}

