/**
 * @file openxum/core/common/abstract_engine.hpp
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2011-2024 Openxum Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPENXUM_CORE_COMMON_ABSTRACT_ENGINE_HPP
#define OPENXUM_CORE_COMMON_ABSTRACT_ENGINE_HPP

#include <nlohmann/json.hpp>

namespace openxum::core::common {

class AbstractEngine {
public:
  AbstractEngine() : _move_number(0) {}

  virtual ~AbstractEngine() = default;

  virtual const std::string &get_name() const = 0;

  virtual std::string hash() const = 0;

  virtual bool is_finished() const = 0;

  virtual void move(const nlohmann::json &move) = 0;

  unsigned int move_number() const { return _move_number; }

  virtual void parse(const std::string &str) = 0;

  virtual std::string to_string() const = 0;

protected:
  unsigned int _move_number;
};

}

#endif // OPENXUM_CORE_COMMON_ABSTRACT_ENGINE_HPP