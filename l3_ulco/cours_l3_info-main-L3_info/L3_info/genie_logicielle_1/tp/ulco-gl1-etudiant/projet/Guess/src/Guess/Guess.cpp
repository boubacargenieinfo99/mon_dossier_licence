#include <Guess/Guess.hpp>
#include <iostream>

void Game::newGame(){

}

void Game::newGame(int target){
    _target = target;
    _tries = 5;
}

void Game::play(int number,bool lastChance){
    if(lastChance && number != _target){
        resultat = Result::Lose;
    }
    else{
        if(number == _target){
            resultat = Result::Win;
        }
        else{
            if(number < _target){
                resultat = Result::Too_low;
            }
            else{
                resultat = Result::Too_high;
            }
        }
    }
}

bool Game::remainingTries(){
    if(_tries > 0){
        _tries --;
        return true;
    }
    else{
        return false;
    }
}

/*int saisieJoueur(){
    int saisie = -1;
    while(saisie < 0 or saisie > 100){
        std::cout << "number?" << std::endl;
        std::cin >> saisie;
        if(saisie < 0 or saisie > 100){
            std::cout << "invalid" << std::endl; 
        }
    }
}*/