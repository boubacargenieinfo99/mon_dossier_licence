#include "Logger.hpp"

#include <algorithm>
#include <iomanip>
#include <map>
#include <sstream>

void Logger::addItem(Level l, const std::string & m) {
  // implement Logger::addItem
  _items.push_back({l,m});
}

std::string Logger::reportByAdded() const {
  // Logger::reportByAdded
  std::string msg = "";
  for (const auto & [l,m]: _items){
    if(l == Level::Info){
      msg += "[I] ";
    }
    if(l == Level::Error){
      msg += "[E] ";
    }
    if(l == Level::Warning){
      msg += "[W] ";
    }
    msg += m + "\n";
  }
  return msg;
}

std::string Logger::reportByLevel() const {
  // TODO Logger::reportByLevel
  std::string msg = "";

  for(int c=0;c < 3;c++){
    for (auto & [l,m]: _items){
      if(l == Level::Info and c ==0){
          msg += "[I] " + m + "\n";
      }
      if(l == Level::Error and c == 2){
          msg += "[E] " + m + "\n";
      }
      if(l == Level::Warning and c == 1){
          msg += "[W] " + m + "\n";
      }
    }
  }
  return msg;
}

