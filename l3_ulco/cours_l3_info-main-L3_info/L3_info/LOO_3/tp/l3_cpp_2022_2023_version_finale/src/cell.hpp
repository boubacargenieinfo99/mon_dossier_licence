#ifndef TRAFFIC_CELL_HPP
#define TRAFFIC_CELL_HPP

#include <memory>

template <typename Data>
class Cell {
public:
  Cell(const std::shared_ptr<Data> &data, Cell *next)
    : _data(data), _next(next) {
  }

  Cell<Data> *get_next() const { return _next; }

  const std::shared_ptr<Data> &get_data() { return _data; }

  void set_next(Cell *cell) { _next = cell; }

  ~Cell() = default;

private:
  std::shared_ptr<Data> _data;
  Cell<Data> *_next;
};

#endif //TRAFFIC_CELL_HPP
