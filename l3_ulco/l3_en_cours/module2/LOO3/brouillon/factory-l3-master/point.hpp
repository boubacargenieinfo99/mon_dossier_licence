#ifndef POINT_HPP
#define POINT_HPP

#include "coordinates.hpp"

class Point : public Coordinates {
public:
    Point(double x, double y) : x(x), y(y) {}

    double getX() const { return x; }
    double getY() const { return y; }

private:
    double x;
    double y;
};

#endif
