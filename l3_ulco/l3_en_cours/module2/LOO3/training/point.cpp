#include "point.hpp"
#include <iostream>

Point::Point(double x, double y) : x(x), y(y) {}

void Point::displayInfo() const {
    std::cout << "Point Coordinates - (x, y): (" << x << ", " << y << ")" << std::endl;
}