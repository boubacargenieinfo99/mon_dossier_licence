"use strict";

const Color = {NONE: -1, BLACK: 0, WHITE: 1, RED: 2};

export default Color;
