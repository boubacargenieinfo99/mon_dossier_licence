#ifndef TRAFFIC_MAP_HPP
#define TRAFFIC_MAP_HPP

#include "inputs.hpp"
#include "link.hpp"
#include "list.hpp"
#include "junction.hpp"

#include <valarray>

class Map {
public:
  Map();

  ~Map();

  void add_link(const std::shared_ptr<Link> &link);

  void add_node(const std::shared_ptr<Node> &node);

  void put_vehicle_into_node(Vehicle *vehicle, const std::string &node_id, unsigned int time) const;

  void run(unsigned int time);

  void run(unsigned int time, Inputs &inputs);

  void run_link(unsigned int time);

  void run_node(unsigned int time);

private:
  List<Link> _links;
  List<Node> _nodes;
  unsigned int _input_index;
};

#endif //TRAFFIC_MAP_HPP
