#include "way.hpp"

namespace traffic {

Way::Way(unsigned int length) : _cells(length / _cell_length) {}

bool Way::is_free() const {
  return _cells[0].is_free();
}

void Way::push_vehicle(unsigned int t, std::unique_ptr<Vehicle> &vehicle) {
  _cells[0].push_vehicle(t, vehicle);
}

bool Way::ready_to_exit() const {
  return not _cells[_cells.size() - 1].is_free();
}

void Way::run(unsigned int t) {
  for (unsigned int index = _cells.size(); index > 0; --index) {
    if (not _cells[index - 1].is_free() and _cells[index].is_free()) {
      _cells[index].push_vehicle(t, _cells[index - 1].vehicle());
    }
  }
}

}