
// construction d'un cube de côté c centrée à l'origine
// Chaque face du cube est d'une couleur différente
// prise parmi les 3 primaires et leur complémentaire
function cubeColore(c){
    const geometry = new THREE.BufferGeometry();
    const vertices = new Float32Array( [
	// face supérieure
	-c/2.0, c/2.0,- c/2.0,  -c/2.0, c/2.0, c/2.0,  c/2.0, c/2.0, c/2.0,  -c/2.0, c/2.0, -c/2.0,  c/2.0, c/2.0, c/2.0,  c/2.0, c/2.0, -c/2.0,
	// face inférieure
	-c/2.0, -c/2.0, -c/2.0,  c/2.0, -c/2.0, -c/2.0,  c/2.0, -c/2.0, c/2.0,  -c/2.0, -c/2.0, -c/2.0,  c/2.0, -c/2.0, c/2.0,  -c/2.0, -c/2.0, c/2.0,
	// face gauche
	-c/2.0, c/2.0, c/2.0,  -c/2.0, c/2.0, -c/2.0,  -c/2.0, -c/2.0, c/2.0,  -c/2.0, -c/2.0, c/2.0,  -c/2.0, c/2.0, -c/2.0,  -c/2.0, -c/2.0, -c/2.0,
	// face droite
	c/2.0, c/2.0, c/2.0,  c/2.0, -c/2.0, c/2.0,  c/2.0, c/2.0, -c/2.0,  c/2.0, c/2.0, -c/2.0,  c/2.0, -c/2.0, c/2.0, c/2.0, -c/2.0, -c/2.0,
	// face avant
	-c/2.0, c/2.0, c/2.0,   -c/2.0, -c/2.0, c/2.0,  c/2.0, c/2.0, c/2.0,  c/2.0, c/2.0, c/2.0,  -c/2.0, -c/2.0, c/2.0, c/2.0, -c/2.0, c/2.0,
	// face arrière
	-c/2.0, c/2.0, -c/2.0,  c/2.0, c/2.0, -c/2.0,  -c/2.0, -c/2.0, -c/2.0,  -c/2.0, -c/2.0, -c/2.0,  c/2.0, c/2.0, -c/2.0,  c/2.0, -c/2.0, -c/2.0
    ] );
    
    const normals = new Float32Array( [
	// face supérieure - normale orientée selon Oy
	0, 1, 0,  0, 1, 0,  0, 1, 0,  0, 1, 0,  0, 1, 0,  0, 1, 0,
	// face inférieure - normale orientée selon -Oy
	0, -1, 0,  0, -1, 0,  0, -1, 0,  0, -1, 0,  0, -1, 0,  0, -1, 0,
	// face gauche - normale orientée selon -Ox
	-1, 0, 0,  -1, 0, 0,  -1, 0, 0,  -1, 0, 0,  -1, 0, 0,  -1, 0, 0,
	// face droite - normale orientée selon Ox
	1, 0, 0,  1, 0, 0,  1, 0, 0,  1, 0, 0,  1, 0, 0,  1, 0, 0,
	// face avant - normale orientée selon Oz
	0, 0, 1,  0, 0, 1,  0, 0, 1,  0, 0, 1,  0, 0, 1,  0, 0, 1,
	// face arrière - normale orientée selon -Oz
	0, 0, -1,  0, 0, -1,  0, 0, -1,  0, 0, -1,  0, 0, -1,  0, 0, -1
    ]);
    
    const colors = new Float32Array( [
	// face supérieure rouge
	1, 0, 0,  1, 0, 0,  1, 0, 0, 1, 0, 0,  1, 0, 0,  1, 0, 0,
	// face inférieure cyan
	0, 1, 1,  0, 1, 1,  0, 1, 1, 0, 1, 1,  0, 1, 1,  0, 1, 1,
	// face gauche vert
	0, 1, 0,  0, 1, 0,  0, 1, 0,  0, 1, 0,  0, 1, 0,  0, 1, 0,
	// face droite magenta
	1, 0, 1,  1, 0, 1,  1, 0, 1,  1, 0, 1,  1, 0, 1,  1, 0, 1,
	// face avant bleu
	0, 0, 1,  0, 0, 1,  0, 0, 1,  0, 0, 1,  0, 0, 1,  0, 0, 1,
	// face arrière jaune
	1, 1, 0,  1, 1, 0,  1, 1, 0,  1, 1, 0,  1, 1, 0,  1, 1, 0
    ]);
    
    geometry.setAttribute( 'position', new THREE.Float32BufferAttribute( vertices, 3 ) );
    geometry.setAttribute( 'normal', new THREE.Float32BufferAttribute( normals, 3 ) );
    geometry.setAttribute( 'color', new THREE.Float32BufferAttribute( colors, 3 ) );

    const lambert = new THREE.MeshLambertMaterial( {
	color: 0xffffff, // reflectance diffuse du materiau
	side: THREE.DoubleSide,// tenir compte des deux faces
	vertexColors: true // il y a des couleurs en chaque sommet
    });
    
    const mesh = new THREE.Mesh( geometry, lambert );
    return mesh;
}

//function repere
function repere(size){
	const geometry=new THREE.BufferGeometry();
	const vertices=new Float32Array([
		//ox
		-size,0,0, size,0,0,
		size,0,0, 0,0,0,
		//oy
		0, -size,0, 0,size,0,
		0,size,0, 0,0,0,
		//oz
		0, 0,-size, 0, 0, size

	]);

	const colors=new Float32Array([
		1,0,0,1,0,0,
		1,0,0,1,0,0,
		0,0,1,0,0,1,
		0,0,1,0,0,1,
		0,1,0,0,1,0,

	]);
	geometry.setAttribute('position',new THREE.Float32BufferAttribute(vertices,3));
	geometry.setAttribute('color',new THREE.Float32BufferAttribute(colors,3));
	const material=new THREE.LineBasicMaterial({
		vertexColors: true,side: THREE.DoubleSide,color:0xffffff
	});
	const mesh=new THREE.Line(geometry,material);
	return mesh;
}
function repereFleche(size) {
	const group = new THREE.Group();

	// Axe X (positif)
	const arrowXPositive = new THREE.ArrowHelper(new THREE.Vector3(1, 0, 0), new THREE.Vector3(0, 0, 0), size, 0xff0000);
	group.add(arrowXPositive);

	// Axe X (négatif)
	const arrowXNegativeGeometry = new THREE.BufferGeometry().setFromPoints([new THREE.Vector3(0, 0, 0), new THREE.Vector3(-size, 0, 0)]);
	const arrowXNegative = new THREE.Line(arrowXNegativeGeometry, new THREE.LineBasicMaterial({ color: 0xff0000 }));
	group.add(arrowXNegative);

	// Axe Y (positif)
	const arrowYPositive = new THREE.ArrowHelper(new THREE.Vector3(0, 1, 0), new THREE.Vector3(0, 0, 0), size, 0x00ff00);
	group.add(arrowYPositive);

	// Axe Y (négatif)
	const arrowYNegativeGeometry = new THREE.BufferGeometry().setFromPoints([new THREE.Vector3(0, 0, 0), new THREE.Vector3(0, -size, 0)]);
	const arrowYNegative = new THREE.Line(arrowYNegativeGeometry, new THREE.LineBasicMaterial({ color: 0x00ff00 }));
	group.add(arrowYNegative);

	// Axe Z (positif)
	const arrowZPositive = new THREE.ArrowHelper(new THREE.Vector3(0, 0, 1), new THREE.Vector3(0, 0, 0), size, 0x0000ff);
	group.add(arrowZPositive);

	// Axe Z (négatif)
	const arrowZNegativeGeometry = new THREE.BufferGeometry().setFromPoints([new THREE.Vector3(0, 0, 0), new THREE.Vector3(0, 0, -size)]);
	const arrowZNegative = new THREE.Line(arrowZNegativeGeometry, new THREE.LineBasicMaterial({ color: 0x0000ff }));
	group.add(arrowZNegative);

	return group;
}
function cubeRepere(c,positionX){
	const cube=cubeColore(c);

	cube.position.x=positionX;

	const repere=repereFleche(c+3.0);

	repere.position.x=positionX;

	const groupe=new THREE.Group();
	groupe.add(cube);
	groupe.add(repere);
	return groupe;
}


