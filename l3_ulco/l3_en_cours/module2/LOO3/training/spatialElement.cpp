#include "spatialElement.hpp"
#include "geometry.hpp"

SpatialElement::SpatialElement(int id, std::unique_ptr<Geometry> geometry)
        : id(id), geometry(std::move(geometry)) {}

void SpatialElement::displayInfo() const {
    std::cout << "Spatial Element ID: " << id << std::endl;
    geometry->displayInfo();
}
