"use strict";

import Coordinates from './coordinates.mjs';
import OpenXum from '../../openxum/index.mjs';

class Move extends OpenXum.Move {
  constructor(t, c,fromX, fromY, toX, toY) {
    super();
    this._type = t;
    this._color = c;
    this.fromX = fromX;
    this.fromY = fromY;
    this.toX = toX;
    this.toY = toY;
  }

// public methods
    decode(str) {
    }

    encode() {
    }

    from_object(data) {
    }

    to_object() {
    }

    to_string() {
    }
}

export default Move;
