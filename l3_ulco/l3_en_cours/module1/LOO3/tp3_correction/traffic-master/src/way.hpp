#ifndef TRAFFIC_WAY_HPP
#define TRAFFIC_WAY_HPP

#include <vector>

#include "cell.hpp"

namespace traffic {

class Way {
public:
  Way(unsigned int length);

  bool is_free() const;

  void push_vehicle(unsigned int t, std::unique_ptr<Vehicle> &vehicle);

  bool ready_to_exit() const;

  void run(unsigned int t);

  std::unique_ptr<Vehicle>& vehicle_to_begin() { return _cells[0].vehicle(); }

  std::unique_ptr<Vehicle>& vehicle_to_end() { return _cells[_cells.size() - 1].vehicle(); }

private:
  static const unsigned int _cell_length{10};
  std::vector<Cell> _cells;
};

}

#endif //TRAFFIC_WAY_HPP
