gaussien=randn(1,10);
figure(1);
plot(gaussien);
title('Representation graphique dun processus');
xlabel('Nombre dechantillons');
ylabel('Valeurs du proccessus ');

gaussien=randn(1,100);
figure(2);
plot(gaussien);
title('Representation graphique dun processus');
xlabel('Nombre dechantillons');
ylabel('Valeurs du proccessus ');

gaussien=randn(1,1000);
figure(3);
plot(gaussien);
title('Representation graphique dun processus');
xlabel('Nombre dechantillons');
ylabel('Valeurs du proccessus ');

help gaussien

disp('Histogramme');
M=10;
figure(2);
hist(gaussien,M);

figure(3);
dx=-2.9:0.1:2.9;
hist(gaussien,dx);

%paramètre de la fonction 
a=std(gaussien); %ecart type 
m=min(gaussien);%moyenne
x=-50:0.1:50;

f=1/(a*sqrt(2*pi)) * exp(-0.5*((x-m)/a).^2);

figure;
plot(x,f,'LineWidth',2);
title('fonction gaussien' );
xlabel('x');
ylabel('f(x)');
grid on;
