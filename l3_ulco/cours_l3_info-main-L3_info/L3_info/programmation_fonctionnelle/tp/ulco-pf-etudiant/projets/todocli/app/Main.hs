-- import Text.Read
import System.Environment
-- import System.IO
import Data.List

type Task = (Int, Bool, String)     -- id, done?, label
type Model = (Int, [Task])          -- next id, tasks

test :: Model -> Model
test (nextI, tasks) = (nextI, reverse tasks)

menu :: String -> IO()
menu nomFichier = do
    print '>'
    commande <- getLine
    if commande == "EXIT"
        then print ""
        else do
            if commande == "PRINT"
                then do
                    contents <- readFile nomFichier

                    let model1 = read contents
                        model2 = test $! model1
                    print model2
                    menu nomFichier
                else menu nomFichier



main :: IO ()
main = do
    args <- getArgs
    let nbArgs = length args
    if nbArgs==1
    then do
        contents <- readFile (head args)

        let model1 = read contents
            model2 = test $! model1
        {-print model2
        mapM_ print (snd model2)-}
        --menu (head args)
        menu (head args)

        
    else print "erreur 0 ou trop de parametres"
            
