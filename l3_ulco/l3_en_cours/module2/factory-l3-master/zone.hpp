#ifndef TP4_ZONE_HPP
#define TP4_ZONE_HPP
#include <vector>
#include <iostream>
#include "spatialElement.hpp"
using namespace std;

class Zone:public SpacialElement{

public:
    Zone(){super();}
    void addSousZone(Zone& szone){
        sous_zones.push_back(szone);
    }
    void addMachine(Machine& machine){
        machines.push_back(machine);
    }
    void addLabor(Labor& labor);

private:
    vector<Zone> sous_zones;
    vector<Machine>machines;
    vector<Labor> labors;

};

#endif //TP4_ZONE_HPP
