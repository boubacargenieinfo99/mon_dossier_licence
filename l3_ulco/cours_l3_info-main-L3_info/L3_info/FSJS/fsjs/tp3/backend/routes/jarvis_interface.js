var Express = require('express');
var router = Express.Router();

const jarvisController = require('../controllers/jarvisController');

router.get('/',jarvisController.home);

router.get('/login',jarvisController.login);

router.post('/login',jarvisController.connected);

router.get('/logout',jarvisController.logout);



module.exports = router;