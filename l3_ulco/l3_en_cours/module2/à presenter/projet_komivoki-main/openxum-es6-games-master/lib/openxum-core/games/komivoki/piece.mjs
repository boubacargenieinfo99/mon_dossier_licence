"use strict";

class Piece {
    constructor(c, co, l) {
        this._color = c;
        this._coordinates = co;
        this._level = l;
        this._stack = [this];
    }
    stackPiece(piece) {
        if (this._stack.length < 3) {
          this._stack.push(piece);
        } else {
          throw new Error('Stack limit reached');
        }
    }
    unstackPiece() {
        return this._stack.pop();
    }
    
    getStack() {
        return this._stack;
    }


    set_coordinates(c) {
        this._coordinates = c;
    }

    color() {
        return this._color;
    }

    coordinates() {
        return this._coordinates;
    }

    level() {
        return this._level;
    }

    clone() {
        return new Piece(this._color, this._coordinates, this._level);
    }
}

export default Piece;
