#ifndef ZONE_HPP
#define ZONE_HPP

#include <string>
#include <vector>
#include <memory>
#include <initializer_list>
#include "spatialElement.hpp"
#include "machine.hpp"

class Zone : public SpatialElement {
public:
    Zone(std::string name, std::unique_ptr<Geometry> geometry, std::initializer_list<std::shared_ptr<Machine>> machines)
            : SpatialElement(name, std::move(geometry)), name(name), machines(machines) {}

private:
    std::string name;
    std::vector<std::shared_ptr<Machine>> machines;
    std::vector<Zone> subZones;
};

#endif
