"use strict";

import Color from './color.mjs';
import Coordinates from './coordinates.mjs';
import OpenXum from '../../openxum/index.mjs';
import Phase from './phase.mjs';

class Move extends OpenXum.Move {
    constructor(t, c, f, to, l) {
        super();
        this._type = t;
        this._color = c;
        this._from = f;
        this._to = to;
        this._list = l;
    }

// public methods
    color() {
        return this._color;
    }

    decode(str) {
        let type = str.substring(0, 2);

        this._color = str.charAt(2) === 'B' ? Color.BLACK : Color.WHITE;
        if (type === 'PP') {
            this._type = Phase.PUT_DVONN_PIECE;
            this._from = new Coordinates(str.charAt(3), parseInt(str.charAt(4)));
        } else if (type === 'Pp') {
            this._type = Phase.PUT_PIECE;
            this._from = new Coordinates(str.charAt(3), parseInt(str.charAt(4)));
        } else if (type === 'Ms') {
            this._type = Phase.MOVE_STACK;
            this._from = new Coordinates(str.charAt(3), parseInt(str.charAt(4)));
            this._to = new Coordinates(str.charAt(5), parseInt(str.charAt(6)));
            this._list = [];
            for (let index = 0; index < (str.length - 8) / 2; ++index) {
                this._list.push(new Coordinates(str.charAt(8 + 2 * index),
                    parseInt(str.charAt(9 + 2 * index))));
            }
        }
    }

    encode() {
        if (this._type === Phase.PUT_DVONN_PIECE) {
            return 'PP' + (this._color === Color.BLACK ? 'B' : 'W') + this._from.to_string();
        } else if (this._type === Phase.PUT_PIECE) {
            return 'Pp' + (this._color === Color.BLACK ? 'B' : 'W') + this._from.to_string();
        } else if (this._type === Phase.MOVE_STACK) {
            let str = 'Ms' + (this._color === Color.BLACK ? 'B' : 'W') + this._from.to_string() +
                this._to.to_string() + '[';

            for (let i = 0; i < this._list.length; ++i) {
                str += this._list[i].to_string();
            }
            str += ']';
            return str;
        }
    }

    from() {
        return this._from;
    }

    from_object(data) {
        this._type = data.type;
        this._color = data.color;
        this._from = new Coordinates(data.from.letter, data.from.number);
        this._to = new Coordinates(data.to.letter, data.to.number);
        this._list = data.list.map((e) => {
            return new Coordinates(e.letter, e.number);
        });
    }

    list() {
        return this._list;
    }

    to() {
        return this._to;
    }

    to_object() {
        return {
            type: this._type,
            color: this._color,
            from: this._from === null ? {letter: -1, number: -1} : {
                letter: this._from._letter,
                number: this._from._number
            },
            to: this._to === null ? {letter: -1, number: -1} : {letter: this._to._letter, number: this._to._number},
            list: this._list.map((e) => {
                return {letter: e._letter, number: e._number};
            })
        };
    }

    to_string() {
        if (this._type === Phase.PUT_DVONN_PIECE) {
            return 'put ' + (this._color === Color.BLACK ? 'black' : 'white') + ' dvonn piece at ' +
                this._from.to_string();
        } else if (this._type === Phase.PUT_PIECE) {
            return 'put ' + (this._color === Color.BLACK ? 'black' : 'white') + ' piece at ' +
                this._from.to_string();
        } else if (this._type === Phase.MOVE_STACK) {
            let str = 'move stack from ' + this._from.to_string() + ' to ' + this._to.to_string();

            if (this._list.length > 0) {
                str += ' and remove pieces at ( ';
                for (let i = 0; i < this._list.length; ++i) {
                    str += this._list[i].to_string() + ' ';
                }
                str += ')';
            }
            return str;
        }
    }

    type() {
        return this._type;
    }
}

export default Move;
