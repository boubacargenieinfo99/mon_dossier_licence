#include "vehicle.hpp"

struct Inputs{

    //constructeur

    Inputs(Vehicle **Vehicles,unsigned int *times,char **node_ids);

    //destructeur

    ~Inputs();

    //attributs

    Vehicle **vehicles;
    unsigned int *times;
    char **node_ids;

    //fonction

};