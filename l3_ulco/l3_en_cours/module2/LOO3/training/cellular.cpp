#include "cellular.hpp"
#include "cell.hpp"
#include <iostream>

Cellular::Cellular(int width, int height) : width(width), height(height) {
    // initialisation avec au moins une cellule
    cells.push_back(std::make_unique<Cell>(0, 0));
}

void Cellular::addCell(std::unique_ptr<Cell> cell) {
    cells.push_back(std::move(cell));
}

void Cellular::displayInfo() const {
    std::cout << "Cellular Geometry" << std::endl;
    std::cout << "Width: " << width << ", Height: " << height << std::endl;
    for (const auto& cell : cells) {
        cell->displayInfo();
    }
}