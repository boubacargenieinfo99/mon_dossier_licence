alphabet_ADN = {0 : "-",1 : "A",2 : "C",3 : "G",4 : "T"}

def charger_sequence(nom_fichier):
    fichier = open(nom_fichier,"r")
    sequence_ADN = fichier.read()
    if not (verif_sequence(sequence_ADN)):
        print("erreur dans le fichier " , nom_fichier)
        fichier.close()
        return 0
    fichier.close()
    return sequence_ADN

def charger_matrice(nom_fichier):
    fichier = open(nom_fichier,"r")
    matrice = {" " : {},"-" : {},"A" : {},"C" : {},"G" : {},"T" : {}}
    col = {" " : " " , "-" : "-" , "A" : "A" , "C" : "C" , "G" : "G" , "T" : "T"}
    for li in matrice:
        matrice[li] = col.copy()
    iterator_ligne = 0
    val = 0
    for ligne in fichier.readlines(): #on prend les lignes sans le \n
        if iterator_ligne == 0:
            iterator_ligne = 0
        else :
            iterator_letter = 0
            for t in range(len(ligne.split())):
                val = ligne.split()[t]
                matrice[(alphabet_ADN[iterator_ligne-1])][(alphabet_ADN[iterator_letter])] = int(val)
                iterator_letter += 1
        iterator_ligne += 1
    return matrice


def affiche_matrice_substitution(matrice):
    for h in matrice:
        print(h,end=" ")
        for jo in matrice[h]:
            txt = "{:^3}"
            print(txt.format(matrice[h][jo]),end=" ")
        print("")

def affiche_matrice_score(matrice,sequence_une,sequence_deux):
    print("{:^4}{:^3}".format(" ","-"),end=" ")
    for i in sequence_une:
        print("{:^3}".format(i),end=" ")
    print("")
    for h in range(len(matrice)):
        if h == 0:
            print("{:^3}".format("-"),end=" ")
        if h != 0:
            print("{:^3}".format(sequence_deux[h-1]),end=" ")
        for jo in range(len(matrice[h])):
            txt = "{:^3}"
            print(txt.format(matrice[h][jo]),end=" ")
        print("")

def verif_sequence(sequence):
    for i in sequence:
       if not(i in alphabet_ADN.values()):
           return False
    return True
        
def donne_matrice_score(sequence_une,sequence_deux,matrice_substitution):
    if not (verif_sequence(sequence_une)) or not (verif_sequence(sequence_deux)):
        print("erreur dans une des deux sequence")
        return 0
    matrice_score = []
    # sequence_une sens ->      matrice substitution[ligne][colonne]  
    # sequence_deux sens v (vers le bas)
    for lin in range(len(sequence_deux)+1):
        ligne = []
        for col in range(len(sequence_une)+1):
            if lin == 0:
                if col == 0:
                    ligne.append((matrice_substitution["-"]["-"]))
                else:
                    ligne.append(ligne[col-1] + matrice_substitution["-"][sequence_une[col-1]])
            else:
                if col == 0:
                    ligne.append(matrice_score[lin-1][col] + matrice_substitution[sequence_deux[lin-1]]["-"])
                else:
                    max:int = matrice_score[lin-1][col-1] + matrice_substitution[sequence_deux[lin-1]][sequence_une[col-1]] # mismatch or not mismatch
                    if (ligne[col-1] + (ligne[0] - matrice_score[lin-1][0])) > max: #del
                        max =ligne[col-1] + (matrice_score[0][col] - matrice_score[0][col-1])
                    if (matrice_score[lin-1][col] + (ligne[0] - matrice_score[lin-1][0])) > max: #ins
                        max = matrice_score[lin-1][col] + (ligne[0] - matrice_score[lin-1][0])
                    ligne.append(max)
        matrice_score.append(ligne.copy())
    return matrice_score

def calcul_sequence(sequence_une,sequence_deux,matrice_substitution,matrice_score):
    al_adn1,al_adn2 = "",""
    origine = {"x":0,"y":0}
    case_actuel = {"x":len(matrice_score[len(matrice_score)-1])-1,"y":len(matrice_score)-1}
    while not (case_actuel["x"] == 0 and case_actuel["y"] == 0):
        if case_actuel["x"] > 0 and case_actuel["y"] > 0:
            if matrice_score[case_actuel["y"]][case_actuel["x"]] == (matrice_score[case_actuel["y"]-1][case_actuel["x"]-1] + matrice_substitution[sequence_deux[case_actuel["y"]-1]][sequence_une[case_actuel["x"]-1]]):    # substitution ou identite
                al_adn1 = sequence_une[case_actuel["x"]-1] + al_adn1
                al_adn2 = sequence_deux[case_actuel["y"]-1] + al_adn2
                case_actuel["x"] = case_actuel["x"] - 1
                case_actuel["y"] = case_actuel["y"] - 1
            else:
                if matrice_score[case_actuel["y"]][case_actuel["x"]] == (matrice_score[case_actuel["y"]-1][case_actuel["x"]] + (matrice_score[case_actuel["y"]][0] - matrice_score[case_actuel["y"]-1][0])):     # ins
                    al_adn1 = "-" + al_adn1
                    al_adn2 = sequence_deux[case_actuel["y"]-1] + al_adn2
                    case_actuel["y"] = case_actuel["y"] - 1
                else:
                    if matrice_score[case_actuel["y"]][case_actuel["x"]] == (matrice_score[case_actuel["y"]][case_actuel["x"]-1] + (matrice_score[0][case_actuel["x"]] - matrice_score[0][case_actuel["x"]-1])):      # del
                        al_adn1 = sequence_une[case_actuel["x"]-1] + al_adn1
                        al_adn2 = "-" + al_adn2
                        case_actuel["x"] = case_actuel["x"] - 1
                    else:
                        print("erreur calcul sequence")
        else:
            if case_actuel["x"] > 0:
                if matrice_score[case_actuel["y"]][case_actuel["x"]] == (matrice_score[case_actuel["y"]][case_actuel["x"]-1] + (matrice_score[0][case_actuel["x"]] - matrice_score[0][case_actuel["x"]-1])):      # del
                        al_adn1 = sequence_une[case_actuel["x"]-1] + al_adn1
                        al_adn2 = "-" + al_adn2
                        case_actuel["x"] = case_actuel["x"] - 1
                else:
                    print("erreur calcul sequence")
            else:
                if case_actuel["y"] > 0:
                    if matrice_score[case_actuel["y"]][case_actuel["x"]] == (matrice_score[case_actuel["y"]-1][case_actuel["x"]] + (matrice_score[case_actuel["y"]][0] - matrice_score[case_actuel["y"]-1][0])):     # ins
                        al_adn1 = "-" + al_adn1
                        al_adn2 = sequence_deux[case_actuel["y"]-1] + al_adn2
                        case_actuel["y"] = case_actuel["y"] - 1
                    else:
                       print("erreur calcul sequence") 
                else:
                    print("erreur calcul sequence")
    return al_adn1,al_adn2

nom_fichier_adn_1 = "Fichiers/dna1.txt"
nom_fichier_adn_2 = "Fichiers/dna2.txt"
nom_fichier_matrice = "Fichiers/mat2.txt"
if charger_sequence(nom_fichier_adn_1) != 0 and charger_sequence(nom_fichier_adn_2):
    sequence_ADN_1 = charger_sequence(nom_fichier_adn_1)
    sequence_ADN_2 = charger_sequence(nom_fichier_adn_2)
    print("sequence adn 1 : ",sequence_ADN_1)
    print("sequence adn 2 : ",sequence_ADN_2)
    matrice_substitution = charger_matrice(nom_fichier_matrice)
    print("\nmatrice de substitution")
    affiche_matrice_substitution(matrice_substitution)
    matrice_score = donne_matrice_score(sequence_ADN_1,sequence_ADN_2,matrice_substitution)
    print("\nmatrice score")
    affiche_matrice_score(matrice_score,sequence_ADN_1,sequence_ADN_2)
    al_adn1,al_adn2 = calcul_sequence(sequence_ADN_1,sequence_ADN_2,matrice_substitution,matrice_score)
    print("")
    print("al_adn1 = ", al_adn1)
    print("al_adn2 = ",al_adn2)
    print("")