"use strict";

import Komivoki from '../../../openxum-core/games/komivoki/index.mjs';
import AI from '../../../openxum-ai/generic/index.mjs';
import Gui from './gui.mjs';
import Manager from './manager.mjs';

export default {
    Gui: Gui,
    Manager: Manager,
    Settings: {
        ai: {
            mcts: AI.RandomPlayer // TODO: MCTSPlayer
        },
        colors: {
            first: Komivoki.Color.BLACK,
            init: Komivoki.Color.BLACK,
            list: [
                {key: Komivoki.Color.BLACK, value: 'black'},
                {key: Komivoki.Color.WHITE, value: 'white'}
            ]
        },
        modes: {
            init: Komivoki.GameType.STANDARD,
            list: [
                {key: Komivoki.GameType.STANDARD, value: 'standard'}
            ]
        },
        opponent_color(color) {
            return color === Komivoki.Color.BLACK ? Komivoki.Color.WHITE : Komivoki.Color.BLACK;
        },
        types: {
            init: 'ai',
            list: [
                {key: 'gui', value: 'GUI'},
                {key: 'ai', value: 'AI'},
                {key: 'online', value: 'Online'},
                {key: 'offline', value: 'Offline'}
            ]
        }
    }
};
