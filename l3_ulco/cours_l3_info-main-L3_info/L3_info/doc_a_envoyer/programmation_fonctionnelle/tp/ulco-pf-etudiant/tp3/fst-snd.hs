myFst :: (a,b) -> a
myFst (a,_) = a 

mySnd :: (a,b) -> b
mySnd (_,b) = b

myFst3 :: (a,b,c) -> a
myFst3 (a,_,_) = a

main :: IO ()
main = do
    print (myFst ("foo",1))
    print (mySnd ("foo",1))
    print (myFst3 ("foo",1,'a'))