require = require('esm')(module, {mode: 'auto', cjs: true});

const core = require('../../../lib/openxum-core').default;
const ai = require('../../../lib/openxum-ai').default;

let e = new core.Dvonn.Engine(core.Dvonn.GameType.STANDARD, core.Dvonn.Color.RED);
let p1 = new ai.Generic.RandomPlayer(core.Dvonn.Color.BLACK, core.Dvonn.Color.WHITE, e);
let p2 = new ai.Generic.RandomPlayer(core.Dvonn.Color.WHITE, core.Dvonn.Color.BLACK, e);
let p = p1;
let moves = [];

while (!e.is_finished()) {
    let move = p.move();

    moves.push(move);
    e.move(move);
    p = p === p1 ? p2 : p1;
}

console.log("Winner is " + (e.winner_is() === core.Dvonn.Color.BLACK ? "black" : "white"));
for (let index = 0; index < moves.length; ++index) {
    console.log(moves[index].to_string());
}
