#include "buffer.hpp"
#include <iostream>

Buffer::Buffer(int capacity) : SpatialElement(), capacity(capacity) {}

int Buffer::getCapacity() const {
    return capacity;
}

void Buffer::displayInfo() const {
    std::cout << "Buffer - Capacity: " << capacity << std::endl;
}
