fibo :: Int -> Int
fibo numero = case numero of
                0 -> 0
                1 -> 1
                _ -> (fibo (numero + (-1)) + fibo (numero + (-2)))

main :: IO()
main = do
    print (fibo 0)
    print (fibo 1)
    print (fibo 2)
    print (fibo 3)
    print (fibo 10)