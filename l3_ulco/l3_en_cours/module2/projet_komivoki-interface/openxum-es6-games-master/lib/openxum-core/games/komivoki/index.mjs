// lib/openxum-core/games/newgame/index.mjs
"use strict";
import Engine from './engine.mjs';
import GameType from './game_type.mjs';
import MoveType from './move_type.mjs';
import Color from './color.mjs';
import Piece from './piece.mjs';
import Coordinates from './coordinates.mjs';

// import ...

export default {
  Coordinates: Coordinates,
  Engine: Engine,
  GameType: GameType,
  MoveType: MoveType,
  Color: Color,
  Piece: Piece,
}
