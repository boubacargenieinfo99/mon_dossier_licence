INCLUDE_DIRECTORIES(
        ${CMAKE_SOURCE_DIR}/src/lib
        ${RESTBED_INCLUDE_DIRS}
        /opt/homebrew/include)

LINK_DIRECTORIES(${RESTBED_LIBRARY_DIRS})

CONFIGURE_FILE(version.hpp.in
        ${OPENXUM_WS_CPP_BINARY_DIR}/src/app/version.hpp)

ADD_EXECUTABLE(openxum-ws-cpp-main main.cpp games.hpp)

TARGET_LINK_LIBRARIES(openxum-ws-cpp-main openxum-core-games-kamisado openxum-core-games-kikotsoka
        openxum-core-games-kikotsoka-polyomino openxum-core-games-komivoki openxum-ai-common
        openxum-ai-specific-kamisado
        openxum-ai-specific-kikotsoka
        openxum-ai-specific-kikotsoka-polyomino
        openxum-ai-specific-komivoki
        restbed
        ${RESTBED_LIBRARIES} ${OPENSSL_LIBRARIES})
