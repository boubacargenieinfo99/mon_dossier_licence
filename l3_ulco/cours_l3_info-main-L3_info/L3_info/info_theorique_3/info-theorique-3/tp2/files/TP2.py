from ply.lex import lex
from ply.yacc import yacc
import sys


global lineNumber

if __name__ == "__main__":
    lineNumber = 0

    def t_ignore_NEWLINE(t):
        r"""\n"""
        t.lexer.lineno += 1


    def t_error(t):
        print(f'Illegal character {t.value[0]!r}')
        t.lexer.skip(1)

    ### TOKENS ###
    tokens = ("NB", "PLUS", "MOINS","MUL","DIV","NEG","NBN")#,"parenthese_gauche","parenthese_droite","egal")

    t_PLUS = r'\+'
    t_MOINS = r'-'
    t_MUL = r'\*'
    t_DIV = r'\/'
    t_NEG = r't'

    def t_NB(t):
        r"""[1-9][0-9]+ | [0-9]"""
        # instruction pour traitement
        t.value = int(t.value)
        return t

    def t_NBN(t):
        r"""[1-9][0-9]+ | [0-9]"""
        t.value = int(t.value)
        return t
    ######################

    # decommenter la ligne du dessous une fois les tokens créés
    lexer = lex()
    
    ### GRAMMAR ###

    def p_regle_plus_ou_moins(p):
        """
        regle_binop : regle_binop PLUS regle_binop
                      | regle_binop MOINS regle_binop
                      | regle_mul_div
        """
        if len(p) == 2:
            p[0] = p[1]
        else:
            p[0] = ('binop',p[2],p[1],p[3])

    def p_regle_operation_prioritaire(p):
        """
        regle_mul_div : regle_mul_div MUL regle_mul_div
                        | regle_mul_div DIV regle_mul_div
                        | regle_neg
                        | regle_nb
        """
        if len(p) == 2:
            p[0] = p[1]
        else:
            p[0] = ("mul_div",p[2],p[1],p[3])

    def p_regle_nb(p):
        """
        regle_nb : NB
        """
        p[0] = ('number',p[1])

    def p_regle_nb_neg(p): #problem avec regle_neg
        """
        regle_neg : NEG NBN
        """
        print(p[2])
        p[0] = ("number",p[2])
    

    ########################

    def p_error(p):
        if hasattr(p, "value"):
            print(f'Syntax error at {p.value!r}')
        else:
            print("Error", p)

    ### FILE ###
    nom_fichier = "test.txt"
    ########################
    
    # decommenter les lignes parser, ast et print pour les grammaires
    parser = yacc()
    file = open(sys.argv[1], "r")
    program = file.read()
    print(program)
    ast = parser.parse(program)
    print(ast)




