mulListe :: Num a => a -> [a] -> [a]
mulListe _ [] = []
mulListe c (x:xs) = (x * c) : mulListe c xs

mylast :: [a] -> a
mylast [] = error "listeVide"
mylast [x] = x
mylast (x:xs) = mylast xs

--selectListe :: (Int,Int) -> [Int] -> [Int]
--selectListe  = 

main :: IO()
main = do
    print (mulListe 3 [3,4,1])
    print (mylast "yooooooooh")
