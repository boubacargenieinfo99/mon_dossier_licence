#ifndef TRAFFIC_NODE_H
#define TRAFFIC_NODE_H

struct Link;
struct Vehicle;

struct Node{
    //constructeur

    Node(char *id, unsigned int duration, unsigned int in_number, unsigned int out_number);

    //destructeur

    ~Node();

    //attributs

    char *id;
    unsigned int duration;
    unsigned int in_number;
    unsigned int out_number;
    Link **in_links;
    Link **out_links;
    Vehicle* vehicle;

    //fonction

    void add_vehicle_to_node(Vehicle *vehicle, unsigned int time);
    void attach_input_link_to_node(Link *link);
    void attach_output_link_to_node(Link *link);

};

typedef Node* NodePtr;

#endif //TRAFFIC_NODE_H