#include "node.h"
#include "link.h"

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct Node *create_node(char *id, unsigned int duration, unsigned int in_number, unsigned int out_number) {
  struct Node *node = (struct Node *) malloc(sizeof(struct Node));

  node->id = (char *) malloc(sizeof(char) * (strlen(id)) + 1);
  strcpy(node->id, id);
  node->duration = duration;
  node->in_number = in_number;
  node->out_number = out_number;
  node->in_links = (struct Link **) malloc(sizeof(struct Link *) * in_number);
  for (unsigned int i = 0; i < in_number; ++i) {
    node->in_links[i] = NULL;
  }
  node->out_links = (struct Link **) malloc(sizeof(struct Link *) * out_number);
  for (unsigned int i = 0; i < out_number; ++i) {
    node->out_links[i] = NULL;
  }
  node->vehicle = NULL;
  return node;
}

void destroy_node(struct Node *node) {
  free(node->id);
  free(node->in_links);
  free(node->out_links);
  free(node);
}

void add_vehicle_to_node(struct Vehicle *vehicle, struct Node *node, unsigned int time) {
  printf("%d: %s enters %s\n", time, vehicle->id, node->id);
  node->vehicle = vehicle;
  vehicle->node = node;
  vehicle->last_time = time;
}

void attach_input_link_to_node(struct Link *link, struct Node *node) {
  unsigned int index = 0;

  while (node->in_links[index] != NULL && index < node->in_number) { ++index; }
  if (index < node->in_number) {
    node->in_links[index] = link;
    attach_out_node_to_link(node, link);
  }
}

void attach_output_link_to_node(struct Link *link, struct Node *node) {
  unsigned int index = 0;

  while (node->out_links[index] != NULL && index < node->out_number) { ++index; }
  if (index < node->out_number) {
    node->out_links[index] = link;
    attach_in_node_to_link(node, link);
  }
}
