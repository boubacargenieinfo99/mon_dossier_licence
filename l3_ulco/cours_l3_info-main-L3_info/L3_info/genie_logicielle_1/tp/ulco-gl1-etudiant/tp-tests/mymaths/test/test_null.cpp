#include <mymaths/mul.hpp>
#include <mymaths/add.hpp>
#include <catch2/catch.hpp>


TEST_CASE( "add2nul", "[add]" ) {
    REQUIRE( add2(0) == 2 );
}


TEST_CASE( "mul2nul", "[mul]" ) {
    REQUIRE( mul2(4) == 8 );
    REQUIRE( mul2(0) == 0 );
    REQUIRE( mul2(-5) == -10 );
}

TEST_CASE( "addnnul", "[add]" ) {
    REQUIRE( addn(0,0) == 0 );
}

TEST_CASE( "mulnnul", "[mul]" ) {
    REQUIRE( muln(0,0) == 0 );
}