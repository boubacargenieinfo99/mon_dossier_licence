import breakthrough
import numpy as np
import matplotlib.pyplot as plt
import joueur


def simulation(n):
    nb_victoire = [0, 0]
    score_total = [0, 0]
    temps_total = [0, 0]
    IA_minmax = [0, 0]

    for i in range(n):
        d_jeu = breakthrough.Jeu() #
        d_jeu.demarrer()
        if d_jeu.winner == "noir":
            nb_victoire[0] += 1
        else:
            nb_victoire[1] += 1
        score_total[0] += d_jeu.score[0]/d_jeu.nb_demande[0]
        score_total[1] += d_jeu.score[1]/d_jeu.nb_demande[1]
        temps_total[0] += d_jeu.compteur_temps[0]
        temps_total[1] += d_jeu.compteur_temps[1]
    print("joueur 1(noir) : nombre de victoire " , nb_victoire[0] , " , score_moyen " , score_total[0]/n , " , temps_moyen " , temps_total[0]/n)
    print("joueur 2(blanc) : nombre de victoire ", nb_victoire[1], " , score_moyen ", score_total[1]/n," , temps_moyen ", temps_total[1]/n)





j = breakthrough.Jeu()
j.demarrer()

plt.figure(1)
plt.title("minMax")
plt.plot([1,2,3],[0.08,1.4,24.9])
plt.xlabel("profondeur")
plt.ylabel("temps (en seconde)")

plt.figure(2)
plt.title("minMax")
plt.plot([1,2,3],[695,10913,196800])
plt.xlabel("profondeur")
plt.ylabel("nombre de jouer()")
plt.show()

plt.figure(3)
plt.title("alphaBeta")
plt.plot([1,2,3],[0.08,0.35,2.04])
plt.xlabel("profondeur")
plt.ylabel("temps (en seconde)")

plt.figure(4)
plt.title("alphaBeta")
plt.plot([1,2,3],[695,2415,114612])
plt.xlabel("profondeur")
plt.ylabel("nombre de jouer()")
plt.show()


m=-1
while m < 0 or m > 9:
    m = ord(input('saisir nombre de simulation'))-48
simulation(m)


#q7 : le compteur de jouer peut servir pour avoir une estimation de la complexite
#     le compteur de temps pour savoir combien de temps la fonction est execute

#q11 : le joueur alphabeta(blanc) sera plus rapide et fera moins d operation que le joueur minmax(noirà