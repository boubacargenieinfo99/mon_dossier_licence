#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include "iterator.hpp"
#include "map.hpp"
#include "junction.hpp"
#include "one_way_link.hpp"
#include "vehicle.hpp"

TEST_CASE("create a vehicle", "tp1")
{
  Vehicle *vehicle = new Vehicle("V1");

  REQUIRE(strcmp(vehicle->id, "V1") == 0);

  delete vehicle;
}

TEST_CASE("create a node without input/output", "tp1")
{
  Junction *node = new Junction("N1", 2, 0, 0);

  REQUIRE(strcmp(node->id, "N1") == 0);
  REQUIRE(node->duration == 2);
  REQUIRE(node->in_number == 0);
  REQUIRE(node->out_number == 0);
  REQUIRE(node->in_links.size() == 0);
  REQUIRE(node->out_links.size() == 0);

  delete node;
}

TEST_CASE("create a node with input/output", "tp1")
{
  Junction node("N2", 2, 1, 1);

  REQUIRE(strcmp(node.id, "N2") == 0);
  REQUIRE(node.duration == 2);
  REQUIRE(node.in_number == 1);
  REQUIRE(node.out_number == 1);
  REQUIRE(node.in_links.size() == 0);
  REQUIRE(node.out_links.size() == 0);
}

TEST_CASE("create a _link", "tp1")
{
  OneWayLink link("L1", 200);

  REQUIRE(strcmp(link._id, "L1") == 0);
  REQUIRE(link._length == 200);
  REQUIRE(link.cell_length == 5);
  REQUIRE(link._cell_number == 40);
  REQUIRE(link._cells != nullptr);
  for (unsigned int i = 0; i < link._cell_number; ++i) {
    REQUIRE(link._cells[i].vehicle == nullptr);
  }
  REQUIRE(link._out_node == nullptr);
}

TEST_CASE("create a _link with out node", "tp1")
{
  OneWayLink *link = new OneWayLink("L1", 200);
  Junction *node = new Junction("N1", 2, 1, 0);

  node->attach_input_link(std::shared_ptr<OneWayLink>(link));

  REQUIRE(link->_out_node == node);
  REQUIRE(node->in_number == 1);
  REQUIRE(node->in_links.get_first()->get_link().get() == link);

  delete node;
//  delete _link;
}

TEST_CASE("create a node with multiple input/output _links", "tp1")
{
  OneWayLink *link_1 = new OneWayLink("L1", 200);
  OneWayLink *link_2 = new OneWayLink("L2", 200);
  OneWayLink *link_3 = new OneWayLink("L3", 200);
  OneWayLink *link_4 = new OneWayLink("L4", 200);
  Junction *node = new Junction("N1", 2, 2, 2);

  node->attach_input_link(std::shared_ptr<OneWayLink>(link_1));
  node->attach_input_link(std::shared_ptr<OneWayLink>(link_2));
  node->attach_output_link(std::shared_ptr<OneWayLink>(link_3));
  node->attach_output_link(std::shared_ptr<OneWayLink>(link_4));

  REQUIRE(link_1->_out_node == node);
  REQUIRE(link_2->_out_node == node);
  REQUIRE(link_3->_in_node == node);
  REQUIRE(link_4->_in_node == node);
  REQUIRE(node->in_number == 2);
  REQUIRE(node->out_number == 2);

  Iterator it_in(node->in_links);

  REQUIRE(it_in.current().get() == link_1);
  it_in.next();
  REQUIRE(it_in.current().get() == link_2);
  it_in.next();
  REQUIRE(not it_in.has_more());

  Iterator it_out(node->out_links);

  REQUIRE(it_out.current().get() == link_3);
  it_out.next();
  REQUIRE(it_out.current().get() == link_4);
  it_out.next();
  REQUIRE(not it_out.has_more());

  delete node;
//  delete link_1;
//  delete link_2;
//  delete link_3;
//  delete link_4;
}

TEST_CASE("create a empty map", "tp1")
{
  Map *map = new Map(0, 0);

  REQUIRE(map->_link_number == 0);
  REQUIRE(map->_node_number == 0);
  REQUIRE(map->_links.size() == 0);
  REQUIRE(map->_nodes == nullptr);

  delete map;
}

TEST_CASE("create a map", "tp1")
{
  Map *map = new Map(4, 3);
  OneWayLink *link_1 = new OneWayLink("L1", 200);
  OneWayLink *link_2 = new OneWayLink("L2", 200);
  OneWayLink *link_3 = new OneWayLink("L3", 200);
  OneWayLink *link_4 = new OneWayLink("L4", 200);
  Junction *node_1 = new Junction("N1", 2, 0, 1);
  Junction *node_2 = new Junction("N2", 2, 1, 1);
  Junction *node_3 = new Junction("N3", 2, 1, 0);

  REQUIRE(map->_link_number == 4);
  REQUIRE(map->_node_number == 3);
  REQUIRE(map->_links.size() == 0);
  REQUIRE(map->_nodes != nullptr);
  for (unsigned int i = 0; i < map->_node_number; ++i) {
    REQUIRE(map->_nodes[i] == nullptr);
  }

  map->add_link(std::shared_ptr<OneWayLink>(link_1));
  map->add_link(std::shared_ptr<OneWayLink>(link_2));
  map->add_link(std::shared_ptr<OneWayLink>(link_3));
  map->add_link(std::shared_ptr<OneWayLink>(link_4));
  map->add_node(node_1);
  map->add_node(node_2);
  map->add_node(node_3);

  Iterator it(map->_links);

  REQUIRE(it.current().get() == link_1);
  it.next();
  REQUIRE(it.current().get() == link_2);
  it.next();
  REQUIRE(it.current().get() == link_3);
  it.next();
  REQUIRE(it.current().get() == link_4);
  it.next();
  REQUIRE(not it.has_more());
  REQUIRE(map->_nodes[0] == node_1);
  REQUIRE(map->_nodes[0] == node_1);
  REQUIRE(map->_nodes[1] == node_2);
  REQUIRE(map->_nodes[2] == node_3);

  delete map;
}

TEST_CASE("create and run a map", "tp1")
{
  Map map(4, 3);
  OneWayLink *link_1 = new OneWayLink("L1", 200);
  OneWayLink *link_2 = new OneWayLink("L2", 200);
  OneWayLink *link_3 = new OneWayLink("L3", 200);
  OneWayLink *link_4 = new OneWayLink("L4", 200);
  Junction *node_1 = new Junction("N1", 2, 1, 1);
  Junction *node_2 = new Junction("N2", 2, 1, 1);
  Junction *node_3 = new Junction("N3", 2, 1, 1);

  map.add_link(std::shared_ptr<OneWayLink>(link_1));
  map.add_link(std::shared_ptr<OneWayLink>(link_2));
  map.add_link(std::shared_ptr<OneWayLink>(link_3));
  map.add_link(std::shared_ptr<OneWayLink>(link_4));
  map.add_node(node_1);
  map.add_node(node_2);
  map.add_node(node_3);

  node_1->attach_input_link(std::shared_ptr<OneWayLink>(link_1));
  node_1->attach_output_link(std::shared_ptr<OneWayLink>(link_2));
  node_2->attach_input_link(std::shared_ptr<OneWayLink>(link_2));
  node_2->attach_output_link(std::shared_ptr<OneWayLink>(link_3));
  node_3->attach_input_link(std::shared_ptr<OneWayLink>(link_3));
  node_3->attach_output_link(std::shared_ptr<OneWayLink>(link_4));

  Vehicle *vehicle = new Vehicle("V1");

  std::stringstream buffer;
  std::streambuf *old = std::cout.rdbuf(buffer.rdbuf());

  link_1->enter_vehicle(vehicle, 0);

  REQUIRE(link_1->_cells[0].vehicle == vehicle);

  unsigned int time = 0;

  while (time < 167) {
    map.run(time);
    ++time;
  }

  REQUIRE(buffer.str() == "0: V1 enters L1\n"
                          "40: V1 exits L1\n"
                          "40: V1 enters N1\n"
                          "42: V1 exits N1\n"
                          "42: V1 enters L2\n"
                          "82: V1 exits L2\n"
                          "82: V1 enters N2\n"
                          "84: V1 exits N2\n"
                          "84: V1 enters L3\n"
                          "124: V1 exits L3\n"
                          "124: V1 enters N3\n"
                          "126: V1 exits N3\n"
                          "126: V1 enters L4\n"
                          "166: V1 exits L4\n");

  std::cout.rdbuf(old);

  delete vehicle;
}

TEST_CASE("create and run a map with inputs", "tp1")
{
  Map map(4, 5);
  OneWayLink *link_1 = new OneWayLink("L1", 200);
  OneWayLink *link_2 = new OneWayLink("L2", 200);
  OneWayLink *link_3 = new OneWayLink("L3", 200);
  OneWayLink *link_4 = new OneWayLink("L4", 200);
  Junction *node_1 = new Junction("N1", 2, 0, 1);
  Junction *node_2 = new Junction("N2", 2, 1, 1);
  Junction *node_3 = new Junction("N3", 2, 1, 1);
  Junction *node_4 = new Junction("N4", 2, 1, 1);
  Junction *node_5 = new Junction("N5", 2, 1, 0);

  map.add_link(std::shared_ptr<OneWayLink>(link_1));
  map.add_link(std::shared_ptr<OneWayLink>(link_2));
  map.add_link(std::shared_ptr<OneWayLink>(link_3));
  map.add_link(std::shared_ptr<OneWayLink>(link_4));
  map.add_node(node_1);
  map.add_node(node_2);
  map.add_node(node_3);
  map.add_node(node_4);
  map.add_node(node_5);

  node_1->attach_output_link(std::shared_ptr<OneWayLink>(link_1));
  node_2->attach_input_link(std::shared_ptr<OneWayLink>(link_1));
  node_2->attach_output_link(std::shared_ptr<OneWayLink>(link_2));
  node_3->attach_input_link(std::shared_ptr<OneWayLink>(link_2));
  node_3->attach_output_link(std::shared_ptr<OneWayLink>(link_3));
  node_4->attach_input_link(std::shared_ptr<OneWayLink>(link_3));
  node_4->attach_output_link(std::shared_ptr<OneWayLink>(link_4));
  node_5->attach_input_link(std::shared_ptr<OneWayLink>(link_4));

  Inputs inputs(1);
  Vehicle *vehicle = new Vehicle("V1");

  inputs.add_vehicle(vehicle, 0, "N1");

  std::stringstream buffer;
  std::streambuf *old = std::cout.rdbuf(buffer.rdbuf());

  unsigned int time = 0;

  while (time < 171) {
    map.run(time, inputs);
    ++time;
  }

  REQUIRE(buffer.str() == "0: V1 enters N1\n"
                          "2: V1 exits N1\n"
                          "2: V1 enters L1\n"
                          "42: V1 exits L1\n"
                          "42: V1 enters N2\n"
                          "44: V1 exits N2\n"
                          "44: V1 enters L2\n"
                          "84: V1 exits L2\n"
                          "84: V1 enters N3\n"
                          "86: V1 exits N3\n"
                          "86: V1 enters L3\n"
                          "126: V1 exits L3\n"
                          "126: V1 enters N4\n"
                          "128: V1 exits N4\n"
                          "128: V1 enters L4\n"
                          "168: V1 exits L4\n"
                          "168: V1 enters N5\n"
                          "170: V1 exits N5\n");

  std::cout.rdbuf(old);
}