// lib/openxum-core/games/newgame/engine.mjs

import OpenXum from '../../openxum/index.mjs';
import Color from './color.mjs';
import Piece from './piece.mjs';
import Coordinates from './coordinates.mjs';
//import les autres classes dont vous avez besoin

class Engine extends OpenXum.Engine {
  constructor(t, c) {
    super();
    this._type = t;
    this._current_color = c;
    this._initialize_board();
    this._move_list = [];
    // autres attributs nécessaires à votre jeu
  }
  get_move() {
    return this._move_list;
  }


  build_move() {
    return new Move();
  }
    
  clone() {
    let o = new Engine(this._type, this._color);
        let b = new Array(6);

        for (let i = 0; i < 6; i++) {
            b[i] = new Array(6);
        }

        for (let x = 0; x < 10; x++) {
            for (let y = 0; y < 8; y++) {
                if (this._board[x][y] !== undefined) {
                    b[x][y] = this._board[x][y].clone();
                }
            }
        }
        o._set(this._is_finished, this._winner_color, b);

        return o;
//comm
  }
  _initialize_board() {
    this._board = new Array(8);
    for (let i = 0; i < 8; i++) {
        this._board[i] = new Array(8);
    }

    // Placer les pions noirs
    // Ligne du milieu pour les 4 pions noirs (index 3 et 4 au milieu de la première ligne)
    this._board[2][0] = new Piece(Color.BLACK, new Coordinates(2, 0));
    this._board[3][0] = new Piece(Color.BLACK, new Coordinates(3, 0));
    this._board[4][0] = new Piece(Color.BLACK, new Coordinates(4, 0));
    this._board[5][0] = new Piece(Color.BLACK, new Coordinates(5, 0));

    // Ligne du milieu pour les 2 pions noirs (index 3 et 4 au milieu de la deuxième ligne)
    this._board[3][1] = new Piece(Color.BLACK, new Coordinates(3, 1));
    this._board[4][1] = new Piece(Color.BLACK, new Coordinates(4, 1));

    // Placer les pions blancs
    // Ligne du milieu pour les 4 pions blancs (index 3 et 4 au milieu de la première ligne)
    this._board[2][7] = new Piece(Color.WHITE, new Coordinates(2, 7));
    this._board[3][7] = new Piece(Color.WHITE, new Coordinates(3, 7));
    this._board[4][7] = new Piece(Color.WHITE, new Coordinates(4, 7));
    this._board[5][7] = new Piece(Color.WHITE, new Coordinates(5, 7));

    // Ligne du milieu pour les 2 pions blancs (index 3 et 4 au milieu de la deuxième ligne)
    this._board[3][6] = new Piece(Color.WHITE, new Coordinates(3, 6));
    this._board[4][6] = new Piece(Color.WHITE, new Coordinates(4, 6));

}

  current_color() {
    return this._current_color;
  }

  get_name() {
    return "Komivoki";
  }

  phase() {
    return this._phase;
  }

  get_possible_move_list() {
    // TODO
  }

  is_finished() {
    return this._phase === Phase.FINISH;
  }

  move(move) {
    // TODO
  }
  _move_piece(fromX, fromY, toX, toY) {
    if (this._is_valid_move(fromX, fromY, toX, toY)) {
        const fromPiece = this._board[fromX][fromY];
        const toPiece = this._board[toX][toY];

        if (toPiece === undefined) {
            // Move the piece to an empty square
            this._board[toX][toY] = fromPiece;
        } else {
            if(toPiece.color() === fromPiece.color() && toPiece.getStack().length < 3){
                // Stack pieces of the same color
                while (fromPiece.getStack().length > 0 && toPiece.getStack().length < 3) {
                  toPiece.stackPiece(fromPiece.unstackPiece());
                }
            }
        }
        if (fromPiece.getStack().length === 0) {
            this._board[fromX][fromY] = undefined;
        }

        this._check_and_capture(toX,toY);

       // Clear the original box
        this._board[fromX][fromY] = undefined;
        this._move_list.push({fromX, fromY, toX, toY});
    }
}

_is_valid_move(fromX, fromY, toX, toY) {
  // Checks that the destination is within the tray limits
  if (toX < 0 || toX >= 8 || toY < 0 || toY >= 8) {
      return false;
  }

  // Checks that the destination is not a wall
  if (this._board[toX][toY] === 'wall') {
      return false;
  }

  const dx = Math.abs(toX - fromX);
  const dy = Math.abs(toY - fromY);
  if (dx > 1 || dy > 1 || (dx === 0 && dy === 0)) {
      return false;
  }

  const fromPiece = this._board[fromX][fromY];
  const toPiece = this._board[toX][toY];
  // Checks if the target box is empty

  if (toPiece === undefined) {
      return true;
  }
    // Checks whether the target square contains a pawn of the same color
  if (toPiece.color() === fromPiece.color() && toPiece.getStack().length < 3) {
      return true;
  }

  return false;
}

_check_and_capture(x, y) {
  if (this.is_possible_capture(x, y)) {
    this.capture(x, y);
  }
}

is_possible_capture(x, y) {
  const piece = this._board[x][y];
  if (!piece) {
    return false;
  }

  const stackLength = piece.getStack().length;
  const maxCapacity = 3 * stackLength;

  const directions = [
    { dx: -1, dy: -1 }, { dx: 0, dy: -1 }, { dx: 1, dy: -1 },
    { dx: -1, dy: 0 }, { dx: 1, dy: 0 },
    { dx: -1, dy: 1 }, { dx: 0, dy: 1 }, { dx: 1, dy: 1 }
  ];

  let adjacentEnemyCount = 0;
  let adjacentEnemyCapacity = 0;
  let edgeCount = 0;

  for (let dir of directions) {
    const nx = x + dir.dx;
    const ny = y + dir.dy;
    if (nx < 0 || nx >= 8 || ny < 0 || ny >= 8) {
      edgeCount++;
    } else {
      const adjacentPiece = this._board[nx][ny];
      if (adjacentPiece && adjacentPiece.color() !== piece.color()) {
        adjacentEnemyCount++;
        adjacentEnemyCapacity += 3 * adjacentPiece.getStack().length;
      }
    }
  }

  return (
    adjacentEnemyCount > 0 &&
    (adjacentEnemyCount + edgeCount) >= stackLength &&
    adjacentEnemyCapacity > maxCapacity
  );
}
capture(x, y) {
  this._board[x][y] = undefined;
}



  parse(str) {
    // TODO
  }

  to_string() {
    // TODO
  }

  winner_is() {
    if (this.is_finished()) {
      return this._color;
  }
  }
  
}

export default Engine
