#ifndef TRAFFIC_ROUND_ABOUT_HPP
#define TRAFFIC_ROUND_ABOUT_HPP

#include "node.hpp"

#include <memory>
#include <queue>
#include <string>
#include <valarray>

class Link;

class Vehicle;

class RoundAbout : public Node {
public:
  RoundAbout(const std::string &id, unsigned int duration, unsigned int in_number, unsigned int out_number, unsigned int capacity);

  ~RoundAbout() override;

  void enter_vehicle(Vehicle *vehicle, unsigned int time) override;

  void exit_vehicle(unsigned int time) override;

  bool is_full() const override;

private:
  const unsigned int _capacity;
  std::queue<std::unique_ptr<Vehicle>> _vehicles;
};

#endif //TRAFFIC_ROUND_ABOUT_HPP
