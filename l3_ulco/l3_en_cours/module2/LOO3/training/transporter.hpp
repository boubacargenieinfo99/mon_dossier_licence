
#ifndef TRAINING_TRANSPORTER_HPP
#define TRAINING_TRANSPORTER_HPP
#pragma once

#include "transportation.hpp"
#include "labor.hpp"
#include <memory>

class Transporter : public Transportation {
public:
    Transporter(const std::string& id, double speed,
                const Coordinates& initialLocation,
                std::initializer_list<Path> paths,
                std::shared_ptr<Labor> labor);

    const std::shared_ptr<Labor>& getLabor() const;

private:
    std::shared_ptr<Labor> labor;
};


#endif //TRAINING_TRANSPORTER_HPP
