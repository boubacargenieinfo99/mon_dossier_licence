#include "inputs.hpp"

#include <cstring>

typedef char *char_ptr;

Inputs::Inputs(unsigned int vehicle_number) : _vehicle_number(vehicle_number), _vehicles(vehicle_number),
                                              _times(vehicle_number), _node_ids(vehicle_number) {
  for (unsigned int i = 0; i < vehicle_number; ++i) {
    _vehicles[i] = nullptr;
    _times[i] = 0;
  }
}

Inputs::~Inputs() {
}

void Inputs::add_vehicle(Vehicle *vehicle, unsigned int time, const std::string &node_id) {
  unsigned int index = 0;

  while (index < _vehicle_number and _vehicles[index]) {
    ++index;
  }
  if (index < _vehicle_number) {
    _vehicles[index].reset(vehicle);
    _times[index] = time;
    _node_ids[index] = node_id;
  }
}