#include "map.hpp"
#include "one_way_link.hpp"
#include "n_ways_link.hpp"
#include "round_about.hpp"

void create_new_map(Map &map) {
  auto link_1 = std::make_shared<OneWayLink>("L1", 500);
  auto link_2 = std::make_shared<OneWayLink>("L2", 500);
  auto link_3 = std::make_shared<OneWayLink>("L3", 500);
  auto link_4 = std::make_shared<NWaysLink>("L4", 500, 2);
  auto link_5 = std::make_shared<OneWayLink>("L5", 500);
  auto link_6 = std::make_shared<OneWayLink>("L6", 500);
  auto link_7 = std::make_shared<OneWayLink>("L7", 500);
  auto link_8 = std::make_shared<OneWayLink>("L8", 500);
  auto link_9 = std::make_shared<OneWayLink>("L9", 500);
  auto link_10 = std::make_shared<OneWayLink>("L10", 500);
  auto link_11 = std::make_shared<OneWayLink>("L11", 500);
  auto link_12 = std::make_shared<OneWayLink>("L12", 500);

  auto node_1 = std::make_shared<Junction>("N1", 2, 0, 1);
  auto node_2 = std::make_shared<Junction>("N2", 2, 1, 0);
  auto node_3 = std::make_shared<Junction>("N3", 2, 1, 2);
  auto node_4 = std::make_shared<Junction>("N4", 2, 1, 1);
  auto node_5 = std::make_shared<Junction>("N5", 2, 2, 1);
  auto node_6 = std::make_shared<Junction>("N6", 2, 1, 2);
  auto node_7 = std::make_shared<Junction>("N7", 2, 1, 0);
  auto node_8 = std::make_shared<Junction>("N8", 0, 0, 1);
  auto node_9 = std::make_shared<Junction>("N9", 2, 1, 2);
  auto node_10 = std::make_shared<RoundAbout>("N10", 5, 2, 1, 5);
  auto node_11 = std::make_shared<Junction>("N11", 2, 0, 1);
  auto node_12 = std::make_shared<Junction>("N12", 2, 1, 0);
  auto node_13 = std::make_shared<Junction>("N13", 2, 1, 0);

  map.add_link(link_1);
  map.add_link(link_2);
  map.add_link(link_3);
  map.add_link(link_4);
  map.add_link(link_5);
  map.add_link(link_6);
  map.add_link(link_7);
  map.add_link(link_8);
  map.add_link(link_9);
  map.add_link(link_10);
  map.add_link(link_11);
  map.add_link(link_12);

  map.add_node(node_1);
  map.add_node(node_2);
  map.add_node(node_3);
  map.add_node(node_4);
  map.add_node(node_5);
  map.add_node(node_6);
  map.add_node(node_7);
  map.add_node(node_8);
  map.add_node(node_9);
  map.add_node(node_10);
  map.add_node(node_11);
  map.add_node(node_12);
  map.add_node(node_13);

  node_1->attach_output_link(link_8);
  link_8->attach_in_node(node_1);

  node_8->attach_output_link(link_4);
  link_4->attach_in_node(node_8);

  node_11->attach_output_link(link_1);
  link_1->attach_in_node(node_11);

  node_2->attach_input_link(link_7);
  link_7->attach_out_node(node_2);

  node_12->attach_input_link(link_10);
  link_10->attach_out_node(node_12);

  node_7->attach_input_link(link_12);
  link_12->attach_out_node(node_7);

  node_3->attach_input_link(link_8);
  link_8->attach_out_node(node_3);
  node_3->attach_output_link(link_9);
  link_9->attach_in_node(node_3);
  node_3->attach_output_link(link_11);
  link_11->attach_in_node(node_3);

  node_6->attach_input_link(link_4);
  link_4->attach_out_node(node_6);
  node_6->attach_output_link(link_6);
  link_6->attach_in_node(node_6);
  node_6->attach_output_link(link_5);
  link_5->attach_in_node(node_6);

  node_9->attach_input_link(link_1);
  link_1->attach_out_node(node_9);
  node_9->attach_output_link(link_3);
  link_3->attach_in_node(node_9);
  node_9->attach_output_link(link_2);
  link_2->attach_in_node(node_9);

  node_5->attach_input_link(link_2);
  link_2->attach_out_node(node_5);
  node_5->attach_input_link(link_6);
  link_6->attach_out_node(node_5);
  node_5->attach_output_link(link_7);
  link_7->attach_in_node(node_5);

  node_10->attach_input_link(link_5);
  link_5->attach_out_node(node_10);
  node_10->attach_input_link(link_9);
  link_9->attach_out_node(node_10);
  node_10->attach_output_link(link_10);
  link_10->attach_in_node(node_10);

  node_4->attach_input_link(link_11);
  link_11->attach_out_node(node_4);
  node_4->attach_output_link(link_12);
  link_12->attach_in_node(node_4);

  link_3->attach_out_node(node_13);
  node_13->attach_input_link(link_3);
}

void run(Map &map, Inputs &inputs) {
  unsigned int time = 0;

  while (time < 10000) {
    map.run(time, inputs);
    ++time;
  }
}

int main() {
  Map map;
  Inputs inputs(10);

  inputs.add_vehicle(new Vehicle("V1", {0, 0, 0}), 0, "N1");
  inputs.add_vehicle(new Vehicle("V3", {0, 0, 0}), 0, "N8");
  inputs.add_vehicle(new Vehicle("V5", {0, 0}), 0, "N11");
  inputs.add_vehicle(new Vehicle("V8", {0, 0, 0}), 1, "N8");
  inputs.add_vehicle(new Vehicle("V2", {0, 1, 0}), 3, "N1");
  inputs.add_vehicle(new Vehicle("V4", {0, 1, 0}), 10, "N8");
  inputs.add_vehicle(new Vehicle("V6", {0, 1, 0}), 10, "N11");
  inputs.add_vehicle(new Vehicle("V7", {0, 0, 0}), 10, "N1");
  inputs.add_vehicle(new Vehicle("V9", {0, 0, 0}), 13, "N1");
  inputs.add_vehicle(new Vehicle("V10", {0, 1, 0}), 15, "N8");
  create_new_map(map);

  run(map, inputs);

  return 0;
}