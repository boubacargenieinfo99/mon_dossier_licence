const express = require('express');
const app = express();

const path = require('path');
app.use(express.static(path.join(__dirname, 'public')));

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// Importation de 'express-ejs-layouts' :
const expressLayouts = require('express-ejs-layouts');
// Ajout du middleware :
app.use(expressLayouts);
// Définition du layout par défaut :
app.set('layout', '../views/layouts/layout') ;

app.use((req, res) => {
    // La vue est automatiquement rendue dans le layout défini
    res.render('pages/home', { user: 'greg' }) ;
});

module.exports = app;