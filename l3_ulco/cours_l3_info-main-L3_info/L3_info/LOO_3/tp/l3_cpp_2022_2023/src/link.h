#ifndef TRAFFIC_LINK_H
#define TRAFFIC_LINK_H

#include "vehicle.h"

#define CELL_LENGTH 5 // meters

struct Node;

struct Cell {
  struct Vehicle *vehicle;
};

struct Link {
  char *id;
  unsigned int length;
  struct Cell *cells;
  unsigned int cell_number;
  struct Node* in_node;
  struct Node* out_node;
};

struct Link *create_link(char *id, unsigned int length);

void destroy_link(struct Link *link);

void add_vehicle_to_link(struct Vehicle *vehicle, struct Link *link, unsigned int time);

void attach_in_node_to_link(struct Node *node, struct Link *link);

void attach_out_node_to_link(struct Node *node, struct Link *link);

struct Vehicle *go_out_link(struct Link *link, unsigned int time);

struct Vehicle *run_link(struct Link *link, unsigned int time);

#endif //TRAFFIC_LINK_H
