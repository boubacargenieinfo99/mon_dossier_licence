#include "agv.hpp"

AGV::AGV(const std::string& id, double speed,
         const Coordinates& initialLocation,
         std::initializer_list<Path> paths)
        : Transportation(id, speed, initialLocation, paths) {}