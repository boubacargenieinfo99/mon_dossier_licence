from ply.lex import lex
from ply.yacc import yacc
import sys


global lineNumber

if __name__ == "__main__":
    lineNumber = 0

    def t_ignore_NEWLINE(t):
        r"""\n"""
        t.lexer.lineno += 1


    def t_error(t):
        print(f'Illegal character {t.value[0]!r}')
        t.lexer.skip(1)

    ### TOKENS ###
    tokens = (
        "NB",
        "PLUS",
        "MOINS",
        "MUL",
        "DIV",
        "NEG",
        "NBN",
        "LPAREN",
        "RPAREN",
        "EQUALS",
        "VARIABLE",

    )

    t_PLUS = r'\+'
    t_MOINS = r'-'
    t_MUL = r'\*'
    t_DIV = r'\/'
    t_NEG = r't'
    t_LPAREN = r'\('
    t_RPAREN = r'\)'
    t_EQUALS = r'='
    t_VARIABLE = r'[a-zA-Z_][a-zA-Z0-9_]*'


    def t_NB(t):
        r"""[1-9][0-9]+ | [0-9]"""
        # instruction pour traitement
        t.value = int(t.value)
        return t

    def t_NBN(t):
        r"""[1-9][0-9]+ | [0-9]"""
        t.value = float(t.value)
        return t
    ######################

    # decommenter la ligne du dessous une fois les tokens créés
    lexer = lex()
    
    ### GRAMMAR ###

    def p_regle_plus_ou_moins(p):
        """
        regle_binop : regle_binop PLUS regle_binop
                      | regle_binop MOINS regle_binop
                      | regle_mul_div
        """
        if len(p) == 2:
            p[0] = p[1]
        else:
            p[0] = ('binop',p[2],p[1],p[3])

    def p_regle_operation_prioritaire(p):
        """
        regle_mul_div : regle_mul_div MUL regle_mul_div
                        | regle_mul_div DIV regle_mul_div
                        | regle_neg
                        | regle_nb
                        | regle_parentheses
                        | regle_assign
                        | regle_variable
        """
        if len(p) == 2:
            p[0] = p[1]
        else:
            p[0] = ("mul_div",p[2],p[1],p[3])

    def p_regle_nb(p):
        """
        regle_nb : NB
        """
        p[0] = ('number',p[1])

    def p_regle_nb_neg(p):
        """
        regle_neg : NEG NBN
        """
        print(p[2][1])
        p[0] = ("number", -p[2][1])


    def p_regle_parentheses(p):
        """
        regle_parentheses : LPAREN regle_binop RPAREN
        """
        p[0] = p[2]


    def p_regle_assign(p):
        """
        regle_assign : VARIABLE EQUALS regle_binop
        """
        p[0] = ('assign', p[1], p[3])


    def p_regle_variable(p):
        """
        regle_variable : VARIABLE
        """
        p[0] = ('variable', p[1])

    ########################
    def p_error(p):
        if hasattr(p, "value"):
            print(f'Syntax error at {p.value!r}')
        else:
            print("Error", p)


    def translate_to_python(node):
        if isinstance(node, tuple):
            if node[0] == 'binop':
                operator = node[1]
                left = translate_to_python(node[2])
                right = translate_to_python(node[3])
                return f'({left} {operator} {right})'
            elif node[0] == 'mul_div':
                operator = node[1]
                left = translate_to_python(node[2])
                right = translate_to_python(node[3])
                return f'({left} {operator} {right})'
            elif node[0] == 'assign':
                variable = node[1]
                value = translate_to_python(node[2])
                return f'{variable} = {value}'
            elif node[0] == 'variable':
                return node[1]
            elif node[0] == 'number':
                return str(node[1])
        return ''

    ### FILE ###
    # Lecture du fichier
    nom_fichier = "test.txt"
    lexer.input(open(nom_fichier, "r").read())

    # Analyse lexicale
    for tok in lexer:
        pass  # Ignore les tokens pour l'instant

    # Analyse syntaxique
    parser = yacc()
    program = open(nom_fichier, "r").read()
    print(program)
    ast = parser.parse(program)
    print(ast)

    # Traduction en code Python
    python_code = translate_to_python(ast)
    print("\nCode Python généré:")
    print(python_code)


