import System.Environment

fiboNaive :: Int -> Int
fiboNaive 0 = 0
fiboNaive 1 = 1
fiboNaive a = fiboNaive (a-1) + fiboNaive (a-2)

fiboTco :: Int -> Int
fiboTco n = go 0 1 0
    where 
        go a b x =
            if x == n
                then a
                else go b (a+b) (x+1)

main ::IO()
main = do
    args <- getArgs
    case args of
        ["naive",str] -> print (fiboNaive (read str))
        ["tco",str] -> print (fiboTco (read str))
        _ -> print ("erreur")
            
