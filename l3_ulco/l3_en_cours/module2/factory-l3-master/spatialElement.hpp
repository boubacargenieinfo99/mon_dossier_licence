
#ifndef TP4_SPATIALELEMENT_HPP
#define TP4_SPATIALELEMENT_HPP
class SpacialElement{
private:
    int Id;
    Geometry& geometry;
public:
    SpacialElement(int Id):Id(Id),geometry(nullptr){}
    void addGeometry(Geometry& geometry){ this->geometry=geometry;}
};

#endif //TP4_SPATIALELEMENT_HPP
