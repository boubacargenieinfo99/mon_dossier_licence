#pragma once
#include <iostream>
#include <vector>
using namespace std;


class Factory {
public:
    Factory(int Id){ this->Id=Id;}
    void addZone(Zone& zone);
    void addLabor(Labor& labor);
    void addTransportation(Transportation& transportation);
private:
    int Id;
    vector<Zone>zones;
    vector<Labor> labors;
    vector<Transportation> transportations;
};


