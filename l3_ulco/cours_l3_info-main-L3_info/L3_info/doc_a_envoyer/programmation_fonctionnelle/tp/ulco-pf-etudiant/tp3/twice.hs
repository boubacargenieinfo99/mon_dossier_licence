twice :: [a] -> [a]
twice liste = liste ++ liste

sym :: [a] -> [a]
sym liste = liste ++ reverse liste

main ::IO ()
main = do
    print (twice [1,2])
    print (twice "to")

    print (sym [1,2])
    print (sym "to")