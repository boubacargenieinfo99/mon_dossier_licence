

function triangle(x1,y1,z1,x2,y2,z2,x3,y3,z3,couleur){ //typeof float couleur type color
    // créer un BufferGeometry
    const geometry = new THREE.BufferGeometry();

    // créer le tableau contenant les coordonnées des sommets des triangles
    const vertices = new Float32Array( [
        x1, y1, z1, // coordonnées du 1er sommet
        x2, y2, z2, // coordonnées du 2nd sommet
        x3, y3, z3 // coordonnées du 3me sommet
    ] );
    // associer les sommets au BufferGeometry (3 coordonnées par sommet -----------|)
    geometry.setAttribute( 'position', new THREE.Float32BufferAttribute( vertices, 3 ) );

    // créer un matériau basique
    const diffus = new THREE.MeshBasicMaterial( couleur );
    diffus.side = THREE.DoubleSide;

    // créer le mesh et le retourner
    const mesh = new THREE.Mesh( geometry, diffus );
    return mesh;
}

function carreBasic(x1,y1,z1,x2,y2,z2,x3,y3,z3,x4,y4,z4,couleur){ // sommet 1 en bas a gauche , sommet 2 en bas a droite , sommet 3 en haut a droite , sommet 4 en haut a gauche
    // créer un BufferGeometry
    const geometry = new THREE.BufferGeometry();

    // créer le tableau contenant les coordonnées des sommets du carré
    const vertices = new Float32Array([
        x1, y1, z1, // coordonnées du 1er sommet
        x2, y2, z2, // coordonnées du 2nd sommet
        x3, y3, z3, // coordonnées du 3me sommet
        x4, y4, z4 // coordonnées du 4eme sommet
    ]);

    /* centreX = (x1 + x2 + x3 + x4) / 4;
    const centreY = (y1 + y2 + y3 + y4) / 4;
    const centreZ = (z1 + z2 + z3 + z4) /4; */


    const triangleUn = triangle(x1,y1,z1,x2,y2,z2,x4,y4,z4,couleur);
    const triangleDeux = triangle(x2,y2,z2,x3,y3,z3,x4,y4,z4,couleur);

    return {first:triangleUn,second:triangleDeux};

}

function addCarreInScene(carre,scene){
    scene.add(carre[0]);
    scene.add(carre[1]);
}

function carreRotate(carre,axe,valeur){
    if(axe == 'x'){
        carre[0].rotateX(valeur);
        carre[1].rotateX(valeur);
    }
    if(axe == 'y'){
        carre[0].rotateY(valeur);
        carre[1].rotateY(valeur);
    }
    if(axe == 'z'){
        carre[0].rotateZ(valeur);
        carre[1].rotateZ(valeur);
    }
}

function moveCarre(carre,x,y,z){
    carre[0].position.set(carre[0].position.x + x,carre[0].position.y + y,carre[0].position.z + z);
    carre[1].position.set(carre[1].position.x +x,carre[1].position.y + y,carre[1].position.z + z);
}

// calcule de normal manquant v
/*function triangleLambert(x1,y1,z1,x2,y2,z2,x3,y3,z3,couleur){ //typeof float couleur type color
    // créer un BufferGeometry
    const geometry = new THREE.BufferGeometry();

    // créer le tableau contenant les coordonnées des sommets des triangles
    const vertices = new Float32Array( [
        x1, y1, z1, // coordonnées du 1er sommet
        x2, y2, z2, // coordonnées du 2nd sommet
        x3, y3, z3 // coordonnées du 3me sommet
    ] );

    // créer le tableau contenant les normales des sommets des triangles
    const normales = new Float32Array( [    // calculer les normales !!!
        x1, y1, z1, // coordonnées du 1er sommet
        x2, y2, z2, // coordonnées du 2nd sommet
        x3, y3, z3 // coordonnées du 3me sommet
    ] );

    // associer les sommets au BufferGeometry (3 coordonnées par sommet -----------|)
    geometry.setAttribute( 'position', new THREE.Float32BufferAttribute( vertices, 3 ) );

    // créer un matériau basique
    const diffus = new THREE.MeshBasicMaterial( couleur );
    diffus.side = THREE.DoubleSide;

    // créer le mesh et le retourner
    const mesh = new THREE.Mesh( geometry, diffus );
    return mesh;
}*/

/*function carreLambert(x1,y1,z1,x2,y2,z2,x3,y3,z3,x4,y4,z4,couleur){ // sommet 1 en bas a gauche , sommet 2 en bas a droite , sommet 3 en haut a droite , sommet 4 en haut a gauche
    // créer un BufferGeometry
    const geometry = new THREE.BufferGeometry();

    // créer le tableau contenant les coordonnées des sommets du carré
    const vertices = new Float32Array([
        x1, y1, z1, // coordonnées du 1er sommet
        x2, y2, z2, // coordonnées du 2nd sommet
        x3, y3, z3, // coordonnées du 3me sommet
        x4, y4, z4 // coordonnées du 4eme sommet
    ]);

    const centreX = (x1 + x2 + x3 + x4) / 4;
    const centreY = (y1 + y2 + y3 + y4) / 4;
    const centreZ = (z1 + z2 + z3 + z4) /4;

    // x1 = x2 - centreX

    const triangleUn = triangle(x1,y1,z1,x2,y2,z2,x4,y4,z4,couleur);
    const triangleDeux = triangle(x2,y2,z2,x3,y3,z3,x4,y4,z4,couleur);

    return {first:triangleUn,second:triangleDeux};

}*/

function carreLambert(longueur){
    // créer un BufferGeometry
    const geometry = new THREE.BufferGeometry();

    // créer le tableau contenant les coordonnées des sommets du carré
    const vertices = new Float32Array([
        (-longueur/2) , 0 , (-longueur/2), // coordonnées du 1er sommet
        (longueur/2) , 0 , (-longueur/2), // coordonnées du 2nd sommet
        (longueur/2) , 0 , (longueur/2), // coordonnées du 3me sommet
        (-longueur/2) , 0 , (longueur/2) // coordonnées du 4eme sommet
    ]);



    const triangleUn = triangleLambert(vertices[0],vertices[1],vertices[2],vertices[3],vertices[4],vertices[5],vertices[9],vertices[10],vertices[11]);
    const triangleDeux = triangleLambert(vertices[6],vertices[7],vertices[8],vertices[9],vertices[10],vertices[11],vertices[3],vertices[4],vertices[5]);

    return {first:triangleUn,second:triangleDeux};

}

function triangleLambert(x1,y1,z1,x2,y2,z2,x3,y3,z3){ //typeof float couleur type color
    // créer un BufferGeometry
    const geometry = new THREE.BufferGeometry();

    // créer le tableau contenant les coordonnées des sommets des triangles
    const vertices = new Float32Array( [
        x1, y1, z1, // coordonnées du 1er sommet
        x2, y2, z2, // coordonnées du 2nd sommet
        x3, y3, z3 // coordonnées du 3me sommet
    ] );

    // créer le tableau contenant les normales des sommets des triangles
    const normals = new Float32Array( [    // calculer les normales !!!
        0, -1, 0, // coordonnées du 1er sommet
        0, -1, 0, // coordonnées du 2nd sommet
        0, -1, 0 // coordonnées du 3me sommet
    ] );

    // créer le tableau contenant les couleurs des sommets des triangles
    const colors = new Float32Array( [
        0,0,100,
        0,100,0,
        100,0,0
    ] );

    // associer les sommets au BufferGeometry (3 coordonn´ees par sommet -----------|)
    geometry.setAttribute( 'position', new THREE.Float32BufferAttribute( vertices, 3 ) );
    // associer les normaless au BufferGeometry (3 coordonn´ees par sommet ------|)
    geometry.setAttribute( 'normal', new THREE.Float32BufferAttribute( normals, 3 ) );
    // associer les couleurs au BufferGeometry (3 couleurs par sommet --------|)
    geometry.setAttribute( 'color', new THREE.Float32BufferAttribute( colors, 3 ) );

    // créer un matériau basique bleu
    const lambert = new THREE.MeshLambertMaterial( {
        color: 'rgb(255,255,255)', // reflectance diffuse du matériau
        side: THREE.DoubleSide,// tenir compte des deux faces
        vertexColors: true, // il y a des couleurs en chaque sommet
        //emissive: 'rgb(100,100,100)',
        //emissiveIntensity: 0.25
    });

    // créer le mesh et le retourner
    const mesh = new THREE.Mesh( geometry, lambert );
    return mesh;
}

function cubeColore(longueur){
    // créer un BufferGeometry
    //const geometry = new THREE.BufferGeometry();

    // créer le tableau contenant les coordonnées des sommets du carré
    /*const vertices = new Float32Array([
        (-longueur/2) , 0 , (-longueur/2), // coordonnées du 1er sommet
        (longueur/2) , 0 , (-longueur/2), // coordonnées du 2nd sommet
        (longueur/2) , 0 , (longueur/2), // coordonnées du 3me sommet
        (-longueur/2) , 0 , (longueur/2) // coordonnées du 4eme sommet
    ]);*/

    const carres = [[carreLambert(longueur).first,carreLambert(longueur).second],[carreLambert(longueur).first,carreLambert(longueur).second],[carreLambert(longueur).first,carreLambert(longueur).second],[carreLambert(longueur).first,carreLambert(longueur).second],[carreLambert(longueur).first,carreLambert(longueur).second],[carreLambert(longueur).first,carreLambert(longueur).second]];
    moveCarre(carres[1],0,longueur/2,longueur/2);
    carreRotate(carres[1],'x',Math.PI/2);
    moveCarre(carres[2],0,longueur,0);
    moveCarre(carres[3],0,longueur/2,-longueur/2);
    carreRotate(carres[3],'x',Math.PI/2);
    moveCarre(carres[4],-longueur/2,longueur/2,0);
    carreRotate(carres[4],'z',Math.PI/2);
    moveCarre(carres[5],longueur/2,longueur/2,0);
    carreRotate(carres[5],'z',Math.PI/2);

    return carres;
}

function addCubeLambertInScene(cube,scene){
    for (let i=0;i<6;i++){
        addCarreInScene(cube[i],scene);
    }
}

function moveCube(cube,x,y,z){
    for (let i=0;i<6;i++){
        moveCarre(cube[i],x,y,z);
    }
}

/*function cubeRotate(cube,axe,valeur){
    // calculer l origine
    //


    for (let i=0;i<6;i++){
        carreRotate(cube[i],axe,valeur);
    }
}*/






function makeCarre(longueur,couleur){ // sommet 1 en bas a gauche , sommet 2 en bas a droite , sommet 3 en haut a droite , sommet 4 en haut a gauche
    // créer un BufferGeometry
    const geometry = new THREE.BufferGeometry();

    // créer le tableau contenant les coordonnées des sommets du carré
    const vertices = new Float32Array([
        -longueur/2, -longueur/2, 0, // coordonnées du 1er sommet
        longueur/2, -longueur/2, 0, // coordonnées du 2nd sommet
        longueur/2, longueur/2, 0,
        -longueur/2, -longueur/2, 0,
        longueur/2, longueur/2, 0,
        -longueur/2, longueur/2, 0
    ]);

    geometry.setAttribute( 'position', new THREE.Float32BufferAttribute( vertices, 3 ) );

    const material = new THREE.MeshBasicMaterial( { color: couleur } );
    const mesh = new THREE.Mesh( geometry, material );

    return mesh;

}

function makeLine(x1,y1,x2,y2,couleur){
    const materialLine = new THREE.LineBasicMaterial({color: couleur});
    
    const points = [];
    points.push( new THREE.Vector3( x1, y1, 0 ));
    points.push( new THREE.Vector3( x2, y2, 0 ));
    
    const geometryLine = new THREE.BufferGeometry().setFromPoints( points );
    
    const line = new THREE.Line( geometryLine, materialLine );
    return line;
}

function fractal2d(ligne,couleur,groupe){
    const materialLine = new THREE.LineBasicMaterial({color:couleur});

    const distanceLineX = Math.pow(Math.pow(ligne.geometry.attributes.position.array[3] - ligne.geometry.attributes.position.array[0],2),0.5); 
    const distanceLineY = Math.pow(Math.pow(ligne.geometry.attributes.position.array[4] - ligne.geometry.attributes.position.array[1],2),0.5); 
    
    const distanceLine = Math.pow(Math.pow(ligne.geometry.attributes.position.array[4]-ligne.geometry.attributes.position.array[1],2)+Math.pow(ligne.geometry.attributes.position.array[3]-ligne.geometry.attributes.position.array[0],2),0.5);
    const teta = Math.acos((distanceLine/6)/(distanceLine/3));
    const angleLigne = Math.acos(Math.pow(Math.pow(distanceLineX,2),0.5) / distanceLine);

    var sensX = 0;
    if(ligne.geometry.attributes.position.array[0] < ligne.geometry.attributes.position.array[3]){
        sensX = 1;
    }
    else{
        sensX = -1;
    }

    var sensY = 0;
    if(ligne.geometry.attributes.position.array[1] < ligne.geometry.attributes.position.array[4]){
        sensY = 1;
    }
    else{
        sensY = -1;
    }

    const pointsLine1 = [];
    const pointsLine2 = [];
    const pointsLine3 = [];
    const pointsLine4 = [];

    pointsLine1.push( new THREE.Vector3( ligne.geometry.attributes.position.array[0], ligne.geometry.attributes.position.array[1], 0 ));
    pointsLine1.push( new THREE.Vector3( ligne.geometry.attributes.position.array[0] + (sensX * (distanceLineX/3)),ligne.geometry.attributes.position.array[1] + (sensY * (distanceLineY/3)) , 0 ));
    
    pointsLine2.push( new THREE.Vector3( ligne.geometry.attributes.position.array[0] + (sensX * (distanceLineX/3)),ligne.geometry.attributes.position.array[1] + (sensY * (distanceLineY/3)) , 0 ));
    if(sensX == 1 && sensY == 1){
        pointsLine2.push( new THREE.Vector3(ligne.geometry.attributes.position.array[0] + (sensX * (distanceLineX/3))+(distanceLine/3)*Math.cos(teta+angleLigne),ligne.geometry.attributes.position.array[1] + (sensY * (distanceLineY/3))+(distanceLine/3) * Math.sin(teta+angleLigne),0));
    
        pointsLine3.push( new THREE.Vector3(ligne.geometry.attributes.position.array[0] + (sensX * (distanceLineX/3))+(distanceLine/3)*Math.cos(teta+angleLigne),ligne.geometry.attributes.position.array[1] + (sensY * (distanceLineY/3))+(distanceLine/3) * Math.sin(teta+angleLigne),0));
    }
    if(sensX != 1 && sensY == 1){
        pointsLine2.push( new THREE.Vector3(ligne.geometry.attributes.position.array[0] + (sensX * (distanceLineX/3))+(distanceLine/3)*Math.cos(Math.PI + teta - angleLigne),ligne.geometry.attributes.position.array[1] + (sensY * (distanceLineY/3))+(distanceLine/3) * Math.sin(Math.PI + teta - angleLigne),0));
    
        pointsLine3.push(new THREE.Vector3(ligne.geometry.attributes.position.array[0] + (sensX * (distanceLineX/3))+(distanceLine/3)*Math.cos(Math.PI + teta - angleLigne),ligne.geometry.attributes.position.array[1] + (sensY * (distanceLineY/3))+(distanceLine/3) * Math.sin(Math.PI + teta - angleLigne),0));
    }
    if(sensX != 1 && sensY != 1){
        pointsLine2.push( new THREE.Vector3(ligne.geometry.attributes.position.array[0] + (sensX * (distanceLineX/3))+(distanceLine/3)*Math.cos(Math.PI + teta + angleLigne),ligne.geometry.attributes.position.array[1] + (sensY * (distanceLineY/3))+(distanceLine/3) * Math.sin(Math.PI + teta + angleLigne),0));
    
        pointsLine3.push(new THREE.Vector3(ligne.geometry.attributes.position.array[0] + (sensX * (distanceLineX/3))+(distanceLine/3)*Math.cos(Math.PI + teta + angleLigne),ligne.geometry.attributes.position.array[1] + (sensY * (distanceLineY/3))+(distanceLine/3) * Math.sin(Math.PI + teta + angleLigne),0));
    }
    if(sensX == 1 && sensY != 1){
        pointsLine2.push( new THREE.Vector3(ligne.geometry.attributes.position.array[0] + (sensX * (distanceLineX/3))+(distanceLine/3)*Math.cos(teta - angleLigne),ligne.geometry.attributes.position.array[1] + (sensY * (distanceLineY/3))+(distanceLine/3) * Math.sin(teta - angleLigne),0));
    
        pointsLine3.push(new THREE.Vector3(ligne.geometry.attributes.position.array[0] + (sensX * (distanceLineX/3))+(distanceLine/3)*Math.cos(teta - angleLigne),ligne.geometry.attributes.position.array[1] + (sensY * (distanceLineY/3))+(distanceLine/3) * Math.sin(teta - angleLigne),0));
    }

    pointsLine3.push( new THREE.Vector3( ligne.geometry.attributes.position.array[3] - (sensX * (distanceLineX/3)), ligne.geometry.attributes.position.array[4] - (sensY * (distanceLineY/3)), 0 ));
    
    pointsLine4.push( new THREE.Vector3( ligne.geometry.attributes.position.array[3] - (sensX * (distanceLineX/3)), ligne.geometry.attributes.position.array[4] - (sensY * (distanceLineY/3)), 0 ));
    pointsLine4.push( new THREE.Vector3( ligne.geometry.attributes.position.array[3], ligne.geometry.attributes.position.array[4], 0 ));

    const geometryLine1 = new THREE.BufferGeometry().setFromPoints( pointsLine1 );
    const geometryLine2 = new THREE.BufferGeometry().setFromPoints( pointsLine2 );
    const geometryLine3 = new THREE.BufferGeometry().setFromPoints( pointsLine3 );
    const geometryLine4 = new THREE.BufferGeometry().setFromPoints( pointsLine4 );

    const line1 = new THREE.Line( geometryLine1, materialLine );
    const line2 = new THREE.Line( geometryLine2, materialLine );
    const line3 = new THREE.Line( geometryLine3, materialLine );
    const line4 = new THREE.Line( geometryLine4, materialLine );

    groupe.add(line1);
    groupe.add(line2);
    groupe.add(line3);
    groupe.add(line4);
}

/*
function fractalyser(ligne,couleur){
    const materialLine = new THREE.LineBasicMaterial({color:couleur});

    var sensX = 0;
    if(ligne.geometry.attributes.position.array[0] < ligne.geometry.attributes.position.array[4]){
        sensX = 1;
    }
    else{
        sensX = -1;
    }

    var sensY = 0;
    if(ligne.geometry.attributes.position.array[1] < ligne.geometry.attributes.position.array[5]){
        sensY = 1;
    }
    else{
        sensY = -1;
    }

    var sensZ = 0;
    if(ligne.geometry.attributes.position.array[2] < ligne.geometry.attributes.position.array[5]){
        sensZ = 1;
    }
    else{
        sensZ = -1;
    }

    const distanceTiersX = Math.pow(Math.pow(ligne.geometry.attributes.position.array[3]-ligne.geometry.attributes.position.array[0],2),0.5);
    const distanceTiersY = Math.pow(Math.pow(ligne.geometry.attributes.position.array[4]-ligne.geometry.attributes.position.array[1],2),0.5);
    const distanceTiersZ = Math.pow(Math.pow(ligne.geometry.attributes.position.array[5]-ligne.geometry.attributes.position.array[2],2),0.5);

    var midDistanceBetween[];
    midDistanceBetween.push(ligne.geometry.attributes.position.array[0] + (sensX * (distanceTiersX/2)));
    midDistanceBetween.push(ligne.geometry.attributes.position.array[1] + (sensY * (distanceTiersY/2)));
    midDistanceBetween.push(ligne.geometry.attributes.position.array[2] + (sensZ * (distanceTiersZ/2)));

    const points = [];
    points.push( new THREE.Vector3( ligne.geometry.attributes.position.array[0], ligne.geometry.attributes.position.array[1] , ligne.geometry.attributes.position.array[2] ));
    points.push( new THREE.Vector3( ligne.geometry.attributes.position.array[0] + (sensX * distanceTiersX), ligne.geometry.attributes.position.array[1] + (sensY * distanceTiersY) , ligne.geometry.attributes.position.array[2] + (sensZ * distanceTiersZ)));
    points.push( new THREE.Vector3( midDistanceBetween[0], midDistanceBetween[1], midDistanceBetween[2] )); // 
    points.push( new THREE.Vector3( ligne.geometry.attributes.position.array[3] - (sensX * distanceTiersX), ligne.geometry.attributes.position.array[4] - (sensY * distanceTiersY), ligne.geometry.attributes.position.array[5] - (sensZ * distanceTiersZ) ));
    points.push( new THREE.Vector3( ligne.geometry.attributes.position.array[3] , ligne.geometry.attributes.position.array[4] , ligne.geometry.attributes.position.array[5] ));


}
*/