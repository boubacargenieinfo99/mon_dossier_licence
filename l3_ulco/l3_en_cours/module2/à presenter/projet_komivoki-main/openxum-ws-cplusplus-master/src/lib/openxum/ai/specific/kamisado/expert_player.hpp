/**
 * @file openxum/ai/specific/kamisado/expert_player.hpp
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2011-2024 Openxum Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPENXUM_AI_SPECIFIC_KAMISADO_EXPERT_PLAYER_HPP
#define OPENXUM_AI_SPECIFIC_KAMISADO_EXPERT_PLAYER_HPP

#include <random>
#include <openxum/core/common/player.hpp>
#include <openxum/core/games/kamisado/engine.hpp>

namespace openxum::ai::specific::kamisado {

template<class Decision>
class ExpertPlayer : public openxum::core::common::Player<Decision> {
public:
  ExpertPlayer(int c, int o, openxum::core::common::TwoPlayerEngine<Decision> *e)
    : openxum::core::common::Player<Decision>(c, o, e) {}

  ~ExpertPlayer() override = default;

  openxum::core::common::Move<Decision> get_move() override {
    const auto &e = dynamic_cast<const openxum::core::games::kamisado::Engine &>(this->engine());
    const auto &list = e.get_possible_move_list();
    openxum::core::common::Move<Decision> move(
      openxum::core::games::kamisado::Decision(openxum::core::games::kamisado::DecisionType::PASS,
                                               openxum::core::games::kamisado::Coordinates(),
                                               openxum::core::games::kamisado::Coordinates()));

    if (not list.empty()) {
      int max = -1;

      for (const auto &m: list) {
        int score = 0;

        for (unsigned int i = 0; i < 10; ++i) {
          openxum::core::common::TwoPlayerEngine<Decision> *e_clone = this->engine().clone();

          while (not e_clone->is_finished()) {
            const openxum::core::common::Moves<Decision> &list2 = e_clone->get_possible_move_list();

            if (not list2.empty()) {
              std::uniform_int_distribution<std::mt19937::result_type> distribution(0, list2.size() - 1);

              e_clone->move(list2[distribution(_rng)]);
            }
          }
          if (e_clone->winner_is() == this->color()) {
            ++score;
          }
        }
        if (max < score) {
          move = m;
          max = score;
        }
      }
    }
    return move;
  }

  double get_next_goal_distance() override { return 0; }

  unsigned int get_next_goal_distance_evaluation() override { return 0; }

private:
  std::random_device _random_device;
  std::mt19937 _rng;
};

}

#endif