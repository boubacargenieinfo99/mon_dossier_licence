--1.1.1

INSERT INTO EMPLOYE(NUMERO,NOM,PRENOM,ADRESSE) 
	VALUES ('EMP1190','ROGER','Stéphane','362, rue stéphane-roger, Ardre');
	
INSERT INTO EMPLOYE(NUMERO,NOM,PRENOM,ADRESSE)
	VALUES ('EMP1401','ROUSSELLE','Nicolas','397, rue noclas-rousselle, st-Omer');
	
SELECT * FROM EMPLOYE;

	/*resultat:
	
		NUMERO			NOM			       			   PRENOM			      		  ADRESSE
		--------------- ------------------------------ ------------------------------ --------------------------------------------------
		EMP1090 		DUPONT			       		   Jean			      			  332, rue jean-dupont, Calais
		EMP1301 		DUVIVIER		       		   Renaud			      		  345, rue renaud-duviver, Paris
		EMP1208 		GODART			       		   Claude			      		  674, rue claude-godart, Ardre
		EMP2025 		FONTAINE		       		   Fabien			      		  456, rue jean-louis, Calais
		EMP1505 		VASSEUR 		       		   Jacques			      		  778, rue vasseur, Saint-Omer
		EMP7645 		BERNARD 		       		   Martin			      		  876, rue martin, Sangatte
		EMP2341 		RICHARD 		       		   Philipe			      		  955, rue bernard, Coudekerque
		EMP8765 		HUGO			       		   Victor			      		  432, rue victor-hugo, Frethun
		EMP4328 		STEVENSON		       		   Max			      			  874, rue max-stevenson, Cologne
		EMP7862 		DURAND			       		   Christophe		      		  543, rue durand, Calais
		EMP1190 		ROGER			       		   St??phane		      		  362, rue st??phane-roger, Ardre
		EMP1401 		ROUSSELLE		       		   Nicolas			      		  397, rue noclas-rousselle, st-Omer

	*/
	
	/*
		SQL> rollback;

		Annulation (rollback) effectuee.

		SQL> select * from employe;

		aucune ligne selectionnee

		conclusion tout a été effacé
	*/
	
--1.1.2

	/*
		SQL> commit;

		Validation effectuee.

		SQL> select * from employe;

		NUMERO			NOM			       				PRENOM			      			ADRESSE
		--------------- ------------------------------ ------------------------------ --------------------------------------------------
		EMP1090 		DUPONT			       			Jean			      			332, rue jean-dupont, Calais
		EMP1301 		DUVIVIER		       			Renaud			      			345, rue renaud-duviver, Paris
		EMP1208 		GODART			       			Claude			      			674, rue claude-godart, Ardre
		EMP2025 		FONTAINE		       			Fabien			      			456, rue jean-louis, Calais
		EMP1505 		VASSEUR 		       			Jacques			      			778, rue vasseur, Saint-Omer
		EMP7645 		BERNARD 		       			Martin			      			876, rue martin, Sangatte
		EMP2341 		RICHARD 		       			Philipe			      			955, rue bernard, Coudekerque
		EMP8765 		HUGO			       			Victor			      			432, rue victor-hugo, Frethun
		EMP4328 		STEVENSON		       			Max			      				874, rue max-stevenson, Cologne
		EMP7862 		DURAND			       			Christophe		      			543, rue durand, Calais
		EMP1190 		ROGER			       			St??phane		      			362, rue st??phane-roger, Ardre
		EMP1401 		ROUSSELLE		       			Nicolas			      			397, rue noclas-rousselle, st-Omer

	le commit n a pas changé la table
		
		SQL> rollback;

		Annulation (rollback) effectuee.

		SQL> select * from employe;

		NUMERO			NOM			       				PRENOM			     	 		ADRESSE
		--------------- ------------------------------ ------------------------------ --------------------------------------------------
		EMP1090 		DUPONT			       			Jean			      			332, rue jean-dupont, Calais
		EMP1301 		DUVIVIER		       			Renaud			      			345, rue renaud-duviver, Paris
		EMP1208 		GODART			       			Claude			      			674, rue claude-godart, Ardre
		EMP2025 		FONTAINE		       			Fabien			      			456, rue jean-louis, Calais
		EMP1505 		VASSEUR 		       			Jacques			      			778, rue vasseur, Saint-Omer
		EMP7645 		BERNARD 		       			Martin			      			876, rue martin, Sangatte
		EMP2341 		RICHARD 		       			Philipe			      			955, rue bernard, Coudekerque
		EMP8765 		HUGO			       			Victor			      			432, rue victor-hugo, Frethun
		EMP4328 		STEVENSON		       			Max			      				874, rue max-stevenson, Cologne
		EMP7862 		DURAND			       			Christophe		    		    543, rue durand, Calais
		EMP1190 		ROGER			       			St??phane		      			362, rue st??phane-roger, Ardre
		EMP1401 		ROUSSELLE		       			Nicolas			      			397, rue noclas-rousselle, st-Omer

	le rollback n a pas modifié les tables grace au rollback avant
	
		*/
		
--1.2

CREATE TABLE Habitant_Calais(NUMERO,NOM,PRENOM,ADRESSE)
AS SELECT EMPLOYE.NUMERO,EMPLOYE.NOM,EMPLOYE.PRENOM,EMPLOYE.ADRESSE
FROM EMPLOYE
WHERE EMPLOYE.ADRESSE LIKE '%Calais';

SELECT * FROM Habitant_Calais;

	/*resultat:
	
		NUMERO			NOM			       				PRENOM			      			ADRESSE
		--------------- ------------------------------ ------------------------------ --------------------------------------------------
		EMP1090 		DUPONT			       			Jean			      			332, rue jean-dupont, Calais
		EMP2025 		FONTAINE		       			Fabien			      			456, rue jean-louis, Calais
		EMP7862 		DURAND			       			Christophe		      			543, rue durand, Calais
	
	*/
	
CREATE TABLE HYPERMARCHE_RAYON(NOM_HYPERMARCHE,NOM_RAYON,CHIFFREAFFAIRE_RAYON)
AS SELECT HYPERMARCHE_NOM,RAYON_NOM,CHIFFREAFFAIRE
FROM (SELECT HYPERMARCHE.NOM AS HYPERMARCHE_NOM,RAYON.NOM AS RAYON_NOM,CHIFFREAFFAIRE FROM HYPERMARCHE
		INNER JOIN RAYON ON HYPERMARCHE.NUMERO = RAYON.NUMEROHYPER);
		
SELECT * FROM HYPERMARCHE_RAYON;

	/*resultat;
	
		NOM_HYPERMARCHE 	       		NOM_RAYON		      		  CHIFFREAFFAIRE_RAYON
		------------------------------ ------------------------------ --------------------
		Marrefour Coquelles	       		Boucherie				   	  1000000
		DO-SPORT		       			Textile					  	  50000000
		DO-SPORT		       			Chaussure				      800000
		Marrefour Coquelles	       		Poissonnerie				  950000
		Dealer Price		       		L??gumes 				      450000
		Dealer Price		       		Boucherie				      500000

	*/
	
--1.3

	DECLARE 
		nom_employe 	EMPLOYE.NOM%TYPE;
		prenom_employe	EMPLOYE.PRENOM%TYPE;
		numero_employe	EMPLOYE.NUMERO%TYPE;
		adresse_employe EMPLOYE.ADRESSE%TYPE;
	BEGIN
		nom_employe := &saisir_nom_employe;
		prenom_employe := &saisir_prenom_employe;
		numero_employe := &saisir_numero_employe;
		adresse_employe := &saisir_adresse_employe;
		INSERT INTO EMPLOYE(NUMERO,NOM,PRENOM,ADRESSE)
			VALUES (numero_employe,nom_employe,prenom_employe,adresse_employe);
		DBMS_OUTPUT.PUT_LINE('Employé Inséré');
	EXCEPTION
		When OTHERS Then
			DBMS_OUTPUT.PUT_LINE('Erreur');
			Null;
	END;
	/

show ERRORS;

SELECT * FROM EMPLOYE;

	/*resultat:
	
		Entrez une valeur pour saisir_nom_employe : 'JEAN'
		ancien	 7 : 		nom_employe := &saisir_nom_employe;
		nouveau   7 : 		nom_employe := 'JEAN';
		Entrez une valeur pour saisir_prenom_employe : 'Jacques'
		ancien	 8 : 		prenom_employe := &saisir_prenom_employe;
		nouveau   8 : 		prenom_employe := 'Jacques';
		Entrez une valeur pour saisir_numero_employe : 'EMP0001'
		ancien	 9 : 		numero_employe := &saisir_numero_employe;
		nouveau   9 : 		numero_employe := 'EMP0001';
		Entrez une valeur pour saisir_adresse_employe : '75, rue Jean Jacques Goldman, Paris'
		ancien	10 : 		adresse_employe := &saisir_adresse_employe;
		nouveau  10 : 		adresse_employe := '75, rue Jean Jacques Goldman, Paris';
		Employ?? Ins??r??

		Procedure PL/SQL terminee avec succes.

	
		NUMERO			NOM			       				PRENOM			      			ADRESSE
		--------------- ------------------------------ ------------------------------ --------------------------------------------------
		EMP1090 		DUPONT			       			Jean			      			332, rue jean-dupont, Calais
		EMP1301 		DUVIVIER		       			Renaud			      			345, rue renaud-duviver, Paris
		EMP1208 		GODART			       			Claude			      			674, rue claude-godart, Ardre
		EMP2025 		FONTAINE		       			Fabien			      			456, rue jean-louis, Calais
		EMP1505 		VASSEUR 		       			Jacques			      			778, rue vasseur, Saint-Omer
		EMP7645 		BERNARD 		       			Martin			      			876, rue martin, Sangatte
		EMP2341 		RICHARD 		       			Philipe			      			955, rue bernard, Coudekerque
		EMP8765 		HUGO			       			Victor			      			432, rue victor-hugo, Frethun
		EMP4328 		STEVENSON		       			Max			      				874, rue max-stevenson, Cologne
		EMP7862 		DURAND			       			Christophe		      			543, rue durand, Calais
		EMP1190 		ROGER			       			St??phane		      			362, rue st??phane-roger, Ardre
		EMP1401 		ROUSSELLE		       			Nicolas			      			397, rue noclas-rousselle, st-Omer
		EMP0001 		JEAN			       			Jacques			      			75, rue Jean Jacques Goldman, Paris

	*/

--1.3.2

	DECLARE 
		nom_employe 	EMPLOYE.NOM%TYPE;
		prenom_employe	EMPLOYE.PRENOM%TYPE;
		numero_employe	EMPLOYE.NUMERO%TYPE;
		adresse_employe EMPLOYE.ADRESSE%TYPE;
		numero_employe_recherche EMPLOYE.NUMERO%TYPE;
	BEGIN
		numero_employe_recherche := &saisir_numero_employe;
		SELECT NOM,PRENOM,NUMERO,ADRESSE INTO nom_employe,prenom_employe,numero_employe,adresse_employe
			FROM EMPLOYE
			WHERE EMPLOYE.NUMERO = numero_employe_recherche;
		DBMS_OUTPUT.PUT_LINE('nom = '||nom_employe);
		DBMS_OUTPUT.PUT_LINE('prenom = '||prenom_employe);
		DBMS_OUTPUT.PUT_LINE('numero = '||numero_employe);
		DBMS_OUTPUT.PUT_LINE('adresse = '||adresse_employe);
	EXCEPTION
		When NO_DATA_FOUND Then
			DBMS_OUTPUT.PUT_LINE('pas d employe avec ce numero');
			Null;--'pas d employe avec ce numero';
		When OTHERS THEN
			DBMS_OUTPUT.PUT_LINE('Erreur');
			Null;
	END;
	/
	SHOW ERROR;
	
	/*resultat:

		Entrez une valeur pour saisir_numero_employe : 'EMP0001'
		ancien   8 :            numero_employe_recherche := &saisir_numero_employe;
		nouveau   8 :           numero_employe_recherche := 'EMP0001';
		nom = JEAN
		prenom = Jacques
		numero = EMP0001
		adresse = 75, rue Jean Jacques Goldman, Paris

		ProcÚdure PL/SQL terminÚe avec succÞs.

	*/

--1.3.3

	DECLARE 
		moyenne PRODUITS.PRIXUNITAIRE%TYPE;
		numero_hyper	VARCHAR2(15);
		numero_rayon_de_l_hypermarche RAYON.NUMERO%TYPE;
	BEGIN
		numero_hyper := &saisir_numero_hypermarche;
		SELECT AVG(PRIXUNITAIRE) INTO moyenne
			FROM PRODUITS
			INNER JOIN RAYON ON RAYON.NUMERO = PRODUITS.NUMERORAYON
			INNER JOIN HYPERMARCHE ON HYPERMARCHE.NUMERO = RAYON.NUMEROHYPER
			WHERE HYPERMARCHE.NUMERO = numero_hyper;
		DBMS_OUTPUT.PUT_LINE('moyenne des produits de l hypermarche = '||moyenne);
	EXCEPTION 
		When NO_DATA_FOUND Then
			DBMS_OUTPUT.PUT_LINE('pas d HYPERMARCHE avec ce numero');
			Null;--'pas d HYPERMARCHE avec ce numero';
		When OTHERS THEN
			DBMS_OUTPUT.PUT_LINE('Erreur');
			Null;
	END;
	/

	/*resultat:

		Entrez une valeur pour saisir_numero_hypermarche : 'HYP56'
		ancien   6 :            numero_hyper := &saisir_numero_hypermarche;
		nouveau   6 :           numero_hyper := 'HYP56';
		moyenne des produits de l hypermarche = 84,5

		ProcÚdure PL/SQL terminÚe avec succÞs.

	*/

--1.3.4

CREATE TABLE promotions ( 
	numero VARCHAR(7),
	numproduit VARCHAR(6),
	pourcentage_remise NUMBER(4,2),
	date_debut DATE,
	date_fin DATE,
	CONSTRAINT pk_promotions PRIMARY KEY (numero),
	CONSTRAINT fk_promotions_produits FOREIGN KEY (numproduit) REFERENCES produits(numero)
);



	DECLARE 
		numero_promo	VARCHAR2(7);--on suppose que des que la date de fin arrive on supprime la promotion
		numero_produits_promo	PRODUITS.NUMERO%TYPE;
		numero_produits_promo_existant	PRODUITS.NUMERO%TYPE;
		promo_pourcentage	NUMBER(4,2);
		numero_jour		varchar2(2);

		date_debut	DATE;
		date_fin	DATE;
	BEGIN
		numero_produits_promo := &saisir_numero_produit_en_promo;
		promo_pourcentage := &saisir_pourcentage_promo;
		SELECT NUMERO INTO numero_produits_promo_existant
			FROM PRODUITS
			WHERE NUMERO = numero_produits_promo;
		numero_jour := TO_CHAR(SYSDATE,'DD');
		numero_promo := TO_CHAR( ((TO_NUMBER(numero_jour)-1) * 60 * 60 * 24) + (60 * 60 * TO_NUMBER(TO_CHAR(SYSDATE,'HH24'))) + (60 * TO_NUMBER(TO_CHAR(SYSDATE,'MI'))) + (TO_NUMBER(TO_CHAR(SYSDATE,'SS'))) );
		date_debut := SYSDATE();
		date_fin := ADD_MONTHS(date_debut,1);

		INSERT INTO promotions(numero,numproduit,pourcentage_remise,date_debut,date_fin)
			VALUES (numero_promo,numero_produits_promo_existant,promo_pourcentage,date_debut,date_fin);
		DBMS_OUTPUT.PUT_LINE('promotions inseree');
	EXCEPTION 
		When NO_DATA_FOUND Then
			DBMS_OUTPUT.PUT_LINE('pas de produit avec ce numero');
			Null;--'pas de produit avec ce numero';
		When OTHERS THEN
			DBMS_OUTPUT.PUT_LINE('Erreur');
			Null;
	END;
	/

--1.3.5

	DECLARE 
		numero_produits		PRODUITS.NUMERO%TYPE;
		quantite_restante	PRODUITS.QUANTITESTOCKHYPER%TYPE;
	BEGIN
		numero_produits := &saisir_numero_produit;
		quantite_restante := &saisir_quantite_restante_du_produit;
		UPDATE PRODUITS 
			SET QUANTITESTOCKHYPER = quantite_restante
			WHERE NUMERO = numero_produits;
	EXCEPTION 
		When OTHERS THEN
			DBMS_OUTPUT.PUT_LINE('Erreur');
			Null;
	END;
	/