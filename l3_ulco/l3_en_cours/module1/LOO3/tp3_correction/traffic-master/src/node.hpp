#ifndef TRAFFIC_NODE_HPP
#define TRAFFIC_NODE_HPP

#include "vehicle.hpp"

#include <memory>
#include <string>
#include <vector>

namespace traffic {

class Link;

class Node {
public:
  Node(const std::string &id, unsigned int duration, size_t in_number, size_t out_number);

  void attach_in_link(size_t index, const std::shared_ptr<Link> &link);

  void attach_out_link(size_t index, const std::shared_ptr<Link> &link);

  const std::string &id() const { return _id; }

  virtual bool is_free() const = 0;

  virtual void push_vehicle(unsigned int t, std::unique_ptr<Vehicle> &vehicle) = 0;

  virtual void run(unsigned int t) = 0;

protected:
  unsigned int duration() const { return _duration; }

  const std::vector<std::shared_ptr<Link>> &in_links() { return _in_links; }

  const std::vector<std::shared_ptr<Link>> &out_links() { return _out_links; }

private:
  std::string _id;
  unsigned int _duration;
  std::vector<std::shared_ptr<Link>> _in_links;
  std::vector<std::shared_ptr<Link>> _out_links;
};

}

#endif //TRAFFIC_NODE_HPP
