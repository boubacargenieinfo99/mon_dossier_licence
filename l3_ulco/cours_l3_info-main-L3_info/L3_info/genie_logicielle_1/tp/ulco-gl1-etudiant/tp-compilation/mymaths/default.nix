with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "play-videos";
  src = ./.;

  buildInputs = [
    cmake
    pkg-config
  ];

}

