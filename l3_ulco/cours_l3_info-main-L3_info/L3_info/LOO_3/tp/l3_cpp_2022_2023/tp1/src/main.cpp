#include "map.hpp"

Map create_new_map() {
    Map map(3, 2);
    Link *link_1 = new Link("L1", 500);
    Link *link_2 = new Link("L2", 500);
    Link *link_3 = new Link("L3", 500);

    Node *node_1 = new Node("N1", 2, 1, 1);
    Node *node_2 = new Node("N2", 2, 1, 1);

    map.add_link_to_map(link_1);
    map.add_link_to_map(link_2);
    map.add_link_to_map(link_3);

    map.add_node_to_map(node_1);
    map.add_node_to_map(node_2);

    node_1->attach_input_link_to_node(link_1);
    node_1->attach_output_link_to_node(link_2);
    node_2->attach_input_link_to_node(link_2);
    node_2->attach_output_link_to_node(link_3);
    return map;
}

void run(Map map) {
    unsigned int time = 0;

    while (time < 10000) {
        map.run_map(time);
        ++time;
    }
}

int main() {
    Map map = create_new_map();
    Vehicle *vehicle = new Vehicle("V1");

    map.links[0]->add_vehicle_to_link(vehicle,0);
    //Input input = new Input()
    run(map);

    vehicle->~Vehicle();
    map.~Map();
    return 0;
}