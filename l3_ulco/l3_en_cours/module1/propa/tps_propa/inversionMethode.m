% Paramètre de la loi de Poisson
a = 3;

% Calcul de la moyenne
mu = a;

% Calcul de la variance
variance = a;

% Affichage des résultats
fprintf('Moyenne (mu) : %f\n', mu);
fprintf('Variance : %f\n', variance);
