const express = require('express');
const router = express.Router();

// Router pour tous les chemins sous /teaching (voir app.js)

router.get('/',(req, res)=>{
    res.render('pages/teaching/teaching');
});

router.get('/javascript',(req, res)=>{
    res.render('pages/teaching/teaching_javascript');
});

router.get('/php',(req, res)=>{
    res.render('pages/teaching/teaching_php');
});

router.get('/node',(req, res)=>{
    res.render('pages/teaching/node');
});

router.get('/node/express',(req, res)=>{
    res.render('pages/teaching/node_express');
});

module.exports = router;