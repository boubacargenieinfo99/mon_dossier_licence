var express = require('express');
var router = express.Router();

const researchController = require('../controllers/researchController') ;

router.get('/', researchController.home);

module.exports = router;