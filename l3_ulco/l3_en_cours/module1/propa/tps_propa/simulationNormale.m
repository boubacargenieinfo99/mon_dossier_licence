
function normales = simulationLoiNormale(n, m, a)
    % n : nombre de réalisations
    % m : moyenne
    % a : écart type

    % Générer deux ensembles de variables aléatoires uniformément distribuées U[0,1]
    u1 = rand(1, n);
    u2 = rand(1, n);

    % Appliquer la méthode de Box-Muller
    x1 = sqrt(-2 * log(u1)) .* cos(2 * pi * u2);

    % Ajuster la moyenne et l'écart type
    normales = m + a * x1;
end