const Express = require('express');
const router = Express.Router();

router.get('/',(req, res)=>{
    res.render('pages/research');
});

module.exports = router;