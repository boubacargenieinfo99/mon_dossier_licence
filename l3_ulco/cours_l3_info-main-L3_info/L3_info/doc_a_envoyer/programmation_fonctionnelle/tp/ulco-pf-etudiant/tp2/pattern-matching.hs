fibo ::Int -> Int
fibo 0 = 0
fibo 1 = 1
--fibo numero = fibo (numero + (-1)) + fibo (numero + (-2)) 
fibo numero = if numero < 0
                then 0 
                else fibo (numero + (-1)) + fibo (numero + (-2)) 

main ::IO()
main = do
    print (fibo 0)
    print (fibo 1)
    print (fibo 2)
    print (fibo 3)
    print (fibo 10)
    print (fibo (-10))