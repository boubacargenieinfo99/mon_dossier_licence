#include "one_way_link.hpp"
#include "junction.hpp"
#include "vehicle.hpp"

#include <cmath>
#include <cstring>

#include <iostream>

OneWayLink::OneWayLink(const std::string &id, unsigned int length) : Link(id, length), _cells(cell_number()) {

//  std::cout << "[OneWayLink] constructor - " << id << std::endl;

}

OneWayLink::OneWayLink(const OneWayLink &link) : Link(link), _cells(cell_number()) {

//  std::cout << "[OneWayLink] copy constructor - " << _id << std::endl;

}

OneWayLink::~OneWayLink() {

//  std::cout << "[OneWayLink] destructor - " << _id << std::endl;

}

void OneWayLink::enter_vehicle(Vehicle *vehicle, unsigned int time) {
  std::cout << time << ": " << vehicle->id() << " enters " << id() << std::endl;
  _cells[0].enter(vehicle, time);
  vehicle->add_link(this);
}

void OneWayLink::exit_vehicle(Vehicle *vehicle, unsigned int time) {
  if (out_node().use_count() != 0) {
    std::cout << time << ": " << vehicle->id() << " exits " << id() << std::endl;
    out_node().lock()->enter_vehicle(vehicle, time);
  } else {
    delete vehicle;
  }
}

Vehicle *OneWayLink::go_out(unsigned int time) {
  if (out_node().lock()->is_full()) { return nullptr; }
  else {
    Vehicle* out_vehicle = nullptr;
    const auto &last_vehicle = _cells[cell_number() - 1].vehicle();

    if (last_vehicle != nullptr and last_vehicle->last_time() < time) {
      out_vehicle = _cells[cell_number() - 1].exit();
    }
    return out_vehicle;
  }
}

Vehicle *OneWayLink::run(unsigned int time) {
  Vehicle *out_vehicle = go_out(time);

  for (unsigned int i = cell_number() - 1; i > 0; --i) {
    if (not _cells[i - 1].is_free() and _cells[i - 1].vehicle_last_time() + _duration == time and _cells[i].is_free()) {
      _cells[i].enter(_cells[i - 1].exit(), time);
    }
  }
  return out_vehicle;
}