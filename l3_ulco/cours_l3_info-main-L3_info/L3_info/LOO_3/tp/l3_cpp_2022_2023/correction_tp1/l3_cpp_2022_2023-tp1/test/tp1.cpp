#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include "map.hpp"
#include "node.hpp"
#include "link.hpp"
#include "vehicle.hpp"

TEST_CASE("create a vehicle", "tp1")
{
  Vehicle *vehicle = new Vehicle("V1");

  REQUIRE(strcmp(vehicle->id, "V1") == 0);

  delete vehicle;
}

TEST_CASE("create a node without input/output", "tp1")
{
  Node *node = new Node("N1", 2, 0, 0);

  REQUIRE(strcmp(node->id, "N1") == 0);
  REQUIRE(node->duration == 2);
  REQUIRE(node->in_number == 0);
  REQUIRE(node->out_number == 0);
  REQUIRE(node->in_links == nullptr);
  REQUIRE(node->out_links == nullptr);

  delete node;
}

TEST_CASE("create a node with input/output", "tp1")
{
  Node node("N2", 2, 1, 1);

  REQUIRE(strcmp(node.id, "N2") == 0);
  REQUIRE(node.duration == 2);
  REQUIRE(node.in_number == 1);
  REQUIRE(node.out_number == 1);
  REQUIRE(node.in_links != nullptr);
  REQUIRE(node.out_links != nullptr);
}

TEST_CASE("create a link", "tp1")
{
  Link link("L1", 200);

  REQUIRE(strcmp(link.id, "L1") == 0);
  REQUIRE(link.length == 200);
  REQUIRE(link.cell_length == 5);
  REQUIRE(link.cell_number == 40);
  REQUIRE(link.cells != nullptr);
  for (unsigned int i = 0; i < link.cell_number; ++i) {
    REQUIRE(link.cells[i].vehicle == nullptr);
  }
  REQUIRE(link.out_node == nullptr);
}

TEST_CASE("create a link with out node", "tp1")
{
  Link *link = new Link("L1", 200);
  Node *node = new Node("N1", 2, 1, 0);

  node->attach_input_link(link);

  REQUIRE(link->out_node == node);
  REQUIRE(node->in_number == 1);
  REQUIRE(node->in_links[0] == link);

  delete node;
  delete link;
}

TEST_CASE("create a node with multiple input/output links", "tp1")
{
  Link *link_1 = new Link("L1", 200);
  Link *link_2 = new Link("L2", 200);
  Link *link_3 = new Link("L3", 200);
  Link *link_4 = new Link("L4", 200);
  Node *node = new Node("N1", 2, 2, 2);

  node->attach_input_link(link_1);
  node->attach_input_link(link_2);
  node->attach_output_link(link_3);
  node->attach_output_link(link_4);

  REQUIRE(link_1->out_node == node);
  REQUIRE(link_2->out_node == node);
  REQUIRE(link_3->in_node == node);
  REQUIRE(link_4->in_node == node);
  REQUIRE(node->in_number == 2);
  REQUIRE(node->out_number == 2);
  REQUIRE(node->in_links[0] == link_1);
  REQUIRE(node->in_links[1] == link_2);
  REQUIRE(node->out_links[0] == link_3);
  REQUIRE(node->out_links[1] == link_4);

  delete node;
  delete link_1;
  delete link_2;
  delete link_3;
  delete link_4;
}

TEST_CASE("create a empty map", "tp1")
{
  Map *map = new Map(0, 0);

  REQUIRE(map->link_number == 0);
  REQUIRE(map->node_number == 0);
  REQUIRE(map->links == nullptr);
  REQUIRE(map->nodes == nullptr);

  delete map;
}

TEST_CASE("create a map", "tp1")
{
  Map *map = new Map(4, 3);
  Link *link_1 = new Link("L1", 200);
  Link *link_2 = new Link("L2", 200);
  Link *link_3 = new Link("L3", 200);
  Link *link_4 = new Link("L4", 200);
  Node *node_1 = new Node("N1", 2, 0, 1);
  Node *node_2 = new Node("N2", 2, 1, 1);
  Node *node_3 = new Node("N3", 2, 1, 0);

  REQUIRE(map->link_number == 4);
  REQUIRE(map->node_number == 3);
  REQUIRE(map->links != nullptr);
  for (unsigned int i = 0; i < map->link_number; ++i) {
    REQUIRE(map->links[i] == nullptr);
  }
  REQUIRE(map->nodes != nullptr);
  for (unsigned int i = 0; i < map->node_number; ++i) {
    REQUIRE(map->nodes[i] == nullptr);
  }

  map->add_link(link_1);
  map->add_link(link_2);
  map->add_link(link_3);
  map->add_link(link_4);
  map->add_node(node_1);
  map->add_node(node_2);
  map->add_node(node_3);

  REQUIRE(map->links[0] == link_1);
  REQUIRE(map->links[1] == link_2);
  REQUIRE(map->links[2] == link_3);
  REQUIRE(map->links[3] == link_4);
  REQUIRE(map->nodes[0] == node_1);
  REQUIRE(map->nodes[0] == node_1);
  REQUIRE(map->nodes[1] == node_2);
  REQUIRE(map->nodes[2] == node_3);

  delete map;
}

TEST_CASE("create and run a map", "tp1")
{
  Map map(4, 3);
  Link *link_1 = new Link("L1", 200);
  Link *link_2 = new Link("L2", 200);
  Link *link_3 = new Link("L3", 200);
  Link *link_4 = new Link("L4", 200);
  Node *node_1 = new Node("N1", 2, 1, 1);
  Node *node_2 = new Node("N2", 2, 1, 1);
  Node *node_3 = new Node("N3", 2, 1, 1);

  map.add_link(link_1);
  map.add_link(link_2);
  map.add_link(link_3);
  map.add_link(link_4);
  map.add_node(node_1);
  map.add_node(node_2);
  map.add_node(node_3);

  node_1->attach_input_link(link_1);
  node_1->attach_output_link(link_2);
  node_2->attach_input_link(link_2);
  node_2->attach_output_link(link_3);
  node_3->attach_input_link(link_3);
  node_3->attach_output_link(link_4);

  Vehicle *vehicle = new Vehicle("V1");

  std::stringstream buffer;
  std::streambuf *old = std::cout.rdbuf(buffer.rdbuf());

  link_1->add_vehicle(vehicle, 0);

  REQUIRE(link_1->cells[0].vehicle == vehicle);

  unsigned int time = 0;

  while (time < 167) {
    map.run(time);
    ++time;
  }

  REQUIRE(buffer.str() == "0: V1 enters L1\n"
                          "40: V1 exits L1\n"
                          "40: V1 enters N1\n"
                          "42: V1 exits N1\n"
                          "42: V1 enters L2\n"
                          "82: V1 exits L2\n"
                          "82: V1 enters N2\n"
                          "84: V1 exits N2\n"
                          "84: V1 enters L3\n"
                          "124: V1 exits L3\n"
                          "124: V1 enters N3\n"
                          "126: V1 exits N3\n"
                          "126: V1 enters L4\n"
                          "166: V1 exits L4\n");

  std::cout.rdbuf(old);

  delete vehicle;
}

TEST_CASE("create and run a map with inputs", "tp1")
{
  Map map(4, 5);
  Link *link_1 = new Link("L1", 200);
  Link *link_2 = new Link("L2", 200);
  Link *link_3 = new Link("L3", 200);
  Link *link_4 = new Link("L4", 200);
  Node *node_1 = new Node("N1", 2, 0, 1);
  Node *node_2 = new Node("N2", 2, 1, 1);
  Node *node_3 = new Node("N3", 2, 1, 1);
  Node *node_4 = new Node("N4", 2, 1, 1);
  Node *node_5 = new Node("N5", 2, 1, 0);

  map.add_link(link_1);
  map.add_link(link_2);
  map.add_link(link_3);
  map.add_link(link_4);
  map.add_node(node_1);
  map.add_node(node_2);
  map.add_node(node_3);
  map.add_node(node_4);
  map.add_node(node_5);

  node_1->attach_output_link(link_1);
  node_2->attach_input_link(link_1);
  node_2->attach_output_link(link_2);
  node_3->attach_input_link(link_2);
  node_3->attach_output_link(link_3);
  node_4->attach_input_link(link_3);
  node_4->attach_output_link(link_4);
  node_5->attach_input_link(link_4);

  Inputs inputs(1);
  Vehicle *vehicle = new Vehicle("V1");

  inputs.add_vehicle(vehicle, 0, "N1");

  std::stringstream buffer;
  std::streambuf *old = std::cout.rdbuf(buffer.rdbuf());

  unsigned int time = 0;

  while (time < 171) {
    map.run(time, inputs);
    ++time;
  }

  REQUIRE(buffer.str() == "0: V1 enters N1\n"
                          "2: V1 exits N1\n"
                          "2: V1 enters L1\n"
                          "42: V1 exits L1\n"
                          "42: V1 enters N2\n"
                          "44: V1 exits N2\n"
                          "44: V1 enters L2\n"
                          "84: V1 exits L2\n"
                          "84: V1 enters N3\n"
                          "86: V1 exits N3\n"
                          "86: V1 enters L3\n"
                          "126: V1 exits L3\n"
                          "126: V1 enters N4\n"
                          "128: V1 exits N4\n"
                          "128: V1 enters L4\n"
                          "168: V1 exits L4\n"
                          "168: V1 enters N5\n"
                          "170: V1 exits N5\n");

  std::cout.rdbuf(old);
}