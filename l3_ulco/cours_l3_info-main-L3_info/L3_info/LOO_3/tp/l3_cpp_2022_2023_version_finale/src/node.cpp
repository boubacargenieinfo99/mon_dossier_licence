#include "link.hpp"
#include "node.hpp"
#include "vehicle.hpp"

Node::Node(const std::string &id, unsigned int duration, unsigned int in_number, unsigned int out_number) :
  _id(id), _duration(duration), _in_links(in_number), _out_links(out_number) {
}

Node::~Node() {
}

void Node::attach_input_link(const std::shared_ptr<Link> &link) {
  unsigned int index = 0;

  while (index < _in_links.size() and _in_links[index].use_count() != 0) { ++index; }
  if (index < _in_links.size()) {
    _in_links[index] = link;
  }
}

void Node::attach_output_link(const std::shared_ptr<Link> &link) {
  unsigned int index = 0;

  while (index < _out_links.size() and _out_links[index].use_count() != 0) { ++index; }
  if (index < _out_links.size()) {
    _out_links[index] = link;
  }
}

void Node::put_vehicle_into_out_link(Vehicle* vehicle, unsigned int time) {
  _out_links[vehicle->current_path_index()].lock()->enter_vehicle(vehicle, time);
  vehicle->next_path();
}

