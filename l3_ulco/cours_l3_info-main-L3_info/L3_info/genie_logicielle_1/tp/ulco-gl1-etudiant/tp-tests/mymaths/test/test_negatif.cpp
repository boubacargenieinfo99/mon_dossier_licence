#include <mymaths/add.hpp>
#include <mymaths/mul.hpp>
#include <catch2/catch.hpp>


TEST_CASE( "add2negatif", "[add]" ) {
    REQUIRE( add2(-2) == 0 );
    REQUIRE( add2(-4) == -2 );
}

TEST_CASE( "mul2negatif", "[mul]" ) {
    REQUIRE( mul2(-5) == -10 );
}

TEST_CASE( "addnnegatif", "[add]" ) {
    REQUIRE( addn(-2,-5) == -7 );
    REQUIRE( addn(-4,0) == -4 );
    REQUIRE( addn(0,-5) == -5 );
    REQUIRE( addn(-4,1) == -3 );
    REQUIRE( addn(8,-10) == -2 );
}

TEST_CASE( "mulnnegatif", "[mul]" ) {
    REQUIRE( muln(-2,-5) == 10 );
    REQUIRE( muln(-4,0) == 0 );
    REQUIRE( muln(0,-5) == 0 );
    REQUIRE( muln(-4,2) == -8 );
    REQUIRE( muln(8,-10) == -80 );
}