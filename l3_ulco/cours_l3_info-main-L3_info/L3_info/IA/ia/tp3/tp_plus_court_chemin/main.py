import graphelib
import matplotlib.pyplot as plt
import dijkstra_et_astar

plt.figure("carte")
l_graphe = graphelib.Graphe()
l_carte = graphelib.Carte(l_graphe)
l_carte.afficher()
#l_carte.afficher_chemin([l_graphe.dic_noeud[1],l_graphe.dic_noeud[39]])

print("dijkstra:")
plt.figure("dijkstra")
distance,prec = dijkstra_et_astar.dijkstra(l_graphe,l_graphe.dic_noeud[62],l_graphe.dic_noeud[6])
liste_noeud = dijkstra_et_astar.precedent(distance,prec,l_graphe.dic_noeud[6])
l_carte.afficher_chemin(liste_noeud)
#plt.show()

print("")
print("A*")
plt.figure("A*")
distance_etoile,prec_etoile = dijkstra_et_astar.Astar(l_graphe,l_graphe.dic_noeud[62],l_graphe.dic_noeud[6])
liste_noeud_etoile = dijkstra_et_astar.precedent(distance_etoile,prec_etoile,l_graphe.dic_noeud[6])
l_carte.afficher_chemin(liste_noeud_etoile)
plt.show()




#cree fonction remonte chemin qui va remonter les prec du noeud arrive jusqu au noeud depart (avec comme valeur None)