#include "junction.hpp"
#include "vehicle.hpp"

#include "cassert"
#include <iostream>

Junction::Junction(const std::string &id, unsigned int duration, unsigned int in_number, unsigned int out_number) :
  Node(id, duration, in_number, out_number), _vehicle(nullptr) {
}

Junction::~Junction() {
}

void Junction::enter_vehicle(Vehicle *vehicle, unsigned int time) {

  assert(not is_full());

  std::cout << time << ": " << vehicle->id() << " enters " << id() << std::endl;

  _vehicle.reset(vehicle);
  vehicle->add_node(this);
  vehicle->last_time(time);
}

void Junction::exit_vehicle(unsigned int time) {
  if (_vehicle != nullptr and _vehicle->last_time() + duration() == time) {
    Vehicle *vehicle = _vehicle.release();

    std::cout << time << ": " << vehicle->id() << " exits " << id() << std::endl;

    if (out_number() != 0) {
      put_vehicle_into_out_link(vehicle, time);
    }
    remove_vehicle();
  }
}