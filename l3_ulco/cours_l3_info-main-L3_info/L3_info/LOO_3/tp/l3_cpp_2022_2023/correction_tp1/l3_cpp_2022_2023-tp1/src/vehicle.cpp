#include "vehicle.hpp"

#include <cstring>

Vehicle::Vehicle(const char *id) : id(new char[strlen(id) + 1]) {
  strcpy(this->id, id);
  link = nullptr;
  node = nullptr;
  last_time = 0;
}

Vehicle::~Vehicle() {
  delete[] id;
}