#ifndef TRAFFIC_VEHICLE_HPP
#define TRAFFIC_VEHICLE_HPP

#include <string>

#include "path.hpp"

class Link;

class Node;

class Vehicle {
public:
  Vehicle(const std::string &id, const std::initializer_list<uint8_t> &indexes);

  Vehicle(const Vehicle &v);

  ~Vehicle();

  void add_link(Link *link) { _link = link; }

  void add_node(Node *node) { _node = node; }

  uint8_t current_path_index() const { return _path.current(); }

  void next_path() { return _path.next(); }

  const std::string &id() const { return _id; }

  unsigned int last_time() const { return _last_time; }

  void last_time(unsigned int time) { _last_time = time; }

  void remove_link() { _link = nullptr; }

  void remove_node() { _node = nullptr; }

private:
  std::string _id;
  Path _path;
  Link *_link;
  Node *_node;
  unsigned int _last_time;
};

typedef Vehicle *VehiclePtr;

#endif //TRAFFIC_VEHICLE_HPP
