#ifndef TRAFFIC_NODE_H
#define TRAFFIC_NODE_H

struct Link;
struct Vehicle;

struct Node {
  char *id;
  unsigned int duration;
  unsigned int in_number;
  unsigned int out_number;
  struct Link **in_links;
  struct Link **out_links;
  struct Vehicle* vehicle;
};

struct Node *create_node(char *id, unsigned int duration, unsigned int in_number, unsigned int out_number);

void destroy_node(struct Node *node);

void add_vehicle_to_node(struct Vehicle *vehicle, struct Node *node, unsigned int time);

void attach_input_link_to_node(struct Link *link, struct Node *node);

void attach_output_link_to_node(struct Link *link, struct Node *node);

#endif //TRAFFIC_NODE_H
