#ifndef TRAFFIC_MAP_HPP
#define TRAFFIC_MAP_HPP

#include <memory>
#include <string>
#include <vector>

#include "link.hpp"
#include "node.hpp"

namespace traffic {

class Map {
public:
  Map();

  void add_link(const std::string &id, unsigned int length, unsigned int way_number);

  void add_junction(const std::string &id, unsigned int duration, size_t in_number, size_t out_number);

  void add_round_about(const std::string &id, unsigned int duration, size_t in_number, size_t out_number,
                       unsigned int capacity);

  void attach_in_link(size_t index, const std::string &link_id, const std::string &node_id);

  void attach_out_link(size_t index, const std::string &node_id, const std::string &link_id);

  void push_vehicle(unsigned int t, std::unique_ptr<Vehicle> &vehicle);

  void run(unsigned int t);

private:
  std::vector<std::shared_ptr<Link>> _links;
  std::vector<std::shared_ptr<Node>> _nodes;
};

}

#endif //TRAFFIC_MAP_HPP
