#pragma once
#include <iostream>
#include "vector"

namespace factory {

class Factory {
public:
    std::vector<Zone>& addZone(const Zone& zone) const;
private:
    std::vector<Zone>zones;
};

} // Factory
