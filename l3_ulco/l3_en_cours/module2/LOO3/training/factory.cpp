#include "factory.hpp"


Factory::Factory(std::string id) : id(id) {}

void Factory::addTransportation(std::shared_ptr<Transportation> transportation) {
    transportations.push_back(transportation);
}

void Factory::addLabor(std::shared_ptr<Labor> labor) {
    labors.push_back(labor);
}



const std::shared_ptr<Labor>& Factory::createLabor(const std::string& laborId) {
    auto labor = std::make_shared<Labor>(laborId, std::shared_ptr<Factory>(this));
    labors.push_back(labor);
    return labors.back();
}

void Factory::displayLaborInfo(const std::shared_ptr<Labor>& labor) const {
    if (labor) {
        labor->displayInfo();
    } else {
        std::cout << "Invalid labor." << std::endl;
    }
}