import subprocess

executable = '../cmake-build-debug/src/node'
identifier = ['A', 'B', 'C']
ports = [3000, 3001, 3002]
commands = []

for index in range(len(ports)):
    if index < len(ports) - 1:
        neighbour_port = ports[index + 1]
    else:
        neighbour_port = ports[0]
    commands.append(
        subprocess.Popen([executable, '-i', identifier[index], '-p', str(ports[index]), '-n', str(neighbour_port), '-h', '127.0.0.1']))

for index in range(len(ports)):
    commands[index].wait()
