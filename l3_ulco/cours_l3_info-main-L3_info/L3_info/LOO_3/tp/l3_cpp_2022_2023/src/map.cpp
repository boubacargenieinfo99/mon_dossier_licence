#include "map.hpp"

#include <cstddef>
#include <cstdio>
#include <cstdlib>

Map::Map(unsigned int link_number, unsigned int node_number){
    this->link_number = link_number;
    this->node_number = node_number;
    this->links = new LinkPtr[link_number];

    for (unsigned int i = 0; i < link_number; ++i) {
        this->links[i] = nullptr;
    }
    this->nodes = new NodePtr[node_number];
    for (unsigned int i = 0; i < node_number; ++i) {
        this->nodes[i] = nullptr;
    }
}

Map::~Map() {
    for (unsigned int i = 0; i < link_number; ++i) {
        if (links[i] != nullptr) {
            links[i]->~Link();
        }
    }
    for (unsigned int i = 0; i < node_number; ++i) {
        if (nodes[i] != nullptr) {
            nodes[i]->~Node();
        }
    }
    delete links;
    delete nodes;
}

void Map::add_link_to_map(Link *link) {
    unsigned int index = 0;

    while (links[index] != nullptr && index < link_number) {
        ++index;
    }
    if (index < link_number) {
        links[index] = link;
    }
}

void Map::add_node_to_map(Node *node) {
    unsigned int index = 0;

    while (nodes[index] != nullptr && index < node_number) {
        ++index;
    }
    if (index < node_number) {
        nodes[index] = node;
    }
}

void Map::run_map(unsigned int time){//},Inputs inputs) {

    for (unsigned int i = 0; i < link_number; ++i) {
        Link *link = links[i];

        if (link != nullptr) {
            Vehicle *out_vehicle = link->run_link(time);

            // has a vehicle left the link?
            if (out_vehicle != nullptr) {
                // is there a next node?
                if (link->out_node != nullptr) {
                    link->out_node->add_vehicle_to_node(out_vehicle,time);
                }
            }
        }
    }
    for (unsigned int i = 0; i < node_number; ++i) {
        Node *node = nodes[i];

        if (node != nullptr) {
            // if a vehicle is present and this vehicle is present until the transit duration
            if (node->vehicle != nullptr && node->vehicle->last_time + node->duration <= time) {
                printf("%d: %s exits %s\n", time, node->vehicle->id, node->id);
                // the vehicle goes to the first out link (feature: use path)
                node->out_links[0]->add_vehicle_to_link(node->vehicle,time);
                // now, no vehicle is present in node
                node->vehicle = nullptr;
            }
        }
    }
}