#ifndef SPATIAL_ELEMENT_HPP
#define SPATIAL_ELEMENT_HPP

#include <string>
#include <memory>
#include "geometry.hpp"



class SpatialElement{
public:
    SpatialElement(std::string ID, std::unique_ptr<Geometry> geometry)
            : ID(ID), geometry(std::move(geometry)) {}

    SpatialElement(const SpatialElement& e)
            : ID(e.ID), geometry(e.geometry == nullptr ? nullptr :e.geometry->clone()) {}

private:
    std::string ID;
    std::unique_ptr<Geometry> geometry;
};

#endif