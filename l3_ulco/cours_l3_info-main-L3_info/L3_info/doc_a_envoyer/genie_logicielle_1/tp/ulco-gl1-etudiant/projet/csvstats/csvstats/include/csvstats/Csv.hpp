#pragma once

#ifndef TRAFFIC_CSV_HPP
#define TRAFFIC_CSV_HPP

#include <vector>
#include <fstream>
#include <sstream>
#include <cmath>


class Csv{
    public:
        Csv();
        ~Csv();

        void loadData(std::ifstream& file);


        std::vector<std::vector<double>> getData();
        void afficheData();
    private:
        std::vector<std::vector<double>> data;
};




#endif // TRAFFIC_CSV_HPP