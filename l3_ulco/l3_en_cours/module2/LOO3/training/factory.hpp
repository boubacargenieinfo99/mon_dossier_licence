

#ifndef TRAINING_FACTORY_HPP
#define TRAINING_FACTORY_HPP
#pragma once

#include <vector>
#include <memory>
#include <unordered_map>
#include "conveyor.hpp"
#include "coordinates.hpp"


class Zone;
class Transportation;
class Labor;

class Factory {
public:
    Factory(std::string id);
    void addTransportation(std::shared_ptr<Transportation> transportation);
    void addLabor(std::shared_ptr<Labor> labor);
    const std::shared_ptr<Labor>& createLabor(const std::string& laborId);
    void displayLaborInfo(const std::shared_ptr<Labor>& labor) const;

private:
    std::string id;
    std::vector<std::shared_ptr<Transportation>> transportations;
    std::vector<std::shared_ptr<Labor>> labors;
    std::vector<std::shared_ptr<Zone>> zones;
    // Dictionnaire pour stocker les associations entre les machines et les convoyeurs
    std::unordered_map<std::string, std::shared_ptr<Conveyor>> downstream_conveyors_;
    std::unordered_map<std::string, std::shared_ptr<Conveyor>> upstream_conveyors_;
};

#endif //TRAINING_FACTORY_HPP
