"use strict";

const Color = {NONE: -1, BLACK: 0, WHITE: 1};

export default Color;
