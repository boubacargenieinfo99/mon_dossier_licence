#include <iostream>
#include "vector"

class Machine{
public:
    Machine(int id): id(id){}
    int getId() const{return id;}
private:
    int id;
};