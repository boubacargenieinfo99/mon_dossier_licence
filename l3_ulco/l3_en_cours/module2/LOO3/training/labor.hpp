#pragma once

#include "unavailableElement.hpp"
#include <string>
#include <memory>

class Factory;

class Labor : public UnavailableElement {
public:
    Labor(const std::string& id, const std::shared_ptr<Factory>& factory);

    void attachToZone(const std::string& zoneId);
    void attachToMachine(const std::string& machineId);
    void attachToTransportation(const std::string& transportationId);

    void displayInfo() const;

private:
    std::weak_ptr<Factory> factory;
    std::string attachedZoneId;
    std::string attachedMachineId;
    std::string attachedTransportationId;
};
