#ifndef TRAFFIC_INPUTS_HPP
#define TRAFFIC_INPUTS_HPP

#include "vehicle.hpp"

#include <memory>
#include <string>
#include <valarray>

class Inputs {
public:
  Inputs(unsigned int vehicle_number);

  ~Inputs();

  void add_vehicle(Vehicle *vehicle, unsigned int time, const std::string &node_id);

  unsigned int time(unsigned int index) const { return _times[index]; }

  const std::string &node_id(unsigned int index) const { return _node_ids[index]; }

  unsigned int vehicle_number() const { return _vehicle_number; }

  std::unique_ptr<Vehicle> &vehicle(unsigned int index) { return _vehicles[index]; }

private:
  unsigned int _vehicle_number;
  std::valarray<std::unique_ptr<Vehicle>> _vehicles;
  std::valarray<unsigned int> _times;
  std::valarray<std::string> _node_ids;
};

#endif //TRAFFIC_INPUTS_HPP
