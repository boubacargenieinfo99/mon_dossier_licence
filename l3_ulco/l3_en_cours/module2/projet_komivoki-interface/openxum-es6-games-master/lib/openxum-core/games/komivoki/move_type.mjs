"use strict";

const MoveType = {MOVE_PIECE: 0, CAPTURE: 1, PASS: 2};

export default MoveType; 
