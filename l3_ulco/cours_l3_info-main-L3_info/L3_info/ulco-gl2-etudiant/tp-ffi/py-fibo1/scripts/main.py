import mypackage.myfibo as fib

if __name__ == '__main__':

    print('TODO')
    
    # print fibo_naive(i) for i from 0 to 10
    
    for i in range(11):
        print("fibo_naive({}) = {}".format(i,fib.fibo_naive(i)))

    # print fibo_iterative(i) for i from 0 to 10

    for i in range(11):
        print("fibo_iterative({}) = {}".format(i,fib.fibo_iterative(i)))