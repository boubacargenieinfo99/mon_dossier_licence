function x1= simulationBoxMuler(n)

    u1=rand(1,n);
    u2=rand(1,n);
    
    x1=(sqrt(-2.*log(u1))).*(cos(2*pi.*u2));   
end
