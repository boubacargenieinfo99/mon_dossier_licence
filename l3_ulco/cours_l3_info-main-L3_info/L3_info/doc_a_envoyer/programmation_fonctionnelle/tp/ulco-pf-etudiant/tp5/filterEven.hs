import Data.Char

filterEven1 :: [Int] -> [Int]
filterEven1 [] = []
filterEven1 (x:xs) = if (even x) then x : filterEven1 xs else filterEven1 xs

filterEven2 :: [Int] -> [Int]
filterEven2 xs = filter even xs

myfilter :: (a -> Bool) -> [a] -> [a]
myfilter f [] = []
myfilter f (x:xs) = if f x then x : myfilter f xs else myfilter f xs

main ::IO()
main = do
    print (filterEven1 [1,2,3,4])
    print (filterEven2 [5,6,7,8])
    print (myfilter even [9,10,11,12])
    print (myfilter isLetter "foobar te salut depuis le 62")
