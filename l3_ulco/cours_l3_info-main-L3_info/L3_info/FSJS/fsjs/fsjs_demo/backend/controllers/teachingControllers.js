module.exports.home = (req, res)=>{
    res.send('<h1>Enseignements</h1>');
}

module.exports.javascript = (req, res) => {
    res.send('<h1>Cours de Javascript</h1>');
}

module.exports.php = (req, res) => {
    res.send('<h1>Cours de PHP</h1>');
}

module.exports.node = (req, res)=>{
    res.send('<h1>Introduction a Node.js</h1>');
}

module.exports.express = (req, res)=>{
    res.send('<h1>Cours sur le framework Express</h1>');
}
