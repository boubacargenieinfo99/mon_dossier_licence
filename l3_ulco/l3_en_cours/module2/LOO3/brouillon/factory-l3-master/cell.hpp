#include "coordinates.hpp"

class Cell : public Coordinates {
public:
    Cell(int x, int y) : x(x), y(y) {}

    int getX() const { return x; }
    int getY() const { return y; }

private:
    int x;
    int y;
};