#include "iterator.hpp"
#include "map.hpp"

#include <iostream>
#include <cstring>

Map::Map() : _input_index(0) {

//  std::cout << "[Map] constructor" << std::endl;

}

Map::~Map() {

//  std::cout << "[Map] destructor" << std::endl;

}

void Map::add_link(const std::shared_ptr<Link> &link) {
  _links.add_last(link);
}

void Map::add_node(const std::shared_ptr<Node> &node) {
  _nodes.add_last(node);
}

void Map::put_vehicle_into_node(Vehicle *vehicle, const std::string &node_id, unsigned int time) const {
  Iterator<Node> it(_nodes);

  while (it.has_more()) {
    if (it.current()->id() == node_id) { break; }
    it.next();
  }
  if (it.has_more()) {
    it.current()->enter_vehicle(vehicle, time);
  }
}

void Map::run(unsigned int time) {
  run_link(time);
  run_node(time);
}

void Map::run(unsigned int time, Inputs &inputs) {
  while (_input_index < inputs.vehicle_number() and inputs.time(_input_index) == time) {
    put_vehicle_into_node(inputs.vehicle(_input_index).release(), inputs.node_id(_input_index), time);
    ++_input_index;
  }
  run_link(time);
  run_node(time);
}

void Map::run_link(unsigned int time) {
  Iterator<Link> it(_links);

  while (it.has_more()) {
    const std::shared_ptr<Link> &link = it.current();
    Vehicle *out_vehicle = link->run(time);

    // has a vehicle left the link?
    if (out_vehicle != nullptr) {
      link->exit_vehicle(out_vehicle, time);
    }
    it.next();
  }
}

void Map::run_node(unsigned int time) {
  Iterator<Node> it(_nodes);

  while (it.has_more()) {
    const std::shared_ptr<Node> &node = it.current();

    node->exit_vehicle(time);
    it.next();
  }
}