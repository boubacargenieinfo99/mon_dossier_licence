var Express = require('express');
var router = Express.Router();

const teachingControllers = require('../controllers/teachingControllers');


// Router pour tous les chemins sous /teaching (voir app.js)

router.get('/',teachingControllers.home);

router.get('/javascript',teachingControllers.javascript);

router.get('/php',teachingControllers.php);;

router.get('/node',teachingControllers.node);

router.get('/node/express',teachingControllers.express);

module.exports = router;