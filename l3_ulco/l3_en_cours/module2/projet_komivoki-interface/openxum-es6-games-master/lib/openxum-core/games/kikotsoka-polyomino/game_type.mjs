"use strict";

const GameType = {SMALL: 0, MEDIUM: 1, LARGE: 2};

export default GameType;