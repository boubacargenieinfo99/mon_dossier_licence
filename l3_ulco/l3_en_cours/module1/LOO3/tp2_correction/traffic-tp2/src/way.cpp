#include "way.hpp"

namespace traffic {

Way::Way(unsigned int length) : _cells(length / _cell_length) {}

bool Way::is_free() const {
  return _cells[0].is_free();
}

void Way::push_vehicle(unsigned int t, std::unique_ptr<Vehicle> &vehicle) {
  _cells[0].push_vehicle(t, vehicle);
}

bool Way::ready_to_exit() const {
  return not _cells[_cells.size() - 1].is_free();
}

}