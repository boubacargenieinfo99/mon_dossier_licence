#ifndef TRAFFIC_MAP_H
#define TRAFFIC_MAP_H

#include "link.hpp"
#include "node.hpp"
#include "inputs.hpp"

struct Map {

    //createur

    Map(unsigned int link_number, unsigned int node_number);

    //destructeur

    ~Map();

    //attributs

    unsigned int link_number;
    unsigned int node_number;
    Link **links;
    Node **nodes;

    //fonctions

    void add_link_to_map(Link *link);
    void add_node_to_map(Node *node);
    void run_map(unsigned int time);//,Inputs inputs);
};

#endif //TRAFFIC_MAP_H