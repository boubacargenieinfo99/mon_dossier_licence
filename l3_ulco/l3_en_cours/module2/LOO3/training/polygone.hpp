
#ifndef TRAINING_POLYGONE_HPP
#define TRAINING_POLYGONE_HPP
#pragma once

#include "geometry.hpp"
#include <vector>
#include <memory>

class Point;

class Polygon : public Geometry {
public:
    Polygon();
    void addPoint(std::unique_ptr<Point> point);
    void displayInfo() const override;

private:
    std::vector<std::unique_ptr<Point>> points;
};

#endif //TRAINING_POLYGONE_HPP
