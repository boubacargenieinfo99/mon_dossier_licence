#include <iostream>
#include <string>
#include "sockpp/tcp_connector.h"
#include <chrono>

using namespace std;
using namespace std::chrono;

int main(int argc, char* argv[]) {
    cout << "Client TCP d'exemple pour 'sockpp'" << endl;

    string host = (argc > 1) ? argv[1] : "localhost";
    in_port_t port = (argc > 2) ? atoi(argv[2]) : 12345;

    sockpp::initialize();

    // Crée implicitement une inet_address à partir de {host, port}
    // et tente ensuite la connexion.
    sockpp::tcp_connector conn({host, port}, seconds{5});
    if (!conn) {
        cerr << "Erreur lors de la connexion au serveur à l'adresse "
             << sockpp::inet_address(host, port)
             << endl;
        return 1;
    }

    cout << "Connexion établie depuis " << conn.address() << endl;

    // Définir un délai d'attente pour les réponses
    if (!conn.read_timeout(seconds(5))) {
        cerr << "Erreur lors du réglage du délai d'attente sur le flux TCP."
             << endl;
    }

    string s, sret;
    while (getline(cin, s) && !s.empty()) {
        auto write_result = conn.write(s);
        if (!write_result || write_result.value() != ssize_t(s.length())) {
            cerr << "Erreur lors de l'écriture sur le flux TCP."
                 << endl;
            break;
        }

        sret.resize(s.length());
        ssize_t n;
        auto read_result = conn.read_n(&sret[0], s.length());
        
        n = static_cast<ssize_t>(read_result.value());

        if (n != ssize_t(s.length())) {
            cerr << "Erreur lors de la lecture depuis le flux TCP."
                 << endl;
            break;
        }

        cout << sret << endl;
    }

    return (!conn) ? 1 : 0;
}
