#include <todolist-cpp/todolist-cpp.hpp>
#include <todolist-cpp/views.hpp>

#include <catch2/catch.hpp>

TEST_CASE( "test creation tache et affichage"){
    struct Task t;
    t.id = 1;
    t.desc = "premiere tache a faire";
    std::string message;
    View v;
    message = v.fmtTask(t);
    REQUIRE( message ==  "premiere tache a faire");
}

TEST_CASE("test creation Board et getTodo et getDone "){
    Board maListe;
    REQUIRE( (maListe.getTodo()).empty());
    REQUIRE( (maListe.getDone()).empty());
}

TEST_CASE("test ajouter tache dans board"){
    Board maListe;
    maListe.addTask("ma premiere tache");
    maListe.addTask("ma deuxieme tache");
    REQUIRE( !((maListe.getTodo()).empty()));
    REQUIRE( ((maListe.getTodo())[0]).desc == "ma premiere tache");
    REQUIRE( ((maListe.getTodo())[1]).desc == "ma deuxieme tache");
}

TEST_CASE("test afficher tache"){
    std::string message;
    Board maListe;
    maListe.addTask("ma premiere tache");
    maListe.addTask("ma deuxieme tache");
    View v;
    message = v.fmtBoard(maListe);
    REQUIRE( message == "Todo:\nma premiere tache\nma deuxieme tache\n\nDone:\n");
}