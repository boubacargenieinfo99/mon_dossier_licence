

#ifndef TRAINING_SPATIALELEMENT_HPP
#define TRAINING_SPATIALELEMENT_HPP
#pragma once

#include <memory>
#include <iostream>

class Geometry;

class SpatialElement {
public:
    SpatialElement();
    SpatialElement(int id, std::unique_ptr<Geometry> geometry);
    virtual void displayInfo() const;

private:
    int id;
    std::unique_ptr<Geometry> geometry;
};
#endif //TRAINING_SPATIALELEMENT_HPP
