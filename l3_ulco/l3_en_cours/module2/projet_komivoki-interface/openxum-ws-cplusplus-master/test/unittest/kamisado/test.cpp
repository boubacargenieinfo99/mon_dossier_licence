#define CATCH_CONFIG_MAIN

#include "../catch.hpp"

#include <openxum/core/games/kamisado/engine.hpp>
#include <openxum/core/games/kamisado/game_type.hpp>

TEST_CASE("default coordinates", "kamisado")
{
  openxum::core::games::kamisado::Coordinates coordinates;

  REQUIRE(coordinates.x() == -1);
  REQUIRE(coordinates.y() == -1);
}

TEST_CASE("invalid coordinates", "kamisado")
{
  openxum::core::games::kamisado::Coordinates coordinates;

  REQUIRE(not coordinates.is_valid());
}

TEST_CASE("valid coordinates", "kamisado")
{
  openxum::core::games::kamisado::Coordinates coordinates(3, 6);

  REQUIRE(coordinates.is_valid());
}

TEST_CASE("coordinates to string", "kamisado")
{
  openxum::core::games::kamisado::Coordinates coordinates(3, 6);

  REQUIRE(coordinates.to_string() == "d7");
}
