#include <todolist-cpp/todolist-cpp.hpp>

void Board::addTask(std::string description){
    struct Task newTask;
    newTask.id = nextId;
    newTask.desc = description;
    Todo.push_back(newTask);
    nextId ++;
}
