import re
import sys

def charger_dico(program):
    ligne = program.split("\n")
    dico = {'TABLE': "", 'AXIOME': "", 'TOKENS': [], 'NON_TERMINAUX': []}
    dico['TABLE'] = program

    compteur = 0
    for x in ligne:
        part_un = (x.split(":"))[0]
        part_deux = (x.split(":"))[1]
        p_un = part_un.split()
        p_deux = part_deux.split()
        compteur_y = 0
        for y in p_un:
            if re.search("""[A-Z]'?""", y):
                if compteur == 0 and compteur_y == 0:
                    compteur_y = 1
                    dico['AXIOME'] = y
                if not (y in dico['NON_TERMINAUX']):
                    dico['NON_TERMINAUX'].append(y)
            if re.search("""[a-z]+""", y) or y == "+" or y == "*":
                if not (y in dico['TOKENS']):
                    dico['TOKENS'].append(y)
        for y in p_deux:
            if y == r"""[A-Z]'?""":
                if not (y in dico['NON_TERMINAUX']):
                    dico['NON_TERMINAUX'].append(y)
            if y == r"""[a-z]+""" or y == r"\+" or y == r"\*":
                if not (y in dico['TOKENS']):
                    dico['TOKENS'].append(y)
        dico[compteur] = (p_un, p_deux)
        compteur = compteur + 1
    return dico

def give_tokens(chaine,dico):
    chaine_splitee = chaine.split()
    for x in dico['TABLE']:



# mot accepte id , id * id , ...
# mot non accepte * , + , ...

# main
file = open(sys.argv[1], "r")
program = file.read()
dico = charger_dico(program)

print(dico['TABLE'])
print(dico['AXIOME'])
print(dico['NON_TERMINAUX'])
print(dico['TOKENS'])


