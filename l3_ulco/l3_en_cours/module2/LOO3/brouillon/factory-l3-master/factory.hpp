#ifndef FACTORY_HPP
#define FACTORY_HPP

#include <vector>
#include "zone.hpp"
#include "transportation.hpp"

class Factory {
public:
    Factory(std::string Id, std::initializer_list<Zone> zones) : Id(Id) {}

    void attach_downstream_conveyor_to_machine(const std::string& machineId, std::shared_ptr<Conveyor> conveyor){
        auto machine= get_machine(machineId);
        machine->attachDownstreamConveyor(conveyor);
    }

    void attach_upstream_conveyor_to_machine(const std::string& machineId, std::shared_ptr<Conveyor> conveyor){
        auto machine= get_machine(machineId);
        machine->attachUpstreamConveyor(conveyor);
    }

    std::shared_ptr<Machine> get_machine(const std::string& machineId) const {
        for(const auto& machine:machines){
            if(machine->getId() == machineId){
                return machine;
            }
        }
        return nullptr;
    }
    void addTransportation(std::shared_ptr<Transportation> transportation){
        transportations.push_back(transportation);
    }


private:
    std::string Id;
    std::vector<Zone> zones;
    std::vector<std::shared_ptr<Machine>> machines;
    std::vector<std::shared_ptr<Transportation>> transportations;
};

#endif