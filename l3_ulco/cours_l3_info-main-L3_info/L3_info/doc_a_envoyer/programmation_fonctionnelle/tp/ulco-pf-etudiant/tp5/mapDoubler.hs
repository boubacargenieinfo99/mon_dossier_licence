import Data.Char

mapDoubler1 :: [Int] -> [Int]
mapDoubler1 [] = []
mapDoubler1 (x:xs) = x*2 : mapDoubler1 xs

mapDoubler2 :: [Int] -> [Int]
mapDoubler2 xs = map (*2) xs

mymap :: (a -> b) -> [a] -> [b]
mymap f [] = []
mymap f (x:xs) = f x : mymap f xs

main :: IO()
main = do
    print (mapDoubler1 [1,2,3])
    print (mapDoubler2 [10,20,30])
    print (mymap (*2) [100,200,300])
    print (mymap toUpper "foobar")
    print (mymap isLetter ['a','2',' '] )