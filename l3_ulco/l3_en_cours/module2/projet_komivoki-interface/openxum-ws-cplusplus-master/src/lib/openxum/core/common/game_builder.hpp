/**
 * @file openxum/core/common/game_builder.hpp
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2011-2024 Openxum Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPENXUM_WS_CPP_GAME_BUILDER_HPP
#define OPENXUM_WS_CPP_GAME_BUILDER_HPP

#include <openxum/core/common/abstract_player.hpp>

namespace openxum::core::common {

class GameBuilder {
public:
  virtual AbstractPlayer *operator()(int player_color, int opponent_color, int type, int color) = 0;

  virtual ~GameBuilder() = default;
};

}

#endif //OPENXUM_WS_CPP_GAME_BUILDER_HPP
