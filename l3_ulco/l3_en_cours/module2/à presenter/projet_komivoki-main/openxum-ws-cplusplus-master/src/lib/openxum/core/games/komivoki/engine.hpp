/**
 * @file openxum/core/games/komivoki/engine.hpp
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2011-2024 Openxum Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPENXUM_CORE_GAMES_KOMIVOKI_ENGINE_HPP
#define OPENXUM_CORE_GAMES_KOMIVOKI_ENGINE_HPP

#include <openxum/core/common/two_player_engine.hpp>

#include <openxum/core/games/komivoki/color.hpp>
#include <openxum/core/games/komivoki/coordinates.hpp>
#include <openxum/core/games/komivoki/decision.hpp>
#include <openxum/core/games/komivoki/entity.hpp>
#include <openxum/core/games/komivoki/game_type.hpp>
#include <openxum/core/games/komivoki/phase.hpp>

#include <vector>

namespace openxum::core::games::komivoki
{

  class Engine;

  struct BoardInitializer
  {
    virtual void operator()(Engine & /* engine */) const {}
  };

  struct FourFourBoardInitializer : public komivoki::BoardInitializer
  {
    void operator()(komivoki::Engine &engine) const override;
  };

  struct FiveFiveBoardInitializer : public komivoki::BoardInitializer
  {
    void operator()(komivoki::Engine &engine) const override;
  };

  struct EightEightBoardInitializer : public komivoki::BoardInitializer
  {
    void operator()(komivoki::Engine &engine) const override;
  };

  class Engine : public openxum::core::common::TwoPlayerEngine<Decision>
  {
  public:
    Engine() : _type(SIMPLE), _size({4, 4}), _max_length(1) {}

    Engine(int type, int color, const std::pair<int, int> &size = {8, 8}, unsigned int max_length = 1,
           const BoardInitializer &init = EightEightBoardInitializer());

    ~Engine() override;

    int best_is() const override { return NONE; }

    Engine *clone() const override;

    int current_color() const override;

    unsigned int current_goal(int color) const override
    {
      return color == Color::BLACK ? _white_entity_number : _black_entity_number;
    }

    double gain(int /* color */, bool /* finish */) const override { return 1; }

    const std::string &get_name() const override { return GAME_NAME; }

    openxum::core::common::Moves<Decision> get_possible_move_list() const override;

    std::string hash() const override;

    bool is_finished() const override;

    bool is_stoppable() const override { return false; }

    void move(const openxum::core::common::Move<Decision> &move) override;

    void parse(const std::string &) override {}

    void put_entity(const Coordinates &coordinates, Entity *entity)
    {
      _board[coordinates.x() + _size.first * coordinates.y()].push_back(
          std::shared_ptr<Entity>(entity));
      if (entity->color == BLACK)
      {
        ++_black_entity_number;
      }
      else
      {
        ++_white_entity_number;
      }
    }

    int type() const { return _type; }

    std::string to_string() const override;

    int winner_is() const override;

    int get_capture_count()
    {
      return _capture_count;
    }

  private:
    void change_color();

    bool is_new_state(const Color &color, const Coordinates &from, const Coordinates &to) const;

    bool is_possible_cell(const Color &color, const Coordinates &coordinates) const;

    bool is_possible_to_capture(const Color &color) const;

    bool is_possible_to_capture(const Color &color, const Coordinates &coordinates) const;

    int next_color(int color);

    int _capture_count;

    const int _type;
    const std::pair<int, int> _size;
    const unsigned int _max_length;

    int _color;
    int _phase;
    std::vector<std::vector<std::shared_ptr<Entity>>> _board;
    unsigned int _black_entity_number;
    unsigned int _white_entity_number;
    std::vector<std::vector<std::string>> _ids;
    unsigned int _pass;

    const static std::string GAME_NAME;
    const static std::vector<std::pair<int, int>> deltas;
  };

}

#endif
