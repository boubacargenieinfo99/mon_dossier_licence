#include "map.hpp"

using namespace traffic;

void create_map(Map &map) {
  map.add_link("L1", 200, 1);
  map.add_link("L2", 500, 1);
  map.add_link("L3", 800, 1);
  map.add_link("L4", 900, 1);
  map.add_link("L5", 300, 1);
  map.add_link("L6", 400, 1);
  map.add_link("L7", 1000, 2);
  map.add_link("L8", 500, 1);
  map.add_link("L9", 300, 1);
  map.add_link("L10", 200, 1);
  map.add_link("L11", 400, 1);
  map.add_link("L12", 700, 1);

  map.add_node("N1", 2, 0, 1);
  map.add_node("N2", 2, 1, 0);
  map.add_node("N3", 2, 1, 2);
  map.add_node("N4", 2, 1, 1);
  map.add_node("N5", 2, 2, 1);
  map.add_node("N6", 2, 1, 2);
  map.add_node("N7", 2, 1, 0);
  map.add_node("N8", 2, 0, 1);
  map.add_node("N9", 2, 1, 2);
  map.add_node("N10", 2, 2, 1);
  map.add_node("N11", 2, 0, 1);
  map.add_node("N12", 2, 1, 0);
  map.add_node("N13", 2, 1, 0);

  map.attach_out_link(0, "N1", "L8");
  map.attach_out_link(0, "N8", "L4");
  map.attach_out_link(0, "N11", "L1");
  map.attach_in_link(0, "L7", "N2");
  map.attach_in_link(0, "L10", "N12");
  map.attach_in_link(0, "L12", "N7");
  map.attach_in_link(0, "L8", "N3");
  map.attach_out_link(0, "N3", "L9");
  map.attach_out_link(1, "N3", "L11");
  map.attach_in_link(0, "L4", "N6");
  map.attach_out_link(0, "N6", "L6");
  map.attach_out_link(1, "N6", "L5");

  map.attach_in_link(0, "L1", "N9");
  map.attach_out_link(0, "N9", "L3");
  map.attach_out_link(1, "N9", "L2");

  map.attach_in_link(0, "L2", "N5");
  map.attach_in_link(1, "L6", "N5");
  map.attach_out_link(0, "N5", "L7");

  map.attach_in_link(0, "L5", "N10");
  map.attach_in_link(1, "L9", "N10");
  map.attach_out_link(0, "N10", "L10");

  map.attach_in_link(0, "L11", "N4");
  map.attach_out_link(0, "N4", "L12");

  map.attach_in_link(0, "L3", "N13");
}

int main() {
  Map map;
  auto vehicle = std::make_unique<Vehicle>("V1", 10);

  create_map(map);
  map.push_vehicle(0, vehicle, "N1");
  return 0;
}