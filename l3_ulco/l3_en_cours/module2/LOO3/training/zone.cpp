#include "zone.hpp"

Zone::Zone(std::string id) : SpatialElement() {}

void Zone::addSubZone(std::shared_ptr<Zone> subZone) {
    subZones.push_back(subZone);
}

void Zone::addLabor(std::shared_ptr<Labor> labor) {
    labors.push_back(labor);
}

void Zone::addMachine(std::shared_ptr<Machine> machine) {
    machines.push_back(machine);
}