/**
 * @file openxum/core/games/komivoki/engine.cpp
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2011-2024 Openxum Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <openxum/core/games/komivoki/engine.hpp>

#include <iostream>
#include <memory>

namespace openxum::core::games::komivoki {

const std::string Engine::GAME_NAME = "komivoki";

// four neighbors
//const std::vector<std::pair<int, int>> TwoPlayerEngine::deltas = {{-1, 0},
//                                                         {0,  -1},
//                                                         {0,  1},
//                                                         {1,  0}};

// eight neighbors
const std::vector<std::pair<int, int>> Engine::deltas = {{-1, -1},
                                                         {-1, 0},
                                                         {-1, 1},
                                                         {0,  -1},
                                                         {0,  1},
                                                         {1,  -1},
                                                         {1,  0},
                                                         {1,  1}};

void FourFourBoardInitializer::operator()(komivoki::Engine &engine) const {
  engine.put_entity(komivoki::Coordinates(1, 0), new komivoki::Entity(komivoki::BLACK, 1));
  engine.put_entity(komivoki::Coordinates(2, 0), new komivoki::Entity(komivoki::BLACK, 1));

  engine.put_entity(komivoki::Coordinates(1, 3), new komivoki::Entity(komivoki::WHITE, 1));
  engine.put_entity(komivoki::Coordinates(2, 3), new komivoki::Entity(komivoki::WHITE, 1));
}

void FiveFiveBoardInitializer::operator()(komivoki::Engine &engine) const {
  engine.put_entity(komivoki::Coordinates(1, 0), new komivoki::Entity(komivoki::BLACK, 1));
  engine.put_entity(komivoki::Coordinates(2, 0), new komivoki::Entity(komivoki::BLACK, 1));
  engine.put_entity(komivoki::Coordinates(3, 0), new komivoki::Entity(komivoki::BLACK, 1));
  engine.put_entity(komivoki::Coordinates(2, 1), new komivoki::Entity(komivoki::BLACK, 2));

  engine.put_entity(komivoki::Coordinates(1, 4), new komivoki::Entity(komivoki::WHITE, 1));
  engine.put_entity(komivoki::Coordinates(2, 4), new komivoki::Entity(komivoki::WHITE, 1));
  engine.put_entity(komivoki::Coordinates(3, 4), new komivoki::Entity(komivoki::WHITE, 1));
  engine.put_entity(komivoki::Coordinates(2, 3), new komivoki::Entity(komivoki::WHITE, 2));
}

void EightEightBoardInitializer::operator()(komivoki::Engine &engine) const {
  engine.put_entity(komivoki::Coordinates(2, 0), new komivoki::Entity(komivoki::BLACK, 1));
  engine.put_entity(komivoki::Coordinates(3, 0), new komivoki::Entity(komivoki::BLACK, 1));
  engine.put_entity(komivoki::Coordinates(4, 0), new komivoki::Entity(komivoki::BLACK, 1));
  engine.put_entity(komivoki::Coordinates(5, 0), new komivoki::Entity(komivoki::BLACK, 1));
  engine.put_entity(komivoki::Coordinates(3, 1), new komivoki::Entity(komivoki::BLACK, 2));
  engine.put_entity(komivoki::Coordinates(4, 1), new komivoki::Entity(komivoki::BLACK, 2));

  engine.put_entity(komivoki::Coordinates(2, 7), new komivoki::Entity(komivoki::WHITE, 1));
  engine.put_entity(komivoki::Coordinates(3, 7), new komivoki::Entity(komivoki::WHITE, 1));
  engine.put_entity(komivoki::Coordinates(4, 7), new komivoki::Entity(komivoki::WHITE, 1));
  engine.put_entity(komivoki::Coordinates(5, 7), new komivoki::Entity(komivoki::WHITE, 1));
  engine.put_entity(komivoki::Coordinates(3, 6), new komivoki::Entity(komivoki::WHITE, 2));
  engine.put_entity(komivoki::Coordinates(4, 6), new komivoki::Entity(komivoki::WHITE, 2));
}

// constructors
Engine::Engine(int type, int color, const std::pair<int, int> &size, unsigned int max_length,
               const BoardInitializer &init)
  : _type(type), _size(size), _max_length(max_length), _color(Color(color)), _phase(MOVE_ENTITY), _black_entity_number(0),
    _white_entity_number(0), _pass(0) {
  for (int x = 0; x < _size.first; ++x) {
    for (int y = 0; y < _size.second; ++y) {
      _board.emplace_back();
    }
  }
  init(*this);
  _ids.emplace_back();
  _ids.emplace_back();
}

Engine::~Engine() {}

// public methods
Engine *Engine::clone() const {
  auto e = new Engine(_type, _color, _size, _max_length, BoardInitializer());

  for (int x = 0; x < _size.first; ++x) {
    for (int y = 0; y < _size.second; ++y) {
      if (not _board[x + _size.first * y].empty()) {
        for (const auto& entity: _board[x + _size.first * y]) {
          e->_board[x + _size.first * y].push_back(std::make_shared<Entity>(entity->color, entity->level));
        }
        std::reverse(e->_board[x + _size.first * y].begin(), e->_board[x + _size.first * y].end());
      }
    }
  }
  e->_phase = _phase;
  e->_black_entity_number = _black_entity_number;
  e->_white_entity_number = _white_entity_number;
  e->_pass = _pass;
  e->_ids = _ids;
  return e;
}

int Engine::current_color() const {
  return _color;
}

openxum::core::common::Moves<Decision> Engine::get_possible_move_list() const {
  openxum::core::common::Moves<Decision> moves;

  if (_phase == MOVE_ENTITY) {
    for (int x = 0; x < _size.first; ++x) {
      for (int y = 0; y < _size.second; ++y) {
        if (not _board[x + _size.first * y].empty() and _board[x + _size.first * y].cbegin()->get()->color == _color) {
          Coordinates from(x, y);

          std::for_each(deltas.cbegin(), deltas.cend(), [this, x, y, from, &moves](const auto &p) {
            Coordinates to(x + p.first, y + p.second);

            if (is_possible_cell(Color(current_color()), to)) {
              if (is_new_state(Color(current_color()), from, to)) {
                moves.push_back(common::Move<Decision>(Decision(MOVE, from, to)));
              }
            }
          });
        }
      }
    }
  } else if (_phase == CAPTURE_ENTITY_BEFORE or _phase == CAPTURE_ENTITY_AFTER) {
    for (int x = 0; x < _size.first; ++x) {
      for (int y = 0; y < _size.second; ++y) {
        Coordinates from(x, y);

        if (is_possible_to_capture(Color(_color), from)) {
          moves.push_back(common::Move<Decision>(Decision(CAPTURE, from)));
        }
      }
    }
  }
  if (moves.empty()) {
    moves.push_back(common::Move<Decision>(Decision(PASS)));
  }
  return moves;
}

std::string Engine::hash() const {
  std::string str;

  for (int x = 0; x < _size.first; ++x) {
    for (int y = 0; y < _size.second; ++y) {
      if (_board[x + _size.first * y].empty()) {
        str += "X";
      } else if (_board[x + _size.first * y].cbegin()->get()->color == current_color()) {
        str += _board[x + _size.first * y].cbegin()->get()->color == BLACK ? "B" : "W";
        for (const auto& e: _board[x + _size.first * y]) {
          str += std::to_string(e->level);
        }
      } else {
        str += "X";
      }
    }
  }
  return str;
}

bool Engine::is_finished() const {
  return _phase == FINISH;
}

void Engine::move(const openxum::core::common::Move<Decision> &move) {
  std::for_each(move.begin(), move.end(), [this](const Decision &m) {
                  ++_move_number;
                  if (m.type() == MOVE) {
                    _pass = 0;
                    _board[m.to().x() + _size.first * m.to().y()].push_back(
                      _board[m.from().x() + _size.first * m.from().y()].back());
                    _board[m.from().x() + _size.first * m.from().y()].pop_back();
                    if (is_possible_to_capture(Color(current_color()))) {
                      _phase = Phase::CAPTURE_ENTITY_AFTER;
                    } else {
                      change_color();
                      if (is_possible_to_capture(Color(current_color()))) {
                        _phase = Phase::CAPTURE_ENTITY_BEFORE;
                      } else {
                        _phase = Phase::MOVE_ENTITY;
                      }
                    }
                  } else if (m.type() == CAPTURE) {
                    std::vector<std::shared_ptr<Entity>> &cell = _board[m.from().x() + _size.first * m.from().y()];

                    _pass = 0;
                    if (_color == BLACK) {
                      _white_entity_number -= cell.size();
                    } else {
                      _black_entity_number -= cell.size();
                    }
                    cell.erase(cell.begin(), cell.end());
                    if ((_color == BLACK and _white_entity_number == 0) or (_color == WHITE and _black_entity_number == 0)) {
                      _phase = Phase::FINISH;
                    } else {
                      if (_phase == CAPTURE_ENTITY_BEFORE) {
                        _phase = Phase::MOVE_ENTITY;
                      } else {
                        change_color();
                        _phase = Phase::MOVE_ENTITY;
                      }
                    }
                  } else if (m.type() == PASS) {
                    ++_pass;
                    if (_pass == 2) {
                      _phase = Phase::FINISH;
                    } else {
                      change_color();
                      _phase = Phase::MOVE_ENTITY;
                    }
                  }
                  _ids[current_color()].push_back(hash());
                }
  );
}

std::string Engine::to_string() const {
  std::string str;

  for (int y = 0; y < _size.first; ++y) {
    for (int x = 0; x < _size.second; ++x) {
      str += "[";
      if (_board[x + _size.first * y].empty()) {
        str += "      ";
      } else {
        const auto& cell = _board[x + _size.first * y];
        unsigned int capacity = std::accumulate(cell.cbegin(), cell.cend(), 0, [](int v, const auto& e) {
          return v + e->level;
        });

        str += cell.cbegin()->get()->color == BLACK ? "B" : "W";
        str += std::to_string(capacity);
        str += "/";
        str += std::to_string(cell.cbegin()->get()->level);
        str += "/";
        str += std::to_string(cell.size());
      }
      str += "]";
    }
    str += "\n";
  }
  return str;
}

int Engine::winner_is() const {
  if (is_finished()) {
    if (_pass == 2) {
      return NONE;
    } else {
      return _color;
    }
  } else {
    return NONE;
  }
}

// private methods
void Engine::change_color() {
  _color = next_color(_color);
}

bool Engine::is_new_state(const Color &color, const Coordinates &from, const Coordinates &to) const {
  std::string str;

  // TODO: verifier l'état de la couleur

  for (int x = 0; x < _size.first; ++x) {
    for (int y = 0; y < _size.second; ++y) {
      if (_board[x + _size.first * y].empty() and (to.x() != x or to.y() != y)) {
        str += "X";
      } else if (from.x() == x and from.y() == y) {
        str += "X";
      } else if (to.x() == x and to.y() == y) {
        str += _board[from.x() + _size.first * from.y()].cbegin()->get()->color == BLACK ? "B" : "W";
        str += std::to_string(_board[from.x() + _size.first * from.y()].cbegin()->get()->level);
        for (const auto& e: _board[to.x() + _size.first * to.y()]) {
          str += std::to_string(e->level);
        }
      } else {
        if (_board[x + _size.first * y].cbegin()->get()->color == color) {
          str += _board[x + _size.first * y].cbegin()->get()->color == BLACK ? "B" : "W";
          for (const auto &e: _board[x + _size.first * y]) {
            str += std::to_string(e->level);
          }
        } else {
          str += "X";
        }
      }
    }
  }
  return std::find(_ids[color].cbegin(), _ids[color].cend(), str) == _ids[color].cend();
}

bool Engine::is_possible_cell(const Color &color, const Coordinates &coordinates) const {
  if (coordinates.in_range(_size)) {
    const std::vector<std::shared_ptr<Entity>> &cell = _board[coordinates.x() + _size.first * coordinates.y()];

    return cell.empty() or (cell.cbegin()->get()->color == color and cell.size() < _max_length);
  } else {
    return false;
  }
}

bool Engine::is_possible_to_capture(const Color &color) const {
  for (int x = 0; x < _size.first; ++x) {
    for (int y = 0; y < _size.second; ++y) {
      if (is_possible_to_capture(color, Coordinates(x, y))) {
        return true;
      }
    }
  }
  return false;
}

bool Engine::is_possible_to_capture(const Color &color, const Coordinates &coordinates) const {
  const std::vector<std::shared_ptr<Entity>> &cell = _board[coordinates.x() + _size.first * coordinates.y()];

  if (not cell.empty() and cell.cbegin()->get()->color != color) {
    unsigned int sum = 0;
    unsigned int neighbour_sum = 0;
    unsigned int level_sum = 0;
    unsigned int capacity = std::accumulate(cell.cbegin(), cell.cend(), 0, [](int v, const auto& e) {
      return v + e->level;
    });

    std::for_each(deltas.cbegin(), deltas.cend(),
                  [this, color, coordinates, &sum, &neighbour_sum, &level_sum](const auto &p) {
                    Coordinates c(coordinates.x() + p.first, coordinates.y() + p.second);

                    if (c.in_range(_size)) {
                      const std::vector<std::shared_ptr<Entity>> &neighbour = _board[c.x() + _size.first * c.y()];

                      if (not neighbour.empty() and neighbour.cbegin()->get()->color == color) {
                        neighbour_sum += neighbour.size();
                        sum += neighbour.size();
                        std::for_each(neighbour.cbegin(), neighbour.cend(), [&level_sum](const auto &e) {
                          level_sum += e->level;
                        });
                      }
                    } else {
                      ++sum;
                    }
                  });
    if (sum >= cell.size() and neighbour_sum > 0 and level_sum > capacity) {
      return true;
    }
  }
  return false;
}

int Engine::next_color(int color) {
  return color == WHITE ? BLACK : WHITE;
}

}