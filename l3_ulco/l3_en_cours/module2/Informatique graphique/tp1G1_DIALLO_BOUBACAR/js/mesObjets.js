function carreBasic(){
    //creation du buffer
    const geometry = new THREE.BufferGeometry();

    // Créer le tableau contenant les coordonnées des sommets des triangles
    const vertices = new Float32Array([
        -1.0, 0.0, -1.0,
        1.0, 0.0, -1.0,
        -1.0, 0.0, 1.0,

        -1.0, 0.0, 1.0,
        1.0, 0.0, -1.0,
        1.0, 0.0, 1.0
    ]);

    // Associer les sommets au BufferGeometry
    geometry.setAttribute('position', new THREE.Float32BufferAttribute(vertices, 3));

    // Créer un matériau basique bleu
    const blueMaterial = new THREE.MeshBasicMaterial({ color: 0x18259e,side: THREE.DoubleSide, });

    // Créer le mesh et le retourner
    const mesh = new THREE.Mesh(geometry, blueMaterial);
    return mesh;
}
//question 7

function carreLambert() {
    // Créer un BufferGeometry
    const geometry = new THREE.BufferGeometry();

    // Créer le tableau contenant les coordonnées des sommets des triangles
    const vertices = new Float32Array([
        -1.0, 0.0, -1.0,
        1.0, 0.0, -1.0,
        -1.0, 0.0, 1.0,

        -1.0, 0.0, 1.0,
        1.0, 0.0, -1.0,
        1.0, 0.0, 1.0
    ]);

    // Création le tableau contenant les normales des sommets des triangles
    const normals = new Float32Array([
        0.0, -1.0, 0.0,
        0.0, -1.0, 0.0,
        0.0, -1.0, 0.0,

        0.0, -1.0, 0.0,
        0.0, -1.0, 0.0,
        0.0, -1.0, 0.0
    ]);

    // Création le tableau contenant les couleurs des sommets des triangles
    const colors = new Float32Array([
        1.0, 0.0, 0.0,
        0.0, 1.0, 0.0,
        0.0, 0.0, 1.0,

        1.0, 0.0, 0.0,
        0.0, 1.0, 0.0,
        0.0, 0.0, 1.0
    ]);

    // Association les sommets au BufferGeometry
    geometry.setAttribute('position', new THREE.Float32BufferAttribute(vertices, 3));

    // Association les normales au BufferGeometry (3 coordonnées par sommet)
    geometry.setAttribute('normal', new THREE.Float32BufferAttribute(normals, 3));

    // Association les couleurs au BufferGeometry (3 couleurs par sommet)
    geometry.setAttribute('color', new THREE.Float32BufferAttribute(colors, 3));

    // Création un matériau basique bleu avec réflexion diffuse
    const lambertMaterial = new THREE.MeshLambertMaterial({
        color:  0xffffff,
        side: THREE.DoubleSide,
        vertexColors: true });


    // Créeation le mesh et le retourner
    const mesh = new THREE.Mesh(geometry, lambertMaterial);
    return mesh;
}


//question 8
function cubeColore() {
    // Créer un BufferGeometry
    const geometry = new THREE.BufferGeometry();

    // Créer le tableau contenant les coordonnées des sommets du cube
    const vertices = new Float32Array([
        // Face avant
        -1.0, 0.0, 1.0,  // 1er sommet
        -1.0, 0.0, -1.0,  // 2nd sommet
        1.0, 0.0, -1.0,  // 3ème sommet

        -1.0, 0.0, 1.0,  // 4ème sommet
        1.0, 0.0, -1.0,  // 5ème sommet
        1.0, 0.0, 1.0,  // 6ème sommet

        // Face arrière
        -1.0, 0.0, 1.0,  // 7ème sommet
        1.0, 0.0, 1.0,  // 8ème sommet
        -1.0, 0.0, -1.0,  // 9ème sommet

        -1.0, 0.0, 1.0,  // 10ème sommet
        1.0, 0.0, 1.0,  // 11ème sommet
        1.0, 0.0, -1.0,  // 12ème sommet

    ]);

    // Créer le tableau contenant les normales des sommets du cube
    const normals = new Float32Array([
        0.0, 1.0, 0.0,
        0.0, 1.0, 0.0,
        0.0, 1.0, 0.0,
        0.0, 1.0, 0.0,
        0.0, 1.0, 0.0,
        0.0, 1.0, 0.0,
        0.0, 1.0, 0.0,
        0.0, 1.0, 0.0,
        0.0, 1.0, 0.0,
        0.0, 1.0, 0.0,
        0.0, 1.0, 0.0,
        0.0, 1.0, 0.0,
    ]);

    // Créer le tableau contenant les couleurs des sommets du cube
    const colors = new Float32Array([

        1, 0, 0,  // Rouge pour la face avant
        1, 0, 0,
        1, 0, 0,
        1, 0, 0,
        0, 1, 0,  // Vert pour la face arrière
        0, 1, 0,
        0, 1, 0,
        0, 1, 0,
        0, 0, 1,  // Bleu pour la face supérieure
        0, 0, 1,
        0, 0, 1,  // Bleu pour la face inférieure
        0, 0, 1,

    ]);

    // Associer les sommets au BufferGeometry (3 coordonnées par sommet)
    geometry.setAttribute('position', new THREE.Float32BufferAttribute(vertices, 3));

    // Associer les normales au BufferGeometry (3 coordonnées par sommet)
    geometry.setAttribute('normal', new THREE.Float32BufferAttribute(normals, 3));

    // Associer les couleurs au BufferGeometry (3 couleurs par sommet)
    geometry.setAttribute('color', new THREE.Float32BufferAttribute(colors, 3));

    // Créer un matériau lambertien avec interpolation des couleurs
    const lambertMaterial = new THREE.MeshLambertMaterial({

        side: THREE.DoubleSide,
        vertexColors: THREE.VertexColors
    });

    // Créer le mesh
    const mesh = new THREE.Mesh(geometry, lambertMaterial);
    return mesh;
}