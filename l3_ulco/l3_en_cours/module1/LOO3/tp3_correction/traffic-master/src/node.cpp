#include "link.hpp"
#include "node.hpp"

#include <iostream>

namespace traffic {

Node::Node(const std::string &id, unsigned int duration, size_t in_number, size_t out_number) :
  _id(id), _duration(duration), _in_links(in_number), _out_links(out_number) {}

void Node::attach_in_link(size_t index, const std::shared_ptr<Link> &link) {
  _in_links[index] = link;
}

void Node::attach_out_link(size_t index, const std::shared_ptr<Link> &link) {
  _out_links[index] = link;
}

}