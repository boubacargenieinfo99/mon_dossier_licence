const express = require('express');
const router = express.Router();

const moviesController = require('../controllers/moviesController')
router.get('/',moviesController.list);

router.get('/create',moviesController.create);

router.post('/create',moviesController.createPost);

router.get('/read',moviesController.read);

router.post('/update',moviesController.update);

router.post('/read',moviesController.readPost);

router.post('/delete',moviesController.delete)

module.exports = router;