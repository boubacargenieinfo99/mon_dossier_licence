#include "map.h"

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

struct Map *create_map(unsigned int link_number, unsigned int node_number) {
  struct Map *map = (struct Map *) malloc(sizeof(struct Map));

  map->link_number = link_number;
  map->node_number = node_number;
  map->links = (struct Link **) malloc(sizeof(struct Link *) * link_number);
  for (unsigned int i = 0; i < link_number; ++i) {
    map->links[i] = NULL;
  }
  map->nodes = (struct Node **) malloc(sizeof(struct Node *) * node_number);
  for (unsigned int i = 0; i < node_number; ++i) {
    map->nodes[i] = NULL;
  }
  return map;
}

void destroy_map(struct Map *map) {
  for (unsigned int i = 0; i < map->link_number; ++i) {
    if (map->links[i] != NULL) {
      destroy_link(map->links[i]);
    }
  }
  for (unsigned int i = 0; i < map->node_number; ++i) {
    if (map->nodes[i] != NULL) {
      destroy_node(map->nodes[i]);
    }
  }
  free(map->links);
  free(map->nodes);
  free(map);
}

void add_link_to_map(struct Link *link, struct Map *map) {
  unsigned int index = 0;

  while (map->links[index] != NULL && index < map->link_number) { ++index; }
  if (index < map->link_number) {
    map->links[index] = link;
  }
}

void add_node_to_map(struct Node *node, struct Map *map) {
  unsigned int index = 0;

  while (map->nodes[index] != NULL && index < map->node_number) { ++index; }
  if (index < map->node_number) {
    map->nodes[index] = node;
  }
}

void run_map(struct Map *map, unsigned int time) {
  for (unsigned int i = 0; i < map->link_number; ++i) {
    struct Link *link = map->links[i];

    if (link != NULL) {
      struct Vehicle *out_vehicle = run_link(link, time);

      // has a vehicle left the link?
      if (out_vehicle != NULL) {
        // is there a next node?
        if (link->out_node != NULL) {
          add_vehicle_to_node(out_vehicle, link->out_node, time);
        }
      }
    }
  }
  for (unsigned int i = 0; i < map->node_number; ++i) {
    struct Node *node = map->nodes[i];

    if (node != NULL) {
      // if a vehicle is present and this vehicle is present until the transit duration
      if (node->vehicle != NULL && node->vehicle->last_time + node->duration <= time) {
        printf("%d: %s exits %s\n", time, node->vehicle->id, node->id);
        // the vehicle goes to the first out link (feature: use path)
        add_vehicle_to_link(node->vehicle, node->out_links[0], time);
        // now, no vehicle is present in node
        node->vehicle = NULL;
      }
    }
  }
}
