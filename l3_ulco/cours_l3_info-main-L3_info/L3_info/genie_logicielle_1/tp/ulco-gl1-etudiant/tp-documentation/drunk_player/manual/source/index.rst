.. drunk_player documentation master file, created by
   sphinx-quickstart on Wed Oct 26 15:10:03 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Drunk_player_cli
========================================

**Description**

Drunk_player_cli permet de lire les vidéos d'un dossier et de générer un fichier vidéo aprés avoir trop bu.

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
