const express = require('express');
const router = express.Router();

const teachinController = require('../controllers/teachingController');

router.get('/', teachinController.home);

router.get('/javascript', teachinController.javascript);

router.get('/php', teachinController.php);

router.get('/node', teachinController.node);

router.get('/node/express', teachinController.express);

module.exports = router;