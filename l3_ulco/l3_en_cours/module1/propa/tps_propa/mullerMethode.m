function normale= mullerMethode(n)

    u1=rand(1,n);
    u2=rand(1,n);
    
    x1=(sqrt(-2.*log(u1))).*(cos(2*pi.*u2));   
    x2=(sqrt(-2.*log(u1))).*(sin(2*pi.*u2));


 normale=[x1;x2];
end
