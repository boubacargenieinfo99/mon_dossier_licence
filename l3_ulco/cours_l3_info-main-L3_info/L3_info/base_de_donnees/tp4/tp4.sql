--1

CREATE OR REPLACE FUNCTION Get_Emp_Name (numero_employe IN EMPLOYE.NUMERO%TYPE)RETURN varchar2
AS
	chaine varchar2(60);
BEGIN
	SELECT CONCAT(NOM,PRENOM) INTO chaine
		FROM EMPLOYE
		WHERE EMPLOYE.NUMERO = numero_employe;
	RETURN chaine;
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			dbms_output.put_line('pas d employe portant ce numero');
			RETURN ('');
		WHEN OTHERS THEN
			RETURN ('');
END Get_Emp_Name;
/

--2

CREATE OR REPLACE PROCEDURE Get_three_betters(numero_hyper IN HYPERMARCHE.NUMERO%TYPE)
AS
	CURSOR cur IS
		SELECT NOM,CHIFFREAFFAIRE,NUMERORESPONSABLE 
		FROM RAYON 
		WHERE NUMEROHYPER = numero_hyper
		ORDER BY CHIFFREAFFAIRE DESC
		FETCH FIRST 3 ROWS ONLY;
BEGIN
	FOR iter IN cur LOOP
		dbms_output.put_line('rayon : ' || iter.NOM || ' chiffre d affaire du rayon : ' || iter.CHIFFREAFFAIRE || ' responsable rayon : ' || Get_Emp_Name(iter.NUMERORESPONSABLE));
	END LOOP;
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			dbms_output.put_line('pas de rayon');
			Null;
		WHEN OTHERS THEN
			dbms_output.put_line('erreur');
			Null;
END;
/

--3

CREATE OR REPLACE PROCEDURE Produit_vendu(numero_produit IN PRODUITS.NUMERO%TYPE,quantite_vendue IN PRODUITS.QUANTITESTOCKHYPER%TYPE)
AS
	quantite_avant PRODUITS.QUANTITESTOCKHYPER%TYPE;
BEGIN
	SELECT QUANTITESTOCKHYPER INTO quantite_avant
	FROM PRODUITS
	WHERE NUMERO = numero_produit;
	IF quantite_vendue > quantite_avant THEN
		RAISE_APPLICATION_ERROR(-20020,'erreur : plus de quantite vendue que de quantite disponible');
	END IF;
	UPDATE PRODUITS
		SET QUANTITESTOCKHYPER = (QUANTITESTOCKHYPER - quantite_vendue)
		WHERE NUMERO = numero_produit;
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			dbms_output.put_line('numero produit invalide');
			Null;
		WHEN OTHERS THEN
			Null;
END;
/

--4

-- dans les hypermarche commencant par 59 ou 62
-- select 

CREATE TABLE PRIMES(
	NUMERO VARCHAR2(15),
	PRIMARY KEY (NUMERO),
	NUMEROEMPLOYE VARCHAR2(15),
	NUMERORAYON VARCHAR2(15),
	MONTANTPRIME NUMBER(6,2),
	MOISANNEE NUMBER(2)
);

CREATE OR REPLACE PROCEDURE give_primes_v1 (premiere_prime IN PRIMES.MONTANTPRIME%TYPE,deuxieme_prime IN PRIMES.MONTANTPRIME%TYPE,troisieme_prime IN PRIMES.MONTANTPRIME%TYPE)
AS 
	CURSOR hyp_npdc IS
		SELECT NUMERO 
		FROM HYPERMARCHE
		WHERE ((CODEPOSTAL LIKE '62%') OR (CODEPOSTAL LIKE '59%'));
	CURSOR trois_meilleurs(numero_hyper IN RAYON.NUMEROHYPER%TYPE) IS
		SELECT NUMERORESPONSABLE,NUMERO
		FROM RAYON
		WHERE NUMEROHYPER = numero_hyper
		ORDER BY CHIFFREAFFAIRE DESC
		FETCH FIRST 3 ROWS ONLY;
	numero_prime PRIMES.NUMERO%TYPE;
	mois NUMBER(2);
	numero_respon RAYON.NUMERORESPONSABLE%TYPE;
	iter NUMBER(1);
	numero_prime VARCHAR2(15);
	numero_rayon RAYON.NUMERO%TYPE;
	montant_prime PRIMES.MONTANTPRIME%TYPE;
	chaine VARCHAR2(15);
BEGIN
	iter := 1;
	mois := TO_NUMBER(TO_CHAR(ADD_MONTHS(SYSDATE,1),'MM'));
	FOR bl IN hyp_npdc LOOP	--on selectionne les numeros des hypermarche du 59 et 62
		FOR tr IN trois_meilleurs (bl.NUMERO) LOOP	--on selectionne les numeros des responsable et du rayon des 3 meilleurs rayon du magasin
			IF iter = 1 THEN
				montant_prime := premiere_prime;
				iter := 2;
			ELSIF iter = 2 THEN
				montant_prime := deuxieme_prime;
				iter := 3;
			ELSE
				montant_prime := troisieme_prime;
			END IF;
			chaine := substr(tr.NUMERORESPONSABLE,4,6) || substr(tr.NUMERO,4,6) || TO_CHAR( ((TO_NUMBER(TO_CHAR(SYSDATE,'DD'))) * 60 * 60 * 24) + (60 * 60 * TO_NUMBER(TO_CHAR(SYSDATE,'HH24'))) + (60 * TO_NUMBER(TO_CHAR(SYSDATE,'MI'))) + (TO_NUMBER(TO_CHAR(SYSDATE,'SS'))) );
			INSERT INTO PRIMES(NUMERO,NUMEROEMPLOYE,NUMERORAYON,MONTANTPRIME,MOISANNEE)
				VALUES(chaine,tr.NUMERORESPONSABLE,tr.NUMERO,montant_prime,mois);
		END LOOP;
		iter :=1;
	END LOOP;
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			dbms_output.put_line('no_data_found');
			Null;
		WHEN OTHERS THEN
			dbms_output.put_line('erreur');
			Null;
END;
/  

CREATE OR REPLACE PROCEDURE give_primes_v2 (premiere_prime IN PRIMES.MONTANTPRIME%TYPE,deuxieme_prime IN PRIMES.MONTANTPRIME%TYPE,troisieme_prime IN PRIMES.MONTANTPRIME%TYPE)
AS 
	CURSOR hyp_npdc IS
		SELECT NUMERO 
		FROM HYPERMARCHE
		WHERE ((CODEPOSTAL LIKE '62%') OR (CODEPOSTAL LIKE '59%'));
	CURSOR trois_meilleurs(numero_hyper IN RAYON.NUMEROHYPER%TYPE) IS
		SELECT NUMERORESPONSABLE,NUMERO
		FROM RAYON
		WHERE NUMEROHYPER = numero_hyper
		ORDER BY CHIFFREAFFAIRE DESC
		FETCH FIRST 3 ROWS ONLY;
	numero_prime PRIMES.NUMERO%TYPE;
	mois NUMBER(2);
	numero_respon RAYON.NUMERORESPONSABLE%TYPE;
	iter NUMBER(1);
	next_iter NUMBER(1);
	numero_prime VARCHAR2(15);
	numero_rayon RAYON.NUMERO%TYPE;
	montant_prime PRIMES.MONTANTPRIME%TYPE;
	chaine VARCHAR2(15);
BEGIN
	iter := 1;
	mois := TO_NUMBER(TO_CHAR(ADD_MONTHS(SYSDATE,1),'MM'));
	FOR bl IN hyp_npdc LOOP	--on selectionne les numeros des hypermarche du 59 et 62
		FOR tr IN trois_meilleurs (bl.NUMERO) LOOP	--on selectionne les numeros des responsable et du rayon des 3 meilleurs rayon du magasin
			CASE iter
				WHEN 1 THEN
					montant_prime := premiere_prime;
					next_iter := 2;
				WHEN 2 THEN
					montant_prime := deuxieme_prime;
					next_iter := 3;
				WHEN 3 THEN
					montant_prime := troisieme_prime;
			END CASE;
			iter := next_iter;
			chaine := substr(tr.NUMERORESPONSABLE,4,6) || substr(tr.NUMERO,4,6) || TO_CHAR( ((TO_NUMBER(TO_CHAR(SYSDATE,'DD'))) * 60 * 60 * 24) + (60 * 60 * TO_NUMBER(TO_CHAR(SYSDATE,'HH24'))) + (60 * TO_NUMBER(TO_CHAR(SYSDATE,'MI'))) + (TO_NUMBER(TO_CHAR(SYSDATE,'SS'))) );
			INSERT INTO PRIMES(NUMERO,NUMEROEMPLOYE,NUMERORAYON,MONTANTPRIME,MOISANNEE)
				VALUES(chaine,tr.NUMERORESPONSABLE,tr.NUMERO,montant_prime,mois);
		END LOOP;
		iter := 1;
	END LOOP;
EXCEPTION
	WHEN NO_DATA_FOUND THEN
		dbms_output.put_line('no_data_found');
		Null;
	WHEN OTHERS THEN
		dbms_output.put_line('erreur');
		Null;
END;
/ 

--5

CREATE OR REPLACE PROCEDURE liste_responsable_v1 (numero_hyper IN HYPERMARCHE.NUMERO%TYPE)
AS
	CURSOR liste (num_hyp IN HYPERMARCHE.NUMERO%TYPE) IS
		SELECT EMPLOYE.NOM AS ENOM,EMPLOYE.PRENOM AS EPRENOM,EMPLOYE.NUMERO AS ENUM
		FROM EMPLOYE
		INNER JOIN RAYON
		ON EMPLOYE.NUMERO = RAYON.NUMERORESPONSABLE
		WHERE RAYON.NUMEROHYPER = num_hyp;
	nb_primes NUMBER(5);
BEGIN
	FOR elem IN liste (numero_hyper) LOOP
		SELECT COUNT(NUMERO) INTO nb_primes
			FROM PRIMES
			WHERE NUMEROEMPLOYE = elem.ENUM;
		dbms_output.put_line(elem.ENOM || ' ' || elem.EPRENOM || ' nombres de prime obtenu :' || nb_primes);	
	END LOOP;
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			dbms_output.put_line('no_data_found');
		WHEN OTHERS THEN
			dbms_output.put_line('erreur');
			Null;		
END;
/

DROP TABLE compteur_primes_par_employe;
CREATE TABLE compteur_primes_par_employe(
	NUMEROEMPLOYE VARCHAR2(15),
	NB_PRIMES NUMBER(5)
);

CREATE OR REPLACE PROCEDURE liste_responsable_v2 (numero_hyper IN HYPERMARCHE.NUMERO%TYPE)
AS
	CURSOR liste_employe_responsable (num_hypermarche HYPERMARCHE.NUMERO%TYPE) IS
		SELECT NUMERORESPONSABLE
		FROM RAYON
		WHERE NUMEROHYPER = num_hypermarche;
	nb_prime NUMBER(5);
	CURSOR tab IS
		SELECT NOM,PRENOM,NB_PRIMES FROM compteur_primes_par_employe
			INNER JOIN EMPLOYE
			ON EMPLOYE.NUMERO = NUMEROEMPLOYE
			ORDER BY NB_PRIMES DESC;
BEGIN
	dbms_output.put_line('test');
	FOR emp IN liste_employe_responsable (numero_hyper) LOOP
		SELECT COUNT(PRIMES.NUMERO) INTO nb_prime
			FROM PRIMES
			WHERE PRIMES.NUMEROEMPLOYE = emp.NUMERORESPONSABLE;
		INSERT INTO compteur_primes_par_employe(NUMEROEMPLOYE,NB_PRIMES)
			VALUES(emp.NUMERORESPONSABLE,nb_prime);
	END LOOP;
	FOR it IN tab LOOP
		dbms_output.put_line(it.NOM || ' ' || it.PRENOM || ' nombres de prime obtenu :' || it.NB_PRIMES);	
	END LOOP;
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			dbms_output.put_line('no_data_found');
			Null;
		WHEN OTHERS THEN
			dbms_output.put_line('erreur');
			Null;
END;
/




--test

DECLARE
	numero_employe EMPLOYE.NUMERO%TYPE;
BEGIN
	dbms_output.put_line('q1');
	numero_employe := &saisir_numero_employe;
	dbms_output.put_line(Get_Emp_Name(numero_employe));
END;
/

DECLARE
	numero_hypermarche HYPERMARCHE.NUMERO%TYPE;
BEGIN
	dbms_output.put_line('q2');
	numero_hypermarche := &saisir_numero_hypermarche;
	Get_three_betters(numero_hypermarche);
END;
/

DECLARE
	numero_produit PRODUITS.NUMERO%TYPE;
	quantite_vendue PRODUITS.QUANTITESTOCKHYPER%TYPE;
BEGIN
	dbms_output.put_line('q3');
	numero_produit := &saisir_numero_produit;
	quantite_vendue := &saisir_quantite_vendue;
	Produit_vendu(numero_produit,quantite_vendue);
END;
/

DECLARE
	prime_un PRIMES.MONTANTPRIME%TYPE;
	prime_deux PRIMES.MONTANTPRIME%TYPE;
	prime_trois PRIMES.MONTANTPRIME%TYPE;
BEGIN
	dbms_output.put_line('q4 v1');
	prime_un := &saisir_montant_de_la_premier_prime;
	prime_deux := &saisir_montant_de_la_deuxieme_prime;
	prime_trois := &saisir_montant_de_la_troisieme_prime;
	give_primes_v1(prime_un,prime_deux,prime_trois);
END;
/

DECLARE
	prime_un PRIMES.MONTANTPRIME%TYPE;
	prime_deux PRIMES.MONTANTPRIME%TYPE;
	prime_trois PRIMES.MONTANTPRIME%TYPE;
BEGIN
	dbms_output.put_line('q4 v2');
	prime_un := &saisir_montant_de_la_premier_prime;
	prime_deux := &saisir_montant_de_la_deuxieme_prime;
	prime_trois := &saisir_montant_de_la_troisieme_prime;
	give_primes_v2(prime_un,prime_deux,prime_trois);
END;
/

DECLARE
	numero_hypermarche HYPERMARCHE.NUMERO%TYPE;
BEGIN
	dbms_output.put_line('q5 v1');
	numero_hypermarche := &saisir_numero_hypermarche;
	liste_responsable_v1(numero_hypermarche);
END;
/

DECLARE
	numero_hypermarche HYPERMARCHE.NUMERO%TYPE;
BEGIN
	dbms_output.put_line('q5 v2');
	numero_hypermarche := &saisir_numero_hypermarche;
	liste_responsable_v2(numero_hypermarche);
END;
/

