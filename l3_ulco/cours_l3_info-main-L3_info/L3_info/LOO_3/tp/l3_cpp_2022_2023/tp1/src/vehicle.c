#include "vehicle.h"

#include <stdlib.h>
#include <string.h>

struct Vehicle *create_vehicle(char *id) {
  struct Vehicle *vehicle = (struct Vehicle *) malloc(sizeof(struct Vehicle));

  vehicle->id = (char *) malloc(sizeof(char) * (strlen(id)) + 1);
  strcpy(vehicle->id, id);
  vehicle->link = NULL;
  vehicle->node = NULL;
  vehicle->last_time = 0;
  return vehicle;
}

void destroy_vehicle(struct Vehicle *vehicle) {
  free(vehicle->id);
  free(vehicle);
}