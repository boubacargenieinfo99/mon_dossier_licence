#ifndef TRAFFIC_JUNCTION_HPP
#define TRAFFIC_JUNCTION_HPP

#include "node.hpp"

namespace traffic {

class Link;

class Junction : public Node {
public:
  Junction(const std::string &id, unsigned int duration, size_t in_number, size_t out_number);

  bool is_free() const override { return not _vehicle; }

  void push_vehicle(unsigned int t, std::unique_ptr<Vehicle> &vehicle) override;

  void run(unsigned int t) override;

private:
  std::unique_ptr<Vehicle> _vehicle;
  unsigned int _out_time;
};

}

#endif //TRAFFIC_JUNCTION_HPP
