import Data.Char

mylength :: [a] -> Int
mylength [] = 0
mylength (x:xs) = 1 + (mylength xs)
--mylength d = 1 + (mylength (tail d))

toUpperString :: String -> String
toUpperString "" = ""
toUpperString (x:xs) = (toUpper x) : (toUpperString xs)

onlyLetters :: String -> String
onlyLetters "" = ""
onlyLetters (x:xs) = if isLetter x
                        then x : onlyLetters xs 
                        else onlyLetters xs

main :: IO()
main = do
    print (show (mylength "foobar"))
    print (toUpperString "foobar")
    print (onlyLetters "foo bar")