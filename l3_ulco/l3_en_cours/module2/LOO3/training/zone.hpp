
#ifndef TRAINING_ZONE_HPP
#define TRAINING_ZONE_HPP
#pragma once

#include <vector>
#include <memory>
#include "spatialElement.hpp"
#include "geometry.hpp"
class Labor;
class SpatialElement;
class Machine;

class Zone : public SpatialElement {
public:
    Zone(std::string id);
    void addSubZone(std::shared_ptr<Zone> subZone);
    void addLabor(std::shared_ptr<Labor> labor);
    void addMachine(std::shared_ptr<Machine> machine);

private:
    std::vector<std::shared_ptr<Zone>> subZones;
    std::vector<std::shared_ptr<Labor>> labors;
    std::vector<std::shared_ptr<Machine>> machines;
};

#endif //TRAINING_ZONE_HPP
