require = require('esm')(module, {mode: 'auto', cjs: true});

const core = require('../../../lib/openxum-core').default;
const ai = require('../../../lib/openxum-ai').default;

let e = new core.Paletto.Engine(core.Paletto.GameType.STANDARD, core.Paletto.Color.PLAYER_1);
let p1 = new ai.Generic.RandomPlayer(core.Paletto.Color.PLAYER_1, core.Paletto.Color.PLAYER_2, e);
let p2 = new ai.Generic.RandomPlayer(core.Paletto.Color.PLAYER_2, core.Paletto.Color.PLAYER_1, e);
let p = p1;
let moves = [];

while (!e.is_finished()) {
    let move = p.move();

    moves.push(move);
    e.move(move);
    p = p === p1 ? p2 : p1;
}

console.log("Winner is " + (e.winner_is() === core.Paletto.Color.PLAYER_1 ? "player 1" : "player 2"));
for (let index = 0; index < moves.length; ++index) {
    console.log(moves[index].to_string());
}
