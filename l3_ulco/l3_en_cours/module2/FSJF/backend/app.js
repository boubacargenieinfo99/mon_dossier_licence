require('dotenv').config({path: './.env'})

const express=require('express');
const app=express();

const path=require('path');


app.use(express.static(path.join(__dirname,'public')));

app.set('view engine', 'ejs');

app.set('views',path.join(__dirname,'views'));

const expressLayouts=require('express-ejs-layouts');
app.use(expressLayouts);
app.set('layout','../views/layouts/layout');


app.get("/",(req,res)=>{
    res.send('<h1>Bienvenue sur la Home page</h1>');

});

app.get('/about',(req,res)=>{
   res.send('<p> Ce cours est inspiré de la documentation </p>');
});
app.get('/*',(req,res)=>{
   res.redirect('/');
});
/*
app.use((req,res)=>{
   //res.render('pages/home',{nickname:'diallo',sex:'female'});
    const user = req.query.user || {};
    res.render('pages/home', { user });
})

 */

const port = process.env.PORT || 3000
app.listen(port,()=>{
    console.log(`Le server écoute sur http://127.0.01:${port}/`);
})

module.exports=app;