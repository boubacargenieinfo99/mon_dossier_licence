"use strict";

import OpenXum from '../../openxum/gui.mjs';
import Komivoki from '../../../openxum-core/games/komivoki/index.mjs';
import Move from '../../../openxum-core/games/komivoki/move.mjs';
import Coordinates from '../../../openxum-core/games/komivoki/coordinates.mjs';

class Gui extends OpenXum.Gui {
    constructor(c, e, l, g) {
        super(c, e, l, g);
        this._deltaX = 0;
        this._deltaY = 0;
        this._offsetX = 0;
        this._offsetY = 0;
        this._move = undefined;
        this._move_list = [];
        this._selected_piece = undefined;
        this._selected_list = [];
        this._size = 0;
        this._currentPlayer = Komivoki.Color.BLACK;  
    }

    // public methods
    draw() {
        // background
        this._context.fillStyle = "#4C3629";
        this._round_rect(0, 0, this._canvas.width, this._canvas.height, 17, true, false);
        

        this._context.lineWidth = 1;
        this._draw_grid();
        this._draw_state();
        this.drawCurrentPlayer();
        this._draw_pass_button();
        this._draw_border();
    }
    _draw_border() {
        const borderWidth = 10; //Border width
        const borderColor = "#ffff00"; //Border color

        this._context.lineWidth = borderWidth;
        this._context.strokeStyle = borderColor;
        this._context.strokeRect(
            this._offsetX - borderWidth / 2,
            this._offsetY - borderWidth / 2,
            this._deltaX * 8 + borderWidth,
            this._deltaY * 8 + borderWidth
        );
    }

    drawCurrentPlayer() {
        this._context.clearRect(0, 0, this._canvas.width, 40); 
        this._context.font = "20px Arial";
        this._context.fillStyle = "#00ff00";
        this._context.fillText("Current player: " + (this._currentPlayer === Komivoki.Color.BLACK ? "Black" : "White"), 10, 30);
    }
    _draw_pass_button() {
        const buttonWidth = 100;
        const buttonHeight = 40;
      const posX = (this._canvas.width - buttonWidth) /2.5;
        const posY = 40;

        this._context.fillStyle = "#4CAF50";
        this._context.fillRect(posX, posY, buttonWidth, buttonHeight);

        this._context.fillStyle = "#FFFFFF";
        this._context.font = "20px Arial";
        this._context.fillText("Pass", posX + 25, posY + 25);

        // Add click event listener for the pass button
        this._canvas.addEventListener("click", (e) => {
            const rect = this._canvas.getBoundingClientRect();
            const mouseX = e.clientX - rect.left;
            const mouseY = e.clientY - rect.top;

            if (mouseX >= posX && mouseX <= posX + buttonWidth && mouseY >= posY && mouseY <= posY + buttonHeight) {
                this._handle_pass();
            }
        });
    }
    _handle_pass() {
        console.log('Player passed');
        // Handle pass logic: switch player and reset selected pieces
        this._selected_piece = undefined;
        this._move_list = [];
        this._currentPlayer = (this._currentPlayer === Komivoki.Color.BLACK ? Komivoki.Color.WHITE : Komivoki.Color.BLACK);
        this.draw();
    }

   

    get_move() {
        return this._move_list;
    }

    is_animate() {
        return false;
    }

    is_remote() {
        return false;
    }

    move(move, color) {
        this._manager.play();
        this._currentPlayer = (this._currentPlayer === Komivoki.Color.BLACK ? Komivoki.Color.WHITE : Komivoki.Color.BLACK); // Changer le joueur
        this.draw();
    }

    unselect() {
        this._selected_piece = undefined;
        this._selected_list = [];
        this._size = 0;
        this._move_list = [];
        this.draw();
    }

    set_canvas(c) {
        super.set_canvas(c);
        this._canvas.addEventListener("click", (e) => {
            let pos = this._get_click_position(e);
            if (pos.x >= 0 && pos.x < 10 && pos.y >= 0 && pos.y < 8) this._on_click(pos.x, pos.y);
        });

        this._deltaX = (this._canvas.width * 0.95 - 40) / 10;
        this._deltaY = (this._canvas.height * 0.95 - 40) / 10;
        this._offsetX = this._canvas.width / 2 - this._deltaX * 5.0;
        this._offsetY = this._canvas.height / 2 - this._deltaY * 4.0;
    
        this.draw();
    }
    _on_click(x, y) {
        if (this._selected_piece) {
            // If a piece is already selected, attempt to move it
            this._attempt_move(this._selected_piece.x, this._selected_piece.y, x, y);
        } else {
            // If no piece is selected, select the piece at the clicked position
            this._select_piece(x, y);
        }
        this.draw();
    }
    _select_piece(x, y) {
        const piece = this._engine._board[x][y];
        if (piece && piece.color() === this._currentPlayer) {
            this._selected_piece = {x: x, y: y};
        } else {
            this._selected_piece = undefined;
        }
    }
    /*
    _attempt_move(fromX, fromY, toX, toY) {
        const fromPiece = this._engine._board[fromX][fromY];
        if(fromPiece && fromPiece.color() === this._currentPlayer){
            // Validate the move according to game rules
            if (this._is_valid_move(fromX, fromY, toX, toY)) {
                // Perform the move
                this._move_piece(fromX, fromY, toX, toY);
                this._selected_piece = undefined;
                this._currentPlayer = (this._currentPlayer === Komivoki.Color.BLACK ? Komivoki.Color.WHITE : Komivoki.Color.BLACK); // Changer le joueur
                this.draw();
            }

        }
       
    }
    */
    _attempt_move(fromX, fromY, toX, toY) {
        if (this._engine._is_valid_move(fromX, fromY, toX, toY)) {
            this._engine.move(new Move(fromX, fromY, toX, toY));
            this._currentPlayer = (this._currentPlayer === Komivoki.Color.BLACK ? Komivoki.Color.WHITE : Komivoki.Color.BLACK); // Changer le joueur
            this.draw();
        }
      }
    
    _is_valid_move(fromX, fromY, toX, toY) {
        // Checks that the destination is within the tray limits
        if (toX < 0 || toX >= 8 || toY < 0 || toY >= 8) {
            return false;
        }
    
        // Checks that the destination is not a wall
        if (this._engine._board[toX][toY] === 'wall') {
            return false;
        }
    
        const fromPiece = this._engine._board[fromX][fromY];
        const toPiece = this._engine._board[toX][toY];
        // Checks if the target box is empty

        if (toPiece === undefined) {
            return true;
        }
          // Checks whether the target square contains a pawn of the same color
        if (toPiece !== undefined && toPiece.color() === fromPiece.color()) {
            return true;
        }
    
        return false;
    }
    _move_piece(fromX, fromY, toX, toY) {
        if (this._is_valid_move(fromX, fromY, toX, toY)) {
            const fromPiece = this._engine._board[fromX][fromY];
            const toPiece = this._engine._board[toX][toY];
    
            if (toPiece === undefined) {
                // Move the piece to an empty square
                this._engine._board[toX][toY] = fromPiece;
            } else {
                // Stack pieces of the same color
                this._engine.get_possible_move_list();
                
            }
    
           // Clear the original box
            this._engine._board[fromX][fromY] = undefined;
            this._move_list.push({fromX, fromY, toX, toY});
        }
    }
    

      _draw_grid() {
        let i, j;
        let alt = 1;

        this._context.lineWidth = 1;
        this._context.strokeStyle = "#000000";
        this._context.fillStyle = "#D59D6C";
        for (i = 0; i < 8; ++i) {
            for (j = 0; j < 8; ++j) {
                this._context.beginPath();
                this._context.moveTo(this._offsetX + i * this._deltaX, this._offsetY + j * this._deltaY);
                this._context.lineTo(this._offsetX + (i + 1) * this._deltaX - 2, this._offsetY + j * this._deltaY);
                this._context.lineTo(this._offsetX + (i + 1) * this._deltaX - 2, this._offsetY + (j + 1) * this._deltaY - 2);
                this._context.lineTo(this._offsetX + i * this._deltaX, this._offsetY + (j + 1) * this._deltaY - 2);
                this._context.moveTo(this._offsetX + i * this._deltaX, this._offsetY + j * this._deltaY);

                this._context.closePath();
                if (alt === 0) {
                    alt = 1;
                    this._context.fillStyle = "#D59D6C";
                    this._context.fill();
                } else if (alt === 1) {
                    alt = 0;
                    this._context.fillStyle = "#00ff00";
                    this._context.fill();
                }


            }
            if (alt === 0) alt = 1;
            else alt = 0;
        }
    }

    _round_rect(x, y, width, height, radius, fill, stroke) {

        this._context.clearRect(x, y, width, height);
        if (typeof stroke === "undefined") {
            stroke = true;
        }
        if (typeof radius === "undefined") {
            radius = 5;
        }
       
        if (stroke) {
            this._context.stroke();
        }
        if (fill) {
            this._context.fill();
        }

        this._context.strokeStyle = "#FFFFFF";
        this._context.beginPath();
        this._context.lineWidth = 5;
        this._context.moveTo(50, 40);
        this._context.lineTo(510, 40);
        this._context.stroke();

        this._context.beginPath();
        this._context.lineWidth = 5;
        this._context.moveTo(50, 50);
        this._context.lineTo(510, 50);
        this._context.stroke();

        this._context.beginPath();
        this._context.lineWidth = 5;
        this._context.moveTo(50, 530);
        this._context.lineTo(510, 530);
        this._context.stroke();

        this._context.beginPath();
        this._context.lineWidth = 5;
        this._context.moveTo(50, 520);
        this._context.lineTo(510, 520);
        this._context.stroke();
    }
   /*
    _draw_state() {
        for (let y = 0; y < 8; y++) {
            for (let x = 0; x < 8; x++) {
                if (this._engine._board[x][y] !== undefined) {
                    let pt = this._compute_coordinates(x, y);
                    this._draw_piece(pt[0], pt[1], this._engine._board[x][y].color());

                }
            }
        }
    }
    */

    _draw_state() {
        for (let y = 0; y < 8; y++) {
          for (let x = 0; x < 8; x++) {
            if (this._engine._board[x][y] !== undefined) {
              let pt = this._compute_coordinates(x, y);
              this._draw_piece_stack(pt[0], pt[1], this._engine._board[x][y].getStack());
            }
          }
        }
      }

      _draw_piece_stack(x, y, stack) {
        let radius = (this._deltaX / 2.3);
        let offset = 5;
    
        stack.forEach((piece, index) => {
          if (piece.color() === Komivoki.Color.BLACK) {
            this._context.strokeStyle = "#303030";
            this._context.fillStyle = "#303030";
          } else {
            this._context.strokeStyle = "#303030";
            this._context.fillStyle = "#F0F0F0";
          }
    
          this._context.lineWidth = 1;
          this._context.beginPath();
          this._context.arc(x, y - index * offset, radius, 0.0, 2 * Math.PI);
          this._context.closePath();
          this._context.fill();
          this._context.stroke();
        });
      }
    
    
  
    _compute_coordinates(x, y) {
        return [this._offsetX + x * this._deltaX + (this._deltaX / 2) - 1, this._offsetY + y * this._deltaY + (this._deltaY / 2) - 1];
    }

     _draw_piece(x, y, color) {
        let radius = (this._deltaX / 2.3);

        if (color === Komivoki.Color.BLACK) {
            this._context.strokeStyle = "#303030";
            this._context.fillStyle = "#303030";
        } else {
            this._context.strokeStyle = "#303030";
            this._context.fillStyle = "#F0F0F0";
        }

        this._context.lineWidth = 1;
        this._context.beginPath();
        this._context.arc(x, y, radius, 0.0, 2 * Math.PI);
        this._context.closePath();
        this._context.fill();
        this._context.stroke();

    }

    _get_click_position(e) {
        let rect = this._canvas.getBoundingClientRect();
        let x = (e.clientX - rect.left) * this._scaleX - this._offsetX;
        let y = (e.clientY - rect.top) * this._scaleY - this._offsetY;

        return {x: Math.floor(x / this._deltaX), y: Math.floor(y / this._deltaX)};
    }
}

export default Gui;
