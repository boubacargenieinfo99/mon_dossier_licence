--VASSEUR CHRISTOPHER
--tp1
--1.1
 

--if EXISTS TABLE PRODUIT
DROP TABLE PRODUITS;

--if EXISTS TABLE RAYON
DROP TABLE RAYON;

--if EXISTS TABLE HYPERMARCHE
DROP TABLE HYPERMARCHE;

--if EXISTS TABLE EMPLOYE
DROP TABLE EMPLOYE;

CREATE TABLE EMPLOYE (
	NUMERO VARCHAR(15),
	PRIMARY KEY (NUMERO),
	NOM VARCHAR(30),
	PRENOM VARCHAR(30),
	ADRESSE VARCHAR(50)
);

CREATE TABLE HYPERMARCHE (
	NUMERO VARCHAR(15),
	PRIMARY KEY (NUMERO),
	NOM VARCHAR(30),
	ADRESSE VARCHAR(50),
	VILLE VARCHAR(30),
	CODEPOSTAL VARCHAR(5),
	NUMERODIRECTEUR VARCHAR(15),
	FOREIGN KEY (NUMERODIRECTEUR) REFERENCES EMPLOYE(NUMERO)
);

CREATE TABLE RAYON (
	NUMERO VARCHAR(15),
	PRIMARY KEY (NUMERO),
	NOM VARCHAR(30),
	DESCRIPTIF VARCHAR(50),
	CHIFFREAFFAIRE NUMBER(10, 2),
	NUMEROHYPER VARCHAR(15),
	FOREIGN KEY (NUMEROHYPER) REFERENCES HYPERMARCHE(NUMERO),
	NUMERORESPONSABLE VARCHAR(15),
	FOREIGN KEY (NUMERORESPONSABLE) REFERENCES EMPLOYE(NUMERO)
);

CREATE TABLE PRODUITS (
	NUMERO VARCHAR(15),
	PRIMARY KEY (NUMERO),
	LIBELLE VARCHAR(50),
	NUMERORAYON VARCHAR(15),
	FOREIGN KEY (NUMERORAYON) REFERENCES RAYON(NUMERO),
	PRIXUNITAIRE NUMBER(5, 2),
	UNITE VARCHAR(30),
	QUANTITESTOCKHYPER NUMBER(8, 2)
);


--1.3

--DESC EMPLOYE			-- 	affiche les attributs de la table EMPLOYE
--DESC HYPERMARCHE		-- 	affiche les attributs de la table HYPERMARCHE
--DESC RAYON			-- 	affiche les attributs de la table RAYON
--DESC PRODUITS			-- 	affiche les attributs de la table PRODUITS

SELECT * FROM EMPLOYE;
-- affiche "aucune ligne selectionnee"

SELECT * FROM HYPERMARCHE;
-- affiche "aucune ligne selectionnee"

SELECT * FROM RAYON;
-- affiche "aucune ligne selectionnee"

SELECT * FROM PRODUITS;
-- affiche "aucune ligne selectionnee"

--1.5





INSERT INTO EMPLOYE (NUMERO,NOM,PRENOM,ADRESSE) VALUES ('EMP1090','DUPONT','Jean','332, rue jean-dupont, Calais');

INSERT INTO EMPLOYE (
	NUMERO,
	NOM,
	PRENOM,
	ADRESSE
) VALUES (
	'EMP1301',
	'DUVIVIER',
	'Renaud',
	'345, rue renaud-duviver, Paris'
);

INSERT INTO EMPLOYE (
	NUMERO,
	NOM,
	PRENOM,
	ADRESSE
) VALUES (
	'EMP1208',
	'GODART',
	'Claude',
	'674, rue claude-godart, Ardre'
);

INSERT INTO EMPLOYE (
	NUMERO,
	NOM,
	PRENOM,
	ADRESSE
) VALUES (
	'EMP2025',
	'FONTAINE',
	'Fabien',
	'456, rue jean-louis, Calais'
);

INSERT INTO EMPLOYE (
	NUMERO,
	NOM,
	PRENOM,
	ADRESSE
) VALUES (
	'EMP1505',
	'VASSEUR',
	'Jacques',
	'778, rue vasseur, Saint-Omer'
);

INSERT INTO EMPLOYE (
	NUMERO,
	NOM,
	PRENOM,
	ADRESSE
) VALUES (
	'EMP7645',
	'BERNARD',
	'Martin',
	'876, rue martin, Sangatte'
);

INSERT INTO EMPLOYE (
	NUMERO,
	NOM,
	PRENOM,
	ADRESSE
) VALUES (
	'EMP2341',
	'RICHARD',
	'Philipe',
	'955, rue bernard, Coudekerque'
);

INSERT INTO EMPLOYE (
	NUMERO,
	NOM,
	PRENOM,
	ADRESSE
) VALUES (
	'EMP8765',
	'HUGO',
	'Victor',
	'432, rue victor-hugo, Frethun'
);

INSERT INTO EMPLOYE (
	NUMERO,
	NOM,
	PRENOM,
	ADRESSE
) VALUES (
	'EMP4328',
	'STEVENSON',
	'Max',
	'874, rue max-stevenson, Cologne'
);

INSERT INTO EMPLOYE (
	NUMERO,
	NOM,
	PRENOM,
	ADRESSE
) VALUES (
	'EMP7862',
	'DURAND',
	'Christophe',
	'543, rue durand, Calais'
);

SELECT
	*
FROM
	EMPLOYE;

INSERT INTO HYPERMARCHE (
	NUMERO,
	NOM,
	ADRESSE,
	VILLE,
	CODEPOSTAL,
	NUMERODIRECTEUR
) VALUES (
	'HYP12',
	'Marrefour Coquelles',
	'12 Bd du Kent',
	'Coquelles',
	62231,
	'EMP1090'
);

INSERT INTO HYPERMARCHE (
	NUMERO,
	NOM,
	ADRESSE,
	VILLE,
	CODEPOSTAL,
	NUMERODIRECTEUR
) VALUES (
	'HYP56',
	'DO-SPORT',
	'45 AV Blue',
	'Calais',
	62100,
	'EMP1208'
);

INSERT INTO HYPERMARCHE (
	NUMERO,
	NOM,
	ADRESSE,
	VILLE,
	CODEPOSTAL,
	NUMERODIRECTEUR
) VALUES (
	'HYP76',
	'Marrefour Market',
	'68 AV Bleriot',
	'Lille',
	59160,
	'EMP1301'
);

INSERT INTO HYPERMARCHE (
	NUMERO,
	NOM,
	ADRESSE,
	VILLE,
	CODEPOSTAL,
	NUMERODIRECTEUR
) VALUES (
	'HYP43',
	'Dealer Price',
	'52, Bd Hugo',
	'Calais',
	62100,
	'EMP8765'
);

SELECT
	*
FROM
	HYPERMARCHE;

INSERT INTO RAYON (
	NUMERO,
	NOM,
	DESCRIPTIF,
	CHIFFREAFFAIRE,
	NUMEROHYPER,
	NUMERORESPONSABLE
) VALUES (
	'RAY34',
	'Boucherie',
	'Vente de viandes,volailles etc.',
	1000000,
	'HYP12',
	'EMP7862'
);

INSERT INTO RAYON (
	NUMERO,
	NOM,
	DESCRIPTIF,
	CHIFFREAFFAIRE,
	NUMEROHYPER,
	NUMERORESPONSABLE
) VALUES (
	'RAY45',
	'Textile',
	'Vétements',
	50000000,
	'HYP56',
	'EMP1505'
);

INSERT INTO RAYON (
	NUMERO,
	NOM,
	DESCRIPTIF,
	CHIFFREAFFAIRE,
	NUMEROHYPER,
	NUMERORESPONSABLE
) VALUES (
	'RAY67',
	'Chaussure',
	'Des chaussures de cuir, des chaussures de marche.',
	800000,
	'HYP56',
	'EMP7645'
);

INSERT INTO RAYON (
	NUMERO,
	NOM,
	DESCRIPTIF,
	CHIFFREAFFAIRE,
	NUMEROHYPER,
	NUMERORESPONSABLE
) VALUES (
	'RAY86',
	'Poissonnerie',
	'Poissonnerie traiteur, plateau de fruits de mer.',
	950000,
	'HYP12',
	'EMP2341'
);

INSERT INTO RAYON (
	NUMERO,
	NOM,
	DESCRIPTIF,
	CHIFFREAFFAIRE,
	NUMEROHYPER,
	NUMERORESPONSABLE
) VALUES (
	'RAY19',
	'Légumes',
	'Fruits et Légumes',
	450000,
	'HYP43',
	'EMP4328'
);

INSERT INTO RAYON (
	NUMERO,
	NOM,
	DESCRIPTIF,
	CHIFFREAFFAIRE,
	NUMEROHYPER,
	NUMERORESPONSABLE
) VALUES (
	'RAY98',
	'Boucherie',
	'Vente de viandes, volailles, etc.',
	500000,
	'HYP43',
	'EMP2025'
);

SELECT
	*
FROM
	RAYON;

INSERT INTO PRODUITS (
	NUMERO,
	LIBELLE,
	NUMERORAYON,
	PRIXUNITAIRE,
	UNITE,
	QUANTITESTOCKHYPER
) VALUES (
	'PR1234',
	'Côtes de bœuf',
	'RAY34',
	12.50,
	'kg',
	345
);

INSERT INTO PRODUITS (
	NUMERO,
	LIBELLE,
	NUMERORAYON,
	PRIXUNITAIRE,
	UNITE,
	QUANTITESTOCKHYPER
) VALUES (
	'PR5783',
	'Chaussure Mike',
	'RAY67',
	109.99,
	'unité',
	56660
);

INSERT INTO PRODUITS (
	NUMERO,
	LIBELLE,
	NUMERORAYON,
	PRIXUNITAIRE,
	UNITE,
	QUANTITESTOCKHYPER
) VALUES (
	'PR6765',
	'SWEAT CORERUNNER',
	'RAY45',
	59.00,
	'unité',
	87778
);

SELECT
	*
FROM
	PRODUITS;

--1.6

DELETE FROM HYPERMARCHE
	WHERE Nom = 'Marrefour Market';
SELECT * FROM HYPERMARCHE;

	/*resultat:
		
		NUMERO          NOM                            ADRESSE                                            VILLE                          CODEPOSTAL NUMERODIRECTEUR
		--------------- ------------------------------ -------------------------------------------------- ------------------------------ ---------- ---------------
		HYP12           Marrefour Coquelles            12 Bd du Kent                                      Coquelles                           62231 EMP1090
		HYP56           DO-SPORT                       45 AV Blue                                         Calais                              62100 EMP1208
		HYP43           Dealer Price                   52, Bd Hugo                                        Calais                              62100 EMP8765
*/

UPDATE RAYON
	SET NUMERORESPONSABLE = (SELECT NUMERO FROM EMPLOYE
								WHERE NOM = 'VASSEUR' and PRENOM = 'Jacques')
	WHERE NOM = 'Poissonnerie';

SELECT * FROM RAYON;

	/*resultat:
		
		NUMERO          NOM                            DESCRIPTIF                                         CHIFFREAFFAIRE NUMEROHYPER     NUMERORESPONSAB
		--------------- ------------------------------ -------------------------------------------------- -------------- --------------- ---------------
		RAY34           Boucherie                      Vente de viandes,volailles etc.                           1000000 HYP12           EMP7862
		RAY45           Textile                        V├®tements                                                50000000 HYP56           EMP1505
		RAY67           Chaussure                      Des chaussures de cuir, des chaussures de marche.          800000 HYP56           EMP7645
		RAY86           Poissonnerie                   Poissonnerie traiteur, plateau de fruits de mer.           950000 HYP12           EMP1505
		RAY19           L├®gumes                        Fruits et L├®gumes                                          450000 HYP43           EMP4328
		RAY98           Boucherie                      Vente de viandes, volailles, etc.                          500000 HYP43           EMP2025

	*/ 
