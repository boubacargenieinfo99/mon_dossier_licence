import math

def affiche_tableau(graphe,distance,prec):
    for a in graphe.dic_noeud.values():
        num_prec = prec[a.numero].numero if prec[a.numero] != None else "None"
        print("noeud : " ,a.numero ," ,distance : " ,distance[a.numero] ," ,prec : ",num_prec)

def dijkstra(graphe,noeud_depart,noeud_arrive):
    compteur = 0
    E = {}
    distance = {}
    prec = {}
    for a in graphe.dic_noeud:  # a prend la cle(valeur) du noeud (et non le noeud , pour que a soit un noeud il faut utiliser graphe.dic_noeud.values())
        E[a] = graphe.dic_noeud[a]
        distance[a] = float('inf')
        prec[a] = None
    distance[noeud_depart.numero] = 0
    #u = noeud_depart
    while len(E) != 0:
        compteur += 1
        #on recherche le minimisant d de E
        distance_la_plus_petite = float('inf')
        u = 0
        for a in E:
            if distance[a] < distance_la_plus_petite:             #distance[a.values()] < distance_la_plus_petite:
                distance_la_plus_petite = distance[a]
                u = E[a]

        #si arrive
        if u.numero == noeud_arrive.numero:
            #affiche_tableau(graphe, distance, prec)
            print("compteur = " , compteur)
            return (distance,prec)

        #retire u de E
        del E[u.numero]

        #on regarde les voisins
        for v,d in u.aretes: #v -> noeud voisin , d -> distance jusqu au voisin v
            if v.numero in E:
                distance_alternative = distance[u.numero] + d
                if distance_alternative < distance[v.numero]:
                    distance[v.numero] = distance_alternative
                    prec[v.numero] = u

def precedent(distance,prec,noeud_arrive):
    d = {}
    n = noeud_arrive
    i = 0
    chemin = ""
    while n != None:
        d[i] = n
        n = prec[n.numero]
        i = i+1
    for elem in range(len(d)-1,-1,-1):
        chemin += str(d[elem].numero)
        if elem != 0:
            chemin += " -> "
    print(chemin)
    print("distance total = " , distance[noeud_arrive.numero])
    return d

def heuristique_distance_2_D(noeud_ou_on_est,noeud_arrive):
    h = math.sqrt(math.pow((noeud_arrive.position[0] - noeud_ou_on_est.position[0]), 2) + math.pow((noeud_arrive.position[1] - noeud_ou_on_est.position[1]), 2))
    return h

def Astar(graphe,noeud_depart,noeud_arrive):
    compteur = 0
    E = {}
    E[noeud_depart.numero] = noeud_depart
    distance = {}
    prec = {}
    f = {}
    for a in graphe.dic_noeud:  # a prend la cle(valeur) du noeud (et non le noeud , pour que a soit un noeud il faut utiliser graphe.dic_noeud.values())
        distance[a] = float('inf')
        f[a] = float('inf')
        prec[a] = None
    distance[noeud_depart.numero] = 0
    f[noeud_depart.numero] = heuristique_distance_2_D(noeud_depart,noeud_arrive)
    while len(E) != 0:
        compteur += 1
        u = 0
        distance_la_plus_petite = float('inf')
        for a in E:
            if f[a] < distance_la_plus_petite:             #distance[a.values()] < distance_la_plus_petite:
                distance_la_plus_petite = f[a]
                u = E[a]

        # si arrive
        if u.numero == noeud_arrive.numero:
            #affiche_tableau(graphe, distance, prec)
            print("compteur = ", compteur)
            return (distance, prec)

        # retire u de E
        del E[u.numero]

        # on regarde les voisins
        for v,d in u.aretes:  # v -> noeud voisin , d -> distance jusqu au voisin v
                distance_alternative = distance[u.numero] + d
                if distance_alternative < distance[v.numero]:
                    distance[v.numero] = distance_alternative
                    f[v.numero] = distance[v.numero] + heuristique_distance_2_D(v,noeud_arrive)
                    prec[v.numero] = u
                    if v not in E:
                        E[v.numero] = v

    #for v voisin de u (dans tout le graphe)(pas for v voisin de u dans E )