#include "round_about.hpp"
#include "vehicle.hpp"

#include "cassert"
#include <iostream>

RoundAbout::RoundAbout(const std::string &id, unsigned int duration, unsigned int in_number, unsigned int out_number, unsigned int capacity) :
  Node(id, duration, in_number, out_number), _capacity(capacity) {
}

RoundAbout::~RoundAbout() {
}

void RoundAbout::enter_vehicle(Vehicle *vehicle, unsigned int time) {

  assert(not is_full());

  std::cout << time << ": " << vehicle->id() << " enters " << id() << std::endl;

 _vehicles.push(std::unique_ptr<Vehicle>(vehicle));
  vehicle->add_node(this);
  vehicle->last_time(time);
}

void RoundAbout::exit_vehicle(unsigned int time) {
  while (not _vehicles.empty() and _vehicles.front()->last_time() + duration() == time) {
    Vehicle *vehicle = _vehicles.front().release();

    std::cout << time << ": " << vehicle->id() << " exits " << id() << std::endl;

    if (out_number() != 0) {
      put_vehicle_into_out_link(vehicle, time);
    }
    _vehicles.pop();
  }
}

bool RoundAbout::is_full() const {
  return _vehicles.size() == _capacity;
}