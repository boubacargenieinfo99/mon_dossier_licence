alphabet_ADN = {0: "-", 1: "A", 2: "C", 3: "G", 4: "T"}
def charger_sequence(nom_fichier):
    fichier = open(nom_fichier, "r")
    sequence_ADN = fichier.read()
    if not (verif_sequence(sequence_ADN)):
        print("erreur dans le fichier ", nom_fichier)
        fichier.close()
        return 0
    fichier.close()
    return sequence_ADN
def verif_sequence(sequence):
    for i in sequence:
        if not (i in alphabet_ADN.values()):
            return False
    return True

def charger_matrice(nom_fichier):
    fichier = open(nom_fichier, "r")
    matrice = {" ": {}, "-": {}, "A": {}, "C": {}, "G": {}, "T": {}}
    col = {" ": " ", "-": "-", "A": "A", "C": "C", "G": "G", "T": "T"}
    for li in matrice:
        matrice[li] = col.copy()
    iterator_ligne = 0
    val = 0
    for ligne in fichier.readlines():  # on prend les lignes sans le \n
        if iterator_ligne == 0:
            iterator_ligne = 0
        else:
            iterator_letter = 0
            for t in range(len(ligne.split())):
                val = ligne.split()[t]
                matrice[(alphabet_ADN[iterator_ligne - 1])][(alphabet_ADN[iterator_letter])] = int(val)
                iterator_letter += 1
        iterator_ligne += 1
    return matrice

def affiche_matrice_substitution(matrice):
    for h in matrice:
        print(h, end=" ")
        for jo in matrice[h]:
            txt = "{:^3}"
            print(txt.format(matrice[h][jo]), end=" ")
        print("")

def donne_matrice_score(sequence_une, sequence_deux, matrice_substitution):
    if not (verif_sequence(sequence_une)) or not (verif_sequence(sequence_deux)):
        print("erreur dans une des deux sequence")
        return 0
    matrice_score = []
    # sequence_une sens ->      matrice substitution[ligne][colonne]
    # sequence_deux sens v (vers le bas)
    for lin in range(len(sequence_deux) + 1):
        ligne = []
        for col in range(len(sequence_une) + 1):
            if lin == 0:
                if col == 0:
                    ligne.append((matrice_substitution["-"]["-"]))
                else:
                    ligne.append(ligne[col - 1] + matrice_substitution["-"][sequence_une[col - 1]])
            else:
                if col == 0:
                    ligne.append(matrice_score[lin - 1][col] + matrice_substitution[sequence_deux[lin - 1]]["-"])
                else:
                    max: int = matrice_score[lin - 1][col - 1] + matrice_substitution[sequence_deux[lin - 1]][
                        sequence_une[col - 1]]  # mismatch or not mismatch
                    if (ligne[col - 1] + (ligne[0] - matrice_score[lin - 1][0])) > max:  # del
                        max = ligne[col - 1] + (matrice_score[0][col] - matrice_score[0][col - 1])
                    if (matrice_score[lin - 1][col] + (ligne[0] - matrice_score[lin - 1][0])) > max:  # ins
                        max = matrice_score[lin - 1][col] + (ligne[0] - matrice_score[lin - 1][0])
                    ligne.append(max)
        matrice_score.append(ligne.copy())
    return matrice_score


nom_fichier="ressources/dna1.txt"
nom_fichier2="ressources/dna2.txt"
nom_matrice="ressources/mat1.txt"

sequence1 =charger_sequence(nom_fichier)
sequence2=charger_sequence(nom_fichier2)
sequence_matrice=charger_matrice(nom_matrice)
print("Mes sequences Adn1, Adn2")
print(sequence1)
print(sequence2)
print( "*********************")
print("Matrice de subtition ")
affiche_matrice_substitution(sequence_matrice)
donne_matrice_score(sequence1,sequence2,sequence_matrice)
