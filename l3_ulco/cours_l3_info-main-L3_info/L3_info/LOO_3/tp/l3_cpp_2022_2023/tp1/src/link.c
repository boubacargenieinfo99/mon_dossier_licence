#include "link.h"

#include <math.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct Link *create_link(char *id, unsigned int length) {
  struct Link *link = (struct Link *) malloc(sizeof(struct Link));

  link->id = (char *) malloc(sizeof(char) * (strlen(id)) + 1);
  strcpy(link->id, id);
  link->length = length;
  link->cell_number = floor((double) length / CELL_LENGTH);
  link->cells = (struct Cell *) malloc(sizeof(struct Cell) * link->cell_number);
  for (unsigned int i = 0; i < link->cell_number; ++i) {
    link->cells[i].vehicle = NULL;
  }
  link->out_node = NULL;
  return link;
}

void add_vehicle_to_link(struct Vehicle *vehicle, struct Link *link, unsigned int time) {
  printf("%d: %s enters %s\n", time, vehicle->id, link->id);
  link->cells[0].vehicle = vehicle;
  vehicle->link = link;
  vehicle->last_time = time;
}

void attach_in_node_to_link(struct Node *node, struct Link *link) {
  link->in_node = node;
}

void attach_out_node_to_link(struct Node *node, struct Link *link) {
  link->out_node = node;
}

struct Vehicle *go_out_link(struct Link *link, unsigned int time) {
  struct Vehicle *last_vehicle = link->cells[link->cell_number - 1].vehicle;
  struct Vehicle *out_vehicle =
    last_vehicle != NULL && last_vehicle->last_time < time ? last_vehicle : NULL;

  if (out_vehicle != NULL) {
    printf("%d: %s exits %s\n", time, out_vehicle->id, link->id);
    link->cells[link->cell_number - 1].vehicle = NULL;
    out_vehicle->link = NULL;
  }
  return out_vehicle;
}

struct Vehicle *run_link(struct Link *link, unsigned int time) {
  struct Vehicle *out_vehicle = go_out_link(link, time);

  for (unsigned int i = link->cell_number - 1; i > 0; --i) {
    struct Vehicle *vehicle = link->cells[i - 1].vehicle;

    if (vehicle != NULL && vehicle->last_time < time && link->cells[i].vehicle == NULL) {
      link->cells[i].vehicle = link->cells[i - 1].vehicle;
      link->cells[i].vehicle->last_time = time;
      link->cells[i - 1].vehicle = NULL;
    }
  }
  return out_vehicle;
}

void destroy_link(struct Link *link) {
  free(link->id);
  free(link->cells);
  free(link);
}
