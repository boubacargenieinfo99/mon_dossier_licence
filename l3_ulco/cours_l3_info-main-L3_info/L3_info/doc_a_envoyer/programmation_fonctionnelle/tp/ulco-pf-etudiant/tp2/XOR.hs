xor1:: Bool -> Bool -> Bool
xor1 condition1 condition2 = (condition1 == not(condition2))

xor2:: Bool -> Bool -> Bool
xor2 condition1 condition2= if condition1 then not(condition2) else condition2

xor3::Bool -> Bool -> Bool
xor3 condition1 condition2 = case condition1 of
                                    True -> not(condition2)
                                    False -> condition2

{-xor3::Bool -> Bool -> Bool
xor3 condition1 condition2 = case (condition1,condition2) of
                                    (True,False) -> True
                                    (False,True) -> True
                                    _ -> False
-}          

xor4::Bool -> Bool -> Bool
xor4 condition1 condition2 
        | condition1 == not(condition2) = True
        | otherwise = False

xor5::Bool -> Bool -> Bool
xor5 True False = True
xor5 False True = True
xor5 _ _ = False

testXor:: (Bool -> Bool -> Bool) -> Bool
testXor f = f False False == False
            && f False True == True
            && f True False == True
            && f True True == False

main::IO()
main = do
    --print (("2 < 6 et 3 < 6 ? ") ++ (xor1 (2<6) (3<6)))
    print (testXor xor1)
    print (testXor xor2)
    print (testXor xor3)
    print (testXor xor4)
    print (testXor xor5)