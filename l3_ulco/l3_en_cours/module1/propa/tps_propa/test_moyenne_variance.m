a = 2; % paramètre de la loi de Poisson
[moyenne, variance] = moyenneVariancePoisson(a);

fprintf('Moyenne : %.2f\n', moyenne);
fprintf('Variance : %.2f\n', variance);