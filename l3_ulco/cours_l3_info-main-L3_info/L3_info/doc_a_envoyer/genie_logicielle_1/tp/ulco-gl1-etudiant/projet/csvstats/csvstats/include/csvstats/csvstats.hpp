#pragma once

#include <vector>
#include <fstream>
#include <sstream>
#include <cmath>

/// \brief class Csv
///
class Csv{
    public:
        /// create a Csv
        Csv();
        /// destroy a Csn
        ~Csv(){};

        /// descript file and push the value in data
        void loadData(std::ifstream& file);

        /// return data
        std::vector<std::vector<double>> getData();

        //affiche les valeur de Data
        void afficheData();
    private:
        //stocke les donnees
        std::vector<std::vector<double>> data;
};

int mul2(int n);



