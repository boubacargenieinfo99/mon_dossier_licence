var express = require('express');
var router = express.Router();

const homeController = require('../controllers/homeControllers')
router.get('/',homeController.home);

module.exports = router;