"use strict";

import Tzaar from '../../../openxum-core/games/gipf/index.mjs';
import AI from '../../../openxum-ai/generic/index.mjs';
import Gui from './gui.mjs';
import Manager from './manager.mjs';

export default {
    Gui: Gui,
    Manager: Manager,
    Settings: {
        ai: {
            mcts: AI.RandomPlayer // TODO: MCTSPlayer
        },
        colors: {
            first: Tzaar.Color.BLACK,
            init: Tzaar.Color.BLACK,
            list: [
                {key: Tzaar.Color.BLACK, value: 'black'},
                {key: Tzaar.Color.WHITE, value: 'white'}
            ]
        },
        modes: {
            init: Tzaar.GameType.STANDARD,
            list: [
                {key: Tzaar.GameType.STANDARD, value: 'standard'}
            ]
        },
        opponent_color(color) {
            return color === Tzaar.Color.BLACK ? Tzaar.Color.WHITE : Tzaar.Color.BLACK;
        },
        types: {
            init: 'ai',
            list: [
                {key: 'gui', value: 'GUI'},
                {key: 'ai', value: 'AI'},
                {key: 'online', value: 'Online'},
                {key: 'offline', value: 'Offline'}
            ]
        }
    }
};
