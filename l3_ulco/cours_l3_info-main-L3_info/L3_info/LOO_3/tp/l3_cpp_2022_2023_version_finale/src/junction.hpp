#ifndef TRAFFIC_JUNCTION_HPP
#define TRAFFIC_JUNCTION_HPP

#include "node.hpp"

#include <memory>
#include <string>
#include <valarray>

class Link;

class Vehicle;

class Junction : public Node {
public:
  Junction(const std::string &id, unsigned int duration, unsigned int in_number, unsigned int out_number);

  ~Junction() override;

  void enter_vehicle(Vehicle *vehicle, unsigned int time) override;

  void exit_vehicle(unsigned int time) override;

  bool is_full() const override { return _vehicle != nullptr; }

private:
  void remove_vehicle() { _vehicle = nullptr; }

  std::unique_ptr<Vehicle> _vehicle;
};

typedef Junction *JunctionPtr;

#endif //TRAFFIC_JUNCTION_HPP
