#include "cell.hpp"
#include <iostream>

Cell::Cell(int x, int y) : x(x), y(y) {}

void Cell::displayInfo() const {
    std::cout << "Cell Coordinates - Indices: (" << x << ", " << y << ")" << std::endl;
}
