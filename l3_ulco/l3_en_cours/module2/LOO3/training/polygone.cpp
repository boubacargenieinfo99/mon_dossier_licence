#include "polygone.hpp"
#include "point.hpp"
#include <iostream>

Polygon::Polygon() {
    // Initialisation avec au moins trois points
    points.push_back(std::make_unique<Point>(0, 0));
    points.push_back(std::make_unique<Point>(1, 1));
    points.push_back(std::make_unique<Point>(2, 2));
}

void Polygon::addPoint(std::unique_ptr<Point> point) {
    points.push_back(std::move(point));
}

void Polygon::displayInfo() const {
    std::cout << "Polygonal Geometry" << std::endl;
    for (const auto& point : points) {
        point->displayInfo();
    }
}
