// lib/openxum-core/games/newgame/engine.mjs

import OpenXum from '../../openxum/index.mjs';
import Color from './color.mjs';
import Piece from './piece.mjs';
import Coordinates from './coordinates.mjs';
import Move from './move.mjs';
//import les autres classes dont vous avez besoin

class Engine extends OpenXum.Engine {
  constructor(t, c) {
    super();
    this._type = t;
    this._current_color = c;
    this._initialize_board();
    // autres attributs nécessaires à votre jeu
  }

  build_move() {
    return new Move();
  }
    
  clone() {
    let o = new Engine(this._type, this._color);
        let b = new Array(11);

        for (let i = 0; i < 10; i++) {
            b[i] = new Array(10);
        }

        for (let x = 0; x < 10; x++) {
            for (let y = 0; y < 8; y++) {
                if (this._board[x][y] !== undefined) {
                    b[x][y] = this._board[x][y].clone();
                }
            }
        }
        o._set(this._is_finished, this._winner_color, b);

        return o;
//comm
  }
  _initialize_board() {
    this._board = new Array(10);
    for (let i = 0; i < 10; i++) {
        this._board[i] = new Array(8);
    }
    let alt = 0;
    let up = false;
    let yy;
    let color = Color.BLACK;
    for (let y = 1; y < 8; y = y + 5) {
        for (let x = 0; x < 8; x++) {
            this._board[x][y] = new Piece(color, new Coordinates(x, y));
            if (alt === 2) {
                alt = 0;
                up = !up;
            }
            if (!up) {
                yy = y + 1;
            } else {
                yy = y - 1;
            }
            this._board[x][yy] = new Piece(color, new Coordinates(x, yy));
            alt++;
        }
        color = Color.WHITE;
    }
   
}

  current_color() {
    return this._current_color;
  }

  get_name() {
    return "Komivoki";
  }

  phase() {
    return this._phase;
  }

  get_possible_move_list() {
    // TODO
    let possible_moves = [];
    // Générer la liste des mouvements possibles
    for (let x = 0; x < 8; x++) {
      for (let y = 0; y < 8; y++) {
        if (this._board[x][y] && this._board[x][y].color() === this._current_color) {
          let moves = this._get_moves_from(x, y);
          possible_moves = possible_moves.concat(moves);
        }
      }
    }
    return possible_moves;
  }

  _get_moves_from(x, y) {
    let directions = [
      { dx: -1, dy: 0 }, { dx: 1, dy: 0 }, { dx: 0, dy: -1 }, { dx: 0, dy: 1 },
      { dx: -1, dy: -1 }, { dx: 1, dy: 1 }, { dx: -1, dy: 1 }, { dx: 1, dy: -1 }
    ];
    let moves = [];
    for (let d of directions) {
      let nx = x + d.dx;
      let ny = y + d.dy;
      if (this._is_valid_move(x, y, nx, ny)) {
        moves.push(new Move(x, y, nx, ny));
      }
    }
    return moves;
  }

  _is_valid_move(fromX, fromY, toX, toY) {
    if (toX < 0 || toX >= 8 || toY < 0 || toY >= 8) return false;

    const fromPiece = this._board[fromX][fromY];
    if (!fromPiece || fromPiece.color() !== this._current_color) return false;

    const toPiece = this._board[toX][toY];
    if (toPiece === undefined) return true;

    if (toPiece.color() === fromPiece.color() && toPiece.getStack().length < 3) return true;

    return false;
  }
  

  is_finished() {
    return this._phase === Phase.FINISH;
  }

  move(move) {
    // TODO
    const { fromX, fromY, toX, toY } = move;
    if (!this._is_valid_move(fromX, fromY, toX, toY)) {
      throw new Error('Invalid move');
    }

    const fromPiece = this._board[fromX][fromY];
    const toPiece = this._board[toX][toY];

    if (toPiece === undefined) {
      this._board[toX][toY] = fromPiece;
    } else {
      toPiece.stackPiece(fromPiece.unstackPiece());
    }

    if (fromPiece.getStack().length === 0) {
      this._board[fromX][fromY] = undefined;
    }

    this._capture(toX, toY);
    this._record_state();
    this._current_color = (this._current_color === Color.BLACK ? Color.WHITE : Color.BLACK);

    if (this._is_game_over()) {
      this._phase = 'FINISH';
      this._winner_color = this._current_color;
    }
  }

  _capture(x, y) {
    const piece = this._board[x][y];
    const neighbors = this._get_neighbors(x, y);
    const enemyNeighbors = neighbors.filter(n => n.piece !== undefined && n.piece.color() !== piece.color());
    const totalCapacity = enemyNeighbors.reduce((sum, n) => sum + n.piece.getStack().length, 0);
    const surroundingEnemyCount = enemyNeighbors.length;

    if (surroundingEnemyCount > 0 && surroundingEnemyCount + this._count_borders(x, y) >= piece.getStack().length && totalCapacity > piece.getStack().reduce((sum, p) => sum + p.getSize(), 0)) {
      this._board[x][y] = undefined;
    }
  }

  _get_neighbors(x, y) {
    const directions = [
      { dx: -1, dy: 0 }, { dx: 1, dy: 0 }, { dx: 0, dy: -1 }, { dx: 0, dy: 1 },
      { dx: -1, dy: -1 }, { dx: 1, dy: 1 }, { dx: -1, dy: 1 }, { dx: 1, dy: -1 }
    ];
    let neighbors = [];
    for (let d of directions) {
      let nx = x + d.dx;
      let ny = y + d.dy;
      if (nx >= 0 && nx < 8 && ny >= 0 && ny < 8) {
        neighbors.push({ x: nx, y: ny, piece: this._board[nx][ny] });
      }
    }
    return neighbors;
  }

  _count_borders(x, y) {
    let borders = 0;
    if (x === 0 || x === 7) borders++;
    if (y === 0 || y === 7) borders++;
    return borders;
  }

  _record_state() {
    const state = this._board.map(row => row.map(piece => (piece ? piece.toString() : ' '))).join(',');
    if (this._previous_states.has(state)) {
      throw new Error('This move repeats a previous state');
    }
    this._previous_states.add(state);
  }

  _is_game_over() {
    const blackPieces = this._board.flat().filter(piece => piece && piece.color() === Color.BLACK).length;
    const whitePieces = this._board.flat().filter(piece => piece && piece.color() === Color.WHITE).length;
    return blackPieces === 0 || whitePieces === 0 || this._pass_count >= 2;
  }
  pass() {
    this._pass_count++;
    this._current_color = (this._current_color === Color.BLACK ? Color.WHITE : Color.BLACK);
    if (this._pass_count >= 2) {
      this._phase = 'FINISH';
    }
  }


  parse(str) {
    // TODO
  }

  to_string() {
    // TODO
  }

  winner_is() {
    if (this.is_finished()) {
      return this._color;
  }
  }
  
}

export default Engine
