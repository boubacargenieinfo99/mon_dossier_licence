n=10;
resultas=mullerMethode(n);

% Afficher un histogramme
figure;
hist(resultas(1,:), 50); % Utilisez la première ligne de resultats
title('Histogramme de variables aléatoires normales N(0,1)');
xlabel('Valeurs');
ylabel('Fréquence');
