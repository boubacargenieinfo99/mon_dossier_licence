foldSum1 :: [Int] -> Int
foldSum1 [] = 0
foldSum1 (x:xs) = x + foldSum1 xs

foldSum2 :: [Int] -> Int
foldSum2 xs = foldl (+) 0 xs

myfoldl :: (b -> a -> b) -> b -> [a] -> b
myfoldl f acc [] = acc
myfoldl f acc (x:xs) = myfoldl f (f acc x) xs

main :: IO()
main = do
    print (foldSum1 [1,2,3,4])
    print (foldSum2 [5,10,15])
    print (myfoldl (+) 0 [20,25,2])
    print (myfoldl max 'r' "barfoo")
