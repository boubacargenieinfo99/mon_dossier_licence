#include "link.hpp"

#include <cmath>
#include <cstring>

#include <iostream>

const unsigned int Link::cell_length = 5;

Link::Link(const char *id, unsigned int length) : id(new char[strlen(id) + 1]), length(length),
                                                  cell_number(floor((double) length / cell_length)),
                                                  cells(new Cell[cell_number]), out_node(nullptr) {
  strcpy(this->id, id);
  for (unsigned int i = 0; i < cell_number; ++i) {
    cells[i].vehicle = nullptr;
  }
}

Link::~Link() {
  delete[] id;
  delete[] cells;
}

void Link::add_vehicle(Vehicle *vehicle, unsigned int time) {
  std::cout << time << ": " << vehicle->id << " enters " << id << std::endl;
  cells[0].vehicle = vehicle;
  vehicle->link = this;
  vehicle->last_time = time;
}

void Link::attach_in_node(Node *node) {
  in_node = node;
}

void Link::attach_out_node(Node *node) {
  out_node = node;
}

Vehicle *Link::go_out(unsigned int time) {
  Vehicle *last_vehicle = cells[cell_number - 1].vehicle;
  Vehicle *out_vehicle = last_vehicle != nullptr && last_vehicle->last_time < time ? last_vehicle : nullptr;

  if (out_vehicle != nullptr) {
    std::cout << time << ": " << out_vehicle->id << " exits " << id << std::endl;
    cells[cell_number - 1].vehicle = nullptr;
    out_vehicle->link = nullptr;
  }
  return out_vehicle;
}

Vehicle *Link::run(unsigned int time) {
  Vehicle *out_vehicle = go_out(time);

  for (unsigned int i = cell_number - 1; i > 0; --i) {
    Vehicle *vehicle = cells[i - 1].vehicle;

    if (vehicle != nullptr && vehicle->last_time < time && cells[i].vehicle == nullptr) {
      cells[i].vehicle = cells[i - 1].vehicle;
      cells[i].vehicle->last_time = time;
      cells[i - 1].vehicle = nullptr;
    }
  }
  return out_vehicle;
}